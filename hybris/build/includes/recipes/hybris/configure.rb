=begin
  * Original Copyright: (C) 2012 Acquity Group, LLC - All Rights Reserved
  * Copyright: (C) 2013 Accenture Interactive (Accenture LLP) - All Rights Reserved
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Name: Hybris configure (configure.rb) 
  * Author: Jason Potkanski <jason.potkanski@acquitygroup.com> <jason.potkanski@accenture.com>
  * Author: Shared Services DevOps <devops@acquitygroup.com>
  * Date:
  * License: Proprietary and confidential
  * Use of this file may require license terms between Acquity Group/Accenture Interactive and Licensee
=end
namespace :hybris do

  # set :private_key_template, File.dirname(__FILE__) + "/private_key.erb"
  # set :public_key_template, File.dirname(__FILE__) + "/public_key.erb"

  
  namespace :configure do


    task :fix_sudoers_for_cap, roles => :app do
      run "sed -i 's/ requiretty/!requiretty/' /etc/sudoers; chmod 0440 /etc/sudoers"
      run "sed -i 's/ !visiblepw/visiblepw/' /etc/sudoers; chmod 0440 /etc/sudoers"
    end

    task :install_audit_tools, roles => :app do
      run "yum -y install policycoreutils-python"
    end

    task :set_hostname, roles => :app do
      if site_name == "hybris.local"
        run "hostname #{site_name}"
       # run "echo -e '10.40.10.34 hybris.local' >> /etc/hosts"
       # run "echo -e '10.40.10.36 master.hybris.local' >> /etc/hosts"
      end
      if site_name == "hybris.acq"
       # run "echo -e '10.50.30.12 hybris.acq' >> /etc/hosts"
       # run "echo -e '10.50.30.13 master.hybris.acq' >> /etc/hosts"
      end
    end

    task :install_svn_directories, roles => :app  do
      sudo "mkdir -p /usr/local/svn"
      sudo "mkdir -p /usr/local/svn/ondemand"
      sudo "mkdir -p /usr/local/svn/conf"
      sudo "mkdir -p /usr/local/svn/logs"
      sudo "groupadd -f svn"
      sudo "useradd -g svn -d /usr/local/svn svn"
      sudo "mkdir -p /usr/local/svn/.ssh;"
      run "touch /usr/local/svn/logs/svnsync.log"
      run "chown -R svn:svn /usr/local/svn"
      run "chown -R svn:svn /usr/local/svn/ondemand"
      run "chmod 700 /usr/local/svn/.ssh"
      #Chcons are for Centos 6
      #run "yum -y install httpd"
      run "yum -y update coreutils"
      run "chcon -R unconfined_u:object_r:httpd_sys_content_rw_t:s0 /usr/local/svn/ondemand"
      run "chcon -R unconfined_u:object_r:httpd_log_t:s0 /usr/local/svn/logs"
      run "chcon unconfined_u:object_r:user_home_dir_t:s0 /usr/local/svn"
      run "chcon unconfined_u:object_r:ssh_home_t:s0 /usr/local/svn/.ssh"
    end

    task :install_deploy_keys,roles => :app do
      timestamp = Time.now.to_i
      # Well, There is a centos bug if Selinux is on. http://bugs.centos.org/view.php?id=5432
      #sudo "setenforce permissive"
      #sudo "ssh-keygen -t rsa -f /usr/local/svn/.ssh/svn-#{timestamp} -P ''", :as => "svn"
      #sudo "cp /usr/local/svn/.ssh/svn-#{timestamp} /usr/local/svn/.ssh/id_rsa", :as => "svn"
      hybris.configure.install_private_key
      hybris.configure.install_public_key
      run "cp /usr/local/svn/.ssh/svn.pub /usr/local/svn/.ssh/authorized_keys"
      run "chown -R svn:svn /usr/local/svn/.ssh"
      run "chmod 600 /usr/local/svn/.ssh/id_rsa"
    end

    task :install_java, roles => :app do
      #run "yum -y install jdk"
      #run "wget -q -O /tmp/jdk-7-linux-x64.rpm http://download.oracle.com/otn-pub/java/jdk/7/jdk-7-linux-x64.rpm"
      #run "rpm -U --force /tmp/jdk-7-linux-x64.rpm"
    end

    task :install_tomcat,roles => :app  do
      run "wget -q -O /usr/local/apache-tomcat-#{tomcat_version}.tar.gz http://archive.apache.org/dist/tomcat/tomcat-7/v#{tomcat_version}/bin/apache-tomcat-#{tomcat_version}.tar.gz"
      run "cd /usr/local; tar -zxf apache-tomcat-#{tomcat_version}.tar.gz"
      run "mkdir -p /releng/master/tomcat; mkdir -p /releng/dev/tomcat; mkdir -p /releng/qa/tomcat; mkdir -p /releng/prod/tomcat"
      run "cp -r /usr/local/apache-tomcat-#{tomcat_version}/conf /releng/master/tomcat"
      run "cp -r /usr/local/apache-tomcat-#{tomcat_version}/conf /releng/dev/tomcat"
      run "cp -r /usr/local/apache-tomcat-#{tomcat_version}/conf /releng/qa/tomcat"
      run "cp -r /usr/local/apache-tomcat-#{tomcat_version}/conf /releng/prod/tomcat"
      run "mkdir -p /releng/master/tomcat/logs; mkdir -p /releng/dev/tomcat/logs; mkdir -p /releng/qa/tomcat/logs; mkdir -p /releng/prod/tomcat/logs"
      run "mkdir -p /releng/master/tomcat/temp; mkdir -p /releng/dev/tomcat/temp; mkdir -p /releng/qa/tomcat/temp; mkdir -p /releng/prod/tomcat/temp"
      run "mkdir -p /releng/master/tomcat/webapps; mkdir -p /releng/dev/tomcat/webapps; mkdir -p /releng/qa/tomcat/webapps; mkdir -p /releng/prod/tomcat/webapps"
      run "mkdir -p /releng/master/tomcat/work; mkdir -p /releng/dev/tomcat/work; mkdir -p /releng/qa/tomcat/work; mkdir -p /releng/prod/tomcat/work"
      run "chown -R deploy-master:deploy-master /releng/master"
      run "chown -R deploy-dev:deploy-dev /releng/dev"
      run "chown -R deploy-qa:deploy-qa /releng/qa"
      run "chown -R deploy-prod:deploy-prod /releng/prod"
      stages.each { |stage|
        set :stage_name, stage
        hybris.configure.install_server_script
        hybris.configure.install_server_xml
      }
    end

    task :install_hybris, roles => :app do
      run "yum --exclude=*.i386 --exclude=*.i586 --exclude=*.i686 -y install autoconf apr-devel apr-util-devel cyrus-sasl-devel db4-devel file-devel httpd-devel libtool neon-devel openssl-devel python-devel sqlite-devel swig zlib-devel"
      run "wget -q -O /tmp/hybris-#{hybris_version}.tar.gz http://archive.apache.org/dist/hybris/hybris-#{hybris_version}.tar.gz"
      run "cd /tmp; tar -zxf hybris-#{hybris_version}.tar.gz"
      run "cd /tmp/hybris-#{hybris_version}; ./configure --libdir=/usr/lib64"
      run "cd /tmp/hybris-#{hybris_version}; make"
      #run "cd /tmp/hybris-#{hybris_version}; make check"
      run "cd /tmp/hybris-#{hybris_version}; make install"
      run "cd /tmp/hybris-#{hybris_version}; make install-swig-py"
      run "echo \"/usr/lib64/svn-python\" > /usr/lib64/python2.6/site-packages/svn.pth"
      run "cd /usr/local/svn/ondemand; #{sudo} -u svn /usr/local/bin/svnadmin create Hybris"
    end

    task :install_viewvc, roles => :app do
      run "yum --exclude=*.i386 --exclude=*.i586 --exclude=*.i686 -y install dos2unix python-pygments"
      run "wget -q -O /tmp/viewvc-1.1.13.zip http://viewvc.tigris.org/files/documents/3330/49162/viewvc-1.1.13.zip"
      run "cd /tmp; unzip -o viewvc-1.1.13.zip"
      run "dos2unix /tmp/viewvc-1.1.13/viewvc-install"
      run "/tmp/viewvc-1.1.13/viewvc-install --prefix=/usr/local/viewvc-1.1.13 --destdir="
      run "sed -i 's/#root_parents =/root_parents = \\/usr\\/local\\/svn\\/ondemand: svn/' /usr/local/viewvc-1.1.13/viewvc.conf"
      run "chcon -R unconfined_u:object_r:httpd_sys_script_rw_t:s0 /usr/local/viewvc-1.1.13"
    end

    task :sync_svn_conf, roles => :app do
      run "#{sudo} -u svn ssh -o 'StrictHostKeyChecking no' tesla.acquityondemand.com -l svn exit"
      run "#{sudo} -u svn rsync -r svn@tesla.acquityondemand.com:/usr/local/svn/conf/* /usr/local/svn/conf"
      run "htpasswd -bc /usr/local/svn/conf/repos-sync-user svnsync svnsync321"
    end
    
    task :install_cron_scripts, roles => :app do
      run "mkdir -p /usr/local/svn/cron/master"
      run "mkdir -p /usr/local/svn/cron/slave"
      hybris.configure.install_master_cron
      hybris.configure.install_slave_cron
      run "chmod +x /usr/local/svn/cron/master/*.sh"
      run "chmod +x /usr/local/svn/cron/slave/*.sh"
      run "chown -R svn.svn /usr/local/svn/cron"
    end
    
    task :install_global_hooks, roles => :app do
      run "mkdir -p /usr/local/svn/hooks/master"
      run "mkdir -p /usr/local/svn/hooks/slave"
      run "mkdir -p /usr/local/svn/hooks/template"
      hybris.configure.install_slave_repository_hooks
      hybris.configure.install_master_repository_hooks
      hybris.configure.install_template_repository_hooks
      run "chmod +x /usr/local/svn/hooks/master/*"
      run "chmod +x /usr/local/svn/hooks/slave/*"
      run "chmod +x /usr/local/svn/hooks/template/*"
      run "chown -R svn.svn /usr/local/svn/hooks"
    end

    task :install_slave_symlinks, roles => :app, :only => { :slave => true } do
      run "mkdir -p /usr/local/svn/current"
      run "ln -sf /usr/local/svn/cron/slave /usr/local/svn/current/cron"
      run "ln -sf /usr/local/svn/hooks/slave /usr/local/svn/current/hooks"
      run "chown -R svn.svn /usr/local/svn/current"
    end

    task :install_master_symlinks, roles => :app, :only => { :master => true } do
      run "mkdir -p /usr/local/svn/current"
      run "ln -sf /usr/local/svn/cron/master /usr/local/svn/current/cron"
      run "ln -sf /usr/local/svn/hooks/master /usr/local/svn/current/hooks"
      run "chown -R svn.svn /usr/local/svn/current"
    end

    task :start_cron, roles => :app do
      run "#{sudo} -u svn ssh -o 'StrictHostKeyChecking no' master.#{site_name} -l svn exit"
      run "#{sudo} -u svn ssh -o 'StrictHostKeyChecking no' #{site_name} -l svn exit"
      run "#{sudo} -u svn crontab /usr/local/svn/current/cron/crontab"
    end

    task :install_local_slave_repository_hooks, roles => :app, :only => { :slave => true } do
      run "#{sudo} su - svn -c \"cp -u /usr/local/svn/hooks/template/* /usr/local/svn/ondemand/#{repo}/hooks/\""
      run "#{sudo} su - svn -c \"chcon -R unconfined_u:object_r:httpd_unconfined_script_exec_t:s0 /usr/local/svn/ondemand/#{repo}/hooks/*\""
    end

    task :install_local_master_repository_hooks, roles => :app, :only => { :master => true } do
      run "#{sudo} su - svn -c \"cp -u /usr/local/svn/hooks/template/* /usr/local/svn/ondemand/#{repo}/hooks/\""
      run "#{sudo} su - svn -c \"chcon -R unconfined_u:object_r:httpd_unconfined_script_exec_t:s0 /usr/local/svn/ondemand/#{repo}/hooks/*\""
    end


    task :populate_master_repositories, roles => :app, :only => { :master => true } do
      run "#{sudo} -u svn scp svn@tesla.acquityondemand.com:/home/svn/hybris_dump.gz /usr/local/svn/hybris_dump.gz"
      run "gunzip /usr/local/svn/hybris_dump.gz"
      run "#{sudo} -u svn /usr/local/bin/svnadmin load --force-uuid /usr/local/svn/ondemand/Hybris < /usr/local/svn/hybris_dump"
    end

    task :sync_slave_repositories, roles => :app, :only => { :slave => true } do
      run "#{sudo} su - svn -c \"/usr/local/bin/svnsync initialize http://#{site_name}/repos-sync/ondemand/#{repo} http://master.#{site_name}/repos-sync/ondemand/#{repo} --sync-username svnsync --sync-password svnsync321 --source-username svnsync --source-password svnsync321 --non-interactive\""
      run "#{sudo} su - svn -c \"/usr/local/bin/svnsync sync http://#{site_name}/repos-sync/ondemand/#{repo} --sync-username svnsync --sync-password svnsync321 --source-username svnsync --source-password svnsync321 --non-interactive\""
    end

    
    task :start_http_server, roles => :app do
      run "service httpd restart"
    end

    task :install_fuse, roles => :app do
      run "yum -y install http://pkgs.repoforge.org/fuse-sshfs/fuse-sshfs-2.2-1.el6.rf.x86_64.rpm"
      run "#{sudo} -u svn /bin/mkdir -p /usr/local/svn/tesla"
      run "usermod -G fuse svn"
    end

    task :mount_fuse, roles => :app, :only => { :master => true } do
      run "#{sudo} -u svn sshfs -o ro tesla.acquityondemand.com:/usr/local/svn /usr/local/svn/tesla"
    end

    task :create_repository, roles => :app do
      
      set(:repo,  Capistrano::CLI.ui.ask("Repository: ") )
      set(:source_repo_dir, Capistrano::CLI.ui.ask("Source UUID Repository Dir (leave blank for none): ") )
      if (!source_repo_dir.empty?)
        uuid = capture("#{sudo} su - svn -c \"svnlook uuid /usr/local/svn/tesla/projects/#{source_repo_dir}\"",:only => { :master => true })
      else
        uuid = ""
      end
      print uuid
      run "#{sudo} su - svn -c \"svnadmin create /usr/local/svn/ondemand/#{repo}\" "
      run "#{sudo} su - svn -c \"svnadmin setuuid /usr/local/svn/ondemand/#{repo}\ #{uuid}\" "
      run "#{sudo} su - svn -c \"rm -rf /usr/local/svn/ondemand/#{repo}/conf/*\""
      run "#{sudo} su - svn -c \"ln -s /usr/local/svn/conf/authz /usr/local/svn/ondemand/#{repo}/conf/authz\""
      run "#{sudo} su - svn -c \"ln -s /usr/local/svn/conf/passwd /usr/local/svn/ondemand/#{repo}/conf/passwd\""
      run "#{sudo} su - svn -c \"ln -s /usr/local/svn/conf/svnserve.conf /usr/local/svn/ondemand/#{repo}/conf/svnserve.conf\""
      hybris.configure.install_local_slave_repository_hooks
      hybris.configure.sync_slave_repositories
      hybris.configure.install_local_master_repository_hooks
    end

    task :delete_repository, roles => :app do

      set(:repo,  Capistrano::CLI.ui.ask("Repository: ") )
      run "#{sudo} su - svn -c \"rm -rf /usr/local/svn/ondemand/#{repo}*\""
    end

    task :import_repository, roles => :app, :only => { :master => true } do
      set(:repo, Capistrano::CLI.ui.ask("Source Repository: ") )
      set(:dest_repo, Capistrano::CLI.ui.ask("Destination Repository: ") )
      #set(:filter_dir, Capistrano::CLI.ui.ask("Repository Directory Filter: ") )
      run "#{sudo} su - svn -c \"svnadmin dump /usr/local/svn/tesla/projects/#{repo} > /usr/local/svn/#{repo}.svndump \""
      #run "#{sudo} su - svn -c \"svndumpfilter include --pattern '*#{filter_dir}' < /usr/local/svn/#{repo}.svndump > /usr/local/svn/#{dest_repo}.svndump \""
      #run "#{sudo} su - svn -c \"svnadmin load /usr/local/svn/ondemand/#{dest_repo} < /usr/local/svn/#{repo}.svndump \""
    end

 

    task :install_apache, roles => :app  do
      run "yum --exclude=*.i386 --exclude=*.i586 --exclude=*.i686 -y install httpd"
      run "mkdir -p /www/logs"
      run "chown -R apache.apache /www/"
      run "chcon -R unconfined_u:object_r:httpd_log_t:s0 /www/logs"
      hybris.configure.install_httpd_conf
      hybris.configure.install_apache_conf
      hybris.configure.install_default_site
      run "touch /var/www/html/favicon.ico"
      run "chkconfig --level 5 httpd on"
      run "iptables -I INPUT -p tcp -m tcp --dport 80 -j ACCEPT"
      #run "setenforce 1"
      run "/usr/sbin/setsebool -P httpd_can_network_connect 1"
      run "/usr/sbin/setsebool -P httpd_read_user_content 1"
      run "/usr/sbin/setsebool -P httpd_enable_homedirs 1"
    end

    task :install_private_key, roles => :web, :except => { :no_release => true } do
      template = File.read("#{private_key_template}")
      buffer = ERB.new(template).result(binding)
      put buffer, "/usr/local/svn/.ssh/id_rsa"
    end
    
    task :install_public_key, roles => :web, :except => { :no_release => true } do
      template = File.read("#{public_key_template}")
      buffer = ERB.new(template).result(binding)
      put buffer, "/usr/local/svn/.ssh/svn.pub"
    end

    task :install_slave_repository_hooks, roles => :app do
      template = File.read("#{svn_slave_pre_revprop_change_template}")
      buffer = ERB.new(template).result(binding)
      put buffer, "/usr/local/svn/hooks/slave/pre-revprop-change"
      template = File.read("#{svn_slave_start_commit_template}")
      buffer = ERB.new(template).result(binding)
      put buffer, "/usr/local/svn/hooks/slave/start-commit"
      template = File.read("#{svn_slave_post_revprop_change_template}")
      buffer = ERB.new(template).result(binding)
      put buffer, "/usr/local/svn/hooks/slave/post-revprop-change"
      template = File.read("#{svn_slave_post_commit_template}")
      buffer = ERB.new(template).result(binding)
      put buffer, "/usr/local/svn/hooks/slave/post-commit"
    end

    task :install_template_repository_hooks, roles => :app do
      template = File.read("#{svn_local_pre_revprop_change_template}")
      buffer = ERB.new(template).result(binding)
      put buffer, "/usr/local/svn/hooks/template/pre-revprop-change"
      template = File.read("#{svn_local_start_commit_template}")
      buffer = ERB.new(template).result(binding)
      put buffer, "/usr/local/svn/hooks/template/start-commit"
      template = File.read("#{svn_local_post_revprop_change_template}")
      buffer = ERB.new(template).result(binding)
      put buffer, "/usr/local/svn/hooks/template/post-revprop-change"
      template = File.read("#{svn_local_post_commit_template}")
      buffer = ERB.new(template).result(binding)
      put buffer, "/usr/local/svn/hooks/template/post-commit"
    end

    task :install_master_repository_hooks, roles => :app  do
      template = File.read("#{svn_master_post_revprop_change_template}")
      buffer = ERB.new(template).result(binding)
      put buffer, "/usr/local/svn/hooks/master/post-revprop-change"
      template = File.read("#{svn_master_post_commit_template}")
      buffer = ERB.new(template).result(binding)
      put buffer, "/usr/local/svn/hooks/master/post-commit"
      template = File.read("#{svn_master_pre_revprop_change_template}")
      buffer = ERB.new(template).result(binding)
      put buffer, "/usr/local/svn/hooks/master/pre-revprop-change"
      template = File.read("#{svn_master_start_commit_template}")
      buffer = ERB.new(template).result(binding)
      put buffer, "/usr/local/svn/hooks/master/start-commit"
    end

    task :install_master_cron, roles => :app  do
      template = File.read("#{cron_master_crontab_template}")
      buffer = ERB.new(template).result(binding)
      put buffer, "/usr/local/svn/cron/master/crontab"
      template = File.read("#{cron_master_remount_tesla_template}")
      buffer = ERB.new(template).result(binding)
      put buffer, "/usr/local/svn/cron/master/remount_tesla.sh"
      template = File.read("#{cron_master_sync_conf_template}")
      buffer = ERB.new(template).result(binding)
      put buffer, "/usr/local/svn/cron/master/sync_conf.sh"
    end

    task :install_slave_cron, roles => :app  do
      template = File.read("#{cron_slave_crontab_template}")
      buffer = ERB.new(template).result(binding)
      put buffer, "/usr/local/svn/cron/slave/crontab"
      template = File.read("#{cron_slave_sync_repos_template}")
      buffer = ERB.new(template).result(binding)
      put buffer, "/usr/local/svn/cron/slave/sync_repos.sh"
      template = File.read("#{cron_slave_sync_conf_template}")
      buffer = ERB.new(template).result(binding)
      put buffer, "/usr/local/svn/cron/slave/sync_conf.sh"
    end


    task :install_httpd_conf, roles => :web, :except => { :no_release => true } do
      template = File.read("#{apache_httpd_conf_template}")
      buffer = ERB.new(template).result(binding)
      put buffer, "/etc/httpd/conf/httpd.conf"
    end

    task :install_apache_conf, roles => :web, :except => { :no_release => true } do
      template = File.read("#{apache_slave_vhost_conf_template}")
      buffer = ERB.new(template).result(binding)
      put buffer, "/etc/httpd/conf.d/#{site_name}.conf"
      template = File.read("#{apache_master_vhost_conf_template}")
      buffer = ERB.new(template).result(binding)
      put buffer, "/etc/httpd/conf.d/master.#{site_name}.conf"
    end

    task :install_default_site, roles => :web, :except => { :no_release => true } do
      template = File.read("#{default_site_template}")
      buffer = ERB.new(template).result(binding)
      put buffer, "/var/www/html/index.html"
    end

  end
end 
