=begin
  * Original Copyright: (C) 2012 Acquity Group, LLC - All Rights Reserved
  * Copyright: (C) 2013 Accenture Interactive (Accenture LLP) - All Rights Reserved
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Name: Hybris build (build.rb) 
  * Author: Jason Potkanski <jason.potkanski@acquitygroup.com> <jason.potkanski@accenture.com>
  * Author: Shared Services DevOps <devops@acquitygroup.com>
  * Date:
  * License: Proprietary and confidential
  * Use of this file may require license terms between Acquity Group/Accenture Interactive and Licensee
=end

namespace :hybris do

  # set :private_key_template, File.dirname(__FILE__) + "/private_key.erb"
  # set :public_key_template, File.dirname(__FILE__) + "/public_key.erb"
  set :sonar_project_properties_template, File.dirname(__FILE__) + "/sonar-project_properties.erb"

  namespace :build do


    task :install_base do
      dirname = File.expand_path(File.dirname(__FILE__))
      puts dirname
      run_locally "mkdir -p '#{dirname}/../../../../../HybrisBase'"
      run_locally "mkdir -p '#{dirname}/../../../../../Packages'"
      path = "#{dirname}/../../../../../HybrisBase/#{hybris_version}"
      puts "Looking for Hybris install..."
      if File.exists?("#{path}/hybris") && File.directory?("#{path}/hybris")
        puts "Base Hybris Install Exists"
      else
        puts "Copying Base Hybris Install"
        run_locally "mkdir -p '#{path}'"
        #set(:acquity_username) { Capistrano::CLI.ui.ask("Acquity username: ") }
        #set(:acquity_password) { Capistrano::CLI.password_prompt("Acquity password: ") }
        if (Gem::Version.new(hybris_version) >= Gem::Version.new('5.0.0'))
          set :hybris_suitename, "commerce"       
        else
          set :hybris_suitename, "multichannel"
        end
        if (exists?(:localbasedir))
          puts "Sourcing from #{localbasedir}"
          run_locally "cp '#{localbasedir}/hybris-#{hybris_suitename}-suite-#{hybris_version}.zip' '#{path}/hybris-#{hybris_suitename}-suite-#{hybris_version}.zip' "
        else  
          #puts "Sourcing from Acquity Srv-AG2"
          #run_locally "smbclient //srv-ag2.acquitygroup.com/Software #{acquity_password} -U acquity/#{acquity_username} -c 'cd \"Hybris\";get hybris-#{hybris_suitename}-suite-#{hybris_version}.zip \"#{path}/hybris-#{hybris_suitename}-suite-#{hybris_version}.zip\";exit' "
          puts "Sourcing from Acquity Artifactory"
          run_locally "wget -q -O \"#{path}/hybris-#{hybris_suitename}-suite-#{hybris_version}.zip\" http://artifact.acq/hybris-source/hybris-#{hybris_suitename}-suite-#{hybris_version}.zip"
        end
        #run_locally "cp /releng/jenkins/hybris_zips/hybris-#{hybris_suitename}-suite-#{hybris_version}.zip \"#{path}/hybris-#{hybris_suitename}-suite-#{hybris_version}.zip\";"
        run_locally "cd '#{path}'; unzip hybris-#{hybris_suitename}-suite-#{hybris_version}.zip"
        run_locally "rm -f '#{path}/hybris-#{hybris_suitename}-suite-#{hybris_version}.zip'"
      end
      if (Gem::Version.new(hybris_version) >= Gem::Version.new('5.0.3'))
        
		#Oracle Install
		#set :oracledb_version,  fetch(:oracledb_version, "12.1.0.1")   
        #run_locally "mkdir -p '#{dirname}/../../../../../OracleBase/#{oracledb_version}'"
        #oracledbpath = "#{dirname}/../../../../../OracleBase/#{oracledb_version}"
		#puts "Looking for Oracle DB install..."
        #if File.exists?("#{oracledbpath}/ojdbc7.jar") && File.directory?("#{oracledbpath}")
        #  puts "Base Oracle DB Install Exists"
        #else
        #  puts "Copying Oracle DB Hybris Install"
        #  if (exists?(:localbasedir))
        #    puts "Sourcing from #{localbasedir}"
        #    run_locally "cp '#{localbasedir}/ojdbc7.jar' '#{oracledbpath}/ojdbc7.jar' "
        #  else  
		       
		#SQL Install
		set :sqldb_version,  fetch(:sqldb_version, "11.0.2")   
        run_locally "mkdir -p '#{dirname}/../../../../../sql/#{sqldb_version}'"
        sqldbpath = "#{dirname}/../../../../../sql/#{sqldb_version}"
      
        puts "Looking for SQL DB install..."
        if File.exists?("#{sqldbpath}/sqljdbc41.jar") && File.directory?("#{sqldbpath}")
          puts "Base SQL DB Install Exists"
        else
          puts "Copying SQL DB Hybris Install"
          if (exists?(:localbasedir))
            puts "Sourcing from #{localbasedir}"
            run_locally "cp '#{localbasedir}/sqljdbc41.jar' '#{oracledbpath}/sqljdbc41.jar' "
          else  
            #puts "Sourcing from Acquity Srv-AG2"
            #run_locally "smbclient //srv-ag2.acquitygroup.com/Software #{acquity_password} -U acquity/#{acquity_username} -c 'cd \"Oracle\\jdbc\\#{oracledb_version}\";get ojdbc7.jar \"#{oracledbpath}/ojdbc7.jar\";exit' "
            puts "Sourcing from Acquity Artifactory"
            #run_locally "wget -q -O \"#{oracledbpath}/ojdbc7.jar\" http://artifact.acq/hybris-source/oracle/jdbc/#{oracledb_version}/ojdbc7.jar"
			run_locally "wget -q -O \"#{sqldbpath}/sqljdbc41.jar\" http://artifact.acq/hybris-source/sql/jdbc/#{sqldb_version}/sqljdbc41.jar"
			
          end
        end
      end
      if (Gem::Version.new(hybris_version) >= Gem::Version.new('5.1.0'))
        set :tcserver_version,  fetch(:tcserver_version, "2.9.5.SR1")   
        run_locally "mkdir -p '#{dirname}/../../../../../tcServer/#{tcserver_version}'"
        tcserverpath = "#{dirname}/../../../../../tcServer/#{tcserver_version}"
      
        puts "Looking for tcServer install..."
        if File.exists?("#{tcserverpath}/vfabric-tc-server-standard-#{tcserver_version}") && File.directory?("#{tcserverpath}/vfabric-tc-server-standard-#{tcserver_version}")
          puts "tcServer Install Exists"
        else
          puts "Copying tcServer Hybris Install"
          if (exists?(:localbasedir))
            puts "Sourcing from #{localbasedir}"
            run_locally "cp '#{localbasedir}/vfabric-tc-server-standard-#{tcserver_version}.zip' '#{tcserverpath}/vfabric-tc-server-standard-#{tcserver_version}.zip' "
          else              
            #puts "Sourcing from Acquity Srv-AG2"
            #run_locally "smbclient //srv-ag2.acquitygroup.com/Software #{acquity_password} -U acquity/#{acquity_username} -c 'cd \"tcServer\";get vfabric-tc-server-standard-#{tcserver_version}.zip \"#{tcserverpath}/vfabric-tc-server-standard-#{tcserver_version}.zip\";exit' "
            puts "Sourcing from Acquity Artifactory"
            run_locally "wget -q -O \"#{tcserverpath}/vfabric-tc-server-standard-#{tcserver_version}.zip\" http://artifact.acq/hybris-source/vfabric-tc-server-standard-#{tcserver_version}.zip"
          end
          run_locally "cd '#{tcserverpath}'; unzip vfabric-tc-server-standard-#{tcserver_version}.zip"
          run_locally "cd '#{tcserverpath}'; rm -f vfabric-tc-server-standard-#{tcserver_version}.zip"
        end
      end
      run_locally "cp -Rnv '#{path}/hybris/bin' '#{dirname}/../../../../' "
      if (Gem::Version.new(hybris_version) >= Gem::Version.new('5.0.3'))
        #Oracle Injection
		#puts "Injecting Oracle DB Driver"
        #run_locally "cp -Rnv '#{oracledbpath}'/* '#{dirname}/../../../../bin/platform/lib/dbdriver' "
		
		#SQL Injection
		puts "Injecting SQL DB Driver"
        run_locally "cp -Rnv '#{sqldbpath}'/* '#{dirname}/../../../../bin/platform/lib/dbdriver' "
      end
      if (Gem::Version.new(hybris_version) >= Gem::Version.new('5.1.0'))
        puts "Injecting tcServer"
        run_locally "cp -Rnv '#{tcserverpath}'/vfabric-tc-server-standard-#{tcserver_version}/* '#{dirname}/../../../../bin/platform/tcServer' "
        if !File.exists?("#{path}/hybris/bin/tcserver.sh")
          run_locally "touch '#{dirname}/../../../../bin/platform/tcserver.sh'"
          run_locally "echo '#!/bin/sh' >> '#{dirname}/../../../../bin/platform/tcserver.sh'"
          run_locally "echo '#--------' >> '#{dirname}/../../../../bin/platform/tcserver.sh'"
          run_locally "echo '#Run Script for the CATALINA Server' >> '#{dirname}/../../../../bin/platform/tcserver.sh'"
          run_locally "echo '' >> '#{dirname}/../../../../bin/platform/tcserver.sh'"
          run_locally "echo 'DIR=`dirname $0`' >> '#{dirname}/../../../../bin/platform/tcserver.sh'"
          run_locally "echo 'TCSERVER_DIR=\"${DIR}/tcServer\"' >> '#{dirname}/../../../../bin/platform/tcserver.sh'"
          run_locally "echo '' >> '#{dirname}/../../../../bin/platform/tcserver.sh'"
          run_locally "echo 'if [ -z \"$1\" ]; then ' >> '#{dirname}/../../../../bin/platform/tcserver.sh'"
          run_locally "echo '    echo \"ERROR Please provide name of tcServer instance to start as parameter\"' >> '#{dirname}/../../../../bin/platform/tcserver.sh'"
          run_locally "echo '    exit 1' >> '#{dirname}/../../../../bin/platform/tcserver.sh'"
          run_locally "echo 'fi' >> '#{dirname}/../../../../bin/platform/tcserver.sh'"
          run_locally "echo '' >> '#{dirname}/../../../../bin/platform/tcserver.sh'"
          run_locally "echo 'cd ${TCSERVER_DIR}' >> '#{dirname}/../../../../bin/platform/tcserver.sh'"
          run_locally "echo 'export INSTANCE_NAME=\"$1\"' >> '#{dirname}/../../../../bin/platform/tcserver.sh'"
          run_locally "echo 'exec \"./tcruntime-ctl.sh\" $1 $2 $3' >> '#{dirname}/../../../../bin/platform/tcserver.sh'"   
        end
      end
      if (Gem::Version.new(hybris_version) >= Gem::Version.new('5.3.0'))
        puts "Injecting tcServer"
        run_locally "cp -Rnv '#{tcserverpath}'/vfabric-tc-server-standard-#{tcserver_version}/* '#{dirname}/../../../../bin/platform/tcServer' "
        if !File.exists?("#{path}/hybris/bin/tcserver.sh")
          run_locally "touch '#{dirname}/../../../../bin/platform/tcserver.sh'"
          run_locally "echo '#!/bin/sh' >> '#{dirname}/../../../../bin/platform/tcserver.sh'"
          run_locally "echo '#--------' >> '#{dirname}/../../../../bin/platform/tcserver.sh'"
          run_locally "echo '#Run Script for the CATALINA Server' >> '#{dirname}/../../../../bin/platform/tcserver.sh'"
          run_locally "echo '' >> '#{dirname}/../../../../bin/platform/tcserver.sh'"
          run_locally "echo 'DIR=`dirname $0`' >> '#{dirname}/../../../../bin/platform/tcserver.sh'"
          run_locally "echo 'TCSERVER_DIR=\"${DIR}/tcServer\"' >> '#{dirname}/../../../../bin/platform/tcserver.sh'"
          run_locally "echo '' >> '#{dirname}/../../../../bin/platform/tcserver.sh'"
          run_locally "echo 'if [ -z \"$1\" ]; then ' >> '#{dirname}/../../../../bin/platform/tcserver.sh'"
          run_locally "echo '    echo \"ERROR Please provide name of tcServer instance to start as parameter\"' >> '#{dirname}/../../../../bin/platform/tcserver.sh'"
          run_locally "echo '    exit 1' >> '#{dirname}/../../../../bin/platform/tcserver.sh'"
          run_locally "echo 'fi' >> '#{dirname}/../../../../bin/platform/tcserver.sh'"
          run_locally "echo '' >> '#{dirname}/../../../../bin/platform/tcserver.sh'"
          run_locally "echo 'cd ${TCSERVER_DIR}' >> '#{dirname}/../../../../bin/platform/tcserver.sh'"
          run_locally "echo 'export INSTANCE_NAME=\"$1\"' >> '#{dirname}/../../../../bin/platform/tcserver.sh'"
          run_locally "echo 'exec \"./tcruntime-ctl.sh\" $1 $2 $3' >> '#{dirname}/../../../../bin/platform/tcserver.sh'"   
        end
      end       
      puts "mv '#{dirname}/../../../../config' '#{dirname}/../../../../config-temp' "
      run_locally "mkdir -p '#{dirname}/../../../../config/languages'"
      run_locally "[ -d '#{dirname}/../../../../config' ] && mv '#{dirname}/../../../../config' '#{dirname}/../../../../config-temp' "
      puts "cd '#{dirname}/../../../../bin/platform'; source setantenv.sh ; ant server -Dinput.template=develop -Dmaven.update.dbdrivers=false"
      run_locally "cd '#{dirname}/../../../../bin/platform'; source ./setantenv.sh ; ant server -Dinput.template=develop -Dmaven.update.dbdrivers=false"
      puts "[ -d '#{dirname}/../../../../config-temp' ] && cp -Rnv '#{dirname}/../../../../config/'* '#{dirname}/../../../../config-temp' "
      run_locally "[ -d '#{dirname}/../../../../config-temp' ] && cp -Rnv  '#{dirname}/../../../../config/'* '#{dirname}/../../../../config-temp' "
      run_locally "[ -d '#{dirname}/../../../../config-temp' ] && rm -rf '#{dirname}/../../../../config' "
      run_locally "[ -d '#{dirname}/../../../../config-temp' ] && mv '#{dirname}/../../../../config-temp' '#{dirname}/../../../../config' "
      puts "Fix Languages directory missing Hybris Bug..."
      run_locally "mkdir -p '#{dirname}/../../../../config/languages'"
     
    end

    task :clean_packages do
      dirname = File.expand_path(File.dirname(__FILE__))
      run_locally "rm -rf '#{dirname}/../../../../../Packages/*'"
    end

    task :ant_build do
      dirname = File.expand_path(File.dirname(__FILE__))
      buildoutput = run_locally "cd '#{dirname}/../../../../bin/platform'; source ./setantenv.sh ; ant build -Dmaven.update.dbdrivers=false"
      if $?.exitstatus != 0 then
        puts buildoutput
        raise "Build Failure!"
      end
    end

    task :ant_clean do
      dirname = File.expand_path(File.dirname(__FILE__))
      system "cd '#{dirname}/../../../../bin/platform'; source ./setantenv.sh ; ant clean -Dmaven.update.dbdrivers=false"
      if $?.exitstatus != 0 then
        raise "Clean Failure!"
      end
    end

    task :ant_customize do
      dirname = File.expand_path(File.dirname(__FILE__))
      system "cd '#{dirname}/../../../../bin/platform'; source ./setantenv.sh ; ant customize -Dmaven.update.dbdrivers=false"
    end

    task :ant_package do
      dirname = File.expand_path(File.dirname(__FILE__))
      if fetch(:os_system, "linux") == "windows"
      system "cd '#{dirname}/../../../../bin/platform'; source ./setantenv.sh ; ant production -Dmaven.update.dbdrivers=false -Dproduction.output.path=\"`cygpath -w '#{dirname}/../../../../../Packages'`\""
      else
      system "cd '#{dirname}/../../../../bin/platform'; source ./setantenv.sh ; ant production -Dmaven.update.dbdrivers=false -Dproduction.output.path='#{dirname}/../../../../../Packages'"
      end
        system "cd '#{dirname}/../../../../temp/'; zip ../../Packages/hybrisServer-AllExtensions.zip hybris/bin/custom/revision.txt"
      if (Gem::Version.new(hybris_version) >= Gem::Version.new('5.0.0.0') && Gem::Version.new(hybris_version) < Gem::Version.new('5.0.1.0') )
        puts "Patching 5.0.0"
        run_locally "cd '#{dirname}/../../../../../HybrisBase/#{hybris_version}/'; zip ../../Packages/hybrisServer-Platform.zip hybris/bin/platform/resources/ant/dist/*" 
        run_locally "cd '#{dirname}/../../../../../HybrisBase/#{hybris_version}/'; zip ../../Packages/hybrisServer-Platform.zip hybris/bin/platform/.ruleset"                
      end
    end

    task :extensions_package do
      dirname = File.expand_path(File.dirname(__FILE__))
      exclusions = ["platform",".svn","doc/*-src.zip","doc/*-sources.jar","doc/resources/*.mdl","doc/install.xml",
        "eclipsebin/*","extensioninfo.xsd","macrotests/*","resources/items.xsd","src/*", "testsrc/*,",
        "temp/*","gensrc/*", "testsrc/*","testclasses/*","classes/*", "hmc/resources/hmc.xsd",
        "hmc/src/*", "hmc/classes/*", "web/src/*",  "build.xml", "platformhome.properties", ".settings/*"]
      tarstring = ""
      exclusions.each {|exclusion| tarstring = tarstring + " --exclude='#{exclusion}" }
      run_locally "cd '#{dirname}/../../../../bin/'; tar -cvzf '#{dirname}/../../../../../Packages/hybrisServer-AllExtensions.tgz' #{tarstring} *"
    end

    task :platform_package do
      dirname = File.expand_path(File.dirname(__FILE__))
      run_locally "cd '#{dirname}/../../../../bin/'; tar -cvzf '#{dirname}/../../../../../Packages/hybrisServer-Platform.tgz' --exclude='.svn' platform"
    end

    task :config_package do
      dirname = File.expand_path(File.dirname(__FILE__))
      run_locally "cd '#{dirname}/../../../../'; tar -cvzf '#{dirname}/../../../../../Packages/hybrisServer-Config.tgz' --exclude='.svn' config"
    end

    task :coherence_package do
      dirname = File.expand_path(File.dirname(__FILE__))
      if use_coherence then
        run_locally "cd '#{dirname}/../../../../'; tar -cvzf '#{dirname}/../../../../../Packages/hybrisServer-Coherence.tgz' --exclude='.svn' coherence"
      end
    end

    task :install_revisiontxt do
      dirname = File.expand_path(File.dirname(__FILE__))
      run_locally "cd '#{dirname}/../../../../'; svn info > '#{dirname}/../../../../bin/platform'/revision.txt; touch '#{dirname}/../../../../bin/platform'/revision.txt;"
      run_locally "cd '#{dirname}/../../../../config'; svn info > '#{dirname}/../../../../config'/revision.txt; touch '#{dirname}/../../../../config'/revision.txt"
      run_locally "mkdir -p '#{dirname}/../../../../bin/custom'; cd '#{dirname}/../../../../bin/custom'; svn info  > '#{dirname}/../../../../bin/custom'/revision.txt; touch '#{dirname}/../../../../bin/custom'/revision.txt;"
      if ENV['BUILD_TAG'] then
        build_tag = ENV['BUILD_TAG']
      else 
        build_tag = "No build tag parameter found"
      end
      File.open("#{dirname}/../../../../bin/platform/revision.txt", 'a') { |f2|  f2.puts "Jenkins Build Tag: #{build_tag}" }
      File.open("#{dirname}/../../../../config/revision.txt", 'a') { |f2|  f2.puts "Jenkins Build Tag: #{build_tag}" }
      File.open("#{dirname}/../../../../bin/custom/revision.txt", 'a') { |f2|  f2.puts "Jenkins Build Tag: #{build_tag}" }
      run_locally "mkdir -p '#{dirname}/../../../../temp/hybris/bin/custom'; cp -f '#{dirname}/../../../../bin/custom'/revision.txt '#{dirname}/../../../../temp/hybris/bin/custom' "
    end

    task :create_sonar_properties do
      set :project_key, "#{client_shortcode}.acquity:Hybris_#{project_code}"
      job_name = "#{client_code} Hybris #{project_code}"
      if ENV['JOB_NAME'] then
        job_name = ENV['JOB_NAME']
      end
      set :project_job, "#{job_name}"
      revision = "0"
      if ENV['SVN_REVISION'] then
        revision = ENV['SVN_REVISION']
      end
      set :revision, "#{revision}"
      workspace = File.expand_path(File.dirname(__FILE__)) + "/../../../../.."
      if ENV['WORKSPACE'] then
        workspace = ENV['WORKSPACE']
      end
      svn_url = "http://"
      if ENV['SVN_URL'] then
        svn_url = ENV['SVN_URL']
      end
      #sources=`find '#{workspace}/source/bin/custom' -type d -name src | grep -v /web/src | grep -v /hmc/src`
      #list local extensions, only take lines containing /custom/
      #Cut out all comments in extensions using sed. (Bug via Fred N)
      #Use cut to seperate the fields by ", take the second field
      #Replace what is before /custom with the absolute path.
      #sources=`cat '#{workspace}/source/config/localextensions.xml' | grep /custom/ | cut -d "\"" -f 2 `
      sources_feed=`cat '#{workspace}/source/config/localextensions.xml' |  tr -d '\\r' | sed '/<!--.*-->/d' | sed '/<!--/,/-->/d' | grep /custom/ | cut -d '\"' -f 2 | sed s/^.*custom/custom/`
      #Cheat by adding /src after new line.
      sources_feed=sources_feed.gsub("custom","#{workspace}/source/bin/custom")
      sources_ary = sources_feed.split(/\n/).reject(&:empty?)
      sources = ""
      sources_ary.each { |source|        
        src = run_locally "if [ -d '#{source}/src' ]; then echo #{source}/src,; fi;" 
        websrc = run_locally "if [ -d '#{source}/web/src' ]; then echo #{source}/web/src,; fi;"
        sources.concat(src).concat(websrc)
      }      
      sources=sources.gsub("\r","").gsub("\n","")
      binaries=`find '#{workspace}/source/bin' -type d -name classes | grep -v /web/ | grep -v /hmc/`
      binaries=binaries.gsub("\n",",")
      tests=`find '#{workspace}/source/bin/custom' -type d -name testsrc | grep -v /web/`
      tests=tests.gsub("\n",",")
      libraries=`find '#{workspace}/source/bin' ! -name batik-xml-1.7-javadoc.jar -name *.jar | grep -v /web/ | grep -v /hmc/`
      libraries=libraries.gsub("\n",",")
      # Nuke File Analyzer crap.
      run_locally "rm -rf '#{workspace}'/source/log/junit/*.log.xml"
      set :sonar_junits_path, "#{workspace}/source/log/junit"
      set :sonar_jacoco_path, "#{workspace}/source/bin/platform/jacoco.exec"
      set :sonar_sources, "#{sources}"
      set :sonar_tests, "#{tests}"
      set :sonar_binaries, "#{binaries}"
      set :sonar_libraries, "#{libraries}"
      set :sonar_svn_url, "#{svn_url}"
    end

    task :install_sonar_properties do
      template = File.read("#{sonar_project_properties_template}")
      buffer = ERB.new(template).result(binding)
      dirname = File.expand_path(File.dirname(__FILE__))
      File.open("#{dirname}/../../../../../sonar-project.properties", 'w') {|f| f.write(buffer) }
    end

    task :build_endeca do
      dirname = File.expand_path(File.dirname(__FILE__))
      system "mkdir -p '#{dirname}/../../../../../Packages'"
      hybris.build.clean_packages
      system "cd '#{dirname}/../../../../endeca'; zip -r -x='.svn' '#{dirname}/../../../../../Packages/hybrisServer-Endeca.zip' *"
    end

    task :fix_platform do
      dirname = File.expand_path(File.dirname(__FILE__))
      puts "Repairing tcserver.sh"
      run_locally "sed -i 's/$1 run/$1 $2 $3/' '#{dirname}/../../../../bin/platform/tcserver.sh';"
      run_locally "sed -i 's/$1 $MODE/$1 $2 $3/' '#{dirname}/../../../../bin/platform/tcserver.sh';"
      puts "Fix 5.1.1.0 bug env.properties"
      run_locally "sed -i 's%^HYBRIS_BIN_DIR.*%HYBRIS_BIN_DIR=${platformhome}\/..\/..\/bin%g' '#{dirname}/../../../../bin/platform/env.properties';"
    end

    task :build do
      hybris.build.install_base      
      #hybris.build.ant_clean
      # Ant clean nukes revision.txts!
      hybris.build.fix_platform
      hybris.build.install_revisiontxt
      hybris.build.clean_packages      
      hybris.build.ant_customize
      hybris.build.ant_build
      hybris.build.ant_package
      hybris.build.config_package
      hybris.build.coherence_package
      hybris.build.create_sonar_properties
      hybris.build.install_sonar_properties
      #hybris.build.extensions_package
    end

    task :full_build do
      dirname = File.expand_path(File.dirname(__FILE__))
      puts dirname
      #run_locally "rm -rf '#{dirname}/../../../../../HybrisBase'"
      hybris.build.install_base
      hybris.build.fix_platform
      hybris.build.ant_clean     
      # Ant clean nukes revision.txts!
      hybris.build.install_revisiontxt
      hybris.build.clean_packages      
      hybris.build.ant_customize
      hybris.build.ant_build
      hybris.build.ant_package
      hybris.build.config_package
      hybris.build.coherence_package
      #hybris.build.extensions_package
      #hybris.build.platform_package
      hybris.build.create_sonar_properties
      hybris.build.install_sonar_properties
    end



  end
end
