=begin
  * Original Copyright: (C) 2012 Acquity Group, LLC - All Rights Reserved
  * Copyright: (C) 2013 Accenture Interactive (Accenture LLP) - All Rights Reserved
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Name: Hybris deploy (deploy.rb) 
  * Author: Jason Potkanski <jason.potkanski@acquitygroup.com> <jason.potkanski@accenture.com>
  * Author: Shared Services DevOps <devops@acquitygroup.com>
  * Date:
  * License: Proprietary and confidential
  * Use of this file may require license terms between Acquity Group/Accenture Interactive and Licensee
=end
namespace :hybris do

  # set :private_key_template, File.dirname(__FILE__) + "/private_key.erb"
  # set :public_key_template, File.dirname(__FILE__) + "/public_key.erb"


  
  namespace :deploy do

    task :create_release_path, :roles => :app do
      run "mkdir -p #{release_path}"
    end
    
    task :fix_packages do
      dirname = File.expand_path(File.dirname(__FILE__))
      run_locally "cd '#{dirname}/../../../../../Packages'; mv ./*/*/* .;  find . -type d  ! -name . -prune -print0 | xargs -0 rm -rf"
    end
    
    task :copy_config, :roles => :app do
      dirname = File.expand_path(File.dirname(__FILE__))
      acq_upload "#{dirname}/../../../../../Packages/hybrisServer-Config.tgz", "#{release_path}/hybrisServer-Config.tgz"
    end

    task :copy_coherence, :roles => :app do
      dirname = File.expand_path(File.dirname(__FILE__))
      if use_coherence then
        acq_upload "#{dirname}/../../../../../Packages/hybrisServer-Coherence.tgz", "#{release_path}/hybrisServer-Coherence.tgz"
      end
    end

    task :copy_endeca, :roles => :endeca do
      dirname = File.expand_path(File.dirname(__FILE__))
      acq_upload "#{dirname}/../../../../../Packages/hybrisServer-Endeca.zip", "#{deploy_to}/hybrisServer-Endeca.zip"
    end

    task :copy_extensions, :roles => :app do
      dirname = File.expand_path(File.dirname(__FILE__))
      acq_upload "#{dirname}/../../../../../Packages/hybrisServer-AllExtensions.zip", "#{release_path}/hybrisServer-AllExtensions.zip"
    end

    task :copy_platform, :roles => :app do
      dirname = File.expand_path(File.dirname(__FILE__))
      if platform_update or remote_file_exists?("#{deploy_to}/hybrisServer-Platform.zip") != true then
        acq_upload "#{dirname}/../../../../../Packages/hybrisServer-Platform.zip", "#{deploy_to}/hybrisServer-Platform.zip"
      end
    end

    task :deploy_platform, :roles => :app do
      run "unzip '#{deploy_to}/hybrisServer-Platform.zip' -d #{release_path}/"
    end

    task :deploy_extensions, :roles => :app do
      run "unzip '#{release_path}/hybrisServer-AllExtensions.zip' -d #{release_path}/"
      # keep extensions artifact.
      #run "rm -f '#{release_path}/hybrisServer-AllExtensions.zip'"
    end

    task :deploy_config, :roles => :app do
      run "tar -zxvf '#{release_path}/hybrisServer-Config.tgz' -C #{release_path}/hybris/"
    end

    task :deploy_endeca, :roles => :endeca do
      run "unzip -o '#{deploy_to}/hybrisServer-Endeca.zip' -d #{deploy_to}/endeca/"
    end

    task :deploy_coherence, :roles => :app do
      run "tar -zxvf '#{release_path}/hybrisServer-Coherence.tgz' -C #{release_path}/hybris/"
    end

    task :setup_config, :roles => :app do
      seddlm = fetch(:sed_delimiter, "/")
      run "rm -f #{release_path}/hybris/config/local.properties"
      #commented out for sed 4.2 BS, done last now.
      #run "cd #{release_path}/hybris/config; ln -s local#{stage_name}.properties local.properties"
      config_rev = capture("grep 'Changed Rev' #{release_path}/hybris/config/revision.txt | cut -c19-").strip
      extension_rev = capture("grep 'Changed Rev' #{release_path}/hybris/bin/custom/revision.txt | cut -c19-").strip
      platform_rev = capture("grep 'Changed Rev' #{release_path}/hybris/bin/platform/revision.txt | cut -c19-").strip
      revision = "Extension CR:#{extension_rev},Config CR:#{config_rev},Platform CR:#{platform_rev}"
      print "Setting up local.properties file.\n"
      run "sed -i 's/build.revision=BUILD REVISION NUMBER GOES HERE/build.revision=#{revision}/g' #{release_path}/hybris/config/local#{stage_name}.properties"     
      if exists?(:db_password2) then
        run "sed -i 's#{seddlm}DB_PASSWORD2#{seddlm}#{db_password2}#{seddlm}g' #{release_path}/hybris/config/local#{stage_name}.properties"
      end
      if exists?(:db_password3) then
        run "sed -i 's#{seddlm}DB_PASSWORD3#{seddlm}#{db_password3}#{seddlm}g' #{release_path}/hybris/config/local#{stage_name}.properties"
      end
      if exists?(:db_password4) then
        run "sed -i 's#{seddlm}DB_PASSWORD4#{seddlm}#{db_password4}#{seddlm}g' #{release_path}/hybris/config/local#{stage_name}.properties"
      end  
      if exists?(:db_password) then
        run "sed -i 's#{seddlm}DB_PASSWORD#{seddlm}#{db_password}#{seddlm}g' #{release_path}/hybris/config/local#{stage_name}.properties"
      end
      if exists?(:celum_password) then
        run "sed -i 's#{seddlm}CELUM_PASSWORD#{seddlm}#{celum_password}#{seddlm}g' #{release_path}/hybris/config/local#{stage_name}.properties"
      end
      if exists?(:ftp_password) then
        run "sed -i 's#{seddlm}FTP_PASSWORD#{seddlm}#{ftp_password}#{seddlm}g' #{release_path}/hybris/config/local#{stage_name}.properties"
      end
      if (Gem::Version.new(hybris_version) >= Gem::Version.new('5.1.0'))
        set :tcserver_dir, "tcServer"       
      else
        set :tcserver_dir, "tcServer-6.0"
      end
      
      servers = find_servers_for_task(current_task)
      servers.each_with_index do |s,index|
        puts "Index is #{index}"
        clusterid = index + 16
        instanceid = index + 1
        maxid = servers.size
        run "sed -i 's/HOST-IP/$CAPISTRANO:HOST$/g' #{release_path}/hybris/config/local#{stage_name}.properties", :hosts => s
        run "sed -i 's/bundled.tcserver.instance=TCINSTANCEID/bundled.tcserver.instance=#{stage_name}-#{client_shortcode}-#{instanceid}/g' #{release_path}/hybris/config/local#{stage_name}.properties", :hosts => s
        run "sed -i 's/tcserver.node=TCINSTANCEID/tcserver.node=#{stage_name}/g' #{release_path}/hybris/config/local#{stage_name}.properties", :hosts => s
        if fetch(:os_system, "linux") == "windows"
          run "cd #{release_path}/hybris/bin/platform/#{tcserver_dir}; export TARGET=`cygpath -wa #{release_path}/hybris/bin/platform/#{tcserver_dir}/#{stage_name}-#{client_shortcode}-#{instanceid}`;cmd /c mklink /D #{stage_name} $TARGET"
        else
          run "ln -s #{release_path}/hybris/bin/platform/#{tcserver_dir}/#{stage_name}-#{client_shortcode}-#{instanceid} #{release_path}/hybris/bin/platform/#{tcserver_dir}/#{stage_name}", :hosts => s
        end
        run "sed -i 's/cluster.id=CLUSTERID/cluster.id=#{clusterid}/g' #{release_path}/hybris/config/local#{stage_name}.properties", :hosts => s
        run "sed -i 's/cluster.maxid=MAXID/cluster.maxid=#{maxid}/g' #{release_path}/hybris/config/local#{stage_name}.properties", :hosts => s
        run "sed -i 's/cluster.broadcast.method.jgroups.tcp.bind_addr=SERVERIP/cluster.broadcast.method.jgroups.tcp.bind_addr=$CAPISTRANO:HOST$/g' #{release_path}/hybris/config/local#{stage_name}.properties", :hosts => s      
      end

      if remote_file_exists?("#{release_path}/hybris/config/tcServer/conf") then        
        run "mv #{release_path}/hybris/config/tcServer/conf #{release_path}/hybris/config/tcServer/default"
      end
      if remote_file_exists?("#{release_path}/hybris/config/tcServer/#{stage_name}")
        if fetch(:os_system, "linux") == "windows"
          run "cd #{release_path}/hybris/config/tcServer; export TARGET=`cygpath -wa #{release_path}/hybris/config/tcServer/#{stage_name}`;cmd /c mklink /D conf $TARGET"
        else
          run "ln -s #{release_path}/hybris/config/tcServer/#{stage_name} #{release_path}/hybris/config/tcServer/conf"
        end
      elsif remote_file_exists?("#{release_path}/hybris/config/tcServer/final")
        if fetch(:os_system, "linux") == "windows"
          run "cd #{release_path}/hybris/config/tcServer; export TARGET=`cygpath -wa #{release_path}/hybris/config/tcServer/prod`;cmd /c mklink /D conf $TARGET"
        else
          run "ln -s #{release_path}/hybris/config/tcServer/prod #{release_path}/hybris/config/tcServer/conf"
        end
      else
        if fetch(:os_system, "linux") == "windows"
          run "cd #{release_path}/hybris/config/tcServer; export TARGET=`cygpath -wa #{release_path}/hybris/config/tcServer/default`;cmd /c mklink /D conf $TARGET"
        else
          run "ln -s #{release_path}/hybris/config/tcServer/default #{release_path}/hybris/config/tcServer/conf"
        end
      end
      #Link last for sed 4.2+
        if fetch(:os_system, "linux") == "windows"
          run "cd #{release_path}/hybris/config/;cmd /c mklink local.properties local#{stage_name}.properties"
        else
          run "cd #{release_path}/hybris/config; ln -s local#{stage_name}.properties local.properties"
        end    
      if (Gem::Version.new(hybris_version) >= Gem::Version.new('5.1.0'))
        #  Another 5.1.1 Bug.
        run "touch '#{release_path}/hybris/bin/platform/lib/dbdriver/.lastupdate'"     
      end
    end

    task :link_hybris, :roles => :app do
      if fetch(:os_system, "linux") == "windows"
        run "cd #{release_path}/hybris; export TARGET=`cygpath -wa #{deploy_to}/temp`;cmd /c mklink /D temp $TARGET"
        run "cd #{release_path}/hybris; export TARGET=`cygpath -wa #{deploy_to}/data`;cmd /c mklink /D data $TARGET"
        run "cd #{release_path}/hybris; export TARGET=`cygpath -wa #{deploy_to}/log`;cmd /c mklink /D log $TARGET"
        run "/usr/bin/sed -i 's/$1 run/$1 $2/' #{release_path}/hybris/bin/platform/tcserver.sh;"
      else
        run "ln -s #{deploy_to}/hybris/temp #{release_path}/hybris/temp"
        run "ln -s #{deploy_to}/hybris/data #{release_path}/hybris/data"
        run "ln -s #{deploy_to}/hybris/log #{release_path}/hybris/log"
        run "sed -i 's/$1 run/$1 $2/' #{release_path}/hybris/bin/platform/tcserver.sh;"
      end
    end

    task :setup_endeca, :roles => :endeca do
      run "find #{deploy_to} -name *.sh | xargs chmod 744 "
    end

    task :ant_server, :roles => :app do
      dirname = File.expand_path(File.dirname(__FILE__))
      run "cd '#{release_path}/hybris/bin/platform'; #{java_home_cmd}; source ./setantenv.sh ; ant clean customize all -Dmaven.update.dbdrivers=false"
      #One day, it will be this simple.
      #run "cd '#{release_path}/hybris/bin/platform'; source ./setantenv.sh ; ant server"
    end

    task :build_coherence, :roles => :app do
      dirname = File.expand_path(File.dirname(__FILE__))
      run "cd '#{release_path}/hybris/bin/platform'; #{java_home_cmd}; source ./setantenv.sh ; ant buildCoherence -Dmaven.update.dbdrivers=false"
    end

    task :install_tcserver, :roles => :app do
      if (Gem::Version.new(hybris_version) >= Gem::Version.new('5.1.0'))
        set :tcserver_dir, "tcServer"       
      else
        set :tcserver_dir, "tcServer-6.0"
      end
      run "chmod 755 #{release_path}/hybris/bin/platform/#{tcserver_dir}/tcruntime-instance.sh; true"
      run "cd #{release_path}/hybris/bin/platform/#{tcserver_dir}; #{java_home_cmd}; ./tcruntime-instance.sh create #{stage_name}"
    end

    task :fix_tcserver, :roles => :app do
      if (Gem::Version.new(hybris_version) >= Gem::Version.new('5.1.0'))
        set :tcserver_dir, "tcServer"       
      else
        set :tcserver_dir, "tcServer-6.0"
      end
      #Nuke ROOT directory to fix context paths.
      run "cd #{release_path}/hybris/bin/platform/#{tcserver_dir}/#{stage_name}/webapps; rm -rf ROOT"
      # comment out tcserver java_opts, we run the memory show.
      if fetch(:os_system, "linux") == "windows"
        run "sed -i 's/^JVM_OPTS/#JVM_OPTS/' #{release_path}/hybris/bin/platform/#{tcserver_dir}/#{stage_name}/bin/setenv.bat"
      else
        run "sed -i 's/^JVM_OPTS/#JVM_OPTS/' #{release_path}/hybris/bin/platform/#{tcserver_dir}/#{stage_name}/bin/setenv.sh"
      end
      #Fix tcserver log location to outside release path.
      run "rm -rf '#{release_path}/hybris/bin/platform/#{tcserver_dir}/#{stage_name}/logs'"
      run "rm -rf '#{release_path}/hybris/bin/platform/#{tcserver_dir}/#{stage_name}/temp'"
      if fetch(:os_system, "linux") == "windows"
        run "cd #{release_path}/hybris/bin/platform/#{tcserver_dir}/#{stage_name}; export TARGET=`cygpath -wa #{release_path}/hybris/log/tcServer/#{stage_name}`;cmd /c mklink /D logs $TARGET"
      else
        run "ln -s #{release_path}/hybris/log/tcServer/ #{release_path}/hybris/bin/platform/#{tcserver_dir}/#{stage_name}/logs"
      end
      if (Gem::Version.new(hybris_version) >= Gem::Version.new('5.1.0'))          
        if fetch(:os_system, "linux") == "windows"
          run "cd #{release_path}/hybris/bin/platform/#{tcserver_dir}/#{stage_name}; export TARGET=`cygpath -wa #{release_path}/hybris/temp/hybrisServer`;cmd /c mklink /D temp $TARGET; true"
        else
          run "ln -s #{release_path}/hybris/temp/hybris/hybrisServer #{release_path}/hybris/bin/platform/#{tcserver_dir}/#{stage_name}/temp"            
        end
      else
        run "ln -s #{release_path}/hybris/temp/hybris #{release_path}/hybris/bin/platform/#{tcserver_dir}/#{stage_name}/temp"
      end
      # Added logs symlink for operations.
      if fetch(:os_system, "linux") == "windows"
        run "cd #{release_path}/hybris/bin/platform; export TARGET=`cygpath -wa #{release_path}/hybris/bin/platform/#{tcserver_dir}/#{stage_name}/logs`;cmd /c mklink /D logs $TARGET"
      else
        run "ln -s #{release_path}/hybris/bin/platform/#{tcserver_dir}/#{stage_name}/logs #{release_path}/hybris/bin/platform/logs"      
      end
      #Make sure stage name log dir exists
      run "mkdir -p #{deploy_to}/log/tcServer/#{stage_name}"  
      if fetch(:os_system, "linux") == "windows"
        run "chmod 755 #{release_path}/hybris/bin/platform/#{tcserver_dir}/tcruntime-ctl.bat"
        #run "chmod +x #{release_path}/hybris/bin/platform/tcserver.bat"
       
      end
    end

    task :install_version_check, :roles => :app do
      #TODO Add code to display CR versions and java version in html or jsp file.
    end

    task :stop_hybris, :roles => :app, :on_error => :continue do
      if (Gem::Version.new(hybris_version) >= Gem::Version.new('5.1.0'))
        set :tcserver_dir, "tcServer"       
      else
        set :tcserver_dir, "tcServer-6.0"
      end
      if fetch(:os_system, "linux") == "windows"
        run "chmod 755 #{deploy_to}/current/hybris/bin/platform/#{tcserver_dir}/tcruntime-ctl.bat"
        #run "chmod +x #{deploy_to}/current/hybris/bin/platform/tcserver.bat"
        run "cd '#{deploy_to}/current/hybris/bin/platform/#{tcserver_dir}'; #{java_home_cmd}; ./tcruntime-ctl.bat #{stage_name} stop"
      else
        run "chmod 755 #{deploy_to}/current/hybris/bin/platform/#{tcserver_dir}/tcruntime-ctl.sh; true"
        run "cd '#{deploy_to}/current/hybris/bin/platform'; #{java_home_cmd}; ./tcserver.sh #{stage_name} stop; true"
      end
    end

    task :restart_hybris, :roles => :app do
      #run "chmod 755 #{deploy_to}/current/hybris/bin/platform/#{tcserver_dir}/tcruntime-ctl.sh; true"
      if (Gem::Version.new(hybris_version) >= Gem::Version.new('5.1.0'))
        set :tcserver_dir, "tcServer"       
      else
        set :tcserver_dir, "tcServer-6.0"
      end
      if fetch(:os_system, "linux") == "windows"
         run "cd '#{deploy_to}/current/hybris/bin/platform/#{tcserver_dir}/'; #{java_home_cmd}; ./tcruntime-ctl.bat #{stage_name} reinstall"
      end
      hybris.deploy.stop_hybris
      print "Sleeping 60 seconds.\n"
      sleep 60
      if fetch(:os_system, "linux") == "windows"
        run "cd '#{deploy_to}/current/hybris/bin/platform/#{tcserver_dir}'; #{java_home_cmd}; ./tcruntime-ctl.bat #{stage_name} start"
      else
        run "cd '#{deploy_to}/current/hybris/bin/platform'; #{java_home_cmd}; ./tcserver.sh #{stage_name} start"
      end
    end

    #Moved to Util
    task :detect_java_home, :roles => :app do         
      begin        
        find_and_execute_task("hybris:util:detect_java_home")
      rescue NoSuchTaskError        
      end
    end
  
    #Moved to Util
    task :initialize_hybris, :roles => :app do        
      begin        
        find_and_execute_task("hybris:util:initialize_hybris")
      rescue NoSuchTaskError        
      end
    end
    
    #Moved to Util
    task :update_hybris, :roles => :app do        
      begin        
        find_and_execute_task("hybris:util:update_hybris")
      rescue NoSuchTaskError        
      end
    end

    #Moved to Util
    task :migrate, :roles => :app do        
      begin        
        find_and_execute_task("hybris:util:migrate")
      rescue NoSuchTaskError        
      end
    end
    
    #Moved to Util
    task :sanitize, :roles => :app do        
      begin        
        find_and_execute_task("hybris:util:sanitize")
      rescue NoSuchTaskError        
      end
    end
      
    
  end
end

def remote_file_exists?(full_path)
  'true' ==  capture("if [ -e #{full_path} ]; then echo 'true'; fi").strip
end
