=begin
  * Original Copyright: (C) 2012 Acquity Group, LLC - All Rights Reserved
  * Copyright: (C) 2013 Accenture Interactive (Accenture LLP) - All Rights Reserved
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Name: Hybris util [Utility task functions] (util.rb) 
  * Author: Jason Potkanski <jason.potkanski@acquitygroup.com> <jason.potkanski@accenture.com>
  * Author: Shared Services DevOps <devops@acquitygroup.com>
  * Date:
  * License: Proprietary and confidential
  * Use of this file may require license terms between Acquity Group/Accenture Interactive and Licensee
=end

namespace :hybris do


  namespace :util do
    
    desc "Force use of Acquity internal servers for fetching Hybris/Oracle files"
    task :force_acquity, :roles => :app do
      unset :localbasedir
    end
   
    task :fetch_logs, :roles => :app do     
      begin        
        find_and_execute_task("acquity:hybris:util:fetch_logs")
      rescue NoSuchTaskError
        puts "Fetch_logs requires a Hybris Build/Deploy FULL license"
      end
    end
    
    task :initialize_hybris, :roles => :app do     
      begin        
        find_and_execute_task("acquity:hybris:util:initialize_hybris")
      rescue NoSuchTaskError
        puts "Initialize_hybris requires a Hybris Build/Deploy FULL license"
      end
    end
    
    task :update_hybris, :roles => :app do     
      begin        
        find_and_execute_task("acquity:hybris:util:update_hybris")
      rescue NoSuchTaskError
        puts "Update_hybris requires a Hybris Build/Deploy FULL license"
      end
    end
    
    task :migrate, :roles => :app do     
      begin        
        find_and_execute_task("acquity:hybris:util:migrate")
      rescue NoSuchTaskError
        puts "Migrate requires a Hybris Build/Deploy FULL license"
      end
    end
    
    task :start_vpn, :roles => :app do     
      if fetch(:use_vpn, false) == true then
        begin        
          find_and_execute_task("acquity:hybris:util:start_vpn")
        rescue NoSuchTaskError        
        end
      end
    end
    
    task :stop_vpn, :roles => :app do  
      if fetch(:use_vpn, false) == true then
        begin        
          find_and_execute_task("acquity:hybris:util:start_vpn")
        rescue NoSuchTaskError 
          puts "VPN usage requires a Hybris Build/Deploy FULL license"
        end
      end
    end
    
    task :sanitize, :roles => :app do     
      begin        
        find_and_execute_task("acquity:hybris:util:sanitize")
      rescue NoSuchTaskError
        puts "Sanitize requires a Hybris Build/Deploy FULL license"
      end
    end
    
    task :restart, :roles => :app do
      if fetch(:stage_name, "dev").downcase == "prod" || fetch(:stage_name, "dev").downcase == "final"
        set :restart_via, "wave" unless exists?(:restart_via)
      end
      case fetch(:restart_via, "default")    
      when "wave"
        begin
          print "Executing wave restart...\n"
          find_and_execute_task("acquity:hybris:util:wave_restart")
        rescue NoSuchTaskError
          print "Wave restart requires FULL license. Falling back to regular restart.\n"
          hybris.deploy.restart_hybris
        end
      else
        print "Executing regular restart...\n"
        hybris.deploy.restart_hybris
      end
    end
  
    task :detect_java_home, :roles => :app do
      if remote_file_exists?("/usr/applications/java/current") then
        java_home = "/usr/applications/java/current"
      elsif remote_file_exists?("/usr/java/latest") then
        java_home = "/usr/java/latest"
      else
       if fetch(:os_system, "linux") == "windows"
        java_home = "C:\\Program Files\\Java\\#{jdk_version}"
       else
        java_home = "/usr/applications/java/#{jdk_version}"
       end
      end
      set :java_home, "#{java_home}"
      set :java_home_cmd, "export JAVA_HOME='#{java_home}'"
    end
    
  end
end