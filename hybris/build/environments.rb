# esc "deploy to master environment"
# task :master do
#   set :stage_name, "master"
  
# end



desc "deploy to dev environment"
task :sradev do
  set :stage_name, "sradev"
  #set :site_name, "training.gsa.acq"
  set :user, "sra-dev"
  set :deploy_to, "/usr/applications/ag-commerce-soriana-dev"
  #set :gateway, "174.143.102.138"
  role :app, "10.57.169.158"
  role :web, "10.57.169.158"
end



desc "deploy to qa environment"
task :sraqa do
  set :stage_name, "sraqa"
  #set :site_name, "qa.gsa.acq"
  set :user, "sra-qa"
  #set :use_vpn, true
  #set :vpn_datacenter, "iad3"
  set :deploy_to, "/usr/applications/ag-commerce-soriana-qa"
  #set :gateway, "174.143.102.138"
  role :app, "10.57.169.158"
  role :app, "10.57.169.157"
  role :web, "10.57.169.158"
  role :web, "10.57.169.157"
end


