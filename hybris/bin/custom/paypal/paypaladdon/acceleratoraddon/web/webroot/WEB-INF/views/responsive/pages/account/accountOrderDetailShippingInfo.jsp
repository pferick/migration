<%@ page trimDirectiveWhitespaces="true" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="order" tagdir="/WEB-INF/tags/responsive/order" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="order-payment-method" tagdir="/WEB-INF/tags/addons/paypaladdon/responsive/order" %>
<spring:eval expression="@configurationService.configuration.getProperty('paypal.requireBillingAddress')" var="isRequireBillingAddress"/>

<div class="account-orderdetail">
	<div class="account-orderdetail-item-section-header  item-box">
		<ycommerce:testId code="orderDetails_paymentDetails_section">
			<c:if test="${(not empty orderData.paymentInfo.billingAddress) && (isRequireBillingAddress)}">
				<div class="col-md-5 order-billing-address">
					<order:billingAddressItem order="${orderData}"/>
				</div>
			</c:if>	
			<c:if test="${not empty orderData.paymentInfo}">
				<div class="col-md-6 order-payment-data">
					<order-payment-method:paymentDetailsItem order="${orderData}"/>
				</div>
			</c:if>
		</ycommerce:testId>
	</div>
</div>