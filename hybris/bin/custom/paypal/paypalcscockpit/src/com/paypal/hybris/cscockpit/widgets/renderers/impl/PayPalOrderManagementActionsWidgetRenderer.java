/**
 *
 */
package com.paypal.hybris.cscockpit.widgets.renderers.impl;

import de.hybris.platform.cockpit.widgets.Widget;
import de.hybris.platform.cockpit.widgets.models.impl.DefaultItemWidgetModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.cscockpit.utils.LabelUtils;
import de.hybris.platform.cscockpit.widgets.controllers.OrderManagementActionsWidgetController;
import de.hybris.platform.cscockpit.widgets.renderers.impl.OrderManagementActionsWidgetRenderer;

import org.zkoss.zk.ui.api.HtmlBasedComponent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zul.Div;

import com.paypal.hybris.constants.PaypalConstants;
import com.paypal.hybris.cscockpit.widgets.controllers.PayPalOrderManagementActionsWidgetController;


public class PayPalOrderManagementActionsWidgetRenderer extends OrderManagementActionsWidgetRenderer
{

	@Override
	protected HtmlBasedComponent createContentInternal(
			final Widget<DefaultItemWidgetModel, OrderManagementActionsWidgetController> widget,
			final HtmlBasedComponent rootContainer)
	{
		final Div component = new Div();
		component.setSclass("orderManagementActionsWidget");

		createButton(widget, component, "cancelWholeOrder", "csFullOrderCancellationWidgetConfig", "csFullOrderCancel-Popup",
				"csFullCancelPopup", "popup.fullCancellationRequestCreate", !widget.getWidgetController().isFullCancelPossible());
		createButton(widget, component, "cancelPartialOrder", "csPartialOrderCancellationWidgetConfig",
				"csPartialOrderCancellationWidgetConfig-Popup", "csPartialCancelPopup", "popup.partialCancellationRequestCreate",
				!isPartialCancelPossible(widget));
		createButton(widget, component, "refundOrder", "csRefundRequestCreateWidgetConfig", "csRefundRequestCreateWidget-Popup",
				"csReturnRequestCreateWidget", "popup.refundRequestCreate", !widget.getWidgetController().isRefundPossible());
		createButton(widget, component, "replaceOrder", "csReplacementRequestCreateWidgetConfig",
				"csReplacementRequestCreateWidget-Popup", "csReturnRequestCreateWidget", "popup.replacementRequestCreate",
				!widget.getWidgetController().isReplacePossible());

		// PayPal extension
		if (widget.getWidgetController() instanceof PayPalOrderManagementActionsWidgetController)
		{
			createButton(widget, component, "reauthorizeOrder", "csReauthorizeRequestCreateWidgetConfig",
					"csReauthorizeRequestCreateWidget-Popup", "csReauthorizeRequestCreateWidget", "popup.reauthorizeRequestCreate",
					!((PayPalOrderManagementActionsWidgetController) widget.getWidgetController()).isReauthorizationPossible());

		}
		else
		{
			createButton(widget, component, "reauthorizeOrder", "csReauthorizeRequestCreateWidgetConfig",
					"csReauthorizeRequestCreateWidget-Popup", "csReauthorizeRequestCreateWidget", "popup.reauthorizeRequestCreate",
					true);
		}

		createButton(widget, component, "multipleCapture", "csMultipleCaptureWidgetConfig", "csMultipleCaptureWidget-Popup",
				"csMultipleCaptureWidget", "popup.multipleCapture",
				!((PayPalOrderManagementActionsWidgetController) widget.getWidgetController()).isMultiCapturingPossible());

		createButton(widget, component, "partialRefund", "csPartialRefundWidgetConfig", "csPartialRefundWidget-Popup",
				"csPartialRefundWidget", "popup.partialRefund",
				!((PayPalOrderManagementActionsWidgetController) widget.getWidgetController()).isPartialRefundPossible());

		return component;
	}

	@Override
	protected void handleButtonClickEvent(final Widget<DefaultItemWidgetModel, OrderManagementActionsWidgetController> widget,
			final Event event, final Div container, final String springWidgetName, final String popupCode, final String cssClass,
			final String popupTitleLabelName)
	{
		if ("csMultipleCaptureWidget".equals(cssClass) || "csPartialRefundWidget".equals(cssClass))
		{
			getPopupWidgetHelper().createPopupWidget(container, springWidgetName, popupCode, cssClass,
					LabelUtils.getLabel(widget, popupTitleLabelName, new Object[0]), 400);
		}
		else
		{
			super.handleButtonClickEvent(widget, event, container, springWidgetName, popupCode, cssClass, popupTitleLabelName);
		}

	}

	private boolean isPartialCancelPossible(final Widget<DefaultItemWidgetModel, OrderManagementActionsWidgetController> widget)
	{
		final OrderManagementActionsWidgetController widgetController = widget.getWidgetController();
		if (widgetController.getOrder().getObject() instanceof OrderModel)
		{
			final OrderModel orderModel = (OrderModel) widgetController.getOrder().getObject();
			if (orderModel != null && orderModel.getPaymentInfo() != null)
			{
				final String paymentCode = orderModel.getPaymentInfo().getCode();
				return !PaypalConstants.PAYPAL_PAYMENT_INFO_CODE.equals(paymentCode)
						&& !PaypalConstants.PAYPAL_CREDIT_PAYMENT_INFO_CODE.equals(paymentCode)
						&& widgetController.isPartialCancelPossible();
			}
		}
		return widget.getWidgetController().isPartialCancelPossible();
	}
}
