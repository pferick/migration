package com.paypal.hybris.cscockpit.widgets.controllers;

import de.hybris.platform.cscockpit.widgets.controllers.OrderManagementActionsWidgetController;


public interface PayPalOrderManagementActionsWidgetController extends OrderManagementActionsWidgetController
{

	boolean isReauthorizationPossible();

	boolean isMultiCapturingPossible();

	boolean isPartialRefundPossible();

}
