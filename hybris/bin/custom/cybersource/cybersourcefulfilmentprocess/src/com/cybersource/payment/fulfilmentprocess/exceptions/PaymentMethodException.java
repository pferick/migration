package com.cybersource.payment.fulfilmentprocess.exceptions;

public class PaymentMethodException extends RuntimeException
{

	public PaymentMethodException(final String message)
	{
		super(message);
	}

}
