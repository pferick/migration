/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.cybersource.payment.fulfilmentprocess.actions.returns;

import de.hybris.platform.basecommerce.enums.ReturnStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.returns.model.ReturnProcessModel;
import de.hybris.platform.returns.model.ReturnRequestModel;

import java.math.BigDecimal;
import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cybersource.payment.constants.CybersourcePaymentConstants;
import com.cybersource.payment.model.CybsPaymentTransactionEntryModel;
import com.cybersource.payment.model.CybsPaymentTransactionModel;
import com.cybersource.payment.service.executor.PaymentServiceExecutor;
import com.cybersource.payment.service.executor.PaymentServiceResult;
import com.cybersource.payment.service.executor.request.PaymentServiceRequest;
import com.cybersource.payment.service.executor.request.builder.creditcard.RefundFollowOnRequestBuilder;


/**
 * Mock implementation for refunding the money to the customer for the ReturnRequest
 */
public class CaptureRefundAction extends AbstractSimpleDecisionAction<ReturnProcessModel>
{
	private static final Logger LOG = LoggerFactory.getLogger(CaptureRefundAction.class);

	@Resource
	private PaymentServiceExecutor paymentServiceExecutor;

	@Override
	public Transition executeAction(final ReturnProcessModel process)
	{
		LOG.info("Process: " + process.getCode() + " in step " + getClass().getSimpleName());

		final ReturnRequestModel returnRequest = process.getReturnRequest();
		final OrderModel order = returnRequest.getOrder();

		for (final PaymentTransactionModel transaction : order.getPaymentTransactions())
		{
			final PaymentTransactionEntryModel refund = refund(order, (CybsPaymentTransactionModel) transaction);

			if (CybersourcePaymentConstants.TransactionStatus.ACCEPT.equals(refund.getTransactionStatus()))
			{
				LOG.debug("The payment has been refunded, order: {}, txn: {}", order.getCode(), refund.getCode());
				setReturnRequestStatus(returnRequest, ReturnStatus.PAYMENT_REVERSED);
			}
			else
			{
				LOG.error("The payment refund has failed, order: {}, txn: {}", order.getCode(), refund.getCode());
				setReturnRequestStatus(returnRequest, ReturnStatus.PAYMENT_REVERSAL_FAILED);
				return Transition.NOK;
			}
		}

		return Transition.OK;
	}

	private CybsPaymentTransactionEntryModel refund(final OrderModel order, final CybsPaymentTransactionModel transaction)
	{
		final PaymentServiceRequest refundRequest = new RefundFollowOnRequestBuilder()
				.setMerchantId(transaction.getMerchantId())
				.setOrder(order)
				.setAmount(BigDecimal.valueOf(order.getTotalPrice()))
				.setTransaction(transaction)
				.build();

		final PaymentServiceResult result = paymentServiceExecutor.execute(refundRequest);

		return result.getTransaction();
	}

	private void setReturnRequestStatus(final ReturnRequestModel returnRequest, final ReturnStatus returnStatus)
	{
		returnRequest.setStatus(returnStatus);
		returnRequest.getReturnEntries().stream().forEach(entry -> {
			entry.setStatus(returnStatus);
			getModelService().save(entry);
		});
		getModelService().save(returnRequest);
	}
}
