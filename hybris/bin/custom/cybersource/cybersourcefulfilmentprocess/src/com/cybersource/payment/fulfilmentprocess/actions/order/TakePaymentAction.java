package com.cybersource.payment.fulfilmentprocess.actions.order;

import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cybersource.payment.constants.CybersourcePaymentConstants;
import com.cybersource.payment.model.CybsPaymentTransactionModel;
import com.cybersource.payment.service.executor.PaymentServiceExecutor;
import com.cybersource.payment.service.executor.PaymentServiceResult;
import com.cybersource.payment.service.executor.request.PaymentServiceRequest;
import com.cybersource.payment.service.executor.request.builder.creditcard.CaptureRequestBuilder;


/**
 * The TakePayment step captures the payment transaction.
 */
public class TakePaymentAction extends AbstractSimpleDecisionAction<OrderProcessModel>
{
	private static final Logger LOG = LoggerFactory.getLogger(TakePaymentAction.class);

	@Resource
	private PaymentServiceExecutor paymentServiceExecutor;

	@Override
	public Transition executeAction(final OrderProcessModel process)
	{
		final OrderModel order = process.getOrder();

		for (final PaymentTransactionModel transaction : order.getPaymentTransactions())
		{
			final PaymentTransactionEntryModel capture = capture(order, (CybsPaymentTransactionModel) transaction);

			if (CybersourcePaymentConstants.TransactionStatus.ACCEPT.equals(capture.getTransactionStatus()))
			{
				LOG.debug("The payment transaction has been captured, order: {}, txn: {}", order.getCode(), capture.getCode());
				setOrderStatus(order, OrderStatus.PAYMENT_CAPTURED);
			}
			else
			{
				LOG.error("The payment transaction capture has failed, order: {}, txn: {}", order.getCode(), capture.getCode());
				setOrderStatus(order, OrderStatus.PAYMENT_NOT_CAPTURED);
				return Transition.NOK;
			}
		}

		return Transition.OK;
	}

	private PaymentTransactionEntryModel capture(final OrderModel order, final CybsPaymentTransactionModel transaction)
	{
		final PaymentServiceRequest captureRequest = new CaptureRequestBuilder()
                .setMerchantId(transaction.getMerchantId())
                .setOrder(order)
				.setTransaction(transaction)
				.build();

		final PaymentServiceResult captureResult = paymentServiceExecutor.execute(captureRequest);

		return captureResult.getTransaction();
	}
}
