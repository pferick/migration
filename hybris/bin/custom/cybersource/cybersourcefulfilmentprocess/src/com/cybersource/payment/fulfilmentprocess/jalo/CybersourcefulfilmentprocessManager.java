package com.cybersource.payment.fulfilmentprocess.jalo;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import com.cybersource.payment.fulfilmentprocess.constants.CybersourcefulfilmentprocessConstants;

@SuppressWarnings("PMD")
public class CybersourcefulfilmentprocessManager extends GeneratedCybersourcefulfilmentprocessManager
{
	public static final CybersourcefulfilmentprocessManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (CybersourcefulfilmentprocessManager) em.getExtension(CybersourcefulfilmentprocessConstants.EXTENSIONNAME);
	}
	
}
