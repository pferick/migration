package com.cybersource.payment.fulfilmentprocess.constants;

public final class CybersourcefulfilmentprocessConstants extends GeneratedCybersourcefulfilmentprocessConstants
{
	public static final String CONSIGNMENT_SUBPROCESS_END_EVENT_NAME = "ConsignmentSubprocessEnd";
	public static final String ORDER_PROCESS_NAME = "order-process";
	public static final String CONSIGNMENT_SUBPROCESS_NAME = "consignment-process";
	public static final String WAIT_FOR_WAREHOUSE = "WaitForWarehouse";
	public static final String CONSIGNMENT_PICKUP = "ConsignmentPickup";
	public static final String CONSIGNMENT_COUNTER = "CONSIGNMENT_COUNTER";
	public static final String PARENT_PROCESS = "PARENT_PROCESS";
}
