package com.cybersource.payment.fulfilmentprocess.actions.order

import com.cybersource.payment.constants.CybersourcePaymentConstants
import com.cybersource.payment.model.CybsPaymentTransactionEntryModel
import com.cybersource.payment.model.CybsPaymentTransactionModel
import com.cybersource.payment.service.executor.PaymentServiceExecutor
import com.cybersource.payment.service.executor.PaymentServiceResult
import com.cybersource.payment.service.executor.request.PaymentServiceRequest
import de.hybris.bootstrap.annotations.UnitTest
import de.hybris.platform.core.enums.OrderStatus
import de.hybris.platform.core.model.order.OrderModel
import de.hybris.platform.orderprocessing.model.OrderProcessModel
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction
import de.hybris.platform.servicelayer.model.ModelService
import org.junit.Test
import spock.lang.Specification

@UnitTest
class TakePaymentActionSpec extends Specification
{
    def process = Mock([useObjenesis: false], OrderProcessModel)

    def order = Mock([useObjenesis: false], OrderModel)

    def transaction1 = Mock([useObjenesis: false], CybsPaymentTransactionModel)

    def transaction2 = Mock([useObjenesis: false], CybsPaymentTransactionModel)

    def captureTransactionEntry = Mock([useObjenesis: false], CybsPaymentTransactionEntryModel)

    def captureResult = new PaymentServiceResult()

    def paymentServiceExecutor = Mock([useObjenesis: false], PaymentServiceExecutor)

    def modelService = Mock([useObjenesis: false], ModelService)

    def action = new TakePaymentAction()

    def setup()
    {
        action.paymentServiceExecutor = paymentServiceExecutor
        action.modelService = modelService

        process.order >> order
        order.paymentTransactions >> [transaction1, transaction2]

        paymentServiceExecutor.execute(_ as PaymentServiceRequest) >> captureResult
        captureResult.addTransaction(captureTransactionEntry)
    }

    @Test
    def 'should return OK transition and capture payment'()
    {
        given:
        captureTransactionEntry.transactionStatus >> CybersourcePaymentConstants.TransactionStatus.ACCEPT

        when:
        def transition = action.executeAction(process)

        then:
        _ * order.setStatus(OrderStatus.PAYMENT_CAPTURED)
        _ * modelService.save(order)
        transition == AbstractSimpleDecisionAction.Transition.OK
    }

    @Test
    def 'should return NOK transition if payment cannot be captured'()
    {
        given:
        captureTransactionEntry.transactionStatus >> CybersourcePaymentConstants.TransactionStatus.REJECT

        when:
        def transition = action.executeAction(process)

        then:
        _ * order.setStatus(OrderStatus.PAYMENT_NOT_CAPTURED)
        _ * modelService.save(order)
        transition == AbstractSimpleDecisionAction.Transition.NOK
    }
}
