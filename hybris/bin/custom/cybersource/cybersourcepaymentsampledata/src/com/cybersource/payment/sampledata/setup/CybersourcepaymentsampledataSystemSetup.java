package com.cybersource.payment.sampledata.setup;

import com.cybersource.payment.sampledata.constants.CybersourcepaymentsampledataConstants;
import de.hybris.platform.commerceservices.setup.SetupImpexService;
import de.hybris.platform.core.initialization.SystemSetup;
import org.springframework.beans.factory.annotation.Autowired;

@SystemSetup(extension = CybersourcepaymentsampledataConstants.EXTENSIONNAME)
public class CybersourcepaymentsampledataSystemSetup
{

    public static final String DATA_IMPORT_FOLDER = "/cybersourcepaymentsampledata/import/";

    @Autowired
    private SetupImpexService setupImpexService;

    public CybersourcepaymentsampledataSystemSetup()
    {
        // EMPTY
    }

    @SystemSetup(process = SystemSetup.Process.ALL, type = SystemSetup.Type.PROJECT)
    public void createEssentialData()
    {
        setupImpexService.importImpexFile(DATA_IMPORT_FOLDER + "merchant_payment_configuration.impex", true);
        setupImpexService.importImpexFile(DATA_IMPORT_FOLDER + "cybersource_reporting_triggers.impex", true);
    }
}
