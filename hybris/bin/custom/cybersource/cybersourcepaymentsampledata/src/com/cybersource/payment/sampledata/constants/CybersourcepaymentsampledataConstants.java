package com.cybersource.payment.sampledata.constants;

public final class CybersourcepaymentsampledataConstants extends GeneratedCybersourcepaymentsampledataConstants
{
    public static final String EXTENSIONNAME = "cybersourcepaymentsampledata";

    private CybersourcepaymentsampledataConstants()
    {
        // EMPTY
    }

    public static final String PLATFORM_LOGO_CODE = "cybersourcepaymentsampledataPlatformLogo";
}
