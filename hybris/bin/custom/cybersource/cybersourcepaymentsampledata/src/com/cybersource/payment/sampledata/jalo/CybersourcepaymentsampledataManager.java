package com.cybersource.payment.sampledata.jalo;

import com.cybersource.payment.sampledata.constants.CybersourcepaymentsampledataConstants;
import de.hybris.platform.core.Registry;
import de.hybris.platform.util.JspContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

public class CybersourcepaymentsampledataManager extends GeneratedCybersourcepaymentsampledataManager
{
	private static final Logger LOG = LoggerFactory.getLogger(CybersourcepaymentsampledataManager.class);

	public static CybersourcepaymentsampledataManager getInstance()
	{
		return (CybersourcepaymentsampledataManager) Registry.getCurrentTenant().getJaloConnection().getExtensionManager()
				.getExtension(CybersourcepaymentsampledataConstants.EXTENSIONNAME);
	}

	public CybersourcepaymentsampledataManager() // NOPMD
	{
		LOG.debug("constructor of CybersourcepaymentsampledataManager called.");
	}

	@Override
	public void init()
	{
		LOG.debug("init() of CybersourcepaymentsampledataManager called, current tenant: {}", getTenant().getTenantID());
	}

	@Override
	public void destroy()
	{
		LOG.debug("destroy() of CybersourcepaymentsampledataManager called, current tenant: {}", getTenant().getTenantID());
	}

	@Override
	public void createEssentialData(final Map<String, String> params, final JspContext jspc)
	{
		// implement here code creating essential data
	}

	@Override
	public void createProjectData(final Map<String, String> params, final JspContext jspc)
	{
		// implement here code creating project data
	}
}
