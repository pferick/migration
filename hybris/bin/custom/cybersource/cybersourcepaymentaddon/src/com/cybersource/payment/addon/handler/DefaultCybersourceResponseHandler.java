package com.cybersource.payment.addon.handler;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.payment.enums.PaymentTransactionType;

import java.util.Map;
import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cybersource.payment.constants.CybersourcePaymentConstants;
import com.cybersource.payment.enums.CybsPaymentSource;
import com.cybersource.payment.enums.CybsPaymentType;
import com.cybersource.payment.model.CybsMerchantModel;
import com.cybersource.payment.model.CybsMerchantProfileModel;
import com.cybersource.payment.security.service.CybersourceSAService;
import com.cybersource.payment.service.MerchantService;
import com.cybersource.payment.service.executor.PaymentServiceExecutor;
import com.cybersource.payment.service.executor.request.PaymentServiceRequest;

public class DefaultCybersourceResponseHandler implements CybersourceResponseHandler
{
    private static final Logger LOG = LoggerFactory.getLogger(DefaultCybersourceResponseHandler.class);

    @Resource
    private CybersourceSAService cybersourceSAService;

    @Resource
    private MerchantService merchantService;

    @Resource
    private PaymentServiceExecutor paymentServiceExecutor;

    public boolean isResponseSuccessful(final Map<String, String> paymentResponse)
    {
        return isDecisionSuccessful(paymentResponse) && isValidSignature(paymentResponse);
    }

    @Override
    public void processResponse(final AbstractOrderModel order, final Map<String, String> paymentResponse)
    {
        final String merchantId = paymentResponse.get(CybersourcePaymentConstants.SAResponseFields.MERCHANT_ID);

        final PaymentServiceRequest request = PaymentServiceRequest.create()
                .source(CybsPaymentSource.SECURE_ACCEPTANCE)
                .method(CybsPaymentType.CREDIT_CARD)
                .service(PaymentTransactionType.AUTHORIZATION)
                .addParam("merchantId", merchantId)
                .addParam("order", order)
                .addParam("paymentResponse", paymentResponse);

        paymentServiceExecutor.execute(request);
    }

    private boolean isDecisionSuccessful(final Map<String, String> paymentResponse)
    {
        final String cybersourceDecision = paymentResponse.get(CybersourcePaymentConstants.SAResponseFields.DECISION);
        final String orderNumber = paymentResponse.get(CybersourcePaymentConstants.SAResponseFields.REFERENCE_NUMBER);
        final String reasonCode = paymentResponse.get(CybersourcePaymentConstants.SAResponseFields.REASON_CODE);

        LOG.info("Secure Acceptance receipt page, decision is: {} for order : {}", cybersourceDecision, orderNumber);

        return CybersourcePaymentConstants.TransactionStatus.ACCEPT.equals(cybersourceDecision)
                || CybersourcePaymentConstants.TransactionStatus.REVIEW.equals(cybersourceDecision)
                || isAuthorizationDuplicateTransaction(reasonCode, cybersourceDecision);
    }

    private boolean isAuthorizationDuplicateTransaction(final String reasonCode, final String cybersourceDecision)
    {
        return CybersourcePaymentConstants.ReasonCode.DUPLICATE_TRANSACTION.equals(reasonCode)
                && CybersourcePaymentConstants.TransactionStatus.ERROR.equals(cybersourceDecision);
    }

    private boolean isValidSignature(final Map<String, String> paymentResponse)
    {
        final String merchantId = paymentResponse.get(CybersourcePaymentConstants.SAResponseFields.MERCHANT_ID);
        final String profileType = paymentResponse.get(CybersourcePaymentConstants.SAResponseFields.PROFILE_TYPE);

        final CybsMerchantModel merchant = merchantService.getMerchant(merchantId);
        final CybsMerchantProfileModel merchantProfile = merchantService.getMerchantProfile(merchant, profileType);

        if (!cybersourceSAService.validateTransactionSignature(paymentResponse, merchantProfile.getSecretKey()))
        {
            LOG.warn("The signature for the cybersource payment response is not valid");
            return false;
        }

        return true;
    }
}
