package com.cybersource.payment.addon.utils;

import com.google.common.base.Joiner;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public final class ListUtils
{
    public static String toString(final Collection<String> values, final Collection<String> excludedValues)
    {
        final List<String> fieldsToSign = values.stream()
                .filter(e -> !excludedValues.contains(e))
                .collect(Collectors.toList());

        return ListUtils.toString(fieldsToSign);
    }

    public static String toString(final Collection<String> values)
    {
        return Joiner.on(",").join(values);
    }
}
