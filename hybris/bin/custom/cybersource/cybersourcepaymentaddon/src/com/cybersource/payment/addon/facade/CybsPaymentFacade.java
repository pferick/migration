package com.cybersource.payment.addon.facade;

import de.hybris.platform.acceleratorfacades.payment.PaymentFacade;
import de.hybris.platform.acceleratorfacades.payment.data.PaymentSubscriptionResultData;
import de.hybris.platform.acceleratorservices.payment.data.PaymentData;

import java.util.Map;

public interface CybsPaymentFacade extends PaymentFacade
{
    PaymentData beginCreateCybsPayment(final String responseUrl);

    PaymentSubscriptionResultData completeCreateCybsPayment(Map<String, String> parameters, boolean saveInAccount);
}
