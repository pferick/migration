package com.cybersource.payment.addon.handler;

import de.hybris.platform.core.model.order.AbstractOrderModel;

import java.util.Map;

/**
 * Interface defines various methods to check on cybersource response.
 */
public interface CybersourceResponseHandler
{
    /**
     * Checks if cybersource response is successful (e.g. has a valid signature, successful status, etc.).
     *
     * @param paymentResponse a map containing cybersource payment response parameters
     * @return true if cybersource response is considered successful
     */
    boolean isResponseSuccessful(final Map<String, String> paymentResponse);

    /**
     * Processes cybersource response and creates order payment transactions.
     *
     * @param order the order
     * @param paymentResponse the payment response data
     */
    void processResponse(final AbstractOrderModel order, final Map<String, String> paymentResponse);
}
