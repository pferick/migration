package com.cybersource.payment.addon.facade;

import com.cybersource.payment.model.CybsPaymentInfoModel;
import de.hybris.platform.acceleratorfacades.payment.data.PaymentSubscriptionResultData;
import de.hybris.platform.acceleratorfacades.payment.impl.DefaultPaymentFacade;
import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionRequest;
import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionResult;
import de.hybris.platform.acceleratorservices.payment.data.CustomerInfoData;
import de.hybris.platform.acceleratorservices.payment.data.PaymentData;
import de.hybris.platform.acceleratorservices.payment.data.PaymentErrorField;
import de.hybris.platform.acceleratorservices.payment.data.PaymentSubscriptionResultItem;
import de.hybris.platform.acceleratorservices.payment.strategies.ClientReferenceLookupStrategy;
import de.hybris.platform.acceleratorservices.payment.strategies.CreateSubscriptionRequestStrategy;
import de.hybris.platform.acceleratorservices.payment.strategies.CreateSubscriptionResultValidationStrategy;
import de.hybris.platform.acceleratorservices.payment.strategies.PaymentResponseInterpretationStrategy;
import de.hybris.platform.commerceservices.customer.CustomerEmailResolutionService;
import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

public class CybsPaymentFacadeImpl extends DefaultPaymentFacade implements CybsPaymentFacade
{
    private static final Logger LOG = Logger.getLogger(CybsPaymentFacadeImpl.class);

    @Resource
    private CreateSubscriptionRequestStrategy createSubscriptionRequestStrategy;

    @Resource
    private Converter<CreateSubscriptionRequest, PaymentData> paymentDataConverter;

    @Resource
    private PaymentResponseInterpretationStrategy paymentResponseInterpretationStrategy;

    @Resource
    private ClientReferenceLookupStrategy clientReferenceLookupStrategy;

    @Resource
    private CreateSubscriptionResultValidationStrategy createSubscriptionResultValidationStrategy;

    @Resource
    private ModelService modelService;

    @Resource
    private CommonI18NService commonI18NService;

    @Resource
    private CartService cartService;

    @Resource
    private CustomerEmailResolutionService customerEmailResolutionService;

    @Override
    public PaymentData beginCreateCybsPayment(final String responseUrl)
    {
        final String fullResponseUrl = getFullResponseUrl(responseUrl, true);
        final String siteName = getCurrentSiteName();

        final CustomerModel customerModel = getCurrentUserForCheckout();
        final AddressModel paymentAddress = getDefaultPaymentAddress(customerModel);

        final CreateSubscriptionRequest request = createSubscriptionRequestStrategy.createSubscriptionRequest(
                siteName, fullResponseUrl, fullResponseUrl, null, customerModel, null, paymentAddress);

        final PaymentData data = paymentDataConverter.convert(request);

        return  Optional.ofNullable(data)
                .orElse(new PaymentData());
    }

    @Override
    public PaymentSubscriptionResultData completeCreateCybsPayment(final Map<String, String> parameters,
            final boolean saveInAccount)
    {
        final CustomerModel customerModel = getCurrentUserForCheckout();
        final PaymentSubscriptionResultItem paymentSubscription = completeCreateCybsPayment(
                customerModel, saveInAccount, parameters);

        if (paymentSubscription != null)
        {
            final CartModel cart = cartService.getSessionCart();
            cart.setPaymentInfo(paymentSubscription.getStoredPayment());
            modelService.save(cart);

            return getPaymentSubscriptionResultDataConverter().convert(paymentSubscription);
        }

        return null;
    }

    private PaymentSubscriptionResultItem completeCreateCybsPayment(final CustomerModel customerModel,
            final boolean saveInAccount, final Map<String, String> parameters)
    {
        final PaymentSubscriptionResultItem paymentSubscription = new PaymentSubscriptionResultItem();
        final Map<String, PaymentErrorField> errors = new HashMap<String, PaymentErrorField>();
        paymentSubscription.setErrors(errors);

        final CreateSubscriptionResult response = paymentResponseInterpretationStrategy.interpretResponse(parameters,
                clientReferenceLookupStrategy.lookupClientReferenceId(), errors);

        validateParameterNotNull(response, "Response is required");

        if (!createSubscriptionResultValidationStrategy.validateCreateSubscriptionResult(errors, response).isEmpty())
        {
            return paymentSubscription;
        }

        paymentSubscription.setSuccess(Boolean.TRUE);

        final PaymentInfoModel paymentInfoModel = savePaymentSubscription(customerModel, response.getCustomerInfoData(),
                saveInAccount);

        paymentSubscription.setStoredPayment(paymentInfoModel);
        modelService.save(paymentInfoModel);

        return paymentSubscription;
    }

    private PaymentInfoModel savePaymentSubscription(final CustomerModel customerModel, final CustomerInfoData customerInfoData,
            final boolean saveInAccount)
    {
        validateParameterNotNull(customerInfoData, "customerInfoData cannot be null");

        final AddressModel billingAddress = modelService.create(AddressModel.class);
        billingAddress.setFirstname(customerInfoData.getBillToFirstName());
        billingAddress.setLastname(customerInfoData.getBillToLastName());
        billingAddress.setLine1(customerInfoData.getBillToStreet1());
        billingAddress.setLine2(customerInfoData.getBillToStreet2());
        billingAddress.setTown(customerInfoData.getBillToCity());
        billingAddress.setPostalcode(customerInfoData.getBillToPostalCode());

        if (StringUtils.isNotBlank(customerInfoData.getBillToTitleCode()))
        {
            billingAddress.setTitle(getUserService().getTitleForCode(customerInfoData.getBillToTitleCode()));
        }

        final CountryModel country = commonI18NService.getCountry(customerInfoData.getBillToCountry());
        billingAddress.setCountry(country);
        if (StringUtils.isNotEmpty(customerInfoData.getBillToState()))
        {
            billingAddress.setRegion(commonI18NService.getRegion(country,
                    country.getIsocode() + "-" + customerInfoData.getBillToState()));
        }
        final String email = customerEmailResolutionService.getEmailForCustomer(customerModel);
        billingAddress.setEmail(email);

        final PaymentInfoModel paymentInfoModel = createPaymentInfo(billingAddress, customerModel, saveInAccount);

        billingAddress.setOwner(paymentInfoModel);

        if (CustomerType.GUEST.equals(customerModel.getType()))
        {
            final StringBuilder name = new StringBuilder();
            if (!StringUtils.isBlank(customerInfoData.getBillToFirstName()))
            {
                name.append(customerInfoData.getBillToFirstName());
                name.append(' ');
            }
            if (!StringUtils.isBlank(customerInfoData.getBillToLastName()))
            {
                name.append(customerInfoData.getBillToLastName());
            }
            customerModel.setName(name.toString());
            modelService.save(customerModel);
        }

        modelService.saveAll(paymentInfoModel, billingAddress);
        modelService.refresh(customerModel);

        final List<PaymentInfoModel> paymentInfoModels = new ArrayList<PaymentInfoModel>(customerModel.getPaymentInfos());
        if (!paymentInfoModels.contains(paymentInfoModel))
        {
            paymentInfoModels.add(paymentInfoModel);
            if (saveInAccount)
            {
                customerModel.setPaymentInfos(paymentInfoModels);
                modelService.save(customerModel);
            }

            modelService.save(paymentInfoModel);
            modelService.refresh(customerModel);
        }

        return paymentInfoModel;
    }

    private CybsPaymentInfoModel createPaymentInfo(final AddressModel billingAddress, final CustomerModel customerModel,
            final boolean saveInAccount)
    {
        validateParameterNotNull(billingAddress, "billingAddress cannot be null");
        validateParameterNotNull(customerModel, "customerModel cannot be null");

        final CybsPaymentInfoModel paymentInfoModel = modelService.create(CybsPaymentInfoModel.class);
        paymentInfoModel.setBillingAddress(billingAddress);
        paymentInfoModel.setCode(customerModel.getUid() + "_" + UUID.randomUUID());
        paymentInfoModel.setUser(customerModel);
        paymentInfoModel.setSaved(saveInAccount);

        return paymentInfoModel;
    }
}
