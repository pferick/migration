package com.cybersource.payment.addon.constants;

public final class CybersourcePaymentAddonConstants extends GeneratedCybersourcePaymentAddonConstants {
    public static final String EXTENSIONNAME = "cybersourcepaymentaddon";

    private CybersourcePaymentAddonConstants() {
    }
}
