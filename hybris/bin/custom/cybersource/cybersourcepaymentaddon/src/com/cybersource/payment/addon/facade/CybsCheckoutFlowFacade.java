package com.cybersource.payment.addon.facade;

import de.hybris.platform.acceleratorfacades.flow.impl.SessionOverrideCheckoutFlowFacade;
import de.hybris.platform.core.model.order.CartModel;

public class CybsCheckoutFlowFacade extends SessionOverrideCheckoutFlowFacade
{
    @Override
    public boolean hasNoPaymentInfo()
    {
        final CartModel cart = getCart();

        return  cart.getPaymentInfo() == null;
    }
}
