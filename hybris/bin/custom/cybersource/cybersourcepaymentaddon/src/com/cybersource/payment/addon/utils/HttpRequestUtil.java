package com.cybersource.payment.addon.utils;

import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;

public class HttpRequestUtil
{
    public static Map<String, String> getParametersMap(final HttpServletRequest request)
    {
        final Map<String, String[]> params = request.getParameterMap();

        final Map<String, String> parameterMap = new HashMap<>(params.size());

        for (final Map.Entry<String, String[]> entry : params.entrySet())
        {
            final String[] value = entry.getValue();
            parameterMap.put(entry.getKey(), value[0]);
        }

        return parameterMap;
    }
}
