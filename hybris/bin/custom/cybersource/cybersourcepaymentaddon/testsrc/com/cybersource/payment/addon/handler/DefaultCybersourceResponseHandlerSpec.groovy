package com.cybersource.payment.addon.handler

import com.cybersource.payment.enums.CybsPaymentSource
import com.cybersource.payment.enums.CybsPaymentType
import com.cybersource.payment.model.CybsMerchantModel
import com.cybersource.payment.model.CybsMerchantProfileModel
import com.cybersource.payment.security.service.CybersourceSAService
import com.cybersource.payment.service.MerchantService
import com.cybersource.payment.service.executor.PaymentServiceExecutor
import com.cybersource.payment.service.executor.request.PaymentServiceRequest
import de.hybris.bootstrap.annotations.UnitTest
import de.hybris.platform.core.model.order.AbstractOrderModel
import de.hybris.platform.payment.enums.PaymentTransactionType
import org.junit.Test
import spock.lang.Specification

@UnitTest
class DefaultCybersourceResponseHandlerSpec extends Specification
{
    def merchant = Mock([useObjenesis: false], CybsMerchantModel)

    def merchantProfile = Mock([useObjenesis: false], CybsMerchantProfileModel)

    def order = Mock([useObjenesis: false], AbstractOrderModel)

    def cybersourceSAService = Mock([useObjenesis: false], CybersourceSAService)

    def merchantService =  Mock([useObjenesis: false], MerchantService)

    def paymentServiceExecutor = Mock([useObjenesis: false], PaymentServiceExecutor)

    def handler = new DefaultCybersourceResponseHandler()

    def setup()
    {
        handler.cybersourceSAService = cybersourceSAService
        handler.merchantService = merchantService
        handler.paymentServiceExecutor = paymentServiceExecutor

        merchantProfile.secretKey >> 'ABCD123'

        merchantService.getMerchant('merchant_1') >> merchant
        merchantService.getMerchantProfile(merchant, 'HOP') >> merchantProfile
    }

    @Test
    def 'should validate cybersource response by decision and signature'()
    {
        given:
        cybersourceSAService.validateTransactionSignature(_ as Map, _ as String) >> validSignature

        when:
        def result = handler.isResponseSuccessful([
                decision: decision, req_reference_number: '12345',
                req_merchant_defined_data99: 'merchant_1', req_merchant_defined_data100: 'HOP',
                reason_code: reason_code
        ])

        then:
        result == expected

        where:
        decision | reason_code | validSignature | expected
        'ACCEPT' | '100'       |true           | true
        'REVIEW' | '100'       |true           | true
        'ACCEPT' | '100'       |false          | false
        'REJECT' | '101'       |true           | false
        'REJECT' | '101'       |false          | false
        'ERROR'  | '101'       |false          | false
        'ERROR'  | '104'       |true           | true
    }

    @Test
    def 'should process cybersource reponse by executing sa authorization service'()
    {
        given:
        def paymentResponse = [decision: 'ACCEPT', req_merchant_defined_data99: 'merchant_1']

        when:
        handler.processResponse(order, paymentResponse)

        then:
        1 * paymentServiceExecutor.execute(_) >> { arguments ->
            def PaymentServiceRequest request = arguments[0]
            assert request.paymentSource == CybsPaymentSource.SECURE_ACCEPTANCE
            assert request.paymentType == CybsPaymentType.CREDIT_CARD
            assert request.paymentTransactionType == PaymentTransactionType.AUTHORIZATION
            assert request.requestParams.merchantId == 'merchant_1'
            assert request.requestParams.order == order
            assert request.requestParams.paymentResponse == paymentResponse as Map
        }
    }
}
