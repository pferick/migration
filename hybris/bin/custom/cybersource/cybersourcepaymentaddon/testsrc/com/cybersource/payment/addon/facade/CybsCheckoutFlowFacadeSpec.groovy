package com.cybersource.payment.addon.facade

import de.hybris.bootstrap.annotations.UnitTest
import de.hybris.platform.commercefacades.order.CartFacade
import de.hybris.platform.core.model.order.CartModel
import de.hybris.platform.core.model.order.payment.PaymentInfoModel
import de.hybris.platform.order.CartService
import org.junit.Test
import spock.lang.Specification

@UnitTest
class CybsCheckoutFlowFacadeSpec extends Specification
{
    def cartService = Mock([useObjenesis: false], CartService)

    def cartFacade = Mock([useObjenesis: false], CartFacade)

    def cart = Mock([useObjenesis: false], CartModel)

    def paymentInfo = Mock([useObjenesis: false], PaymentInfoModel)

    def facade = new CybsCheckoutFlowFacade()

    def setup()
    {
        facade.setCartService(cartService)
        facade.setCartFacade(cartFacade)

        cartFacade.hasSessionCart() >> true
        cartService.getSessionCart() >> cart
    }

    @Test
    def 'hasNoPayment should return false when cart has payment is setup'()
    {
        when:
        cart.getPaymentInfo() >> paymentInfo

        then:
        !facade.hasNoPaymentInfo()
    }

    @Test
    def 'hasNoPayment should return ture when cart has no payment setup'()
    {
        when:
        cart.getPaymentInfo() >> null

        then:
        facade.hasNoPaymentInfo()
    }
}
