package com.cybersource.payment.addon.controllers.utils

import com.cybersource.payment.addon.utils.HttpRequestUtil
import de.hybris.bootstrap.annotations.UnitTest
import org.junit.Test
import spock.lang.Specification

import javax.servlet.http.HttpServletRequest

@UnitTest
class HttpRequestUtilSpock extends Specification
{
    def request = Mock([useObjenesis: false], HttpServletRequest)

    @Test
    def 'should convert request parameters into a map'()
    {
        given:
        request.parameterMap >> [param1: ['value1'] as String[], param2: ['value2'] as String[]]

        when:
        def params = HttpRequestUtil.getParametersMap(request)

        then:
        params != null
        params.param1 == 'value1'
        params.param2 == 'value2'
    }
}
