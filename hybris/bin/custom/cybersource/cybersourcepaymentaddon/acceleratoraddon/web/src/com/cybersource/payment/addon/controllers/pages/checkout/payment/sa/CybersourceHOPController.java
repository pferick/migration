package com.cybersource.payment.addon.controllers.pages.checkout.payment.sa;

import static com.cybersource.payment.constants.CybersourcePaymentConstants.SARequestFields.ACCESS_KEY;
import static com.cybersource.payment.constants.CybersourcePaymentConstants.SARequestFields.AMOUNT;
import static com.cybersource.payment.constants.CybersourcePaymentConstants.SARequestFields.BILL_TO_ADDRESS_CITY;
import static com.cybersource.payment.constants.CybersourcePaymentConstants.SARequestFields.BILL_TO_ADDRESS_COUNTRY;
import static com.cybersource.payment.constants.CybersourcePaymentConstants.SARequestFields.BILL_TO_ADDRESS_LINE1;
import static com.cybersource.payment.constants.CybersourcePaymentConstants.SARequestFields.BILL_TO_ADDRESS_LINE2;
import static com.cybersource.payment.constants.CybersourcePaymentConstants.SARequestFields.BILL_TO_ADDRESS_POSTAL_CODE;
import static com.cybersource.payment.constants.CybersourcePaymentConstants.SARequestFields.BILL_TO_ADDRESS_STATE;
import static com.cybersource.payment.constants.CybersourcePaymentConstants.SARequestFields.BILL_TO_EMAIL;
import static com.cybersource.payment.constants.CybersourcePaymentConstants.SARequestFields.BILL_TO_FORENAME;
import static com.cybersource.payment.constants.CybersourcePaymentConstants.SARequestFields.BILL_TO_SURNAME;
import static com.cybersource.payment.constants.CybersourcePaymentConstants.SARequestFields.CURRENCY;
import static com.cybersource.payment.constants.CybersourcePaymentConstants.SARequestFields.DEVELOPER_ID;
import static com.cybersource.payment.constants.CybersourcePaymentConstants.SARequestFields.LOCALE;
import static com.cybersource.payment.constants.CybersourcePaymentConstants.SARequestFields.MERCHANT_DEFINED_DATA_100;
import static com.cybersource.payment.constants.CybersourcePaymentConstants.SARequestFields.MERCHANT_DEFINED_DATA_99;
import static com.cybersource.payment.constants.CybersourcePaymentConstants.SARequestFields.OVERRIDE_CUSTOM_MERCHANT_POST_PAGE;
import static com.cybersource.payment.constants.CybersourcePaymentConstants.SARequestFields.OVERRIDE_CUSTOM_RECEIPT_PAGE;
import static com.cybersource.payment.constants.CybersourcePaymentConstants.SARequestFields.PARTNER_SOLUTION_ID;
import static com.cybersource.payment.constants.CybersourcePaymentConstants.SARequestFields.PROFILE_ID;
import static com.cybersource.payment.constants.CybersourcePaymentConstants.SARequestFields.REFERENCE_NUMBER;
import static com.cybersource.payment.constants.CybersourcePaymentConstants.SARequestFields.SIGNED_DATE_TIME;
import static com.cybersource.payment.constants.CybersourcePaymentConstants.SARequestFields.TRANSACTION_TYPE;
import static com.cybersource.payment.constants.CybersourcePaymentConstants.SARequestFields.TRANSACTION_UUID;

import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.AddressModel;

import java.util.Date;
import java.util.Map;
import java.util.UUID;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.cybersource.payment.enums.CybsMerchantProfileType;
import com.cybersource.payment.model.CybsMerchantProfileModel;
import com.cybersource.payment.utils.TimeUtils;

@Controller
@RequestMapping(value = "/checkout/payment/sa")
public class CybersourceHOPController extends CybersourceSecureAcceptanceController
{
    @RequestMapping(value = "/hop", method = RequestMethod.POST)
    public String sendPaymentRequest(final Model model) throws Exception
    {
        model.addAttribute("cartGuid", getCartService().getSessionCart().getGuid());
        model.addAttribute("postUrl", getHopUrl());
        model.addAttribute("formFields", buildCybersourceRequest());

        return "addon:/cybersourcepaymentaddon/pages/checkout/payment/sa/hopRequest";
    }

    private Map<String, String> buildCybersourceRequest()
    {
        final CybsMerchantProfileModel profile = getMerchantService().getCurrentMerchantProfile(
                CybsMerchantProfileType.HOP);

        final CartModel cart = getCartService().getSessionCart();
        final AddressModel billingAddress = cart.getPaymentInfo().getBillingAddress();

        return new SecureAcceptanceRequestBuilder(signatureFunction(profile.getSecretKey()))
                .addSignedField(ACCESS_KEY, profile.getAccessKey())
                .addSignedField(PROFILE_ID, profile.getProfileId())
                .addSignedField(TRANSACTION_UUID, UUID.randomUUID().toString())
                .addSignedField(SIGNED_DATE_TIME, TimeUtils.toUTCDateTime(new Date()))
                .addSignedField(LOCALE, getI18NService().getCurrentLocale().toString())
                .addSignedField(TRANSACTION_TYPE, "authorization")
                .addSignedField(REFERENCE_NUMBER, cart.getGuid())
                .addSignedField(AMOUNT, cart.getTotalPrice().toString())
                .addSignedField(CURRENCY, cart.getCurrency().getIsocode())
                .addSignedField(BILL_TO_FORENAME, billingAddress.getFirstname())
                .addSignedField(BILL_TO_SURNAME, billingAddress.getLastname())
                .addSignedField(BILL_TO_EMAIL, billingAddress.getEmail())
                .addSignedField(BILL_TO_ADDRESS_COUNTRY, billingAddress.getCountry().getIsocode())
                .addSignedField(BILL_TO_ADDRESS_STATE, billingAddress.getRegion(), RegionModel::getIsocodeShort)
                .addSignedField(BILL_TO_ADDRESS_CITY, billingAddress.getTown())
                .addSignedField(BILL_TO_ADDRESS_LINE1, billingAddress.getStreetname())
                .addSignedField(BILL_TO_ADDRESS_LINE2, billingAddress.getStreetnumber())
                .addSignedField(BILL_TO_ADDRESS_POSTAL_CODE, billingAddress.getPostalcode())
                .addSignedField(OVERRIDE_CUSTOM_RECEIPT_PAGE, getReceiptUrl())
                .addSignedField(OVERRIDE_CUSTOM_MERCHANT_POST_PAGE, getMerchantPostUrl())
                .addUnsignedField(PARTNER_SOLUTION_ID, paymentSystemInfo.getPartnerSolutionID())
                .addUnsignedField(DEVELOPER_ID, paymentSystemInfo.getDeveloperID())
                .addUnsignedField(MERCHANT_DEFINED_DATA_99, profile.getMerchant().getId())
                .addUnsignedField(MERCHANT_DEFINED_DATA_100, CybsMerchantProfileType.HOP.getCode())
                .build();
    }

    private String getHopUrl()
    {
        return getConfigurationService().getConfiguration().getString("cybersource.secure.acceptance.hop.post.url");
    }
}
