package com.cybersource.payment.addon.controllers.pages.checkout.payment.sa;

import com.cybersource.payment.addon.utils.ListUtils;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;

import static com.cybersource.payment.constants.CybersourcePaymentConstants.SARequestFields.SIGNATURE;
import static com.cybersource.payment.constants.CybersourcePaymentConstants.SARequestFields.SIGNED_FIELD_NAMES;
import static com.cybersource.payment.constants.CybersourcePaymentConstants.SARequestFields.UNSIGNED_FIELD_NAMES;

class SecureAcceptanceRequestBuilder
{
    private Function<Map<String, String>, String> signingFunction;

    private Map<String, String> signedParams = Maps.newLinkedHashMap();

    private Map<String, String> unSignedParams = Maps.newLinkedHashMap();

    SecureAcceptanceRequestBuilder(final Function<Map<String, String>, String> signedFunction)
    {
        this.signingFunction = signedFunction;
    }

    SecureAcceptanceRequestBuilder addSignedField(final String fieldName, final String fieldValue)
    {
        signedParams.put(fieldName, fieldValue);
        return this;
    }

    SecureAcceptanceRequestBuilder addUnsignedField(final String fieldName, final String fieldValue)
    {
        unSignedParams.put(fieldName, fieldValue);
        return this;
    }

    <T> SecureAcceptanceRequestBuilder addSignedField(final String fieldName, final T object,
            final Function<T, String> function)
    {
        Optional.ofNullable(object)
                .map(function)
                .ifPresent(v -> signedParams.put(fieldName, v));

        return this;
    }

    Map<String, String> build()
    {
        final List<String> signedFields = ImmutableList.<String>builder()
                .addAll(signedParams.keySet())
                .add(UNSIGNED_FIELD_NAMES)
                .add(SIGNED_FIELD_NAMES)
                .build();

        final Map<String, String> mergedParamsMap = ImmutableMap.<String, String>builder()
                .putAll(signedParams)
                .putAll(unSignedParams)
                .put(SIGNED_FIELD_NAMES, ListUtils.toString(signedFields))
                .put(UNSIGNED_FIELD_NAMES, ListUtils.toString(unSignedParams.keySet()))
                .build();

        return ImmutableMap.<String, String>builder()
                .putAll(mergedParamsMap)
                .put(SIGNATURE, signingFunction.apply(mergedParamsMap))
                .build();
    }
}