package com.cybersource.payment.addon.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractCheckoutController;
import de.hybris.platform.commercefacades.order.OrderFacade;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import java.util.Map;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.cybersource.payment.addon.handler.CybersourceResponseHandler;
import com.cybersource.payment.addon.utils.HttpRequestUtil;
import com.cybersource.payment.commercefacades.order.PaymentCheckoutFacade;
import com.cybersource.payment.commerceservices.order.PaymentCartService;
import com.cybersource.payment.constants.CybersourcePaymentConstants;

@Controller
@RequestMapping("/checkout/payment/sa")
public class CybersourcePaymentController extends AbstractCheckoutController
{
    private static final Logger LOG = LoggerFactory.getLogger(CybersourcePaymentController.class);

    private static final String RESPONSE_ACTION = "responseAction";

    @Resource
    private PaymentCartService paymentCartService;

    @Resource
    private CybersourceResponseHandler cybersourceResponseHandler;

    @Resource
    private PaymentCheckoutFacade paymentCheckoutFacade;

    @Resource
    private OrderFacade orderFacade;

    @RequestMapping(value = "/receipt", method = RequestMethod.POST)
    public String handlerReceiptPost(final HttpServletRequest request, final Model model)
    {
        logResponseParams(request);

        model.addAttribute(RESPONSE_ACTION, "/checkout/multi/summary/view/payment/error");

        final String orderNumber = getReferenceNumber(request);
        LOG.info("Creating order [{}] from Cybersource Receipt POST", orderNumber);

        if (StringUtils.isNotEmpty(orderNumber))
        {
            final CartModel cart = getCart(orderNumber);
            if (cart != null)
            {
                paymentCartService.executeWithCartLock(cart, () -> {
                    final OrderData orderData = doHandlePlaceOrder(request, cart);
                    if (orderData != null)
                    {
                        model.addAttribute(RESPONSE_ACTION, "/checkout/orderConfirmation/"
                                + (getCheckoutCustomerStrategy().isAnonymousCheckout() ? orderData.getGuid() : orderData.getCode()));
                    }
                });
            }
        }

        return "addon:/cybersourcepaymentaddon/pages/checkout/payment/sa/response";
    }

    @RequestMapping(value = "/merchantpost", method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.OK)
    public void handlerMerchantPost(final HttpServletRequest request)
    {
        logResponseParams(request);

        final String orderNumber = getReferenceNumber(request);
        LOG.info("Creating order [{}] from Cybersource Merchant POST", orderNumber);

        if (StringUtils.isEmpty(orderNumber))
        {
            return;
        }

        final CartModel cart = getCart(orderNumber);
        if (cart != null)
        {
            paymentCartService.executeWithCartLock(cart, () -> doHandlePlaceOrder(request, cart));
        }
    }

    @RequestMapping(value = "/isorderplaced", method = RequestMethod.GET)
    @ResponseBody
    public String isOrderPlaced(@RequestParam final String cartGuid)
    {
        try
        {
            final OrderData orderData = orderFacade.getOrderDetailsForGUID(cartGuid);
            return getCheckoutCustomerStrategy().isAnonymousCheckout() ? orderData.getGuid() : orderData.getCode();
        }
        catch (final UnknownIdentifierException e)
        {
            return StringUtils.EMPTY;
        }
    }

    private void logResponseParams(final HttpServletRequest request)
    {
        LOG.debug("Processing Cybersource response: {}", HttpRequestUtil.getParametersMap(request));
    }

    private String getReferenceNumber(final HttpServletRequest request)
    {
        final String referenceNumber = request.getParameter(CybersourcePaymentConstants.SAResponseFields.REFERENCE_NUMBER);
        if (referenceNumber == null)
        {
            LOG.error("Got an empty order reference number on cybersource payment response");
        }

        return referenceNumber;
    }

    private CartModel getCart(final String guid)
    {
        final CartModel cart = paymentCartService.getCartForGuid(guid);
        if (cart == null)
        {
            LOG.warn("Cannot find cart for code: {}", guid);
        }

        return cart;
    }

    private OrderData doHandlePlaceOrder(final HttpServletRequest request, final CartModel cart)
    {
        final Map<String, String> paymentResponse = HttpRequestUtil.getParametersMap(request);
        cybersourceResponseHandler.processResponse(cart, paymentResponse);

        try
        {
            if (paymentCheckoutFacade.validOrder(cart) && cybersourceResponseHandler.isResponseSuccessful(paymentResponse))
            {
                return paymentCheckoutFacade.performPlaceOrder(cart);
            }
            else
            {
                LOG.error("Failed to place Order, please check order data and payment transaction");
            }
        }
        catch (final Exception e)
        {
            LOG.error("Failed to place Order", e);
        }

        return null;
    }
}
