package com.cybersource.payment.addon.controllers.pages.checkout.payment.sa;

import static com.cybersource.payment.constants.CybersourcePaymentConstants.SARequestFields.ACCESS_KEY;
import static com.cybersource.payment.constants.CybersourcePaymentConstants.SARequestFields.AMOUNT;
import static com.cybersource.payment.constants.CybersourcePaymentConstants.SARequestFields.BILL_TO_ADDRESS_CITY;
import static com.cybersource.payment.constants.CybersourcePaymentConstants.SARequestFields.BILL_TO_ADDRESS_COUNTRY;
import static com.cybersource.payment.constants.CybersourcePaymentConstants.SARequestFields.BILL_TO_ADDRESS_LINE1;
import static com.cybersource.payment.constants.CybersourcePaymentConstants.SARequestFields.BILL_TO_ADDRESS_POSTAL_CODE;
import static com.cybersource.payment.constants.CybersourcePaymentConstants.SARequestFields.BILL_TO_ADDRESS_STATE;
import static com.cybersource.payment.constants.CybersourcePaymentConstants.SARequestFields.BILL_TO_EMAIL;
import static com.cybersource.payment.constants.CybersourcePaymentConstants.SARequestFields.BILL_TO_FORENAME;
import static com.cybersource.payment.constants.CybersourcePaymentConstants.SARequestFields.BILL_TO_SURNAME;
import static com.cybersource.payment.constants.CybersourcePaymentConstants.SARequestFields.CARD_EXPIRE_DATE;
import static com.cybersource.payment.constants.CybersourcePaymentConstants.SARequestFields.CARD_NUMBER;
import static com.cybersource.payment.constants.CybersourcePaymentConstants.SARequestFields.CARD_TYPE;
import static com.cybersource.payment.constants.CybersourcePaymentConstants.SARequestFields.CURRENCY;
import static com.cybersource.payment.constants.CybersourcePaymentConstants.SARequestFields.DEVELOPER_ID;
import static com.cybersource.payment.constants.CybersourcePaymentConstants.SARequestFields.LOCALE;
import static com.cybersource.payment.constants.CybersourcePaymentConstants.SARequestFields.MERCHANT_DEFINED_DATA_100;
import static com.cybersource.payment.constants.CybersourcePaymentConstants.SARequestFields.MERCHANT_DEFINED_DATA_99;
import static com.cybersource.payment.constants.CybersourcePaymentConstants.SARequestFields.OVERRIDE_CUSTOM_MERCHANT_POST_PAGE;
import static com.cybersource.payment.constants.CybersourcePaymentConstants.SARequestFields.OVERRIDE_CUSTOM_RECEIPT_PAGE;
import static com.cybersource.payment.constants.CybersourcePaymentConstants.SARequestFields.PARTNER_SOLUTION_ID;
import static com.cybersource.payment.constants.CybersourcePaymentConstants.SARequestFields.PAYMENT_METHOD;
import static com.cybersource.payment.constants.CybersourcePaymentConstants.SARequestFields.PROFILE_ID;
import static com.cybersource.payment.constants.CybersourcePaymentConstants.SARequestFields.REFERENCE_NUMBER;
import static com.cybersource.payment.constants.CybersourcePaymentConstants.SARequestFields.SIGNED_DATE_TIME;
import static com.cybersource.payment.constants.CybersourcePaymentConstants.SARequestFields.TRANSACTION_TYPE;
import static com.cybersource.payment.constants.CybersourcePaymentConstants.SARequestFields.TRANSACTION_UUID;
import static java.util.Objects.isNull;
import static org.apache.commons.lang.StringUtils.EMPTY;

import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.AddressModel;

import java.util.Date;
import java.util.Map;
import java.util.UUID;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.cybersource.payment.enums.CybsMerchantProfileType;
import com.cybersource.payment.model.CybsMerchantProfileModel;
import com.cybersource.payment.utils.TimeUtils;

@Controller
@RequestMapping(value = "/checkout/payment/sa")
public class CybersourceSOPController extends CybersourceSecureAcceptanceController
{
    @RequestMapping(value = "sop", method = RequestMethod.GET)
    public String getSopForm(final ModelMap model)
    {
        model.addAttribute("cartGuid", getCartService().getSessionCart().getGuid());
        model.addAttribute("postUrl", getSopPostUrl());
        model.addAttribute("formFields", buildCybersourceRequest());

        return "addon:/cybersourcepaymentaddon/pages/checkout/payment/sa/sopRequest";
    }

    private Map<String, String> buildCybersourceRequest()
    {
        final CybsMerchantProfileModel profile = getMerchantService().getCurrentMerchantProfile(
                CybsMerchantProfileType.SOP);

        final CartModel cart = getCartService().getSessionCart();
        final AddressModel billingAddress = cart.getPaymentInfo().getBillingAddress();
        final String state = isNull(billingAddress.getRegion()) ? EMPTY : billingAddress.getRegion().getIsocodeShort();

        return new SecureAcceptanceRequestBuilder(signatureFunction(profile.getSecretKey()))
                .addSignedField(ACCESS_KEY, profile.getAccessKey())
                .addSignedField(PROFILE_ID, profile.getProfileId())
                .addSignedField(TRANSACTION_UUID, UUID.randomUUID().toString())
                .addSignedField(SIGNED_DATE_TIME, TimeUtils.toUTCDateTime(new Date()))
                .addSignedField(LOCALE, getI18NService().getCurrentLocale().toString())
                .addSignedField(TRANSACTION_TYPE, "authorization")
                .addSignedField(REFERENCE_NUMBER, cart.getGuid())
                .addSignedField(AMOUNT, cart.getTotalPrice().toString())
                .addSignedField(CURRENCY, cart.getCurrency().getIsocode())
                .addSignedField(PAYMENT_METHOD, "card")
                .addSignedField(BILL_TO_FORENAME, billingAddress.getFirstname())
                .addSignedField(BILL_TO_SURNAME, billingAddress.getLastname())
                .addSignedField(BILL_TO_EMAIL, billingAddress.getEmail())
                .addSignedField(BILL_TO_ADDRESS_LINE1, billingAddress.getLine1())
                .addSignedField(BILL_TO_ADDRESS_CITY, billingAddress.getTown())
                .addSignedField(BILL_TO_ADDRESS_COUNTRY, billingAddress.getCountry().getIsocode())
                .addSignedField(BILL_TO_ADDRESS_POSTAL_CODE, billingAddress.getPostalcode())
                .addSignedField(BILL_TO_ADDRESS_STATE, state)
                .addSignedField(OVERRIDE_CUSTOM_RECEIPT_PAGE, getReceiptUrl())
                .addSignedField(OVERRIDE_CUSTOM_MERCHANT_POST_PAGE, getMerchantPostUrl())
                .addUnsignedField(PARTNER_SOLUTION_ID, paymentSystemInfo.getPartnerSolutionID())
                .addUnsignedField(DEVELOPER_ID, paymentSystemInfo.getDeveloperID())
                .addUnsignedField(CARD_TYPE, EMPTY)
                .addUnsignedField(CARD_NUMBER, EMPTY)
                .addUnsignedField(CARD_EXPIRE_DATE, EMPTY)
                .addUnsignedField(MERCHANT_DEFINED_DATA_99, profile.getMerchant().getId())
                .addUnsignedField(MERCHANT_DEFINED_DATA_100, CybsMerchantProfileType.SOP.getCode())
                .build();
    }

    private String getSopPostUrl()
    {
        return getConfigurationService().getConfiguration().getString("cybersource.secure.acceptance.sop.post.url");
    }
}
