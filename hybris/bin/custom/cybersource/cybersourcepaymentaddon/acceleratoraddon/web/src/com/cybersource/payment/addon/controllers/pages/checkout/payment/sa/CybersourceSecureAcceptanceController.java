package com.cybersource.payment.addon.controllers.pages.checkout.payment.sa;

import de.hybris.platform.acceleratorservices.urlresolver.SiteBaseUrlResolutionService;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.site.BaseSiteService;

import java.util.Map;
import java.util.function.Function;
import javax.annotation.Resource;

import com.cybersource.payment.data.PaymentSystemInfo;
import com.cybersource.payment.security.service.CybersourceSAService;
import com.cybersource.payment.service.MerchantService;

public class CybersourceSecureAcceptanceController extends AbstractPageController
{
    @Resource
    private MerchantService merchantService;

    @Resource
    private CybersourceSAService cybersourceSAService;

    @Resource
    private BaseSiteService siteService;

    @Resource
    private I18NService i18NService;

    @Resource
    private SiteBaseUrlResolutionService siteBaseUrlResolutionService;

    @Resource
    private ConfigurationService configurationService;

    @Resource
    private CartService cartService;

    @Resource
    protected PaymentSystemInfo paymentSystemInfo;

    protected Function<Map<String, String>, String> signatureFunction(final String secretKey)
    {
        return map -> cybersourceSAService.getDigest(map, secretKey);
    }

    protected String getReceiptUrl()
    {
        return siteBaseUrlResolutionService.getWebsiteUrlForSite(siteService.getCurrentBaseSite(), true,
                "/checkout/payment/sa/receipt");
    }

    protected String getMerchantPostUrl()
    {
        return siteBaseUrlResolutionService.getWebsiteUrlForSite(siteService.getCurrentBaseSite(), true,
                "/checkout/payment/sa/merchantpost");
    }

    protected I18NService getI18NService()
    {
        return i18NService;
    }

    protected ConfigurationService getConfigurationService()
    {
        return configurationService;
    }

    protected CartService getCartService()
    {
        return cartService;
    }

    protected MerchantService getMerchantService()
    {
        return merchantService;
    }
}
