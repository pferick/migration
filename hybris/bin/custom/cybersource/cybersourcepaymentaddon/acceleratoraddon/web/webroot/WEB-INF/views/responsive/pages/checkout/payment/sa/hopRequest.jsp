<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="cybersource" tagdir="/WEB-INF/tags/addons/cybersourcepaymentaddon/responsive/payment/sa" %>

<html>
    <head>
        <template:javaScriptVariables/>

        <script type="text/javascript" src="${commonResourcePath}/js/jquery-2.1.1.min.js"></script>
        <script type="text/javascript" src="${contextPath}/_ui/addons/cybersourcepaymentaddon/responsive/common/js/cybersourcepaymentaddon.js"></script>
    </head>
    <body>
        <iframe name="cybsIframe" width="100%" height="100%" frameborder="0px"></iframe>
        <cybersource:form formId="hopRequestForm" postUrl="${postUrl}" formFields="${formFields}" targetUrl="cybsIframe" />

        <script type="text/javascript" language="JavaScript">
            $(function() {
                var payPageRequestForm = $('#hopRequestForm');
                payPageRequestForm.submit();

                ACC.secureacceptance.checkOrderStatusInterval('${cartGuid}', 10000);
            });
        </script>
    </body>
</html>
