ACC.secureacceptance = {

    _autoload: ['attachHandlerForPlaceOrderBtn', 'initSOPIframeDialog'],

    checkOrderStatusInterval: function (cartGuid, timeout) {
        setInterval(function() { ACC.secureacceptance.checkOrderStatus(cartGuid) }, timeout);
    },

    checkOrderStatus: function (cartGuid) {
        $.ajax({
            url: ACC.config.contextPath + '/checkout/payment/sa/isorderplaced',
            cache: false,
            dataType: 'json',
            data: {
                cartGuid: cartGuid
            },
            success: function (orderCode) {

                if (orderCode) {
                    window.location = ACC.config.contextPath + '/checkout/orderConfirmation/' + orderCode;
                }
            }
        });
    },

    initSOPIframeDialog: function() {
        var $sopIframeCbox = $('#sopIframeCbox');

        if ($sopIframeCbox) {
            $sopIframeCbox.dialog({
                title: "Payment Details",
                autoOpen: false,
                width: 500,
                dialogClass: 'no-close',
                modal: true
            });
        }
    },

    startSOPIframeTimeout: function($sopRequestIframe) {
        var previousSOPIframeTimeout = null;

        $sopRequestIframe.load(function() {
            if (previousSOPIframeTimeout) {
                clearTimeout(previousSOPIframeTimeout);
            }

            previousSOPIframeTimeout = setTimeout(function () {
                $('#sopIframeCbox').dialog('open');
            }, 3000);
        });
    },

    attachHandlerForPlaceOrderBtn: function () {

        var submitHOPForm = function () {
            if(termsAndConditionsChecked()) {
                var hopForm = $("#hopRequestForm");
                hopForm.submit();
            }
        };

        var submitSOPForm = function() {
            if (isValidCardData() && termsAndConditionsChecked()) {

                var $sopRequestIframe = $('#sopRequestIframe');
                var sopForm = $sopRequestIframe.contents().find('#sopRequestForm');

                sopForm.find("#card_type").val(getCardType());
                sopForm.find("#card_number").val(getCardNumber());
                sopForm.find("#card_expiry_date").val(getCardExpireDate());

                sopForm.submit();

                ACC.secureacceptance.startSOPIframeTimeout($sopRequestIframe);

                var cartGuid = $sopRequestIframe.contents().find("#cart_guid").val();
                ACC.secureacceptance.checkOrderStatusInterval(cartGuid, 10000);
            }
        };

        var isValidCardData = function () {

            var fieldsToValidate = [
                {value: getCardType(), error: $("#card_cardType_errors")},
                {value: getCardNumber(), error: $("#card_accountNumber_errors")},
                {value: getCardExpireYear(), error: $("#card_expirationYear_errors")},
                {value: getCardExpireMonth(), error: $("#card_expirationMonth_errors")}
            ];

            var isValid = true;

            fieldsToValidate.forEach(function (field) {
                if (Boolean(field.value)) {
                    hide(field.error)
                } else {
                    show(field.error);
                    isValid = false;
                }
            });

            return isValid;

        };

        var termsAndConditionsChecked = function () {
            if($("#Terms1").is(":checked")) {
                $(".tc-unchecked-alert").attr("hidden", "hidden");
                return true;
            } else {
                $(".tc-unchecked-alert").removeAttr("hidden");
                return false;
            }
        };

        var show = function (validationMsg) {
            validationMsg.removeAttr("hidden");
            validationMsg.closest(".form-group").addClass("has-error");
        };

        var hide = function (validationMsg) {
            validationMsg.attr("hidden", "hidden");
            validationMsg.closest(".form-group").removeClass("has-error");
        };

        var getCardType = function () {
            return $("#card_cardType option:selected").val();
        };

        var getCardNumber = function () {
            return $("#card_accountNumber").val();
        };

        var getCardExpireDate = function () {
            return $("#ExpiryMonth option:selected").text() + '-' + $("#ExpiryYear option:selected").text();
        };

        var getCardExpireYear = function () {
            return $("#ExpiryYear option:selected").val();
        };

        var getCardExpireMonth = function () {
            return $("#ExpiryMonth option:selected").val();
        };

        var placeOrderBtn = $('.cs_btn-place-order');

        if (placeOrderBtn) {

            placeOrderBtn.click(
                    function () {
                        if ($('#hopRequestForm').length) {
                            submitHOPForm();
                        } else {
                            submitSOPForm();
                        }
                    }
            );
        }

    }
};
