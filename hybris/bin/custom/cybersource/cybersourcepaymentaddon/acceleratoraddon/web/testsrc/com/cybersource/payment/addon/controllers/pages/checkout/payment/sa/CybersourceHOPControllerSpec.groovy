package com.cybersource.payment.addon.controllers.pages.checkout.payment.sa

import com.cybersource.payment.configuration.service.DefaultPaymentConfigurationService
import com.cybersource.payment.enums.CybsConfigurationType
import com.cybersource.payment.enums.CybsMerchantProfileType
import com.cybersource.payment.enums.CybsPaymentChannel
import com.cybersource.payment.enums.CybsPaymentType
import com.cybersource.payment.model.CybsMerchantModel
import com.cybersource.payment.model.CybsMerchantPaymentConfigurationModel
import com.cybersource.payment.model.CybsMerchantProfileModel
import com.cybersource.payment.security.service.CybersourceSAService
import de.hybris.bootstrap.annotations.UnitTest
import de.hybris.platform.acceleratorservices.urlresolver.SiteBaseUrlResolutionService
import de.hybris.platform.basecommerce.model.site.BaseSiteModel
import de.hybris.platform.core.model.c2l.CountryModel
import de.hybris.platform.core.model.c2l.CurrencyModel
import de.hybris.platform.core.model.c2l.RegionModel
import de.hybris.platform.core.model.order.CartModel
import de.hybris.platform.core.model.order.payment.PaymentInfoModel
import de.hybris.platform.core.model.user.AddressModel
import de.hybris.platform.order.CartService
import de.hybris.platform.servicelayer.config.ConfigurationService
import de.hybris.platform.servicelayer.i18n.I18NService
import de.hybris.platform.site.BaseSiteService
import org.apache.commons.configuration.Configuration
import org.junit.Test
import org.springframework.ui.Model
import spock.lang.Specification

@UnitTest
class CybersourceHOPControllerSpec extends Specification
{
    def cybersourceSAService = Mock([useObjenesis: false], CybersourceSAService);

    def defaultPaymentConfigurationService = Mock([useObjenesis: false], DefaultPaymentConfigurationService)

    def siteService = Mock([useObjenesis: false], BaseSiteService)

    def i18NService = Mock([useObjenesis: false], I18NService)

    def siteBaseUrlResolutionService = Mock([useObjenesis: false], SiteBaseUrlResolutionService)

    def configurationService = Mock([useObjenesis: false], ConfigurationService)

    def cartService = Mock([useObjenesis: false], CartService)

    def model = Mock([useObjenesis: false], Model)

    def currentSite = Mock([useObjenesis: false], BaseSiteModel)

    def currency = Mock([useObjenesis: false], CurrencyModel)

    def paymentConfiguration = Mock([useObjenesis: false], CybsMerchantPaymentConfigurationModel)

    def merchant = Mock([useObjenesis: false], CybsMerchantModel)

    def profile = Mock([useObjenesis: false], CybsMerchantProfileModel)

    def cart = Mock([useObjenesis: false], CartModel)

    def paymentInfo = Mock([useObjenesis: false], PaymentInfoModel)

    def billingAddress = Mock([useObjenesis: false], AddressModel)

    def country = Mock([useObjenesis: false], CountryModel)

    def region = Mock([useObjenesis: false], RegionModel)

    def configuration = Mock([useObjenesis: false], Configuration)

    def currentLocale = Locale.ENGLISH

    def controller = new CybersourceHOPController()

    def setup()
    {
        controller.cybersourceSAService = cybersourceSAService
        controller.defaultPaymentConfigurationService = defaultPaymentConfigurationService
        controller.siteService = siteService
        controller.i18NService = i18NService
        controller.siteBaseUrlResolutionService = siteBaseUrlResolutionService
        controller.configurationService = configurationService
        controller.cartService = cartService

        siteService.currentBaseSite >> currentSite

        defaultPaymentConfigurationService.getConfiguration(
                CybsConfigurationType.MERCHANT_DATA,
                [site: currentSite, paymentType: CybsPaymentType.CREDIT_CARD, paymentChannel: CybsPaymentChannel.WEB, 'currency': currency]
        ) >> paymentConfiguration

        paymentConfiguration.getMerchant() >> merchant

        merchant.id >> 'MERCHANT1'
        merchant.profiles >> [profile]

        profile.profileId >> 'merchantProfileId'
        profile.profileType >> CybsMerchantProfileType.HOP
        profile.accessKey >> 'accessKey'
        profile.secretKey >> 'securityKey'
        profile.merchant >> merchant

        cartService.sessionCart >> cart

        cart.code >> '1234567890'
        cart.paymentInfo >> paymentInfo
        cart.totalPrice >> 200D
        cart.currency >> currency

        currency.isocode >> 'USD'
        region.isocodeShort >> 'CA'

        paymentInfo.billingAddress >> billingAddress

        billingAddress.lastname >> 'Smith'
        billingAddress.firstname >> 'John'
        billingAddress.email >> 'jsmith@mail.com'
        billingAddress.town >> 'San Francisco'
        billingAddress.streetname >> 'Embarcadero'
        billingAddress.streetnumber >> '1'
        billingAddress.postalcode >> '94111'
        billingAddress.country >> country
        billingAddress.region >> region

        country.isocode >> 'US'

        i18NService.currentLocale >> currentLocale

        configurationService.configuration >> configuration

        siteBaseUrlResolutionService.getWebsiteUrlForSite(currentSite, true, '/checkout/payment/sa/receipt') >> 'https://apparel-uk/checkout/payment/sa/receipt'

        configuration.getString('cybersource.secure.acceptance.hop.post.url') >> 'https://testsecureacceptance.cybersource.com/pay'

        cybersourceSAService.getDigest(_, 'securityKey') >> 'signed_data'
    }

    @Test
    def 'controller should render hopRequestForm'()
    {
        when:
        def result = controller.sendPaymentRequest(model)

        then:
        result == 'addon:/cybersourcepaymentaddon/pages/checkout/payment/sa/hopRequest'
    }

    @Test
    def 'controller should send valid request params'()
    {
        when:
        def result = controller.sendPaymentRequest(model)

        then:
        model.addAttribute('cybsRequest', *_) >> { argurmants ->
            def map = argurmants[1] as Map<String, Object>

            assert map.access_key == 'accessKey'
            assert map.profile_id == 'merchantProfileId'
            assert map.profile_id == 'merchantProfileId'
            assert map.transaction_uuid =~ /.{8}-.{4}-.{4}-.{4}-.{12}/
            assert map.signed_field_names == 'access_key,profile_id,transaction_uuid,signed_field_names,unsigned_field_names,signed_date_time,locale,transaction_type,reference_number,amount,currency,bill_to_forename,bill_to_surname,bill_to_email,bill_to_address_country,bill_to_address_city,bill_to_address_line1,bill_to_address_line2,bill_to_address_postal_code,override_custom_receipt_page'
            assert map.unsigned_field_names == 'merchant_defined_data99,merchant_defined_data100'
            assert map.signed_date_time =~ /\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}Z/
            assert map.locale == 'en'
            assert map.transaction_type == 'authorization'
            assert map.reference_number == '1234567890'
            assert map.amount == '200.0'
            assert map.currency == 'USD'
            assert map.bill_to_forename == 'John'
            assert map.bill_to_surname == 'Smith'
            assert map.bill_to_email == 'jsmith@mail.com'
            assert map.bill_to_address_country == 'US'
            assert map.bill_to_address_state == 'CA'
            assert map.bill_to_address_city == 'San Francisco'
            assert map.bill_to_address_line1 == 'Embarcadero'
            assert map.bill_to_address_line2 == '1'
            assert map.bill_to_address_postal_code == '94111'
            assert map.override_custom_receipt_page == 'https://apparel-uk/checkout/payment/sa/receipt'
            assert map.hop_url == 'https://testsecureacceptance.cybersource.com/pay'
            assert map.signature == 'signed_data'
            assert map.merchant_defined_data99 == 'MERCHANT1'
            assert map.merchant_defined_data100 == 'hop'
        }
    }
}
