package com.cybersource.payment.addon.tags


import de.hybris.bootstrap.annotations.UnitTest
import de.hybris.platform.acceleratorservices.checkout.pci.impl.ConfiguredCheckoutPciStrategy
import de.hybris.platform.acceleratorservices.enums.CheckoutPciOptionEnum
import org.junit.Test
import spock.lang.Specification

import javax.servlet.jsp.tagext.Tag

@UnitTest
class SecureAcceptanceTypeTagSpec extends Specification
{
    def configuredCheckoutPciStrategy = Mock([useObjenesis: false], ConfiguredCheckoutPciStrategy)

    def tag = Spy(SecureAcceptanceTypeTag, constructorArgs: [])

    def setup()
    {
        tag.configuredCheckoutPciStrategy >> configuredCheckoutPciStrategy
        configuredCheckoutPciStrategy.subscriptionPciOption >> CheckoutPciOptionEnum.CYBS_HOP
    }

    @Test
    def 'tag should evaluate and include body'()
    {
        when:
        tag.type = CheckoutPciOptionEnum.CYBS_HOP

        then:
        tag.doStartTag() == Tag.EVAL_BODY_INCLUDE
    }

    @Test
    def 'tag should evaluate and skip body'()
    {
        when:
        tag.type = CheckoutPciOptionEnum.CYBS_SOP

        then:
        tag.doStartTag() == Tag.SKIP_BODY
    }
}
