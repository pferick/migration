package com.cybersource.payment.addon.listener

import com.netflix.hystrix.contrib.servopublisher.HystrixServoMetricsPublisher
import com.netflix.hystrix.strategy.HystrixPlugins
import spock.lang.Specification

import javax.servlet.ServletContext
import javax.servlet.ServletContextEvent

class HystrixMetricsPublisherListenerSpec extends Specification
{

  def servletContext = Mock([useObjenesis: false], ServletContext)
  def listener = new HystrixMetricsPublisherListener()

  def "ContextInitialized servo metrics publisher registered "()
  {
    when:
    listener.contextInitialized(new ServletContextEvent(servletContext))

    then:
    HystrixServoMetricsPublisher.class == HystrixPlugins.getInstance().getMetricsPublisher().getClass()

  }

  def "Register when ContextInitialized is called several times "()
  {
    when:
    listener.contextInitialized(new ServletContextEvent(servletContext))
    listener.contextInitialized(new ServletContextEvent(servletContext))
    listener.contextInitialized(new ServletContextEvent(servletContext))

    then:
    HystrixServoMetricsPublisher.class == HystrixPlugins.getInstance().getMetricsPublisher().getClass()

  }
}
