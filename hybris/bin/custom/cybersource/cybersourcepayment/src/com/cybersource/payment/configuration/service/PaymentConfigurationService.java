package com.cybersource.payment.configuration.service;

import com.cybersource.payment.enums.CybsConfigurationType;

import java.util.Map;

/**
 * This class is responsible for configuration lookup
 */
public interface PaymentConfigurationService
{
    /**
     * Find configuration instance based by type
     *
     * @param configType configuration type
     * @param params required params in order to identify appropriate configuration instance
     * @param <T> configuration value type
     * @return configuration value
     */
    <T> T getConfiguration(final CybsConfigurationType configType, final Map<String, Object> params);
}
