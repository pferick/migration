package com.cybersource.payment.service.executor.request.converter;

import com.cybersource.payment.service.executor.request.PaymentServiceRequest;
import com.cybersource.payment.service.request.CybersourceRequest;
import com.cybersource.payment.service.request.value.CardType;
import com.cybersource.payment.utils.PaymentParamUtils;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.payment.dto.CardInfo;

/*
* A component that encapsulates conversion logic from {@link PaymentServiceRequest} to
* {@link CybersourceRequest} for payment enrollment request.
*/
public class EnrollmentRequestConverter extends AbstractRequestConverter
{
    @Override
    public CybersourceRequest convert(final PaymentServiceRequest source)
    {
        final AbstractOrderModel order = source.getRequiredParam("order");
        final CardInfo cardInfo = source.getRequiredParam("card");

        return cybersourceRequestFactory.enrollment()
                .merchantId(source.getRequiredParam("merchantId"))
                .merchantReferenceCode(order.getGuid())
                .purchaseTotalsCurrency(order.getCurrency().getIsocode())
                .payerAuthEnrollServiceRun(true)
                .cardType(CardType.valueOf(cardInfo.getCardType().toString()))
                .cardAccountNumber(cardInfo.getCardNumber())
                .cardExpirationMonth(PaymentParamUtils.getMonth(cardInfo.getExpirationMonth()))
                .cardExpirationYear(PaymentParamUtils.toString(cardInfo.getExpirationYear()))
                .cardStartMonth(PaymentParamUtils.getMonth(cardInfo.getIssueMonth()))
                .cardStartYear(PaymentParamUtils.toString(cardInfo.getIssueYear()))
                .request();
    }
}
