package com.cybersource.payment.service.executor.request.builder.creditcard;

import com.cybersource.payment.service.executor.request.builder.PaymentServiceRequestBuilder;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionModel;

import java.math.BigDecimal;

import com.cybersource.payment.enums.CybsPaymentType;

/**
 * Builder for payment refund follow-on request.
 */
public class RefundFollowOnRequestBuilder extends PaymentServiceRequestBuilder<RefundFollowOnRequestBuilder>
{
    /**
     * Creates a new instance of {@link RefundFollowOnRequestBuilder} class with method CREDIT_CARD and
     * service REFUND_FOLLOW_ON.
     *
     * @see CybsPaymentType
     * @see PaymentTransactionType
     */
    public RefundFollowOnRequestBuilder()
    {
        paymentServiceRequest.method(CybsPaymentType.CREDIT_CARD);
        paymentServiceRequest.service(PaymentTransactionType.REFUND_FOLLOW_ON);
    }

    public RefundFollowOnRequestBuilder setOrder(final AbstractOrderModel order)
    {
        paymentServiceRequest.addParam("order", order);
        return this;
    }

    public RefundFollowOnRequestBuilder setAmount(final BigDecimal amount)
    {
        paymentServiceRequest.addParam("amount", amount);
        return this;
    }

    public RefundFollowOnRequestBuilder setTransaction(final PaymentTransactionModel transaction)
    {
        paymentServiceRequest.addParam("transaction", transaction);
        return this;
    }
}
