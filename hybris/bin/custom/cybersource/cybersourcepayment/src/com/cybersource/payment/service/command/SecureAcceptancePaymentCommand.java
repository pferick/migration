package com.cybersource.payment.service.command;

import java.util.Map;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import com.cybersource.payment.service.request.CybersourceRequest;
import com.cybersource.payment.service.response.CybersourceResponse;
import com.cybersource.payment.service.response.DefaultCybersourceResponse;

import ma.glasnost.orika.MapperFacade;

/*
*  A concrete implementation of {@link PaymentCommand} which
*  executes a command based on secure acceptance.
*/
public class SecureAcceptancePaymentCommand implements PaymentCommand
{
    private MapperFacade responseMapper;

    @Override
    public CybersourceResponse perform(final CybersourceRequest request)
    {
        Assert.notNull(request, "request cannot be null");

        final Map paymentResponse = (Map) request.getRequestFields().get("paymentResponse");
        Assert.notNull(paymentResponse, "payment response parameter should be provided");

        final CybersourceResponse response = new DefaultCybersourceResponse();

        responseMapper.map(paymentResponse, response);

        return response;
    }

    @Required
    public void setResponseMapper(final MapperFacade responseMapper)
    {
        this.responseMapper = responseMapper;
    }
}
