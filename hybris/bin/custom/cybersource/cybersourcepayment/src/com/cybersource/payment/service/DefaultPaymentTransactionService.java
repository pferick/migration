package com.cybersource.payment.service;

import com.cybersource.payment.constants.CybersourcePaymentConstants;
import com.cybersource.payment.enums.CybsPaymentType;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class DefaultPaymentTransactionService implements PaymentTransactionService
{
    @Override
    public Optional<PaymentTransactionModel> getTransaction(final CybsPaymentType paymentType, final AbstractOrderModel order)
    {
        return order.getPaymentTransactions().stream().filter(t -> paymentType.name().equals(t.getPaymentProvider()))
                .findFirst();
    }

    @Override
    public Optional<PaymentTransactionEntryModel> getLatestAcceptedTransactionEntry(final PaymentTransactionModel paymentTransaction,
                                                                                    final PaymentTransactionType type)
    {
        return getLatestTransactionEntry(paymentTransaction, type, CybersourcePaymentConstants.TransactionStatus.ACCEPT);
    }

    @Override
    public Optional<PaymentTransactionEntryModel> getLatestTransactionEntry(final PaymentTransactionModel paymentTransaction,
                                                                            final PaymentTransactionType type,
                                                                            final String... statuses)
    {
        final List<PaymentTransactionEntryModel> reversedTransactionEntries = new ArrayList<>(paymentTransaction.getEntries());
        Collections.reverse(reversedTransactionEntries);

        return reversedTransactionEntries.stream()
                .filter(e -> type.equals(e.getType())
                        && Arrays.stream(statuses).anyMatch(s -> s.equals(e.getTransactionStatus())))
                .findFirst();
    }
}
