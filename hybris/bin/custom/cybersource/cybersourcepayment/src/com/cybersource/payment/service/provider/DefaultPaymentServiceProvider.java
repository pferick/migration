package com.cybersource.payment.service.provider;

import com.cybersource.payment.enums.CybsPaymentSource;
import com.cybersource.payment.enums.CybsPaymentType;
import com.cybersource.payment.model.CybsPaymentTransactionEntryModel;
import com.cybersource.payment.service.command.PaymentCommand;
import com.cybersource.payment.service.executor.PaymentServiceResult;
import com.cybersource.payment.service.executor.request.PaymentServiceRequest;
import com.cybersource.payment.service.executor.request.converter.AbstractRequestConverter;
import com.cybersource.payment.service.request.CybersourceRequest;
import com.cybersource.payment.service.response.CybersourceResponse;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import org.springframework.beans.factory.annotation.Required;

import javax.annotation.Resource;

/*
* The default implementation of {@link PaymentServiceProvider}.
*/
public class DefaultPaymentServiceProvider implements PaymentServiceProvider
{
    private CybsPaymentSource paymentSource = CybsPaymentSource.SERVICE_API;

    private CybsPaymentType paymentType;

    private PaymentTransactionType paymentTransactionType;

    private AbstractRequestConverter cybersourceRequestConverter;

    private PaymentCommand paymentCommand;

    @Resource
    private PaymentTransactionCreator paymentTransactionCreator;

    @Override
    public boolean supports(final PaymentServiceRequest request)
    {
        return paymentSource == request.getPaymentSource()
                && paymentType == request.getPaymentType()
                && paymentTransactionType == request.getPaymentTransactionType();
    }

    @Override
    public PaymentServiceResult invoke(final PaymentServiceRequest request)
    {
        final CybersourceRequest cybersourceRequest = cybersourceRequestConverter.convert(request);

        final CybersourceResponse cybersourceResponse = paymentCommand.perform(cybersourceRequest);

        final CybsPaymentTransactionEntryModel transactionEntry = paymentTransactionCreator
                .createTransactionEntry(request, cybersourceResponse);

        return PaymentServiceResult.create().addTransaction(transactionEntry);
    }

    public void setPaymentSource(final CybsPaymentSource paymentSource)
    {
        this.paymentSource = paymentSource;
    }

    @Required
    public void setPaymentType(final CybsPaymentType paymentType)
    {
        this.paymentType = paymentType;
    }

    @Required
    public void setPaymentTransactionType(final PaymentTransactionType paymentTransactionType)
    {
        this.paymentTransactionType = paymentTransactionType;
    }

    @Required
    public void setPaymentCommand(final PaymentCommand paymentCommand)
    {
        this.paymentCommand = paymentCommand;
    }

    @Required
    public void setCybersourceRequestConverter(final AbstractRequestConverter cybersourceRequestConverter)
    {
        this.cybersourceRequestConverter = cybersourceRequestConverter;
    }
}
