package com.cybersource.payment.security.ws;

import java.util.Set;
import java.util.TreeSet;
import javax.annotation.Resource;
import javax.xml.namespace.QName;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPFactory;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

import com.cybersource.payment.exception.CybersourcePaymentException;
import com.cybersource.payment.model.CybsMerchantModel;
import com.cybersource.payment.service.MerchantService;

/**
 * Cybersource web service has an authentication mechanism in order allow access payment functionality.
 * This class is responsible for gathering and providing security credentials to the Cybersource web service.
 */
public class WSSUsernameTokenSecurityHandler implements SOAPHandler<SOAPMessageContext>
{
    private static final Logger LOG = LoggerFactory.getLogger(WSSUsernameTokenSecurityHandler.class);

    private static final String WSS_SECURITY_XSD = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd";

    private static final String WSS_PASSWORD_TYPE_ATTRIBUTE = "Type";

    private static final String WSS_PASSWORD_TYPE =
            "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText";

    private static final String WSSE_PREFIX = "wsse";

    private static final String SECURITY_ELEMENT = "Security";

    private static final String USERNAME_TOKEN_ELEMENT = "UsernameToken";

    private static final String PASSWORD_ELEMENT = "Password";

    private static final String USERNAME_ELEMENT = "Username";

    @Resource
    private MerchantService merchantService;

    @Resource
    private WSSMerchantResolver wssMerchantResolver;

    @Override
    public boolean handleMessage(final SOAPMessageContext context)
    {
        final Boolean outboundProperty = (Boolean) context.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);
        if (outboundProperty)
        {
            try
            {
                final SOAPFactory factory = SOAPFactory.newInstance();
                final SOAPEnvelope envelope = context.getMessage().getSOAPPart().getEnvelope();

                final SOAPElement security = createSecurity(factory);
                final SOAPElement usernameToken = createUsernameToken(factory);

                final String merchantId = getMerchantId(envelope);
                createUsername(factory, merchantId, usernameToken);
                createSecurityToken(factory, merchantId, usernameToken);

                security.addChildElement(usernameToken);
                envelope.getHeader().addChildElement(security);

                LOG.debug("Add Cybersource WS-Security Header to SOAP message, merchant id: {}", merchantId);
            }
            catch (final SOAPException e)
            {
                throw new CybersourcePaymentException("Cannot create Cybersource WS-Security Header", e);
            }
        }

        return true;
    }

    private SOAPElement createSecurity(final SOAPFactory factory) throws SOAPException
    {
        return factory.createElement(SECURITY_ELEMENT, WSSE_PREFIX, WSS_SECURITY_XSD);
    }

    private SOAPElement createUsernameToken(final SOAPFactory factory) throws SOAPException
    {
        return factory.createElement(USERNAME_TOKEN_ELEMENT, WSSE_PREFIX, WSS_SECURITY_XSD);
    }

    private void createSecurityToken(final SOAPFactory factory, final String merchantId, final SOAPElement usernameToken)
            throws SOAPException
    {
        final SOAPElement token = factory.createElement(PASSWORD_ELEMENT, WSSE_PREFIX, WSS_SECURITY_XSD);
        token.addAttribute(QName.valueOf(WSS_PASSWORD_TYPE_ATTRIBUTE), WSS_PASSWORD_TYPE);
        token.addTextNode(getPasswordTokenByMerchant(merchantId));
        usernameToken.addChildElement(token);
    }

    private void createUsername(final SOAPFactory factory, final String merchantId, final SOAPElement usernameToken) throws SOAPException
    {
        final SOAPElement username = factory.createElement(USERNAME_ELEMENT, WSSE_PREFIX, WSS_SECURITY_XSD);
        username.addTextNode(merchantId);
        usernameToken.addChildElement(username);
    }

    private String getMerchantId(final SOAPEnvelope envelope) throws SOAPException
    {
        final String merchantId = wssMerchantResolver.getMerchantId(envelope);

        Assert.notNull(merchantId, "Merchant id must not be null");

        return merchantId;
    }

    private String getPasswordTokenByMerchant(final String merchantId)
    {
        final CybsMerchantModel merchant = merchantService.getMerchant(merchantId);
        return merchant.getPasswordToken();
    }

    @Override
    public Set<QName> getHeaders()
    {
        return new TreeSet<>();
    }

    @Override
    public boolean handleFault(final SOAPMessageContext context)
    {
        return false;
    }

    @Override
    public void close(final MessageContext context)
    {
        // empty
    }
}
