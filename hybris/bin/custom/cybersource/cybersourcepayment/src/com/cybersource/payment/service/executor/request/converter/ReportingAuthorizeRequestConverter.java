package com.cybersource.payment.service.executor.request.converter;

import com.cybersource.payment.model.CybsPaymentTransactionModel;
import com.cybersource.payment.service.executor.request.PaymentServiceRequest;
import com.cybersource.payment.service.request.CybersourceRequest;
import de.hybris.platform.core.model.order.AbstractOrderModel;

/*
* A component that encapsulates conversion logic from {@link PaymentServiceRequest} to
* {@link CybersourceRequest} for report authorization request.
*/
public class ReportingAuthorizeRequestConverter extends AbstractRequestConverter
{
    @Override
    public CybersourceRequest convert(final PaymentServiceRequest source)
    {
        final AbstractOrderModel order = source.getRequiredParam("order");
        final CybsPaymentTransactionModel paymentTransaction = source.getRequiredParam("paymentTransaction");

        return cybersourceRequestFactory.reportAuthorize()
                .order(order)
                .paymentTransaction(paymentTransaction)
                .request();
    }
}
