package com.cybersource.payment.report.provider.unmarshaller;

import com.cybersource.payment.utils.XMLNamespaceFilter;
import org.apache.commons.io.input.TeeInputStream;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLFilter;
import org.xml.sax.XMLReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.UnmarshallerHandler;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.function.Function;

/**
 * This class is responsible for converting xml string representation into report object
 *
 * @param <R> report type
 */
public class XmlReportUnmarshaller<R> implements Function<String, R>
{
    private String packageName;

    private Class<R> reportType;

    public XmlReportUnmarshaller(final Class<R> reportType)
    {
        this.reportType = reportType;
    }

    private static final Logger LOG = LoggerFactory.getLogger(XmlReportUnmarshaller.class);

    @Override
    public R apply(final String content)
    {
        final byte[] reportContentBytes = StringUtils.isBlank(content) ? new byte[] { } : content.getBytes();

        final ByteArrayOutputStream branch = new ByteArrayOutputStream();
        final ByteArrayInputStream reportInputStream = new ByteArrayInputStream(reportContentBytes);
        final TeeInputStream teeStream = new TeeInputStream(reportInputStream, branch);

        try
        {
            final XMLFilter xmlFilter = new XMLNamespaceFilter();
            final SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
            final SAXParser saxParser = saxParserFactory.newSAXParser();
            final XMLReader xmlReader = saxParser.getXMLReader();

            xmlFilter.setParent(xmlReader);

            final JAXBContext jaxbContext = JAXBContext.newInstance(packageName);
            final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            final UnmarshallerHandler unmarshallerHandler = unmarshaller.getUnmarshallerHandler();

            xmlFilter.setContentHandler(unmarshallerHandler);

            xmlFilter.parse(new InputSource(teeStream));

            return (R) unmarshallerHandler.getResult();
        }
        catch (final JAXBException | SAXException | IOException | ParserConfigurationException e)
        {
            LOG.warn("Error creating Report. Could not parse response stream: {}", e.getMessage());
            LOG.debug("Report content: {}", content);
        }
        finally
        {
            try
            {
                teeStream.close();
            }
            catch (IOException e)
            {
                LOG.warn("Could not close report input stream", e);
            }
        }
        throw new IllegalArgumentException("Can not convert " + content + " to report : " + reportType);
    }

    public String getPackageName()
    {
        return packageName;
    }

    @Required
    public void setPackageName(final String packageName)
    {
        this.packageName = packageName;
    }
}
