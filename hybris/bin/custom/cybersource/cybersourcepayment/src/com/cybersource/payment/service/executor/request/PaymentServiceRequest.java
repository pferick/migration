package com.cybersource.payment.service.executor.request;

import de.hybris.platform.payment.enums.PaymentTransactionType;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import com.cybersource.payment.enums.CybsPaymentSource;
import com.cybersource.payment.enums.CybsPaymentType;
import com.cybersource.payment.utils.PaymentParamUtils;

/**
 * An object that represents a payment service request.
 */
public class PaymentServiceRequest
{
    private CybsPaymentSource paymentSource = CybsPaymentSource.SERVICE_API;

    private CybsPaymentType paymentType;

    private PaymentTransactionType paymentTransactionType;

    private final Map<String, Object> requestParams = new HashMap<>();

    private PaymentServiceRequest()
    {
        // private constructor
    }

    /**
     * Creates a new payment service request instance.
     *
     * @return a new instance of payment service request
     */
    public static PaymentServiceRequest create()
    {
        return new PaymentServiceRequest();
    }

    /**
     * Sets payment source (secure acceptance, service api, etc.) to be invoked as a result of this request.
     *
     * @param paymentSource the payment source to be used
     * @return this
     */
    public PaymentServiceRequest source(final CybsPaymentSource paymentSource)
    {
        this.paymentSource = paymentSource;
        return this;
    }

    /**
     * Sets payment method (credit_card, pay_pal, etc.) to be invoked as a result of this request.
     *
     * @param paymentType the payment method to be used
     * @return this
     */
    public PaymentServiceRequest method(final CybsPaymentType paymentType)
    {
        this.paymentType = paymentType;
        return this;
    }

    /**
     * Sets payment service (authorize, capture, refund, etc.) to be executed as a result of this request.
     *
     * @param paymentTransactionType the payment service (operation) to be used
     * @return this
     */
    public PaymentServiceRequest service(final PaymentTransactionType paymentTransactionType)
    {
        this.paymentTransactionType = paymentTransactionType;
        return this;
    }

    /**
     * Adds request parameter to be passed to the payment service.
     *
     * @param name the name of request parameter
     * @param value the value of request parameter
     * @return this
     */
    public PaymentServiceRequest addParam(final String name, final Object value)
    {
        requestParams.put(name, value);
        return this;
    }

    @SuppressWarnings("unchecked")
    public <T> T getRequiredParam(final String name)
    {
        return PaymentParamUtils.getParam(name, requestParams);
    }

    @SuppressWarnings("unchecked")
    public <T> T getParam(final String name)
    {
        return (T) requestParams.get(name);
    }

    public CybsPaymentSource getPaymentSource()
    {
        return paymentSource;
    }

    public CybsPaymentType getPaymentType()
    {
        return paymentType;
    }

    public PaymentTransactionType getPaymentTransactionType()
    {
        return paymentTransactionType;
    }

    public Map<String, Object> getRequestParams()
    {
        return Collections.unmodifiableMap(requestParams);
    }

    @Override
    public String toString()
    {
        return "PaymentServiceRequest{" +
                "payment source:" + paymentSource +
                ", payment method:" + paymentType +
                ", payment service:" + paymentTransactionType +
                ", request params:" + requestParams.keySet() +
                '}';
    }
}
