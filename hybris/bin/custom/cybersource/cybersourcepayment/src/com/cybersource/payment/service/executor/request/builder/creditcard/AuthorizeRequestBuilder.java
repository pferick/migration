package com.cybersource.payment.service.executor.request.builder.creditcard;

import com.cybersource.payment.service.executor.request.builder.PaymentServiceRequestBuilder;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.payment.dto.CardInfo;
import de.hybris.platform.payment.enums.PaymentTransactionType;

import com.cybersource.payment.enums.CybsPaymentType;

/**
 * Builder for payment authorization request.
 */
public class AuthorizeRequestBuilder extends PaymentServiceRequestBuilder<AuthorizeRequestBuilder>
{
    public AuthorizeRequestBuilder()
    {
        paymentServiceRequest.method(CybsPaymentType.CREDIT_CARD);
        paymentServiceRequest.service(PaymentTransactionType.AUTHORIZATION);
    }

    public AuthorizeRequestBuilder setOrder(final AbstractOrderModel order)
    {
        paymentServiceRequest.addParam("order", order);
        return this;
    }

    public AuthorizeRequestBuilder setCardInfo(final CardInfo cardInfo)
    {
        paymentServiceRequest.addParam("card", cardInfo);
        return this;
    }

    public AuthorizeRequestBuilder setSubscriptionID(final String subscriptionID)
    {
        paymentServiceRequest.addParam("subscriptionID", subscriptionID);
        return this;
    }

    public AuthorizeRequestBuilder setPARes(final String paRes)
    {
        paymentServiceRequest.addParam("paRes", paRes);
        return this;
    }
}
