package com.cybersource.payment.cronjob;

import com.cybersource.payment.model.CybsMerchantModel;
import com.cybersource.payment.model.cron.CybsConversionReportPollingCronJobModel;
import com.cybersource.payment.report.executor.ReportExecutor;
import com.cybersource.payment.report.listener.ReportListener;
import com.cybersource.payment.report.request.DailyConversionReportRequest;
import com.cybersource.payment.service.MerchantService;
import com.cybersource.payment.report.service.ReportTimeService;
import com.cybersource.reports.conversion.Report;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import org.springframework.beans.factory.annotation.Required;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

import static com.google.common.collect.Lists.newArrayList;

/**
 * This class is responsible for loading daily conversion reports and notify subscribed listeners
 */
public class DailyConversionReportPollingJob extends AbstractJobPerformable<CybsConversionReportPollingCronJobModel>
{
    @Resource
    private MerchantService merchantService;

    @Resource
    private ReportExecutor reportExecutor;

    private ReportTimeService reportTimeService;

    private List<ReportListener<Report>> listeners = newArrayList();

    @Override
    public PerformResult perform(final CybsConversionReportPollingCronJobModel cronJob)
    {
        final List<CybsMerchantModel> merchants = merchantService.getAllMerchants();

        final Date lastRunDate = cronJob.getLastRunDate();
        final Date startDate = reportTimeService.getReportInterval(lastRunDate).getStart().toDate();

        merchants.forEach(m -> processDailyReport(m, startDate));

        cronJob.setLastRunDate(startDate);
        modelService.save(cronJob);

        return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
    }

    private void processDailyReport(final CybsMerchantModel merchant, final Date startDate)
    {
        final DailyConversionReportRequest request = new DailyConversionReportRequest();

        request.setMerchantId(merchant.getId());
        request.setDate(startDate);

        final Report report = reportExecutor.getReport(request);

        listeners.forEach(h -> h.handle(merchant, report));
    }

    public void setListeners(final List<ReportListener<Report>> listeners)
    {
        this.listeners = listeners;
    }

    @Required
    public void setReportTimeService(final ReportTimeService reportTimeService)
    {
        this.reportTimeService = reportTimeService;
    }
}
