/**
 * Contains classes that defines request params for reporting functionality
 */
package com.cybersource.payment.report.request;