/**
 * Contains strategies responsible for different custom actions, based on report result
 */
package com.cybersource.payment.report.listener.conversion.decision;