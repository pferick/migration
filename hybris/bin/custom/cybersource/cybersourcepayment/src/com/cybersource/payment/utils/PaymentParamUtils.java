package com.cybersource.payment.utils;

import com.cybersource.payment.service.request.value.Month;
import org.springframework.util.Assert;

import java.util.Arrays;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;

/**
 * An utility class that is responsible for assisting on processing payment param values
 */
public final class PaymentParamUtils
{
    /**
     * Assert requested params from map by key for existence
     *
     * @param paramName requested param name
     * @param params map with params
     * @param <T> output type
     * @return param value if exist otherwise throws IllegalArgumentException
     */
    @SuppressWarnings("unchecked")
    public static <T> T getParam(final String paramName, final Map<String, Object> params)
    {
        final T value = (T) params.get(paramName);

        Assert.notNull(value, paramName + " is required param");

        return value;
    }

    /**
     * Convert a non nullable object to string
     *
     * @param value which should be converted to string
     * @return converted value or null if value is null
     */
    public static String toString(final Object value)
    {
        return Optional.ofNullable(value)
                .map(Object::toString)
                .orElse(null);
    }

    /**
     * Gets month by given month integer value (e.g. 01, 02, 03, etc.).
     *
     * @param month month integer value
     * @return month enumeration value
     */
    public static Month getMonth(final Integer month)
    {
        if (month != null)
        {
            return Arrays.stream(Month.values())
                    .filter(m -> month.equals(Integer.valueOf(m.getName())))
                    .findFirst()
                    .orElseThrow(() -> new IllegalArgumentException("Invalid argument for month: " + month));
        }

        return null;
    }

    /**
     * Evaluates object for null and returns result based on function
     *
     * @param object evaluated object
     * @param function algorithm for extracting object vbalue
     * @param <P> input object type
     * @param <R> output object type
     * @return an object value of type <R> or null value if object is null
     */
    public static <P, R> R getValue(final P object,  final Function<P, R> function)
    {
        return Optional.ofNullable(object)
                .map(function)
                .orElse(null);
    }
}
