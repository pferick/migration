package com.cybersource.payment.security.ws;

import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;

import org.w3c.dom.NodeList;

public class DefaultWSSMerchantResolver implements WSSMerchantResolver
{
    private static final String MERCHANT_ID_ELEMENT = "merchantID";

    @Override
    public String getMerchantId(final SOAPEnvelope envelope) throws SOAPException
    {
        final NodeList merchantIdNode = envelope.getBody().getElementsByTagNameNS("*", MERCHANT_ID_ELEMENT);
        final SOAPElement merchantIdElement = merchantIdNode != null && merchantIdNode.getLength() > 0
                ? (SOAPElement) merchantIdNode.item(0) : null;

        return merchantIdElement != null ? merchantIdElement.getFirstChild().getNodeValue() : null;
    }
}
