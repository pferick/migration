package com.cybersource.payment.constants;

/**
 * Defines global constants for Cybersource payment extension
 */
public final class CybersourcePaymentConstants extends GeneratedCybersourcePaymentConstants
{
    public static final String EXTENSIONNAME = "cybersourcepayment";

    private CybersourcePaymentConstants()
    {
        // EMPTY
    }

    public static final String PLATFORM_LOGO_CODE = "cybersourcepaymentPlatformLogo";

    /**
     * Defines payment transactions status constants.
     */
    public interface TransactionStatus
    {
        String ACCEPT = "ACCEPT";
        String ERROR = "ERROR";
        String REJECT = "REJECT";
        String REVIEW = "REVIEW";
    }

    /**
     * Defines the name of Secure Acceptance SOP/HOP response fields.
     */
    public interface SAResponseFields
    {
        String REFERENCE_NUMBER = "req_reference_number";
        String DECISION = "decision";
        String MERCHANT_ID = "req_merchant_defined_data99";
        String PROFILE_TYPE = "req_merchant_defined_data100";
        String REASON_CODE = "reason_code";
    }

    /**
     * Defines payment transactions reason code reply constants.
     */
    public interface ReasonCode
    {
        String OK = "100";
        String ERROR = "101";
        String DUPLICATE_TRANSACTION = "104";
    }

    /**
     * Defines the name of Secure Acceptance SOP/HOP request fields.
     */
    public interface SARequestFields
    {
        String ACCESS_KEY = "access_key";
        String PROFILE_ID = "profile_id";
        String TRANSACTION_UUID = "transaction_uuid";
        String SIGNED_FIELD_NAMES = "signed_field_names";
        String UNSIGNED_FIELD_NAMES = "unsigned_field_names";
        String SIGNED_DATE_TIME = "signed_date_time";
        String LOCALE = "locale";
        String PAYMENT_METHOD = "payment_method";
        String TRANSACTION_TYPE = "transaction_type";
        String REFERENCE_NUMBER = "reference_number";
        String AMOUNT = "amount";
        String CURRENCY = "currency";
        String BILL_TO_FORENAME = "bill_to_forename";
        String BILL_TO_SURNAME = "bill_to_surname";
        String BILL_TO_EMAIL = "bill_to_email";
        String BILL_TO_ADDRESS_COUNTRY = "bill_to_address_country";
        String BILL_TO_ADDRESS_STATE = "bill_to_address_state";
        String BILL_TO_ADDRESS_CITY = "bill_to_address_city";
        String BILL_TO_ADDRESS_LINE1 = "bill_to_address_line1";
        String BILL_TO_ADDRESS_LINE2 = "bill_to_address_line2";
        String BILL_TO_ADDRESS_POSTAL_CODE = "bill_to_address_postal_code";
        String OVERRIDE_CUSTOM_RECEIPT_PAGE = "override_custom_receipt_page";
        String OVERRIDE_CUSTOM_MERCHANT_POST_PAGE = "override_backoffice_post_url";
        String SIGNATURE = "signature";
        String CARD_TYPE = "card_type";
        String CARD_NUMBER = "card_number";
        String CARD_EXPIRE_DATE = "card_expiry_date";
        String PARTNER_SOLUTION_ID = "partner_solution_id";
        String DEVELOPER_ID = "developer_id";
        String MERCHANT_DEFINED_DATA_99 = "merchant_defined_data99";
        String MERCHANT_DEFINED_DATA_100 = "merchant_defined_data100";
    }
}
