package com.cybersource.payment.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * An utility class responsible for dealing with date time conversion specifics
 */
public final class TimeUtils
{
    private static final String CONVERSION_REPORT_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";

    private static final String DATE_TIME_DELIMITER = "T";

    /**
     * Converts a date into UTC format like: yyyy-MM-dd'T'HH:mm:ss'Z'
     *
     * @param date value which should be converted
     * @return a string value in UTC format
     */
    public static String toUTCDateTime(final Date date)
    {
        final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        formatter.setTimeZone(TimeZone.getTimeZone("UTC"));

        return formatter.format(date);
    }

    /**
     * Converts a date into Cybersource specific date format like: yyyy/MM/dd,
     * which is used in some reporting queries
     *
     * @param date value which should be converted
     * @return a string value in yyyy/MM/dd format
     */
    public static String toCybersourceReportDate(final Date date)
    {
        return new SimpleDateFormat("yyyy/MM/dd").format(date);
    }

    /**
     * Converts a date into Cybersource specific date format like: yyyyMMdd,
     * which is used in single transaction reporting queries
     *
     * @param date value which should be converted
     * @return a string value in yyyyMMdd format
     */
    public static String toCybersourceSingleTransactionDate(final Date date)
    {
        return new SimpleDateFormat("yyyyMMdd").format(date);
    }

    /**
     * Converts a date into specific date only format like: yyyy-MM-dd
     *
     * @param date value which should be converted
     * @return a string value in yyyy-MM-dd format
     */
    public static String toConversionReportDate(final Date date)
    {
        return utcFormatter(CONVERSION_REPORT_DATE_FORMAT).format(date).split(DATE_TIME_DELIMITER)[0];
    }

    /**
     * Converts a date into specific time only format like: HH:mm:ss
     *
     * @param date value which should be converted
     * @return a string value in HH:mm:ss format
     */
    public static String toConversionReportTime(final Date date)
    {
        return utcFormatter(CONVERSION_REPORT_DATE_FORMAT).format(date).split(DATE_TIME_DELIMITER)[1];
    }

    private static SimpleDateFormat utcFormatter(final String format)
    {
        final SimpleDateFormat formatter = new SimpleDateFormat(format);
        formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
        return formatter;
    }
}
