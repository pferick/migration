package com.cybersource.payment.service.executor.request.builder.creditcard;

import com.cybersource.payment.enums.CybsPaymentType;
import com.cybersource.payment.service.executor.request.builder.PaymentServiceRequestBuilder;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.payment.enums.PaymentTransactionType;

/**
 * Builder for payment validate request.
 */
public class ValidateRequestBuilder extends PaymentServiceRequestBuilder<EnrollmentRequestBuilder>
{
    /**
     * Creates a new instance of {@link ValidateRequestBuilder} class with method CREDIT_CARD and
     * service VALIDATE.
     *
     * @see CybsPaymentType
     * @see PaymentTransactionType
     */
    public ValidateRequestBuilder()
    {
        paymentServiceRequest.method(CybsPaymentType.CREDIT_CARD);
        paymentServiceRequest.service(PaymentTransactionType.VALIDATE);
    }

    public ValidateRequestBuilder setOrder(final AbstractOrderModel order)
    {
        paymentServiceRequest.addParam("order", order);
        return this;
    }

    public ValidateRequestBuilder setPARes(final String paRes)
    {
        paymentServiceRequest.addParam("paRes", paRes);
        return this;
    }
}
