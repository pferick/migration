package com.cybersource.payment.dao;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.order.daos.OrderDao;

/**
 * This class is intended to extend the OOTB OrderDao
 */
public interface PaymentOrderDao extends OrderDao
{
    /**
     * Find order instance by guid, which is an unique identifier
     *
     * @param guid order unique identifier
     * @return an order instance
     */
    OrderModel findOrderByGuid(String guid);
}
