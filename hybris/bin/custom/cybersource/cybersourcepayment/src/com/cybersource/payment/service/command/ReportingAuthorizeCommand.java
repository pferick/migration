package com.cybersource.payment.service.command;

import com.cybersource.payment.constants.CybersourcePaymentConstants.ReasonCode;
import com.cybersource.payment.model.CybsPaymentTransactionModel;
import com.cybersource.payment.report.executor.ReportExecutor;
import com.cybersource.payment.report.request.SingleTransactionReportRequest;
import com.cybersource.payment.service.request.CybersourceRequest;
import com.cybersource.payment.service.response.CybersourceResponse;
import com.cybersource.payment.service.response.DefaultCybersourceResponse;
import com.cybersource.reporting.request.value.TransactionQuerySubType;
import com.cybersource.reporting.request.value.TransactionQueryType;
import com.cybersource.reports.transaction.detail.ApplicationReply;
import com.cybersource.reports.transaction.detail.Report;
import com.cybersource.reports.transaction.detail.Request;
import de.hybris.platform.core.model.order.OrderModel;
import ma.glasnost.orika.MapperFacade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

/*
*  A concrete implementation of {@link PaymentCommand} which retrieves
*  authorization transaction through single transaction report invocation.
*/
public class ReportingAuthorizeCommand implements PaymentCommand
{
    private static final String REPORT_VERSION = "1.7";

    private static final String ORDER_PARAMETER_NAME = "order";

    private static final String PAYMENT_TRANSACTION_PARAMETER_NAME = "paymentTransaction";

    private interface ApplicationReplyConstants
    {
        String ICS_AUTH_REPLY_NAME = "ics_auth";
        String SUCCESSFUL_AUTH_REPLY_CODE = "1";
        String SUCCESSFUL_AUTH_REPLY_FLAG = "SOK";
    }

    private static final Logger LOG = LoggerFactory.getLogger(ReportingAuthorizeCommand.class);

    @Resource
    private ReportExecutor reportExecutor;

    private MapperFacade responseMapper;

    @Override
    public CybersourceResponse perform(final CybersourceRequest request)
    {
        final Report downloadedReport = downloadSingleTransactionReport(request);
        final CybersourceResponse response = createCybersourceResponse();

        final Request authorizationTransaction = getRemoteAuthorizationTransaction(downloadedReport.getRequests().getRequest());
        if (authorizationTransaction != null)
        {
            responseMapper.map(authorizationTransaction, response);
            response.addResponseField("reasonCode", ReasonCode.OK);
        }
        else
        {
            final OrderModel order = getRequestParameter(request, ORDER_PARAMETER_NAME);
            LOG.warn("Order [{}] was placed after a 104 reason code but no successful authorizations can be found.", order.getGuid());
        }
        return response;
    }

    private Report downloadSingleTransactionReport(final CybersourceRequest request)
    {
        final OrderModel order = getRequestParameter(request, ORDER_PARAMETER_NAME);
        final CybsPaymentTransactionModel paymentTransaction = getRequestParameter(request, PAYMENT_TRANSACTION_PARAMETER_NAME);

        final SingleTransactionReportRequest reportRequest = buildReportRequest(order, paymentTransaction);
        return reportExecutor.getReport(reportRequest);
    }

    private SingleTransactionReportRequest buildReportRequest(final OrderModel order,
                                                              final CybsPaymentTransactionModel existingTransaction)
    {
        final SingleTransactionReportRequest reportRequest = new SingleTransactionReportRequest();

        reportRequest.setMerchantID(existingTransaction.getMerchantId());
        reportRequest.setMerchantReferenceNumber(order.getGuid());
        reportRequest.setTargetDate(order.getDate());
        reportRequest.setType(TransactionQueryType.TRANSACTION);
        reportRequest.setSubtype(TransactionQuerySubType.TRANSACTION_DETAIL);
        reportRequest.setVersionNumber(REPORT_VERSION);

        return reportRequest;
    }

    private Request getRemoteAuthorizationTransaction(final List<Request> requestList)
    {
        final List<Request> authorizeTransactions = requestList.stream().filter(
                this::isSuccessfulAuthorizationTranazation).collect(Collectors.toList());

        return authorizeTransactions.isEmpty() ? null : authorizeTransactions.get(authorizeTransactions.size() - 1);

    }

    private boolean isSuccessfulAuthorizationTranazation(final Request request) {
        return request.getApplicationReplies().getApplicationReply().stream()
                .anyMatch(this::isSuccessfulAuthorizationReply);
    }

    @SuppressWarnings("unchecked")
    private <T> T getRequestParameter(final CybersourceRequest request, final String parameterName)
    {
        return (T) request.getRequestFields().get(parameterName);
    }

    private boolean isSuccessfulAuthorizationReply(final ApplicationReply applicationReply)
    {
        return ApplicationReplyConstants.ICS_AUTH_REPLY_NAME.equals(applicationReply.getName())
                && ApplicationReplyConstants.SUCCESSFUL_AUTH_REPLY_CODE.equals(applicationReply.getRCode())
                && ApplicationReplyConstants.SUCCESSFUL_AUTH_REPLY_FLAG.equals(applicationReply.getRFlag());
    }

    protected CybersourceResponse createCybersourceResponse()
    {
        return new DefaultCybersourceResponse();
    }

    @Required
    public void setResponseMapper(final MapperFacade responseMapper)
    {
        this.responseMapper = responseMapper;
    }
}
