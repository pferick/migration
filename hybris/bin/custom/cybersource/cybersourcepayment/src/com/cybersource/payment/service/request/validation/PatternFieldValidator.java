package com.cybersource.payment.service.request.validation;

import org.apache.commons.lang.StringUtils;

import java.util.Map;

/*
 * A concrete implementation of {@link CybersourceFieldValidator} which validates field value
 * based on a pattern.
 */
public class PatternFieldValidator implements CybersourceFieldValidator
{
    private static final String PATTERN_RULE = "pattern";

    @Override
    public boolean validate(final String fieldValue, final Map<String, String> rules)
    {
        final String pattern = rules.get(PATTERN_RULE);
        return StringUtils.isEmpty(pattern) || (fieldValue != null && fieldValue.matches(pattern));
    }
}
