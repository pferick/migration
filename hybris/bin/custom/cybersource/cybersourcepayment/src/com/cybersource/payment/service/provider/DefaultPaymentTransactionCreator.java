package com.cybersource.payment.service.provider;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.payment.strategy.TransactionCodeGenerator;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.time.TimeService;

import java.math.BigDecimal;
import java.util.Map;
import java.util.stream.Collectors;
import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;

import com.cybersource.payment.model.CybsPaymentTransactionEntryModel;
import com.cybersource.payment.model.CybsPaymentTransactionModel;
import com.cybersource.payment.service.response.CybersourceResponse;
import com.cybersource.payment.service.executor.request.PaymentServiceRequest;
import com.google.common.collect.Maps;

public class DefaultPaymentTransactionCreator implements PaymentTransactionCreator
{
    @Resource
    private ModelService modelService;

    @Resource
    private TransactionCodeGenerator transactionCodeGenerator;

    @Resource
    private TimeService timeService;

    @Resource
    private CommonI18NService commonI18NService;

    @Override
    public CybsPaymentTransactionEntryModel createTransactionEntry(final PaymentServiceRequest request,
            final CybersourceResponse cybersourceResponse)
    {
        final PaymentTransactionModel transaction = getOrCreatePaymentTransaction(request, cybersourceResponse);

        return createTransactionEntry(transaction, request, cybersourceResponse);
    }

    private PaymentTransactionModel getOrCreatePaymentTransaction(final PaymentServiceRequest request,
            final CybersourceResponse response)
    {
        final AbstractOrderModel order = request.getRequiredParam("order");

        return order.getPaymentTransactions().stream()
                .filter(t -> StringUtils.equals(request.getPaymentType().name(), t.getPaymentProvider()))
                .findFirst()
                .orElseGet(() -> createTransaction(request, order, response));
    }

    private PaymentTransactionModel createTransaction(final PaymentServiceRequest request,
            final AbstractOrderModel order, final CybersourceResponse cybersourceResponse)
    {
        final CybsPaymentTransactionModel transaction = modelService.create(CybsPaymentTransactionModel.class);
        transaction.setCode(transactionCodeGenerator.generateCode(order.getCode()));
        transaction.setMerchantId(request.getParam("merchantId"));
        transaction.setRequestId(cybersourceResponse.getResponseField("requestID"));
        transaction.setRequestToken(cybersourceResponse.getResponseField("requestToken"));
        transaction.setPaymentProvider(request.getPaymentType().name());
        transaction.setOrder(order);

        modelService.saveAll(transaction, order);

        return transaction;
    }

    private CybsPaymentTransactionEntryModel createTransactionEntry(final PaymentTransactionModel transaction,
                                                                    final PaymentServiceRequest request,
                                                                    final CybersourceResponse cybersourceResponse)
    {
        final CybsPaymentTransactionEntryModel entry = modelService.create(CybsPaymentTransactionEntryModel.class);
        entry.setCode(getEntryCode(transaction));
        entry.setPaymentTransaction(transaction);
        entry.setRequestId(cybersourceResponse.getResponseField("requestID"));
        entry.setRequestToken(cybersourceResponse.getResponseField("requestToken"));
        entry.setType(request.getPaymentTransactionType());
        entry.setTransactionStatus(cybersourceResponse.getResponseField("decision"));
        entry.setTime(timeService.getCurrentTime());

        final String reasonCode = String.valueOf((Object) cybersourceResponse.getResponseField("reasonCode"));
        entry.setTransactionStatusDetails(reasonCode);

        final String currencyCode = cybersourceResponse.getResponseField("purchaseTotalsCurrency");
        entry.setCurrency(currencyCode != null ? commonI18NService.getCurrency(currencyCode) : null);

        final String amount = cybersourceResponse.getResponseField("amount");
        entry.setAmount(amount != null ? new BigDecimal(amount) : null);

        entry.setProperties(getProperties(cybersourceResponse.getResponseFields()));
        entry.setSubscriptionID(cybersourceResponse.getResponseField("paySubscriptionCreateReplySubscriptionID"));

        modelService.saveAll(entry, transaction);

        return entry;
    }

    private Map<String, String> getProperties(final Map<String, Object> response)
    {
        return response.entrySet().stream()
                .filter(e -> e.getValue() != null)
                .map(e -> Maps.immutableEntry(e.getKey(), e.getValue().toString()))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }

    private String getEntryCode(final PaymentTransactionModel transaction)
    {
        return String.format("%s-%d", transaction.getCode(), transaction.getEntries().size() + 1);
    }
}
