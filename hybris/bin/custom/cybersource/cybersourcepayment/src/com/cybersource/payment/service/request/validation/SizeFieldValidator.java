package com.cybersource.payment.service.request.validation;

import java.util.Map;

import org.apache.commons.lang.StringUtils;

public class SizeFieldValidator implements CybersourceFieldValidator
{
    private static final String SIZE_RULE = "size";

    @Override
    public boolean validate(final String fieldValue, final Map<String, String> rules)
    {
        final String sizeRule = rules.get(SIZE_RULE);
        final Integer size = StringUtils.isNotEmpty(sizeRule) ? Integer.valueOf(sizeRule) : 0;
        return size == 0 || StringUtils.length(fieldValue) <= size;
    }
}
