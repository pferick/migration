package com.cybersource.payment.configuration.resolver;

import com.cybersource.payment.utils.Assert;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import org.springframework.beans.factory.annotation.Required;

import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

import static org.apache.commons.collections.CollectionUtils.isNotEmpty;

/**
 * Base implementation for configuration lookup
 *
 * @param <T> configuration value type
 */
public abstract class AbstractPaymentConfigurationResolver<T> implements PaymentConfigurationResolver<T>
{
    private FlexibleSearchService flexibleSearchService;

    public abstract String getSearchQuery(final Map<String, Object> params);

    @Override
    public T resolve(final Map<String, Object> params)
    {
        final FlexibleSearchQuery query = new FlexibleSearchQuery(getSearchQuery(params), params);
        final List<T> result = getFlexibleSearchService().<T>search(query).getResult();

        Assert.isTrue(isNotEmpty(result), () -> new ModelNotFoundException("No result for the given query."));
        Assert.isTrue(result.size() == 1, () -> new AmbiguousIdentifierException(
                "More than one result found for given query."));

        return result.get(0);
    }

    private void assertResult(final boolean expression, final Supplier<RuntimeException> exception)
    {
        if (expression)
        {
            throw exception.get();
        }
    }

    public FlexibleSearchService getFlexibleSearchService()
    {
        return flexibleSearchService;
    }

    @Required
    public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService)
    {
        this.flexibleSearchService = flexibleSearchService;
    }
}
