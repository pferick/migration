package com.cybersource.payment.service.executor.request.converter;

import com.cybersource.payment.service.executor.request.PaymentServiceRequest;
import com.cybersource.payment.service.request.CybersourceRequest;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import org.springframework.util.Assert;

import java.math.BigDecimal;

public class AuthorizeReversalRequestConverter extends AbstractRequestConverter
{
    @Override
    public CybersourceRequest convert(final PaymentServiceRequest source)
    {
        final AbstractOrderModel order = source.getRequiredParam("order");

        final PaymentTransactionEntryModel authorization = getAuthorization(source);
        final CurrencyModel currency = order.getCurrency();

        Assert.notNull(currency, String.format("The currency property is missing on order [%s]", order.getCode()));

        return cybersourceRequestFactory.authorizeReversal()
                .merchantId(source.getRequiredParam("merchantId"))
                .merchantReferenceCode(order.getGuid())
                .ccAuthReversalServiceAuthRequestID(authorization.getRequestId())
                .ccAuthReversalServiceRun(true)
                .purchaseTotalsCurrency(currency.getIsocode())
                .purchaseTotalsGrandTotalAmount(BigDecimal.valueOf(order.getTotalPrice()))
                .request();
    }

    private PaymentTransactionEntryModel getAuthorization(final PaymentServiceRequest request)
    {
        final PaymentTransactionModel transaction = request.getRequiredParam("transaction");

        return paymentTransactionService
                .getLatestAcceptedTransactionEntry(transaction, PaymentTransactionType.AUTHORIZATION)
                .orElseThrow(() -> new IllegalStateException("order authorization transaction entry not found"));
    }
}
