package com.cybersource.payment.hystrix;

import de.hybris.platform.core.Registry;
import de.hybris.platform.core.Tenant;

import com.netflix.hystrix.HystrixCommand;

/**
 * Abstract Hystrix command to activate the tenant in the current thread before execution.
 */
public abstract class TenantAwareHystrixCommand<R> extends HystrixCommand<R>
{
    private final Tenant tenant;

    protected TenantAwareHystrixCommand(final Setter setter)
    {
        super(setter);
        this.tenant = Registry.getCurrentTenantNoFallback();
    }

    @Override
    protected R run() throws Exception
    {
        if (Registry.hasCurrentTenant() && Registry.isCurrentTenant(tenant))
        {
            return runCommand();
        }

        Registry.setCurrentTenant(tenant);

        try
        {
            return runCommand();
        }
        finally
        {
            Registry.unsetCurrentTenant();
        }
    }

    /**
     * Implement this method with code to be executed when {@link #execute()} or {@link #queue()} are invoked.
     *
     * @return R command execution result
     * @throws Exception if command execution fails
     */
    protected abstract R runCommand() throws Exception;
}
