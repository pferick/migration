package com.cybersource.payment.report.provider;

import com.cybersource.payment.report.request.SingleTransactionReportRequest;
import com.cybersource.reports.transaction.detail.Report;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.beans.factory.annotation.Required;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import static com.cybersource.payment.utils.TimeUtils.toCybersourceSingleTransactionDate;
import static org.apache.commons.lang.StringUtils.EMPTY;

/**
 * This class is responsible for obtaining single transaction report
 */
public class SingleTransactionReportProvider extends AbstractHttpReportProvider<SingleTransactionReportRequest, Report>
{
    private String reportUrl;

    @Override
    protected HttpUriRequest buildRequest(final SingleTransactionReportRequest request)
    {
        final HttpPost httpPost = new HttpPost(reportUrl);
        httpPost.setEntity(new UrlEncodedFormEntity(httpRequestParams(request)));
        return httpPost;
    }

    private Collection<BasicNameValuePair> httpRequestParams(final SingleTransactionReportRequest request)
    {
        final Collection<BasicNameValuePair> params = new ArrayList<>();

        params.add(new BasicNameValuePair("merchantID", request.getMerchantID()));
        params.add(new BasicNameValuePair("requestID", request.getRequestID()));
        params.add(new BasicNameValuePair("merchantReferenceNumber", request.getMerchantReferenceNumber()));

        final Date dt = request.getTargetDate();
        params.add(new BasicNameValuePair("targetDate", dt == null ? EMPTY : toCybersourceSingleTransactionDate(dt)));

        params.add(new BasicNameValuePair("type", request.getType().getName()));
        params.add(new BasicNameValuePair("subtype", request.getSubtype().getName()));
        params.add(new BasicNameValuePair("versionNumber", request.getVersionNumber()));

        return params;
    }

    @Required
    public void setReportUrl(final String reportUrl)
    {
        this.reportUrl = reportUrl;
    }
}
