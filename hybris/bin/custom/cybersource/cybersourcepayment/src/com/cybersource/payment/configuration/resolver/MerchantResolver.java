package com.cybersource.payment.configuration.resolver;

import com.cybersource.payment.model.CybsMerchantModel;

import java.util.Map;

/**
 * This class is responsible for retrieving merchant configuration
 */
public class MerchantResolver extends AbstractPaymentConfigurationResolver<CybsMerchantModel>
{
    private static final String FIND_MERCHANT_BY_ID = "SELECT {pk} FROM {" + CybsMerchantModel._TYPECODE + "} WHERE {id} = ?id";

    @Override
    public String getSearchQuery(final Map<String, Object> params)
    {
        return FIND_MERCHANT_BY_ID;
    }
}
