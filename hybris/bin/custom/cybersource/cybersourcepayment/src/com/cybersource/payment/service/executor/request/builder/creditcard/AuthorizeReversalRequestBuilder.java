package com.cybersource.payment.service.executor.request.builder.creditcard;

import com.cybersource.payment.service.executor.request.builder.PaymentServiceRequestBuilder;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionModel;

import com.cybersource.payment.enums.CybsPaymentType;

/**
 * Builder for payment authorization reversal request.
 */
public class AuthorizeReversalRequestBuilder extends PaymentServiceRequestBuilder<AuthorizeReversalRequestBuilder>
{
    /**
     * Creates a new instance of {@link AuthorizeReversalRequestBuilder} class with method CREDIT_CARD and
     * service AUTHORIZATION_REVERSAL.
     *
     * @see CybsPaymentType
     * @see PaymentTransactionType
     */
    public AuthorizeReversalRequestBuilder()
    {
        paymentServiceRequest.method(CybsPaymentType.CREDIT_CARD);
        paymentServiceRequest.service(PaymentTransactionType.AUTHORIZATION_REVERSAL);
    }

    public AuthorizeReversalRequestBuilder setOrder(final AbstractOrderModel order)
    {
        paymentServiceRequest.addParam("order", order);
        return this;
    }

    public AuthorizeReversalRequestBuilder setTransaction(final PaymentTransactionModel transaction)
    {
        paymentServiceRequest.addParam("transaction", transaction);
        return this;
    }
}
