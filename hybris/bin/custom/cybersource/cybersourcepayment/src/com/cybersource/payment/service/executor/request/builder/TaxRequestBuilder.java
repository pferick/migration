package com.cybersource.payment.service.executor.request.builder;

import com.cybersource.payment.enums.CybsPaymentType;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.payment.enums.PaymentTransactionType;


/**
 * Builder for payment tax request.
 */
public class TaxRequestBuilder extends PaymentServiceRequestBuilder<TaxRequestBuilder>
{
    /**
     * Creates a new instance of {@link TaxRequestBuilder} class with method TAX_CALCULATION and
     * service TAX.
     *
     * @see CybsPaymentType
     * @see PaymentTransactionType
     */
    public TaxRequestBuilder()
    {
        paymentServiceRequest.method(CybsPaymentType.TAX_CALCULATION);
        paymentServiceRequest.service(PaymentTransactionType.TAX);
    }

    public TaxRequestBuilder setOrder(final AbstractOrderModel order)
    {
        paymentServiceRequest.addParam("order", order);
        return this;
    }
}