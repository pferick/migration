package com.cybersource.payment.security.service;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;
import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

import com.cybersource.payment.constants.CybersourcePaymentConstants;

public class DefaultCybersourceSAService implements CybersourceSAService
{
    private static final Logger LOG = LoggerFactory.getLogger(DefaultCybersourceSAService.class);

    private static final String FIELD_SEPARATOR = ",";

    private static final String FIELD_VALUE_SEPARATOR = "=";

    @Resource
    private EncryptionService encryptionService;

    @Override
    public String getDigest(final Map<String, String> paymentData, final String secretKey)
    {
        Assert.notNull(paymentData, "payment data map must not be null");
        Assert.notNull(secretKey, "secret key must be provided for Cybersource SA signature");

        try
        {
            LOG.debug("Payment data used for signing the request {}", paymentData);

            final String dataToSign = buildPaymentDataToSign(paymentData);
            return encryptionService.signForSecureAcceptance(dataToSign, secretKey);
        }
        catch (final Exception e)
        {
            LOG.error("Error while creating payment data signature", e);
            return StringUtils.EMPTY;
        }
    }

    @Override
    public boolean validateTransactionSignature(final Map<String, String> paymentData, final String secretKey)
    {
        Assert.notNull(paymentData, "payment data map must not be null");
        Assert.notNull(secretKey, "secret key must be provided for Cybersource SA signature");

        final String cybersourceSignature = paymentData.get(CybersourcePaymentConstants.SARequestFields.SIGNATURE);
        if (cybersourceSignature == null)
        {
            LOG.debug("transaction signature field is null, transaction signature is invalid");
            return false;
        }

        final String signature = getDigest(paymentData, secretKey);

        LOG.debug("Comparing signatures, Cybersource signature is:'{}', generated signature:'{}'", cybersourceSignature, signature);
        return cybersourceSignature.equals(signature);
    }

    private String buildPaymentDataToSign(final Map<String, String> paymentData)
    {
        final String signedFieldNamesValue = paymentData.get(CybersourcePaymentConstants.SARequestFields.SIGNED_FIELD_NAMES);

        final String[] signedFieldNames = StringUtils.split(signedFieldNamesValue, FIELD_SEPARATOR);

        return Arrays.stream(signedFieldNames)
                .map(field -> field + FIELD_VALUE_SEPARATOR + String.valueOf(paymentData.get(field)))
                .collect(Collectors.joining(FIELD_SEPARATOR));
    }
}
