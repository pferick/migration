package com.cybersource.payment.utils;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.XMLFilterImpl;

import static org.apache.commons.lang.StringUtils.EMPTY;

/**
 * This class is intended to support xml file processing where namespace is empty or is not provided
 */
public class XMLNamespaceFilter extends XMLFilterImpl
{
    private static final String NAMESPACE = EMPTY;

    @Override
    public void endElement(final String uri, final String localName, final String qName) throws SAXException
    {
        super.endElement(NAMESPACE, localName, qName);
    }

    @Override
    public void startElement(final String uri, final String localName, final String qName, final Attributes atts)
            throws SAXException
    {
        super.startElement(NAMESPACE, localName, qName, atts);
    }
}