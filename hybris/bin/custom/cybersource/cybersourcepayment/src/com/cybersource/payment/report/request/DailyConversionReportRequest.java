package com.cybersource.payment.report.request;

import java.util.Date;

/**
 * Defines request object which contains params for querying daily conversion report
 */
public class DailyConversionReportRequest
{
    private String merchantId;

    private Date date;

    public String getMerchantId()
    {
        return merchantId;
    }

    public void setMerchantId(final String merchantId)
    {
        this.merchantId = merchantId;
    }

    public Date getDate()
    {
        return date;
    }

    public void setDate(final Date date)
    {
        this.date = date;
    }

    @Override
    public String toString()
    {
        return "DailyConversionReportRequest{" + "merchantId='" + merchantId + '\'' + ", date=" + date + '}';
    }
}
