package com.cybersource.payment.security.ws;

import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;

/**
 * Defines an interface responsible for obtaining merchant identity from SOAP message
 */
public interface WSSMerchantResolver
{
    /**
     * Obtains merchant identity from SOAP message
     * @param soapEnvelope object which contains merchant id
     * @return a merchantId as string
     * @throws SOAPException
     */
    String getMerchantId(final SOAPEnvelope soapEnvelope) throws SOAPException;
}
