/**
 * Contains definition of application exceptions used by Cybersource extension
 */
package com.cybersource.payment.exception;