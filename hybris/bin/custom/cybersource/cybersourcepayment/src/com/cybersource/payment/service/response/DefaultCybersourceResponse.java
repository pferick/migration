package com.cybersource.payment.service.response;

import java.util.HashMap;
import java.util.Map;

/**
 * Default implementation of Cybersource reponse (backed by map).
 */
public class DefaultCybersourceResponse implements CybersourceResponse
{
    private final Map<String, Object> responseFields = new HashMap<>();

    @Override
    public void addResponseField(final String name, final Object value)
    {
        responseFields.put(name, value);
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> T getResponseField(final String name)
    {
        return (T) responseFields.get(name);
    }

    @Override
    public Map<String, Object> getResponseFields()
    {
        return responseFields;
    }

    @Override
    public String toString()
    {
        return "DefaultCybersourceResponse{ responseFields=" + responseFields.keySet() + '}';
    }
}
