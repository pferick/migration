package com.cybersource.payment.httpclient;

import org.apache.http.auth.AuthScope;
import org.apache.http.auth.Credentials;
import org.apache.http.impl.client.BasicCredentialsProvider;

/**
 * Responsible for creating credential provider
 */
public class CredentialsProviderFactory
{
    private AuthScope authScope;

    private Credentials credentials;

    /**
     * Creates credential provider based on specified authScope and credentials
     *
     * @return BasicCredentialsProvider instance
     */
    public BasicCredentialsProvider create()
    {
        final BasicCredentialsProvider provider = new BasicCredentialsProvider();
        provider.setCredentials(authScope, credentials);

        return provider;
    }

    public void setAuthScope(final AuthScope authScope)
    {
        this.authScope = authScope;
    }

    public void setCredentials(final Credentials credentials)
    {
        this.credentials = credentials;
    }
}
