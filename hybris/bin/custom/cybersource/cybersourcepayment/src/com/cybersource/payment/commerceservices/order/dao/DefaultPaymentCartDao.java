package com.cybersource.payment.commerceservices.order.dao;

import de.hybris.platform.commerceservices.order.dao.impl.DefaultCommerceCartDao;
import de.hybris.platform.core.model.order.CartModel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;

public class DefaultPaymentCartDao extends DefaultCommerceCartDao implements PaymentCartDao
{
    private final static String FIND_CART_FOR_GUID = SELECTCLAUSE + "WHERE {" + CartModel.GUID + "} = ?guid ";

    @Override
    public CartModel getCartForGuid(final String guid)
    {
        final Map<String, Object> params = new HashMap<>();
        params.put("guid", guid);

        final List<CartModel> carts = doSearch(FIND_CART_FOR_GUID, params, CartModel.class);
        return CollectionUtils.isNotEmpty(carts) ? carts.get(0) : null;
    }
}
