package com.cybersource.payment.service.provider;

import com.cybersource.payment.model.CybsPaymentTransactionEntryModel;
import com.cybersource.payment.service.executor.request.PaymentServiceRequest;
import com.cybersource.payment.service.response.CybersourceResponse;

/*
 * Defines a component responsible for payment transaction entry creation.
 */
public interface PaymentTransactionCreator
{
    /**
     * Create and populate payment transaction entry with cybersource response data
     * for existing or new requested order payment transaction.
     *
     * @param request service request params
     * @param response cybersource response
     * @return created transaction entry
     */
    CybsPaymentTransactionEntryModel createTransactionEntry(final PaymentServiceRequest request,
            final CybersourceResponse response);
}
