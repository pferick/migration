package com.cybersource.payment.data.mapper;

import com.google.common.base.Splitter;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.Assert;

import java.util.List;

/**
 * This class defines mapping field properties, which are used in defining a mapper instance
 */
public class FieldInfo
{
    private String name;

    private String converter;

    public FieldInfo(final String name, final String converter)
    {
        this.name = name;
        this.converter = converter;
    }

    public String getName()
    {
        return name;
    }

    public String getConverter()
    {
        return converter;
    }

    public boolean hasConverterId()
    {
        return StringUtils.isNotEmpty(converter);
    }

    public static FieldInfo parse(final String expression)
    {
        Assert.isTrue(StringUtils.isNotEmpty(expression), "Field is required param");

        final List<String> values = Splitter.on("|").splitToList(expression);

        return new FieldInfo(values.get(0), values.size() > 1 ? values.get(1): null);
    }
}