package com.cybersource.payment.service.executor.request.converter;

import com.cybersource.payment.service.executor.request.PaymentServiceRequest;
import com.cybersource.payment.service.request.CybersourceRequest;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;

import static com.cybersource.payment.constants.CybersourcePaymentConstants.TransactionStatus;

/*
* A component that encapsulates conversion logic from {@link PaymentServiceRequest} to
* {@link CybersourceRequest} for payment capture request.
*/
public class CaptureRequestConverter extends AbstractRequestConverter
{
    @Override
    public CybersourceRequest convert(final PaymentServiceRequest source)
    {
        final AbstractOrderModel order = source.getRequiredParam("order");

        final PaymentTransactionEntryModel authorization = getAuthorization(source);

        return cybersourceRequestFactory.capture()
                .merchantId(source.getRequiredParam("merchantId"))
                .merchantReferenceCode(order.getGuid())
                .ccCaptureServiceRun(true)
                .ccCaptureServiceAuthRequestID(authorization.getRequestId())
                .purchaseTotalsCurrency(authorization.getCurrency().getIsocode())
                .ccCaptureServiceAuthRequestToken(authorization.getRequestToken())
                .purchaseTotalsGrandTotalAmount(authorization.getAmount())
                .request();
    }

    private PaymentTransactionEntryModel getAuthorization(final PaymentServiceRequest request)
    {
        final PaymentTransactionModel transaction = request.getRequiredParam("transaction");

        return paymentTransactionService.getLatestTransactionEntry(transaction, PaymentTransactionType.AUTHORIZATION,
                TransactionStatus.ACCEPT, TransactionStatus.REVIEW)
                .orElseThrow(() -> new IllegalStateException("order authorization transaction entry not found"));
    }
}
