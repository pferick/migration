package com.cybersource.payment.service.executor.request.builder.creditcard;

import com.cybersource.payment.service.executor.request.builder.PaymentServiceRequestBuilder;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionModel;

import com.cybersource.payment.enums.CybsPaymentType;

/**
 * Builder for payment token create request (Subscription).
 */
public class PaymentTokenCreateRequestBuilder extends PaymentServiceRequestBuilder<PaymentTokenCreateRequestBuilder>
{
    /**
     * Creates a new instance of {@link PaymentTokenCreateRequestBuilder} class with method CREDIT_CARD and
     * service CREATE_SUBSCRIPTION.
     *
     * @see CybsPaymentType
     * @see PaymentTransactionType
     */
    public PaymentTokenCreateRequestBuilder()
    {
        paymentServiceRequest.method(CybsPaymentType.CREDIT_CARD);
        paymentServiceRequest.service(PaymentTransactionType.CREATE_SUBSCRIPTION);
    }

    public PaymentTokenCreateRequestBuilder setOrder(final AbstractOrderModel order)
    {
        paymentServiceRequest.addParam("order", order);
        return this;
    }

    public PaymentTokenCreateRequestBuilder setTransaction(final PaymentTransactionModel transaction)
    {
        paymentServiceRequest.addParam("transaction", transaction);
        return this;
    }
}
