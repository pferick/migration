package com.cybersource.payment.service.executor.request.converter;

import com.cybersource.payment.service.executor.request.PaymentServiceRequest;
import com.cybersource.payment.service.request.CybersourceRequest;
import com.cybersource.payment.service.request.RefundStandaloneType;
import com.cybersource.payment.service.request.value.CardType;
import com.cybersource.payment.utils.PaymentParamUtils;
import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.payment.dto.CardInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.Assert;

import java.math.BigDecimal;
import java.util.Optional;

/*
* A component that encapsulates conversion logic from {@link PaymentServiceRequest} to
* {@link CybersourceRequest} for payment refund standalone request.
*/
public class RefundStandaloneRequestConverter extends AbstractRequestConverter
{
    @Override
    public CybersourceRequest convert(final PaymentServiceRequest source)
    {
        final AbstractOrderModel order = source.getRequiredParam("order");

        final RefundStandaloneType.Optional requestBuilder = addCommonFields(source, order);

        Optional.ofNullable(order.getPaymentAddress())
                .ifPresent(address -> addBillingAddress(address, requestBuilder));

        addCardInfoFields(source, requestBuilder);

        return requestBuilder.request();
    }

    private RefundStandaloneType.Optional addCommonFields(final PaymentServiceRequest request, final AbstractOrderModel order)
    {
        return cybersourceRequestFactory.refundStandalone()
                .merchantId(request.getRequiredParam("merchantId"))
                .merchantReferenceCode(order.getGuid())
                .ccCreditServiceRun(true)
                .purchaseTotalsCurrency(order.getCurrency().getIsocode())
                .purchaseTotalsGrandTotalAmount(BigDecimal.valueOf(order.getTotalPrice()));
    }

    private void addCardInfoFields(final PaymentServiceRequest request, final RefundStandaloneType.Optional requestBuilder)
    {
        final CardInfo cardInfo = request.getParam("card");
        final String subscriptionID = request.getParam("subscriptionID");

        Assert.isTrue(cardInfo != null || StringUtils.isNotEmpty(subscriptionID), "one of card info or subscription id must be provided");

        if (StringUtils.isNotEmpty(subscriptionID))
        {
            requestBuilder.recurringSubscriptionInfoSubscriptionID(subscriptionID);
        }
        else
        {
            addCardDetails(cardInfo, requestBuilder);
        }
    }

    private void addCardDetails(final CardInfo cardInfo, final RefundStandaloneType.Optional requestBuilder)
    {
        final CardType cardType = CardType.valueOf(cardInfo.getCardType().toString());

        requestBuilder.cardType(cardType)
                .cardAccountNumber(cardInfo.getCardNumber())
                .cardExpirationMonth(PaymentParamUtils.getMonth(cardInfo.getExpirationMonth()))
                .cardExpirationYear(PaymentParamUtils.toString(cardInfo.getExpirationYear()))
                .cardStartMonth(PaymentParamUtils.getMonth(cardInfo.getIssueMonth()))
                .cardStartYear(PaymentParamUtils.toString(cardInfo.getIssueYear()));
    }

    private RefundStandaloneType.Optional addBillingAddress(final AddressModel billingAddress,
                                                            final RefundStandaloneType.Optional requestBuilder)
    {
        return requestBuilder.billToFirstName(billingAddress.getFirstname())
                .billToLastName(billingAddress.getLastname())
                .billToEmail(billingAddress.getEmail())
                .billToCountry(billingAddress.getCountry().getIsocode())
                .billToCity(billingAddress.getTown())
                .billToPostalCode(billingAddress.getPostalcode())
                .billToState(PaymentParamUtils.getValue(billingAddress.getRegion(), RegionModel::getIsocodeShort))
                .billToStreet1(billingAddress.getStreetnumber())
                .billToStreet2(billingAddress.getStreetname());
    }
}
