package com.cybersource.payment.service.executor.request.builder.creditcard;

import com.cybersource.payment.service.executor.request.builder.PaymentServiceRequestBuilder;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionModel;

import com.cybersource.payment.enums.CybsPaymentType;

/**
 * Builder for payment capture requests.
 */
public class CaptureRequestBuilder extends PaymentServiceRequestBuilder<CaptureRequestBuilder>
{
    /**
     * Creates a new instance of {@link CaptureRequestBuilder} class with method CREDIT_CARD and
     * service CAPTURE.
     *
     * @see CybsPaymentType
     * @see PaymentTransactionType
     */
    public CaptureRequestBuilder()
    {
        paymentServiceRequest.method(CybsPaymentType.CREDIT_CARD);
        paymentServiceRequest.service(PaymentTransactionType.CAPTURE);
    }

    public CaptureRequestBuilder setOrder(final AbstractOrderModel order)
    {
        paymentServiceRequest.addParam("order", order);
        return this;
    }

    public CaptureRequestBuilder setTransaction(final PaymentTransactionModel transaction)
    {
        paymentServiceRequest.addParam("transaction", transaction);
        return this;
    }
}
