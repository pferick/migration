package com.cybersource.payment.configuration.service;

import com.cybersource.payment.configuration.resolver.PaymentConfigurationResolver;
import com.cybersource.payment.enums.CybsConfigurationType;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import java.util.Map;

import static java.util.Collections.emptyMap;

public class DefaultPaymentConfigurationService implements PaymentConfigurationService
{
    private Map<CybsConfigurationType, PaymentConfigurationResolver> resolverMap = emptyMap();

    @Override
    public <T> T getConfiguration(final CybsConfigurationType type, final Map<String, Object> params)
    {
        final PaymentConfigurationResolver resolver = getResolverMap().get(type);

        Assert.notNull(resolver, "No resolver defined for configuration type.");

        return (T) resolver.resolve(params);
    }

    @Required
    public void setResolverMap(final Map<CybsConfigurationType, PaymentConfigurationResolver> resolverMap)
    {
        this.resolverMap = resolverMap;
    }

    public Map<CybsConfigurationType, PaymentConfigurationResolver> getResolverMap()
    {
        return resolverMap;
    }
}
