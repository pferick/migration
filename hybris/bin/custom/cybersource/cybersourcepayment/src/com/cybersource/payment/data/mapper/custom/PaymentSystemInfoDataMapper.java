package com.cybersource.payment.data.mapper.custom;

import javax.annotation.Resource;

import com.cybersource.payment.data.PaymentSystemInfo;
import com.cybersource.payment.service.request.CybersourceRequest;
import com.cybersource.schemas.transaction_data_1.RequestMessage;

import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MappingContext;

/**
 * Add payment system info to the request message
 */
public class PaymentSystemInfoDataMapper extends CustomMapper<CybersourceRequest, RequestMessage>
{
    @Resource
    private PaymentSystemInfo paymentSystemInfo;

    @Override
    public void mapAtoB(final CybersourceRequest request, final RequestMessage requestMessage, final MappingContext context)
    {
        requestMessage.setPartnerSolutionID(paymentSystemInfo.getPartnerSolutionID());
        requestMessage.setDeveloperID(paymentSystemInfo.getDeveloperID());
        requestMessage.setClientApplication(paymentSystemInfo.getClientApplication());
        requestMessage.setClientApplicationVersion(paymentSystemInfo.getClientApplicationVersion());
        requestMessage.setClientEnvironment(paymentSystemInfo.getClientEnvironment());
        requestMessage.setClientLibrary(paymentSystemInfo.getClientLibrary());
        requestMessage.setClientLibraryVersion(paymentSystemInfo.getClientLibraryVersion());
    }
}
