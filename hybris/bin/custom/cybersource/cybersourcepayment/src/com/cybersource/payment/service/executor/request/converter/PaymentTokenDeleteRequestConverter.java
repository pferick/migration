package com.cybersource.payment.service.executor.request.converter;

import com.cybersource.payment.service.executor.request.PaymentServiceRequest;
import com.cybersource.payment.service.request.CybersourceRequest;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;

/*
* A component that encapsulates conversion logic from {@link PaymentServiceRequest} to
* {@link CybersourceRequest} for payment token delete request.
*/
public class PaymentTokenDeleteRequestConverter extends AbstractRequestConverter
{
    @Override
    public CybersourceRequest convert(final PaymentServiceRequest source)
    {
        final AbstractOrderModel order = source.getRequiredParam("order");
        final PaymentTransactionEntryModel transaction = getCreateSubscription(source);

        return cybersourceRequestFactory.paymentTokenDelete()
                .merchantId(source.getRequiredParam("merchantId"))
                .merchantReferenceCode(order.getGuid())
                .recurringSubscriptionInfoSubscriptionID(transaction.getSubscriptionID())
                .paySubscriptionDeleteServiceRun(true)
                .request();
    }

    private PaymentTransactionEntryModel getCreateSubscription(final PaymentServiceRequest request)
    {
        final PaymentTransactionModel transaction = request.getRequiredParam("transaction");

        return paymentTransactionService
                .getLatestAcceptedTransactionEntry(transaction, PaymentTransactionType.CREATE_SUBSCRIPTION)
                .orElseThrow(() -> new IllegalStateException("order create subscription transaction entry not found"));
    }
}
