package com.cybersource.payment.logging;

import org.aopalliance.aop.Advice;
import org.aspectj.lang.JoinPoint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

/**
 * Interceptor responsible for logging method executions.
 */
public class DefaultLoggingInterceptor implements Advice
{
    /**
     * Log as debug a method execution with useful information.
     *
     * @param point joint point which identifies method that is logged.
     */
    public void logMethodCall(final JoinPoint point)
    {
        final Logger log = LoggerFactory.getLogger(point.getSignature().getDeclaringType());
        log.debug("{}({})", point.getSignature().getName(), Arrays.toString(point.getArgs()));
    }
}
