package com.cybersource.payment.data;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import javax.annotation.PostConstruct;

/**
 * Provides information about payment api system (e.g. version, application, etc.).
 */
public class PaymentSystemInfo
{
    private static final Logger LOG = LoggerFactory.getLogger(PaymentSystemInfo.class);

    private static final String VERSION_INFO_FILE = "version.info";

    private static final String DEFAULT_PARTNER_SOLUTION_ID = "QCWL23QN";

    private static final String DEFAULT_CLIENT_APPLICATION = "SOAP Toolkit API";

    @Value("${build.vendor}")
    private String clientEnvironment;

    @Value("${build.version}")
    private String clientApplicationVersion;

    @Value("${cybersource.request.developer.id}")
    private String developerID;

    @Value("${cybersource.request.client.library}")
    private String clientLibrary;

    private String clientLibraryVersion;

    @PostConstruct
    public void init() throws ConfigurationException
    {
        try
        {
            final Configuration versionInfo = new PropertiesConfiguration(VERSION_INFO_FILE);

            clientLibraryVersion = versionInfo.getString("build_version");
        }
        catch (final ConfigurationException e)
        {
            LOG.warn("Cannot load version.info file", e.getMessage());
        }
    }

    public String getPartnerSolutionID()
    {
        return DEFAULT_PARTNER_SOLUTION_ID;
    }

    public String getClientApplication()
    {
        return DEFAULT_CLIENT_APPLICATION;
    }

    public String getDeveloperID()
    {
        return developerID;
    }

    public String getClientEnvironment()
    {
        return clientEnvironment;
    }

    public String getClientApplicationVersion()
    {
        return clientApplicationVersion;
    }

    public String getClientLibrary()
    {
        return clientLibrary;
    }

    public String getClientLibraryVersion()
    {
        return clientLibraryVersion;
    }
}
