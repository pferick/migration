/**
 * Contains classes responsible for loading specific configuration data
 */
package com.cybersource.payment.configuration.resolver;