package com.cybersource.payment.service.provider;

import com.cybersource.payment.service.executor.PaymentServiceResult;
import com.cybersource.payment.service.executor.request.PaymentServiceRequest;

/**
 * Defines a payment service provider interface (e.g. credit-card authorization payment service, capture service, etc.)
 */
public interface PaymentServiceProvider
{
    /**
     * Checks if specified service request is supported by this payment service provider.
     *
     * @param request the request to check for support
     * @return returns true if payment service can serve payment service request, otherwise false
     */
    boolean supports(final PaymentServiceRequest request);

    /**
     * Provides the implementation of payment service (operation) logic.
     *
     * @param request the request to be used by payment service operation
     * @return the result of the payment operation
     */
    PaymentServiceResult invoke(final PaymentServiceRequest request);
}
