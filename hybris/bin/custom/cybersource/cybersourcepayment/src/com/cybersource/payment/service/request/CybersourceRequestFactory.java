package com.cybersource.payment.service.request;

import com.cybersource.payment.request.ReportAuthorizeType;
import com.cybersource.payment.service.request.validation.CybersourceFieldValidator;
import org.springframework.beans.factory.annotation.Required;

import java.util.List;

/*
 * Component that creates Cybersource requests.
 */
public class CybersourceRequestFactory
{
    private List<CybersourceFieldValidator> validators;

    protected CybersourceRequest createRequest()
    {
        return new DefaultCybersourceRequest(validators);
    }

	public SaAuthorizationType saAuthorize()
	{
		return new SaAuthorizationType(createRequest());
	}

	public ReportAuthorizeType reportAuthorize()
	{
		return new ReportAuthorizeType(createRequest());
	}

    public AuthorizationType authorize()
    {
        return new AuthorizationType(createRequest());
    }

    public TaxType tax()
    {
        return new TaxType(createRequest());
    }

    public CaptureType capture()
    {
        return new CaptureType(createRequest());
    }

    public PaymentTokenCreateType paymentTokenCreate()
    {
        return new PaymentTokenCreateType(createRequest());
    }

    public PaymentTokenDeleteType paymentTokenDelete()
    {
        return new PaymentTokenDeleteType(createRequest());
    }

	public AuthorizationReversalType authorizeReversal()
	{
		return new AuthorizationReversalType(createRequest());
	}

    public RefundStandaloneType refundStandalone()
    {
        return new RefundStandaloneType(createRequest());
    }

    public EnrollmentType enrollment()
    {
        return new EnrollmentType(createRequest());
    }

    public ValidateType validate()
    {
        return new ValidateType(createRequest());
    }

    public RefundFollowOnType refundFollowOn()
    {
        return new RefundFollowOnType(createRequest());
    }

    public VoidType voidTransaction()
    {
        return new VoidType(createRequest());
    }

    @Required
    public void setValidators(final List<CybersourceFieldValidator> validators)
    {
        this.validators = validators;
    }
}
