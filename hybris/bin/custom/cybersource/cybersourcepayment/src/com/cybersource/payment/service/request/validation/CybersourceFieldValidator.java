package com.cybersource.payment.service.request.validation;

import java.util.Map;

/**
 * Defines a Cybersource field validator.
 */
public interface CybersourceFieldValidator
{
    /**
     * Validates field value against given rule set.
     *
     * @param fieldValue field value to be validated
     * @param rules rules describing a valid field value
     * @return returns true if value is valid, otherwise false
     */
    boolean validate(final String fieldValue, final Map<String, String> rules);
}
