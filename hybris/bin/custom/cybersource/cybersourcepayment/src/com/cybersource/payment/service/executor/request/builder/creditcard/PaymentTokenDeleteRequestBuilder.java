package com.cybersource.payment.service.executor.request.builder.creditcard;

import com.cybersource.payment.service.executor.request.builder.PaymentServiceRequestBuilder;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionModel;

import com.cybersource.payment.enums.CybsPaymentType;

/**
 * Builder for payment token delete request (Subscription).
 */
public class PaymentTokenDeleteRequestBuilder extends PaymentServiceRequestBuilder<PaymentTokenDeleteRequestBuilder>
{
    /**
     * Creates a new instance of {@link PaymentTokenDeleteRequestBuilder} class with method CREDIT_CARD and
     * service DELETE_SUBSCRIPTION.
     *
     * @see CybsPaymentType
     * @see PaymentTransactionType
     */
    public PaymentTokenDeleteRequestBuilder()
    {
        paymentServiceRequest.method(CybsPaymentType.CREDIT_CARD);
        paymentServiceRequest.service(PaymentTransactionType.DELETE_SUBSCRIPTION);
    }

    public PaymentTokenDeleteRequestBuilder setOrder(final AbstractOrderModel order)
    {
        paymentServiceRequest.addParam("order", order);
        return this;
    }

    public PaymentTokenDeleteRequestBuilder setTransaction(final PaymentTransactionModel transaction)
    {
        paymentServiceRequest.addParam("transaction", transaction);
        return this;
    }
}
