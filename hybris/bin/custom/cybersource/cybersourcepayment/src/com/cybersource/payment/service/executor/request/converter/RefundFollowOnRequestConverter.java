package com.cybersource.payment.service.executor.request.converter;

import com.cybersource.payment.service.executor.request.PaymentServiceRequest;
import com.cybersource.payment.service.request.CybersourceRequest;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;

import java.math.BigDecimal;


/*
* A component that encapsulates conversion logic from {@link PaymentServiceRequest} to
* {@link CybersourceRequest} for payment refund follow-on request.
*/
public class RefundFollowOnRequestConverter extends AbstractRequestConverter
{
    @Override
    public CybersourceRequest convert(final PaymentServiceRequest source)
    {
        final AbstractOrderModel order = source.getRequiredParam("order");
        final PaymentTransactionEntryModel capture = getCapture(source);
        final BigDecimal amount = source.getRequiredParam("amount");

        return cybersourceRequestFactory.refundFollowOn()
                .merchantId(source.getRequiredParam("merchantId"))
                .merchantReferenceCode(order.getGuid())
                .purchaseTotalsCurrency(order.getCurrency().getIsocode())
                .purchaseTotalsGrandTotalAmount(amount)
                .ccCreditServiceCaptureRequestID(capture.getRequestId())
                .ccCreditServiceRun(true)
                .request();
    }

    private PaymentTransactionEntryModel getCapture(final PaymentServiceRequest request)
    {
        final PaymentTransactionModel transaction = request.getRequiredParam("transaction");

        return paymentTransactionService.getLatestAcceptedTransactionEntry(transaction, PaymentTransactionType.CAPTURE)
                .orElseThrow(() -> new IllegalStateException("Order capture transaction entry not found"));
    }
}
