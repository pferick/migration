package com.cybersource.payment.hystrix;

import com.netflix.config.AggregatedConfiguration;
import com.netflix.config.ConcurrentMapConfiguration;
import com.netflix.config.ConfigurationManager;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import org.apache.commons.configuration.AbstractConfiguration;

import javax.annotation.PostConstruct;

/**
 * This class is intended to load hystrix configuration
 */
public class HystrixConfigurationLoader
{
    private static final String HYBRIS_CONFIG_KEY = "hybrisConfig";

    private ConfigurationService configurationService;

    public HystrixConfigurationLoader(final ConfigurationService configurationService)
    {
        this.configurationService = configurationService;
    }

    /**
     * Load configuration properties, during bean creation, into ConfigurationManager used by hystrix
     */
    @PostConstruct
    public void loadHystrixConfiguration()
    {
        final ConcurrentMapConfiguration hybrisConfiguration = new ConcurrentMapConfiguration(
                configurationService.getConfiguration());

        loadHybrisConfiguration(hybrisConfiguration);
    }

    private void loadHybrisConfiguration(final ConcurrentMapConfiguration hybrisConfiguration)
    {
        final AbstractConfiguration masterConfiguration = getConfigurationInstance();
        if (masterConfiguration instanceof AggregatedConfiguration)
        {
            addHybrisConfiguration(hybrisConfiguration, (AggregatedConfiguration) masterConfiguration);
        }
        else
        {
            ConfigurationManager.loadPropertiesFromConfiguration(hybrisConfiguration);
        }
    }

    private void addHybrisConfiguration(final ConcurrentMapConfiguration hybrisConfiguration,
            final AggregatedConfiguration masterConfiguration)
    {
        if (!masterConfiguration.getConfigurationNames().contains(HYBRIS_CONFIG_KEY))
        {
            masterConfiguration.addConfiguration(hybrisConfiguration, HYBRIS_CONFIG_KEY);
        }
    }

    protected AbstractConfiguration getConfigurationInstance()
    {
        return ConfigurationManager.getConfigInstance();
    }
}