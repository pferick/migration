package com.cybersource.payment.data.mapper.converter;

import ma.glasnost.orika.CustomConverter;
import ma.glasnost.orika.metadata.Type;
import org.apache.commons.lang3.StringUtils;

import java.math.BigInteger;
import java.util.Optional;

/**
 * Converts non nullable object into big integer
 */
public class ObjectToBigIntegerConverter extends CustomConverter<Object, BigInteger>
{
    @Override
    public BigInteger convert(final Object value, final Type<? extends BigInteger> type)
    {
        return Optional.ofNullable(value)
                .map(Object::toString)
                .map(BigInteger::new)
                .orElse(null);
    }
}