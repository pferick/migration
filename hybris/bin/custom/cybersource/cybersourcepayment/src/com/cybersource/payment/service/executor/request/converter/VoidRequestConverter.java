package com.cybersource.payment.service.executor.request.converter;

import com.cybersource.payment.service.executor.request.PaymentServiceRequest;
import com.cybersource.payment.service.request.CybersourceRequest;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;

/*
* A component that encapsulates conversion logic from {@link PaymentServiceRequest} to
* {@link CybersourceRequest} for payment transaction void request.
*/
public class VoidRequestConverter extends AbstractRequestConverter
{
    @Override
    public CybersourceRequest convert(final PaymentServiceRequest source)
    {
        final AbstractOrderModel order = source.getRequiredParam("order");
        final PaymentTransactionEntryModel transactionEntry = source.getRequiredParam("transactionEntry");

        final AddressModel billingAddress = order.getPaymentAddress();

        return cybersourceRequestFactory.voidTransaction()
                .merchantId(source.getRequiredParam("merchantId"))
                .merchantReferenceCode(order.getGuid())
                .voidServiceVoidRequestID(transactionEntry.getRequestId())
                .voidServiceRun(true)
                .voidServiceVoidRequestToken(transactionEntry.getRequestToken())
                .billToFirstName(billingAddress.getFirstname())
                .billToLastName(billingAddress.getLastname())
                .billToEmail(billingAddress.getEmail())
                .request();
    }
}
