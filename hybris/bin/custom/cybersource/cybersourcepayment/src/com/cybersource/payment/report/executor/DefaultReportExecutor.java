package com.cybersource.payment.report.executor;

import com.cybersource.payment.report.provider.ReportProvider;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import java.util.Map;

/**
 * Default implementation of report executor.
 * Dispatches report request to an instance of {@link ReportProvider} which provides requested type of report.
 */
public class DefaultReportExecutor implements ReportExecutor
{
    private Map<Class, ReportProvider> providers;

    @Override
    public <P, R> R getReport(final P param)
    {
        final ReportProvider<P, R> provider = providers.get(param.getClass());

        Assert.notNull(provider, "Report provider not found.");

        return provider.getReport(param);
    }

    @Required
    public void setProviders(final Map<Class, ReportProvider> providers)
    {
        this.providers = providers;
    }
}
