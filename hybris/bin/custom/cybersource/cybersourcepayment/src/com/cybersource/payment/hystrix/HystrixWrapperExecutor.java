package com.cybersource.payment.hystrix;

import com.cybersource.payment.executor.WrapperExecutor;
import com.netflix.hystrix.HystrixCommand;
import com.netflix.hystrix.HystrixCommandGroupKey;
import com.netflix.hystrix.HystrixCommandKey;
import com.netflix.hystrix.HystrixThreadPoolKey;
import com.netflix.hystrix.exception.HystrixRuntimeException;

import java.util.function.Supplier;

/**
 * Executes all actions in the hystrix context, using circuit-breaker pattern provided by library
 * @param <R> result type
 */
public class HystrixWrapperExecutor<R> implements WrapperExecutor<String, R>
{
    private String groupName;

    private String threadPoolName;

    public R execute(final String commandKey, final Supplier<R> action)
    {
        final HystrixCommand.Setter setter = HystrixCommand.Setter
                .withGroupKey(HystrixCommandGroupKey.Factory.asKey(groupName))
                .andThreadPoolKey(HystrixThreadPoolKey.Factory.asKey(threadPoolName))
                .andCommandKey(HystrixCommandKey.Factory.asKey(commandKey));

        final HystrixCommand<R> command = new TenantAwareHystrixCommand<R>(setter)
        {
            @Override
            protected R runCommand() throws Exception
            {
                return action.get();
            }
        };

        return execute(command);
    }

    private R execute(final HystrixCommand<R> command)
    {
        try
        {
            return command.execute();
        }
        catch (final HystrixRuntimeException e)
        {
            throw new RuntimeException("Failed to execute command: " + command.getCommandKey().name(), e.getCause());
        }
    }

    public void setGroupName(final String groupName)
    {
        this.groupName = groupName;
    }

    public void setThreadPoolName(final String threadPoolName)
    {
        this.threadPoolName = threadPoolName;
    }
}
