/**
 * Contains classes responsible for requesting concrete report from Cybersource and returning appropriate result as POJO object
 */
package com.cybersource.payment.report.provider;