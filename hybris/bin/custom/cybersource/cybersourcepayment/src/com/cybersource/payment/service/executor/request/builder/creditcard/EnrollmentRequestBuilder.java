package com.cybersource.payment.service.executor.request.builder.creditcard;

import com.cybersource.payment.enums.CybsPaymentType;
import com.cybersource.payment.service.executor.request.builder.PaymentServiceRequestBuilder;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.payment.dto.CardInfo;
import de.hybris.platform.payment.enums.PaymentTransactionType;

/**
 * Builder for payer authentication enrollment request.
 */
public class EnrollmentRequestBuilder extends PaymentServiceRequestBuilder<EnrollmentRequestBuilder>
{
    /**
     * Creates a new instance of {@link EnrollmentRequestBuilder} class with method
     * CREDIT_CARD and service ENROLLMENT.
     *
     * @see CybsPaymentType
     * @see PaymentTransactionType
     */
    public EnrollmentRequestBuilder()
    {
        paymentServiceRequest.method(CybsPaymentType.CREDIT_CARD);
        paymentServiceRequest.service(PaymentTransactionType.ENROLLMENT);
    }

    public EnrollmentRequestBuilder setOrder(final AbstractOrderModel order)
    {
        paymentServiceRequest.addParam("order", order);
        return this;
    }

    public EnrollmentRequestBuilder setCardInfo(final CardInfo cardInfo)
    {
        paymentServiceRequest.addParam("card", cardInfo);
        return this;
    }
}
