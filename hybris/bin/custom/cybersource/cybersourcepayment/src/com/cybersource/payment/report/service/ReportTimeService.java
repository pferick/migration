package com.cybersource.payment.report.service;

import org.joda.time.Interval;

import java.util.Date;

/**
 * This is a common class used for report period calculation
 */
public interface ReportTimeService
{
    /**
     * Calculates report querying interval
     *
     * @param lastRunDate date when report was called last time
     * @return an interval instance
     */
    Interval getReportInterval(final Date lastRunDate);
}
