package com.cybersource.payment.service.executor.request.builder;

import com.cybersource.payment.service.executor.request.PaymentServiceRequest;

/**
 * Base class for payment service request builders.
 */
public class PaymentServiceRequestBuilder<B extends PaymentServiceRequestBuilder>
{
    protected final PaymentServiceRequest paymentServiceRequest = PaymentServiceRequest.create();

    protected PaymentServiceRequestBuilder()
    {
        // EMPTY
    }

    public B setMerchantId(final String merchantId)
    {
        paymentServiceRequest.addParam("merchantId", merchantId);
        return (B) this;
    }

    public PaymentServiceRequest build()
    {
        return paymentServiceRequest;
    }
}
