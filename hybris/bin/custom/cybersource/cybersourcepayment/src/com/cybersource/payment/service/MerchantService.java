package com.cybersource.payment.service;

import com.cybersource.payment.enums.CybsMerchantProfileType;
import com.cybersource.payment.model.CybsMerchantModel;
import com.cybersource.payment.model.CybsMerchantProfileModel;

import java.util.List;

/**
 * Service to provide methods to lookup merchant, merchant profiles, etc.
 */
public interface MerchantService
{
    /**
     * Resolves merchant by current site, currency, payment channel, etc.
     *
     * @return merchant model
     */
    CybsMerchantModel getCurrentMerchant();

    /**
     * Finds merchant by specified merchant id.
     *
     * @param merchantId the id of the merchant
     * @return merchant model
     */
    CybsMerchantModel getMerchant(final String merchantId);

    /**
     * Gets merchant profile by specified profile type.
     *
     * @param merchant the merchant model
     * @param profileType the profile type code to get from the merchant
     * @return merchant profile model for given type
     */
    CybsMerchantProfileModel getMerchantProfile(final CybsMerchantModel merchant, final String profileType);

    /**
     * Gets current merchant profile by specified profile type
     *
     * @param profileType the profile type to get from the merchant
     * @return merchant profile model for given type
     */
    CybsMerchantProfileModel getCurrentMerchantProfile(final CybsMerchantProfileType profileType);

    /**
     * Gets merchant profile by specified profile type.
     *
     * @param merchant the merchant model
     * @param profileType the profile type to get from the merchant
     * @return merchant profile model for given type
     */
    CybsMerchantProfileModel getMerchantProfile(final CybsMerchantModel merchant, final CybsMerchantProfileType profileType);

    /**
     * Gets a list of existing merchants
     * @return list of merchant models
     */
    List<CybsMerchantModel> getAllMerchants();
}
