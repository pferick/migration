package com.cybersource.payment.service.executor.request.builder.creditcard;

import com.cybersource.payment.enums.CybsPaymentType;
import com.cybersource.payment.service.executor.request.PaymentServiceRequest;
import com.cybersource.payment.service.executor.request.builder.PaymentServiceRequestBuilder;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.payment.dto.CardInfo;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import org.springframework.util.Assert;

/**
 * Builder for payment refund standalone request.
 */
public class RefundStandaloneRequestBuilder extends PaymentServiceRequestBuilder<RefundStandaloneRequestBuilder>
{
    private static final String ORDER_PARAM = "order";

    private static final String CARD_PARAM = "card";

    private static final String SUBSCRIPTION_ID_PARAM = "subscriptionID";

    /**
     * Creates a new instance of {@link RefundStandaloneRequestBuilder} class with method CREDIT_CARD and
     * service REFUND_STANDALONE.
     *
     * @see CybsPaymentType
     * @see PaymentTransactionType
     */
    public RefundStandaloneRequestBuilder()
    {
        paymentServiceRequest.method(CybsPaymentType.CREDIT_CARD);
        paymentServiceRequest.service(PaymentTransactionType.REFUND_STANDALONE);
    }

    public RefundStandaloneRequestBuilder setOrder(final AbstractOrderModel order)
    {
        paymentServiceRequest.addParam(ORDER_PARAM, order);
        return this;
    }

    public InternalBuilder setCard(final CardInfo card)
    {
        paymentServiceRequest.addParam(CARD_PARAM, card);
        return new InternalBuilder(this);
    }

    public InternalBuilder setSubscriptionId(final String subscriptionId)
    {
        paymentServiceRequest.addParam(SUBSCRIPTION_ID_PARAM, subscriptionId);
        return new InternalBuilder(this);
    }

    static private class InternalBuilder
    {
        private RefundStandaloneRequestBuilder baseBuilder;

        InternalBuilder(RefundStandaloneRequestBuilder baseBuilder)
        {
            Assert.notNull(baseBuilder);
            this.baseBuilder = baseBuilder;
        }

        public InternalBuilder setOrder(final AbstractOrderModel order)
        {
            baseBuilder = baseBuilder.setOrder(order);
            return this;
        }

        public PaymentServiceRequest build()
        {
            return baseBuilder.build();
        }
    }
}
