package com.cybersource.payment.report.request;

import com.cybersource.reporting.request.value.TransactionQuerySubType;
import com.cybersource.reporting.request.value.TransactionQueryType;

import java.util.Date;

/**
 * Defines request object which contains params for querying single transaction report
 */
public class SingleTransactionReportRequest
{
    private String merchantID;

    private String merchantReferenceNumber;

    private Date targetDate;

    private String requestID;

    private TransactionQueryType type;

    private TransactionQuerySubType subtype;

    private String versionNumber;

    public String getMerchantReferenceNumber()
    {
        return merchantReferenceNumber;
    }

    public void setMerchantReferenceNumber(final String merchantReferenceNumber)
    {
        this.merchantReferenceNumber = merchantReferenceNumber;
    }

    public Date getTargetDate()
    {
        return targetDate;
    }

    public void setTargetDate(final Date targetDate)
    {
        this.targetDate = targetDate;
    }

    public String getRequestID()
    {
        return requestID;
    }

    public void setRequestID(final String requestID)
    {
        this.requestID = requestID;
    }

    public TransactionQueryType getType()
    {
        return type;
    }

    public void setType(final TransactionQueryType type)
    {
        this.type = type;
    }

    public TransactionQuerySubType getSubtype()
    {
        return subtype;
    }

    public void setSubtype(final TransactionQuerySubType subtype)
    {
        this.subtype = subtype;
    }

    public String getMerchantID()
    {
        return merchantID;
    }

    public void setMerchantID(final String merchantID)
    {
        this.merchantID = merchantID;
    }

    public String getVersionNumber()
    {
        return versionNumber;
    }

    public void setVersionNumber(final String versionNumber)
    {
        this.versionNumber = versionNumber;
    }

    @Override
    public String toString()
    {
        return "SingleTransactionReportRequest{" +
                "merchantID='" + merchantID + '\'' +
                ", merchantReferenceNumber='" + merchantReferenceNumber + '\'' +
                ", targetDate=" + targetDate +
                ", requestID='" + requestID + '\'' +
                ", type=" + type +
                ", subtype=" + subtype +
                ", versionNumber='" + versionNumber + '\'' +
                '}';
    }
}
