/**
 * Contains classes that defines report conversion from original type to more specific one (POJO)
 */
package com.cybersource.payment.report.provider.unmarshaller;