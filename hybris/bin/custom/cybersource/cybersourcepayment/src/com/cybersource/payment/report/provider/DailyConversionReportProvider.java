package com.cybersource.payment.report.provider;

import com.cybersource.payment.report.request.DailyConversionReportRequest;
import com.cybersource.payment.utils.TimeUtils;
import com.cybersource.reports.conversion.Report;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;

import java.text.MessageFormat;

/**
 * This class is responsible for obtaining daily conversion report
 */
public class DailyConversionReportProvider extends AbstractHttpReportProvider <DailyConversionReportRequest, Report>
{
    private String requestUrlPattern;

    @Override
    protected HttpUriRequest buildRequest(final DailyConversionReportRequest request)
    {
        final String requestUrl = MessageFormat.format(requestUrlPattern,
                TimeUtils.toCybersourceReportDate(request.getDate()), request.getMerchantId());

        return new HttpGet(requestUrl);
    }

    public void setRequestUrlPattern(final String requestUrlPattern)
    {
        this.requestUrlPattern = requestUrlPattern;
    }
}