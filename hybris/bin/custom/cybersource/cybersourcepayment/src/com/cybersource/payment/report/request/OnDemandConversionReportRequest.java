package com.cybersource.payment.report.request;

import java.util.Date;

/**
 * Defines request object which contains params for querying on demand conversion report
 */
public class OnDemandConversionReportRequest
{
    private String merchantId;

    private Date startDate;

    private Date endDate;

    public String getMerchantId()
    {
        return merchantId;
    }

    public void setMerchantId(final String merchantId)
    {
        this.merchantId = merchantId;
    }

    public Date getStartDate()
    {
        return startDate;
    }

    public void setStartDate(final Date startDate)
    {
        this.startDate = startDate;
    }

    public Date getEndDate()
    {
        return endDate;
    }

    public void setEndDate(final Date endDate)
    {
        this.endDate = endDate;
    }

    @Override
    public String toString()
    {
        return "OnDemandConversionReportRequest{merchantId='" + merchantId + '\'' +
                ", startDate=" + startDate + ", endDate=" + endDate + '}';
    }
}
