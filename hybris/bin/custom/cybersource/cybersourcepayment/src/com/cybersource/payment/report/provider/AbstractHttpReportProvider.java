package com.cybersource.payment.report.provider;

import com.cybersource.payment.executor.WrapperExecutor;
import com.cybersource.payment.hystrix.HystrixWrapperExecutor;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Required;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.function.Function;

/**
 * Implements common logic for retrieving http report
 *
 * @param <P> input type params
 * @param <R> report output type
 */
public abstract class AbstractHttpReportProvider<P, R> implements ReportProvider<P, R>
{
    private String reportName;

    private Function<String, R> unmarshaller;

    @Resource
    private WrapperExecutor<String, String> reportWrapperExecutor;

    @Resource
    private CloseableHttpClient reportHttpClient;

    @Override
    public R getReport(final P request)
    {
        final String resultAsString = reportWrapperExecutor.execute(reportName, () -> loadReport(request));

        return unmarshaller.apply(resultAsString);
    }

    private String loadReport(final P request)
    {
        try (final CloseableHttpResponse response = reportHttpClient.execute(buildRequest(request)))
        {
            return EntityUtils.toString(response.getEntity());
        }
        catch (final IOException e)
        {
            throw new RuntimeException("Can't load http report from cybersource", e);
        }
    }

    protected abstract HttpUriRequest buildRequest(final P param);

    @Required
    public void setUnmarshaller(final Function<String, R> unmarshaller)
    {
        this.unmarshaller = unmarshaller;
    }

    @Required
    public void setReportName(final String reportName)
    {
        this.reportName = reportName;
    }

    public void setReportWrapperExecutor(final HystrixWrapperExecutor<String> reportWrapperExecutor)
    {
        this.reportWrapperExecutor = reportWrapperExecutor;
    }

    public void setReportHttpClient(final CloseableHttpClient reportHttpClient)
    {
        this.reportHttpClient = reportHttpClient;
    }
}