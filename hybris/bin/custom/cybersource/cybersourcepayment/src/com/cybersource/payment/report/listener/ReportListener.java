package com.cybersource.payment.report.listener;

import com.cybersource.payment.model.CybsMerchantModel;

/*
* Defines report listener component that processes a report.
*/
public interface ReportListener<T>
{
    void handle(final CybsMerchantModel merchant, final T report);
}
