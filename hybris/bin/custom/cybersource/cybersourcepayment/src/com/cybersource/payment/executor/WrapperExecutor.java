package com.cybersource.payment.executor;

import java.util.function.Supplier;

/**
 * Defines an interface to support template pattern
 * @param <P>
 * @param <R>
 */
public interface WrapperExecutor<P, R>
{
    /**
     * Wrap an action execution into specific logic
     *
     * @param param input params for action
     * @param action logic that should be wrapped in specific context
     * @return an action result
     */
    R execute(final P param, final Supplier<R> action);
}
