/**
 * Contains classes related to data (simple POJO) and conversion functionality
 */
package com.cybersource.payment.data;