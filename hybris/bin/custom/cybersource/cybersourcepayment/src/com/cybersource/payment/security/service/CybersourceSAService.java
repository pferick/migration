package com.cybersource.payment.security.service;

import java.util.Map;

/**
 * Secure Acceptance (SOP/HOP) helper service.
 */
public interface CybersourceSAService
{
    /**
     * Signs passed parameters.
     *
     * @param paymentData parameters to sign
     * @param secretKey secret key to be used for signature
     * @return signature for passed parameters
     */
    String getDigest(Map<String, String> paymentData, final String secretKey);


    /**
     * Verifies the signature of the received transaction.
     *
     * @param paymentData containing transaction signature and signed fields
     *            Should contain 'signedFields' and 'transactionSignature' fields
     * @param secretKey secret key to be used for signature
     *
     * @return true if data can be verified
     */
    boolean validateTransactionSignature(final Map<String, String> paymentData, final String secretKey);
}
