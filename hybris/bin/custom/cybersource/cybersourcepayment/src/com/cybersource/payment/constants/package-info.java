/**
 * Contains classes with constants, used by Cyberousource extension. It's the right place to define global constant
 */
package com.cybersource.payment.constants;