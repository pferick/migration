package com.cybersource.payment.service.executor;

import com.cybersource.payment.service.executor.request.PaymentServiceRequest;
import com.cybersource.payment.service.provider.PaymentServiceProvider;

/**
 *  Defines a payment service executor interface.
 * Is responsible for dispatching of a given {@link PaymentServiceRequest} to a {@link PaymentServiceProvider}.
 */
public interface PaymentServiceExecutor
{
    /**
     * Executes a payment service by specified payment request.
     *
     * @param request the request to be used for payment service lookup and execution
     * @return the result of the payment operation
     */
    PaymentServiceResult execute(final PaymentServiceRequest request);
}
