package com.cybersource.payment.service.command;

import com.cybersource.payment.executor.WrapperExecutor;
import com.cybersource.payment.hystrix.HystrixWrapperExecutor;
import com.cybersource.payment.service.request.CybersourceRequest;
import com.cybersource.payment.service.response.CybersourceResponse;
import com.cybersource.payment.service.response.DefaultCybersourceResponse;
import com.cybersource.schemas.transaction_data.transactionprocessor.ITransactionProcessor;
import com.cybersource.schemas.transaction_data_1.ReplyMessage;
import com.cybersource.schemas.transaction_data_1.RequestMessage;
import ma.glasnost.orika.MapperFacade;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import javax.annotation.Resource;

/**
 * The default implementation of {@link PaymentCommand}.
 *
 * It delegates payment operation execution to Cybersource web service
 * and uses request/response mappers to convert Hybris/Cybersource DTOs.
 */
public class DefaultPaymentCommand implements PaymentCommand
{
    private String commandName;

    private MapperFacade requestMapper;

    private MapperFacade responseMapper;

    @Resource
    private WrapperExecutor<String, ReplyMessage> paymentWrapperExecutor;

    @Resource
    private ITransactionProcessor cybersourceWebService;

    @Override
    public CybersourceResponse perform(final CybersourceRequest request)
    {
        Assert.notNull(request, "request cannot be null");

        final RequestMessage requestMessage = requestMapper.map(request, RequestMessage.class);

        final ReplyMessage replyMessage = paymentWrapperExecutor.execute(commandName,
                () -> cybersourceWebService.runTransaction(requestMessage));

        final CybersourceResponse response = createCybersourceResponse();

        responseMapper.map(replyMessage, response);

        return response;
    }

    protected CybersourceResponse createCybersourceResponse()
    {
        return new DefaultCybersourceResponse();
    }

    @Required
    public void setRequestMapper(final MapperFacade requestMapper)
    {
        this.requestMapper = requestMapper;
    }

    @Required
    public void setResponseMapper(final MapperFacade responseMapper)
    {
        this.responseMapper = responseMapper;
    }

    @Required
    public void setCommandName(final String commandName)
    {
        this.commandName = commandName;
    }

    public void setPaymentWrapperExecutor(final HystrixWrapperExecutor<ReplyMessage> paymentWrapperExecutor)
    {
        this.paymentWrapperExecutor = paymentWrapperExecutor;
    }

    public void setCybersourceWebService(final ITransactionProcessor cybersourceWebService)
    {
        this.cybersourceWebService = cybersourceWebService;
    }
}
