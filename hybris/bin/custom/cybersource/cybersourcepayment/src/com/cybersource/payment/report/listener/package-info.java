/**
 * Contains classes that defines report observers, are mostly used in cronjobs
 * in order to take some recovery actions for fraud marked transactions
 */
package com.cybersource.payment.report.listener;