package com.cybersource.payment.report.service;

import de.hybris.platform.servicelayer.time.TimeService;
import org.joda.time.DateTime;
import org.joda.time.Interval;

import javax.annotation.Resource;
import java.util.Date;

import static org.apache.commons.lang.math.NumberUtils.INTEGER_ONE;

/**
 * Calculates time interval for on demand conversion report
 */
public class OnDemandConversionReportTimeService implements ReportTimeService
{
    private int correctionMinutes = 3;

    @Resource
    private TimeService timeService;

    @Override
    public Interval getReportInterval(final Date lastRunDate)
    {
        return lastRunDate == null ? firstOnDemandReportInterval() : nextOnDemandReportInterval(lastRunDate);
    }

    private Interval firstOnDemandReportInterval()
    {
        final DateTime now = new DateTime(timeService.getCurrentTime());

        final DateTime today = now.minusMinutes(correctionMinutes);
        final DateTime yesterday = now.minusDays(INTEGER_ONE).plusMinutes(correctionMinutes);

        return new Interval(yesterday.getMillis(), today.getMillis());
    }

    private Interval nextOnDemandReportInterval(final Date lastRunDate)
    {
        final Date currentDate = timeService.getCurrentTime();
        final DateTime yesterday = new DateTime(currentDate).minusDays(INTEGER_ONE);

        final boolean moreThanOneDay = lastRunDate.before(yesterday.toDate());

        if (moreThanOneDay)
        {
            return firstOnDemandReportInterval();
        }
        else
        {
            final DateTime startDate = new DateTime(lastRunDate).minusMinutes(correctionMinutes);
            final DateTime endDate = new DateTime(currentDate).minusMinutes(correctionMinutes);

            return new Interval(startDate.getMillis(), endDate.getMillis());
        }
    }

    public void setCorrectionMinutes(final int correctionMinutes)
    {
        this.correctionMinutes = correctionMinutes;
    }
}
