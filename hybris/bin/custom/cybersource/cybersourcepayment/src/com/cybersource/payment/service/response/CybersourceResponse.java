package com.cybersource.payment.service.response;

import java.util.Map;

/**
 * Defines a Cybersource response object.
 */
public interface CybersourceResponse
{
    /**
     * Adds a new response field for given name.
     *
     * @param name the name of the field
     * @param value the value of the field
     */
    void addResponseField(final String name, final Object value);

    <T> T getResponseField(final String name);

    /**
     * Returns response fields added by #{@link CybersourceResponse#addResponseField(String, Object)} method.
     *
     * @return response fields map
     */
    Map<String, Object> getResponseFields();
}
