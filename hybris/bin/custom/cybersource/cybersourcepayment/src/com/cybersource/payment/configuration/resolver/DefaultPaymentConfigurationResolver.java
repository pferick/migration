package com.cybersource.payment.configuration.resolver;

import com.cybersource.payment.model.CybsPaymentConfigurationModel;

import java.util.Map;

/**
 * This class is intended to lookup payment configuration
 */
public class DefaultPaymentConfigurationResolver extends AbstractPaymentConfigurationResolver<CybsPaymentConfigurationModel>
{
    public static final String QUERY_TEMPLATE = "SELECT {pk} FROM {CybsPaymentConfiguration} WHERE {key}=?key";

    @Override
    public String getSearchQuery(final Map<String, Object> params)
    {
        return QUERY_TEMPLATE;
    }
}
