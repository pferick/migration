package com.cybersource.payment.service.executor.request.converter;

import com.cybersource.payment.service.executor.request.PaymentServiceRequest;
import com.cybersource.payment.service.request.CybersourceRequest;
import com.cybersource.payment.service.request.TaxType;
import com.cybersource.payment.utils.PaymentParamUtils;
import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;
import java.math.BigInteger;

/*
* A component that encapsulates conversion logic from {@link PaymentServiceRequest} to
* {@link CybersourceRequest} for payment tax request.
*/
public class TaxRequestConverter extends AbstractRequestConverter
{
    @Override
    public CybersourceRequest convert(final PaymentServiceRequest source)
    {
        final AbstractOrderModel order = source.getRequiredParam("order");

        final TaxType.Optional tax = addBaseFields(source, order);

        addProductFields(tax, order);

        return tax.request();
    }

    private TaxType.Optional addBaseFields(final PaymentServiceRequest request, final AbstractOrderModel order)
    {
        final AddressModel billingAddress = order.getPaymentAddress();
        final AddressModel shippingAddress = order.getDeliveryAddress();

        return cybersourceRequestFactory.tax()
                .merchantId(request.getRequiredParam("merchantId"))
                .merchantReferenceCode(order.getGuid())
                .purchaseTotalsCurrency(order.getCurrency().getIsocode())
                .taxServiceRun(true)

                .billToCountry(billingAddress.getCountry().getIsocode())
                .billToCity(billingAddress.getTown())
                .billToCompany(billingAddress.getCompany())
                .billToCounty(billingAddress.getDistrict())
                .billToEmail(billingAddress.getEmail())
                .billToFirstName(billingAddress.getFirstname())
                .billToLastName(billingAddress.getLastname())
                .billToPhoneNumber(billingAddress.getPhone1())
                .billToPostalCode(billingAddress.getPostalcode())
                .billToState(PaymentParamUtils.getValue(billingAddress.getRegion(), RegionModel::getIsocodeShort))
                .billToStreet1(billingAddress.getStreetnumber())
                .billToStreet2(billingAddress.getStreetname())
                .billToStreet3(billingAddress.getBuilding())

                .shipToBuildingNumber(shippingAddress.getBuilding())
                .shipToCity(shippingAddress.getTown())
                .shipToCompany(shippingAddress.getCompany())
                .shipToCountry(shippingAddress.getCountry().getIsocode())
                .shipToCounty(shippingAddress.getDistrict())
                .shipToDistrict(shippingAddress.getDistrict())
                .shipToFirstName(shippingAddress.getFirstname())
                .shipToLastName(shippingAddress.getLastname())
                .shipToPhoneNumber(shippingAddress.getPhone1())
                .shipToState(PaymentParamUtils.getValue(shippingAddress.getRegion(), RegionModel::getIsocodeShort))
                .shipToStreet1(shippingAddress.getStreetnumber())
                .shipToStreet2(shippingAddress.getStreetname())
                .shipToStreet3(shippingAddress.getBuilding());
    }

    private void addProductFields(TaxType.Optional tax, final AbstractOrderModel order)
    {
        for (int i = 0; i < order.getEntries().size(); i++)
        {
            addProductField(tax, i, order.getEntries().get(i));
        }
    }

    private void addProductField(final TaxType.Optional tax, final int i,
            final AbstractOrderEntryModel entry)
    {
        tax.itemId(i, BigInteger.valueOf(i));
        tax.itemProductCode(i, entry.getProduct().getCode());
        tax.itemProductName(i, StringUtils.abbreviate(entry.getProduct().getName(), 15));
        tax.itemProductSKU(i, entry.getProduct().getCode());
        tax.itemQuantity(i, entry.getQuantity().intValue());
        tax.itemUnitPrice(i, BigDecimal.valueOf(entry.getBasePrice()));
    }
}
