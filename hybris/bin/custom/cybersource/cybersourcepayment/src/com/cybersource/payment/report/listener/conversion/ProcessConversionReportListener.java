package com.cybersource.payment.report.listener.conversion;

import com.cybersource.payment.dao.PaymentOrderDao;
import com.cybersource.payment.model.CybsMerchantModel;
import com.cybersource.payment.report.listener.ReportListener;
import com.cybersource.payment.report.listener.conversion.decision.DecisionChangeStrategy;
import com.cybersource.reports.conversion.Conversion;
import com.cybersource.reports.conversion.Report;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.processengine.BusinessProcessService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import javax.annotation.Resource;
import java.util.List;

/**
 * Conversion report listener responsible for block/unblock order process based on a conversion decision
 */
public class ProcessConversionReportListener implements ReportListener<Report>
{
    private static final Logger LOG = LoggerFactory.getLogger(ProcessConversionReportListener.class);

    @Resource
    private PaymentOrderDao orderDao;

    @Resource
    private BusinessProcessService businessProcessService;

    private List<DecisionChangeStrategy> decisionStrategies;

    @Override
    public void handle(final CybsMerchantModel merchant, final Report report)
    {
        for(final Conversion conversion : report.getConversion())
        {
            try
            {
                final OrderModel order = orderDao.findOrderByGuid(conversion.getMerchantReferenceNumber());
                if (shouldProcessOrder(order))
                {
                    final DecisionChangeStrategy decisionStrategy = getDecisionStrategy(
                            StringUtils.upperCase(conversion.getOriginalDecision() + conversion.getNewDecision()));

                    decisionStrategy.updateOrderFraudStatus(order, conversion.getNotes());
                    triggerOrderInReviewUnblockedEvent(order);

                    LOG.info("Order: {} checked for fraud. Order status: {}", order.getCode(), order.getStatus());
                }
            }
            catch (final Exception e)
            {
                LOG.warn("Error while updating Order: {} for fraud", conversion.getMerchantReferenceNumber(), e);
            }
        }
    }

    private boolean shouldProcessOrder(final AbstractOrderModel order)
    {
        return order != null && OrderStatus.FRAUD_DECISION.equals(order.getStatus());
    }

    private DecisionChangeStrategy getDecisionStrategy(final String originalDecision)
    {
        return decisionStrategies.stream()
                .filter(d -> d.supports(originalDecision))
                .findFirst()
                .orElseThrow(() -> new IllegalStateException("No decision strategy for: " + originalDecision));
    }

    private void triggerOrderInReviewUnblockedEvent(final OrderModel order)
    {
        businessProcessService.triggerEvent(order.getCode() + "_ReviewDecision");
    }

    @Required
    public void setDecisionStrategies(final List<DecisionChangeStrategy> decisionStrategies)
    {
        this.decisionStrategies = decisionStrategies;
    }
}
