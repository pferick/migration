package com.cybersource.payment.service.executor.request.converter;

import com.cybersource.payment.service.executor.request.PaymentServiceRequest;
import com.cybersource.payment.service.request.CybersourceRequest;
import de.hybris.platform.core.model.order.AbstractOrderModel;

/*
* A component that encapsulates conversion logic from {@link PaymentServiceRequest} to
* {@link CybersourceRequest} for payment validate request.
*/
public class ValidateRequestConverter extends AbstractRequestConverter
{
    @Override
    public CybersourceRequest convert(final PaymentServiceRequest source)
    {
        final AbstractOrderModel order = source.getRequiredParam("order");
        final String paRes = source.getRequiredParam("paRes");

        return cybersourceRequestFactory.validate()
                .merchantId(source.getRequiredParam("merchantId"))
                .merchantReferenceCode(order.getGuid())
                .purchaseTotalsCurrency(order.getCurrency().getIsocode())
                .payerAuthValidateServiceRun(true)
                .payerAuthValidateServiceSignedPARes(paRes)
                .request();

    }
}
