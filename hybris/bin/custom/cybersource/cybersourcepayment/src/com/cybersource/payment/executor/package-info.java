/**
 * Contains interfaces for wrapping execution, of peace of code, into specific class, following template pattern
 */
package com.cybersource.payment.executor;