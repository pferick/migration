package com.cybersource.payment.report.executor;

/**
 * Defines a lookup interface for retrieving a report
 */
public interface ReportExecutor
{
    <P, R> R getReport(final P param);
}
