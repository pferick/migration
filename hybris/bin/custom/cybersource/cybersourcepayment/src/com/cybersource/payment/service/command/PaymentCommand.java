package com.cybersource.payment.service.command;

import com.cybersource.payment.service.request.CybersourceRequest;
import com.cybersource.payment.service.response.CybersourceResponse;

/**
 * Encapsulates a Cybersource payment command component.
 */
public interface PaymentCommand
{
    /**
     * Executes payment command with provided {@link CybersourceRequest}
     *
     * @param request payment request to be executed
     * @return the result of payment command execution
     */
    CybersourceResponse perform(final CybersourceRequest request);
}
