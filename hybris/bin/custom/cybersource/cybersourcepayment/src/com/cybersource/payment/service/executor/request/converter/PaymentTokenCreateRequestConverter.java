package com.cybersource.payment.service.executor.request.converter;

import com.cybersource.payment.service.executor.request.PaymentServiceRequest;
import com.cybersource.payment.service.request.CybersourceRequest;
import com.cybersource.payment.service.request.value.RecurringFrequency;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;

/*
* A component that encapsulates conversion logic from {@link PaymentServiceRequest} to
* {@link CybersourceRequest} for payment token create request.
*/
public class PaymentTokenCreateRequestConverter extends AbstractRequestConverter
{
    @Override
    public CybersourceRequest convert(final PaymentServiceRequest source)
    {
        final AbstractOrderModel order = source.getRequiredParam("order");
        final PaymentTransactionEntryModel lastAuthenticationTransaction = getAuthorization(source);

        return cybersourceRequestFactory.paymentTokenCreate()
                .merchantId(source.getRequiredParam("merchantId"))
                .merchantReferenceCode(order.getGuid())
                .paySubscriptionCreateServiceRun(true)
                .paySubscriptionCreateServicePaymentRequestID(lastAuthenticationTransaction.getRequestId())
                .recurringSubscriptionInfoFrequency(RecurringFrequency.ON_DEMAND)
                .request();
    }

    private PaymentTransactionEntryModel getAuthorization(final PaymentServiceRequest request)
    {
        final PaymentTransactionModel transaction = request.getRequiredParam("transaction");

        return paymentTransactionService.getLatestAcceptedTransactionEntry(transaction, PaymentTransactionType.AUTHORIZATION)
                .orElseThrow(() -> new IllegalStateException("order authorization transaction entry not found"));
    }
}
