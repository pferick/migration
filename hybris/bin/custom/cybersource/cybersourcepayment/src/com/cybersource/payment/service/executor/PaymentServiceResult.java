package com.cybersource.payment.service.executor;

import com.cybersource.payment.model.CybsPaymentTransactionEntryModel;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * An object that encapsulates a payment service execution result.
 */
public class PaymentServiceResult
{
    private static final String TRANSACTION_ENTRY = "transactionEntry";

    private final Map<String, Object> data = new HashMap<>();

    private PaymentServiceResult()
    {
        // EMPTY
    }

    /**
     * Creates a new payment service result instance.
     *
     * @return a new instance of payment service result
     */
    public static PaymentServiceResult create()
    {
        return new PaymentServiceResult();
    }

    /**
     * Adds a new data entry to the result object.
     *
     * @param name the name of result data entry
     * @param value the value of the result object
     * @return this
     */
    public PaymentServiceResult addData(final String name, final Object value)
    {
        data.put(name, value);
        return this;
    }

    /**
     * Returns payment result as map
     * @return a map whith data
     */
    public Map<String, Object> getData()
    {
        return Collections.unmodifiableMap(data);
    }

    /**
     * Returns payment result data for specified name.
     *
     * @return a payment result data object
     */
    @SuppressWarnings("unchecked")
    public <T> T getData(final String name)
    {
        return (T) data.get(name);
    }

    /**
     * Sets payment transaction entry on the result data.
     *
     * @param transaction the transaction entry to be set
     * @return this
     */
    public PaymentServiceResult addTransaction(final CybsPaymentTransactionEntryModel transaction)
    {
        data.put(TRANSACTION_ENTRY, transaction);
        return this;
    }

    /**
     * Returns payment transaction from result data.
     *
     * @return the resulting payment transaction entry
     */
    public CybsPaymentTransactionEntryModel getTransaction()
    {
        return (CybsPaymentTransactionEntryModel) data.get(TRANSACTION_ENTRY);
    }


    @Override
    public String toString()
    {
        return "PaymentServiceResult { " + data.keySet() + " }";
    }
}
