/**
 * Contains classes related to data access object (DAO), for querying DB using FlexibleSearch service
 */
package com.cybersource.payment.dao;