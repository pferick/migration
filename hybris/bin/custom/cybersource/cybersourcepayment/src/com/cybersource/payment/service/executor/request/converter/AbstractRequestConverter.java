package com.cybersource.payment.service.executor.request.converter;

import com.cybersource.payment.data.Converter;
import com.cybersource.payment.service.PaymentTransactionService;
import com.cybersource.payment.service.executor.request.PaymentServiceRequest;
import com.cybersource.payment.service.request.CybersourceRequest;
import com.cybersource.payment.service.request.CybersourceRequestFactory;

import javax.annotation.Resource;

public abstract class AbstractRequestConverter implements Converter<PaymentServiceRequest, CybersourceRequest>
{
    @Resource
    protected CybersourceRequestFactory cybersourceRequestFactory;

    @Resource
    protected PaymentTransactionService paymentTransactionService;

    public abstract CybersourceRequest convert(final PaymentServiceRequest source);

    public CybersourceRequestFactory getCybersourceRequestFactory()
    {
        return cybersourceRequestFactory;
    }

    public void setCybersourceRequestFactory(final CybersourceRequestFactory cybersourceRequestFactory)
    {
        this.cybersourceRequestFactory = cybersourceRequestFactory;
    }

    public PaymentTransactionService getPaymentTransactionService()
    {
        return paymentTransactionService;
    }

    public void setPaymentTransactionService(final PaymentTransactionService paymentTransactionService)
    {
        this.paymentTransactionService = paymentTransactionService;
    }
}
