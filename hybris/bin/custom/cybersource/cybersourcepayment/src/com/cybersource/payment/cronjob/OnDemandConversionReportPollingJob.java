package com.cybersource.payment.cronjob;

import com.cybersource.payment.model.cron.CybsConversionReportPollingCronJobModel;
import com.cybersource.payment.report.executor.ReportExecutor;
import com.cybersource.payment.report.listener.ReportListener;
import com.cybersource.payment.report.request.OnDemandConversionReportRequest;
import com.cybersource.payment.service.MerchantService;
import com.cybersource.payment.report.service.ReportTimeService;
import com.cybersource.reports.conversion.Report;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.time.TimeService;
import org.joda.time.Interval;
import org.springframework.beans.factory.annotation.Required;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

import static com.google.common.collect.Lists.newArrayList;
import static de.hybris.platform.cronjob.enums.CronJobResult.SUCCESS;
import static de.hybris.platform.cronjob.enums.CronJobStatus.FINISHED;

/**
 * This class is responsible for loading on demand conversion reports and notify subscribed listeners
 */
public class OnDemandConversionReportPollingJob extends AbstractJobPerformable<CybsConversionReportPollingCronJobModel>
{
    @Resource
    private MerchantService merchantService;

    @Resource
    private ReportExecutor reportExecutor;

    @Resource
    private TimeService timeService;

    private ReportTimeService reportTimeService;

    private List<ReportListener<Report>> listeners = newArrayList();

    @Override
    public PerformResult perform(final CybsConversionReportPollingCronJobModel cronJob)
    {
        final Interval interval = reportTimeService.getReportInterval(cronJob.getLastRunDate());

        final Date startDate = interval.getStart().toDate();
        final Date endDate = interval.getEnd().toDate();

        merchantService.getAllMerchants().forEach(m -> {
            final Report report = reportExecutor.getReport(request(m.getId(), startDate, endDate));
            listeners.forEach(l -> l.handle(m, report));
        });

        cronJob.setLastRunDate(timeService.getCurrentTime());
        modelService.save(cronJob);

        return new PerformResult(SUCCESS, FINISHED);
    }

    private OnDemandConversionReportRequest request(final String merchantId, final Date startDate, final Date endDate)
    {
        final OnDemandConversionReportRequest request = new OnDemandConversionReportRequest();

        request.setMerchantId(merchantId);

        request.setStartDate(startDate);
        request.setEndDate(endDate);

        return request;
    }

    public void setListeners(final List<ReportListener<Report>> listeners)
    {
        this.listeners = listeners;
    }

    @Required
    public void setReportTimeService(final ReportTimeService reportTimeService)
    {
        this.reportTimeService = reportTimeService;
    }
}
