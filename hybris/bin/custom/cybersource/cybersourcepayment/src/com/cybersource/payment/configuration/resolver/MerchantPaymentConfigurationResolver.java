package com.cybersource.payment.configuration.resolver;

import com.cybersource.payment.model.CybsMerchantPaymentConfigurationModel;

import java.util.Collection;
import java.util.Map;

import static com.cybersource.payment.model.CybsMerchantPaymentConfigurationModel._TYPECODE;
import static java.lang.String.format;
import static java.lang.String.join;
import static java.util.stream.Collectors.toList;

/**
 * This class is responsible for retrieving merchant payment configuration
 */
public class MerchantPaymentConfigurationResolver extends AbstractPaymentConfigurationResolver<CybsMerchantPaymentConfigurationModel>
{
    private static final String QUERY_TEMPLATE = "SELECT {pk} FROM {"+ _TYPECODE + "} WHERE %s";

    @Override
    public String getSearchQuery(final Map<String, Object> params)
    {
        final Collection<String> keys = params.keySet().stream().map(k -> format("{%s}=?%s", k, k)).collect(toList());
        return format(QUERY_TEMPLATE, join(" AND ", keys));
    }
}
