package com.cybersource.payment.security.ws;

import java.util.Collections;
import java.util.List;
import javax.xml.ws.handler.Handler;
import javax.xml.ws.handler.HandlerResolver;
import javax.xml.ws.handler.PortInfo;

import org.springframework.beans.factory.annotation.Required;

public class SingleHandlerResolver implements HandlerResolver
{
    private Handler securityHandler;

    @Override
    public List<Handler> getHandlerChain(final PortInfo portInfo)
    {
        return Collections.singletonList(securityHandler);
    }

    @Required
    public void setSecurityHandler(final Handler securityHandler)
    {
        this.securityHandler = securityHandler;
    }
}
