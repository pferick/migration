package com.cybersource.payment.data.mapper.converter;

import com.google.gson.Gson;
import ma.glasnost.orika.CustomConverter;
import ma.glasnost.orika.metadata.Type;

import java.util.Optional;

/**
 * Converts non null object into json string format
 */
public class ObjectToJsonConverter extends CustomConverter<Object, String>
{
    @Override
    public String convert(final Object value, final Type<? extends String> type)
    {
        return Optional.ofNullable(value)
                .map(v -> new Gson().toJson(v))
                .orElse(null);
    }
}
