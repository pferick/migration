package com.cybersource.payment.service;

import com.cybersource.payment.configuration.service.PaymentConfigurationService;
import com.cybersource.payment.enums.CybsConfigurationType;
import com.cybersource.payment.enums.CybsMerchantProfileType;
import com.cybersource.payment.enums.CybsPaymentChannel;
import com.cybersource.payment.enums.CybsPaymentType;
import com.cybersource.payment.model.CybsMerchantModel;
import com.cybersource.payment.model.CybsMerchantPaymentConfigurationModel;
import com.cybersource.payment.model.CybsMerchantProfileModel;
import com.google.common.collect.ImmutableMap;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.site.BaseSiteService;

import javax.annotation.Resource;
import java.util.List;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

/*
 * Default implementation of {@link MerchantService}.
 */
public class DefaultMerchantService implements MerchantService
{
    private static final String SELECT_ALL_MERCHANTS_QUERY = "SELECT {PK} FROM {" + CybsMerchantModel._TYPECODE + "}";

    @Resource
    private BaseSiteService baseSiteService;

    @Resource
    private CommonI18NService commonI18NService;

    @Resource
    private PaymentConfigurationService paymentConfigurationService;

    @Resource
    private FlexibleSearchService flexibleSearchService;

    @Override
    public CybsMerchantModel getCurrentMerchant()
    {
        final CybsMerchantPaymentConfigurationModel paymentConfiguration = paymentConfigurationService
                .getConfiguration(
                        CybsConfigurationType.MERCHANT_CONFIG,
                        ImmutableMap.of(
                                CybsMerchantPaymentConfigurationModel.SITE, baseSiteService.getCurrentBaseSite(),
                                CybsMerchantPaymentConfigurationModel.PAYMENTTYPE, CybsPaymentType.CREDIT_CARD,
                                CybsMerchantPaymentConfigurationModel.PAYMENTCHANNEL, CybsPaymentChannel.WEB,
                                CybsMerchantPaymentConfigurationModel.CURRENCY, commonI18NService.getCurrentCurrency()
                        )
                );

        return paymentConfiguration.getMerchant();
    }

    @Override
    public CybsMerchantModel getMerchant(final String merchantId)
    {
        validateParameterNotNull(merchantId, "merchant id must not be null");

        return paymentConfigurationService.getConfiguration(CybsConfigurationType.MERCHANT,
                ImmutableMap.of(CybsMerchantModel.ID, merchantId));
    }

    @Override
    public CybsMerchantProfileModel getCurrentMerchantProfile(final CybsMerchantProfileType profileType)
    {
        final CybsMerchantModel merchant = getCurrentMerchant();
        return getMerchantProfile(merchant, profileType);
    }

    @Override
    public CybsMerchantProfileModel getMerchantProfile(final CybsMerchantModel merchant, final String profileType)
    {
        validateParameterNotNull(merchant, "merchant must not be null");
        validateParameterNotNull(profileType, "merchant profile must not be null");

        return getMerchantProfile(merchant, CybsMerchantProfileType.valueOf(profileType));
    }

    @Override
    public CybsMerchantProfileModel getMerchantProfile(final CybsMerchantModel merchant, final CybsMerchantProfileType profileType)
    {
        validateParameterNotNull(merchant, "merchant must not be null");
        validateParameterNotNull(profileType, "merchant profile must not be null");

        return merchant.getProfiles().stream()
                .filter(p -> profileType.equals(p.getProfileType()))
                .findFirst().get();
    }

    @Override
    public List<CybsMerchantModel> getAllMerchants()
    {
        return flexibleSearchService
                .<CybsMerchantModel>search(SELECT_ALL_MERCHANTS_QUERY)
                .getResult();
    }
}
