package com.cybersource.payment.service.executor.request.converter;

import com.cybersource.payment.service.executor.request.PaymentServiceRequest;
import com.cybersource.payment.service.request.AuthorizationType;
import com.cybersource.payment.service.request.CybersourceRequest;
import com.cybersource.payment.service.request.value.CardType;
import com.cybersource.payment.utils.PaymentParamUtils;
import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.payment.dto.CardInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.Assert;

import java.math.BigDecimal;

/*
* A component that encapsulates conversion logic from {@link PaymentServiceRequest} to
* {@link CybersourceRequest} for payment authorize request.
*/
public class AuthorizeRequestConverter extends AbstractRequestConverter
{
    @Override
    public CybersourceRequest convert(final PaymentServiceRequest source)
    {
        final AuthorizationType.Optional authorization = setCommonFields(source);
        setCardInfoFields(source, authorization);
        setPayerAuthenticationFields(source, authorization);

        return authorization.request();
    }

    private AuthorizationType.Optional setCommonFields(final PaymentServiceRequest request)
    {
        final AbstractOrderModel order = request.getRequiredParam("order");
        final AddressModel billingAddress = order.getPaymentAddress();
        Assert.notNull(billingAddress, "billingAddress is required on order");

        return cybersourceRequestFactory.authorize()
                .merchantId(request.getRequiredParam("merchantId"))
                .merchantReferenceCode(order.getGuid())
                .ccAuthServiceRun(true)

                .purchaseTotalsCurrency(order.getCurrency().getIsocode())
                .purchaseTotalsGrandTotalAmount(BigDecimal.valueOf(order.getTotalPrice()))

                .billToFirstName(billingAddress.getFirstname())
                .billToLastName(billingAddress.getLastname())
                .billToEmail(billingAddress.getEmail())
                .billToCountry(billingAddress.getCountry().getIsocode())
                .billToCity(billingAddress.getTown())
                .billToPostalCode(billingAddress.getPostalcode())
                .billToState(PaymentParamUtils.getValue(billingAddress.getRegion(), RegionModel::getIsocodeShort))
                .billToStreet1(billingAddress.getStreetnumber())
                .billToStreet2(billingAddress.getStreetname());
    }

    private void setCardInfoFields(final PaymentServiceRequest request, final AuthorizationType.Optional authorization)
    {
        final CardInfo cardInfo = request.getParam("card");
        final String subscriptionID = request.getParam("subscriptionID");
        Assert.isTrue(cardInfo != null || StringUtils.isNotEmpty(subscriptionID), "one of card info or subscription id must be supplied");

        if (StringUtils.isNotEmpty(subscriptionID))
        {
            authorization.recurringSubscriptionInfoSubscriptionID(subscriptionID);
        }
        else if (cardInfo != null)
        {
            authorization
                    .cardType(CardType.valueOf(cardInfo.getCardType().toString()))
                    .cardAccountNumber(cardInfo.getCardNumber())
                    .cardCvNumber(cardInfo.getCv2Number())
                    .cardExpirationMonth(PaymentParamUtils.getMonth(cardInfo.getExpirationMonth()))
                    .cardExpirationYear(PaymentParamUtils.toString(cardInfo.getExpirationYear()))
                    .cardStartMonth(PaymentParamUtils.getMonth(cardInfo.getIssueMonth()))
                    .cardStartYear(PaymentParamUtils.toString(cardInfo.getIssueYear()));
        }
    }

    private void setPayerAuthenticationFields(final PaymentServiceRequest request, final AuthorizationType.Optional authorization)
    {
        final String paRes = request.getParam("paRes");

        if (StringUtils.isNotEmpty(paRes))
        {
            authorization
                    .payerAuthValidateServiceRun(true)
                    .payerAuthValidateServiceSignedPARes(paRes);
        }
    }
}
