package com.cybersource.payment.security.service;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DefaultEncryptionService implements EncryptionService
{
    private static final Logger LOG = LoggerFactory.getLogger(DefaultEncryptionService.class);

    private static final String HMAC_SHA_256 = "HmacSHA256";

    @Override
    public String signForSecureAcceptance(final String dataToSign, final String secretKey)
    {
        try
        {
            final SecretKeySpec secretKeySpec = new SecretKeySpec(secretKey.getBytes(), HMAC_SHA_256);
            final Mac mac = Mac.getInstance(HMAC_SHA_256);
            mac.init(secretKeySpec);
            byte[] rawHmac = mac.doFinal(dataToSign.getBytes());
            return Base64.encodeBase64String(rawHmac).trim();
        }
        catch (final Exception e)
        {
            LOG.error("Couldn't sign data for Secure Acceptance SOP", e);
            return StringUtils.EMPTY;
        }
    }
}
