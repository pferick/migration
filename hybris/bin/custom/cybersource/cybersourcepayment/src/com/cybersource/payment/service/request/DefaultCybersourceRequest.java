package com.cybersource.payment.service.request;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.ObjectUtils;
import org.springframework.util.Assert;

import com.cybersource.payment.service.request.validation.CybersourceFieldValidator;
import com.cybersource.payment.service.request.value.MerchantDefinedField;
import com.google.common.base.Splitter;

public class DefaultCybersourceRequest implements CybersourceRequest
{
	private static final String VALIDATION_ERROR_MESSAGE = "request validation failed for {0}, field {1}";

	private final Map<String, Object> requestFields = new HashMap<>();

	private final Map<MerchantDefinedField, Object> customFields = new HashMap<>();

	private final Map<String, Map<String, String>> requestFieldRules = new HashMap<>();

	private final Map<MerchantDefinedField, Map<String, String>> customFieldRules = new HashMap<>();

	private final List<CybersourceFieldValidator> validators;

	protected DefaultCybersourceRequest(final List<CybersourceFieldValidator> validators)
	{
		this.validators = validators;
	}

	public Map<String, Object> getRequestFields()
	{
		return Collections.unmodifiableMap(requestFields);
	}

	public Map<MerchantDefinedField, Object> getCustomFields()
	{
		return Collections.unmodifiableMap(customFields);
	}

	public void addRequestField(final String name, final Object value, final String rulesStr)
	{
		requestFields.put(name, value);
		addFieldValidationRules(requestFieldRules, name, rulesStr);
	}

	public void addCustomField(final MerchantDefinedField field, final Object value, final String rulesStr)
	{
		customFields.put(field, value);
		addFieldValidationRules(customFieldRules, field, rulesStr);
	}

	public void validate()
	{
		validate(requestFields, requestFieldRules);
		validate(customFields, customFieldRules);
	}

	private <T> void validate(final Map<T, Object> fields, final Map<T, Map<String, String>> fieldRules)
	{
		final List<String> errors = new ArrayList<>();

		for (final Map.Entry<T, Map<String, String>> fieldRule : fieldRules.entrySet())
		{
			final T field = fieldRule.getKey();
			final String value = ObjectUtils.toString(fields.get(field));

			errors.addAll(validators.stream().map(validator -> validator.validate(value, fieldRule.getValue())
					? null : MessageFormat.format(VALIDATION_ERROR_MESSAGE, validator.getClass().getSimpleName(), field))
					.filter(Objects::nonNull).collect(Collectors.toList()));
		}

		Assert.isTrue(CollectionUtils.isEmpty(errors), "Cybersource request validation failed, errors: " + errors);
	}

	private <T> void addFieldValidationRules(final Map<T, Map<String, String>> rulesMap, final T field, final String rulesStr)
	{
		final Map<String, String> rules = Splitter.on(",").omitEmptyStrings().trimResults().withKeyValueSeparator(":").split(rulesStr);
		if (MapUtils.isNotEmpty(rules))
		{
			rulesMap.put(field, rules);
		}
	}
}
