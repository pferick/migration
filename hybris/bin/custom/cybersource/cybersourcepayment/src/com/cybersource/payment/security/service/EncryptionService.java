package com.cybersource.payment.security.service;

/**
 * Service used to encrypt values
 */
public interface EncryptionService
{
    /**
     * Signs payment data for Secure Acceptance.
     *
     * @param dataToSign data string to sign
     * @param secretKey secret key
     * @return signed payment data
     */
    String signForSecureAcceptance(final String dataToSign, final String secretKey);
}
