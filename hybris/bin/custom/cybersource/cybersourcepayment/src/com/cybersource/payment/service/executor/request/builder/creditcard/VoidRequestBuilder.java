package com.cybersource.payment.service.executor.request.builder.creditcard;

import com.cybersource.payment.enums.CybsPaymentType;
import com.cybersource.payment.service.executor.request.builder.PaymentServiceRequestBuilder;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;

/**
 * Builder for payment void transaction request.
 */
public class VoidRequestBuilder extends PaymentServiceRequestBuilder<VoidRequestBuilder>
{
    /**
     * Creates a new instance of {@link VoidRequestBuilder} class with method CREDIT_CARD and
     * service VOID.
     *
     * @see CybsPaymentType
     * @see PaymentTransactionType
     */
    public VoidRequestBuilder()
    {
        paymentServiceRequest.method(CybsPaymentType.CREDIT_CARD);
        paymentServiceRequest.service(PaymentTransactionType.VOID);
    }

    public VoidRequestBuilder setOrder(final AbstractOrderModel order)
    {
        paymentServiceRequest.addParam("order", order);
        return this;
    }

    public VoidRequestBuilder setTransactionEntry(final PaymentTransactionEntryModel transactionEntry)
    {
        paymentServiceRequest.addParam("transactionEntry", transactionEntry);
        return this;
    }
}
