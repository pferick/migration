package com.cybersource.payment.data.mapper;

import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;

import ma.glasnost.orika.metadata.FieldMapBuilder;
import org.springframework.beans.factory.annotation.Required;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import ma.glasnost.orika.CustomConverter;
import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.converter.ConverterFactory;
import ma.glasnost.orika.impl.ConfigurableMapper;
import ma.glasnost.orika.metadata.ClassMapBuilder;

/**
 * Orika based data mapper to allow declarative/dynamic definition of field mappings (using map).
 * Keys from field mappings map will be used as source bean field name and values as destination field name.
 */
public class DeclarativeDataMapper extends ConfigurableMapper
{
    private Class source;

    private Class destination;

    private Map<String, String> fieldMappings = Maps.newConcurrentMap();

    private List<CustomMapper> customMappers = Lists.newArrayList();

    private List<CustomConverter> customConverters = Lists.newArrayList();

    private Map<String, CustomConverter> fieldConverters = Maps.newConcurrentMap();

    public DeclarativeDataMapper()
    {
        // disable mapper auto initialize
        super(false);
    }

    /**
     * Initialize data mapper for given source, destination and fields mappings.
     */
    @PostConstruct
    public void initialize()
    {
        super.init();
    }

    @Override
    @SuppressWarnings("unchecked")
    protected void configure(final MapperFactory factory)
    {
        final ClassMapBuilder builder = factory.classMap(source, destination);

        addCustomConverters(factory);
        addFieldConverters(factory);

        addFieldsMappings(builder);
        addCustomMappers(builder);

        builder.mapNulls(false).register();
    }

    private void addFieldsMappings(final ClassMapBuilder builder)
    {
        fieldMappings.entrySet().forEach(mapping -> addFieldMap(builder, mapping.getKey(), mapping.getValue()));
    }

    @SuppressWarnings("unchecked")
    private void addCustomMappers(final ClassMapBuilder builder)
    {
        builder.customize(new CustomMapper()
        {
            @Override
            public void mapAtoB(final Object a, final Object b, final MappingContext context)
            {
                customMappers.forEach(customMapper -> customMapper.mapAtoB(a, b, context));
            }
        });
    }

    private void addCustomConverters(final MapperFactory factory)
    {
        final ConverterFactory converterFactory = factory.getConverterFactory();
        customConverters.forEach(converterFactory::registerConverter);
    }

    @SuppressWarnings("unchecked")
    private void addFieldConverters(final MapperFactory factory)
    {
        final ConverterFactory converterFactory = factory.getConverterFactory();
        fieldConverters.entrySet().forEach(c -> converterFactory.registerConverter(c.getKey(), c.getValue()));
    }

    private void addFieldMap(final ClassMapBuilder builder, final String fieldA, final String fieldB)
    {
        final FieldInfo fieldInfoB = FieldInfo.parse(fieldB);

        final FieldMapBuilder fieldMapBuilder = builder.fieldMap(fieldA, fieldInfoB.getName());

        if (fieldInfoB.hasConverterId())
        {
            fieldMapBuilder.converter(fieldInfoB.getConverter());
        }

        fieldMapBuilder.aToB().add();
    }

    /**
     * Sets the source class to map the fields from.
     *
     * @param source the class of the source object
     */
    @Required
    public void setSource(final Class source)
    {
        this.source = source;
    }

    /**
     * Sets the destination class to map the fields to.
     *
     * @param destination the class of the destination object
     */
    @Required
    public void setDestination(final Class destination)
    {
        this.destination = destination;
    }

    /**
     * Sets the map containing mapping rules.
     * map key - the name/expression of the source field
     * map value - the name/expression of the destination field
     *
     * @param fieldMappings field mapping rules
     */
    @Required
    public void setFieldMappings(final Map<String, String> fieldMappings)
    {
        this.fieldMappings = fieldMappings;
    }

    /**
     * Sets the customMappers with custom mappers required to convert complex types, which are not supported by Orika OOTB
     * @param customMappers list of custom converters
     */
    public void setCustomMappers(final List<CustomMapper> customMappers)
    {
        this.customMappers = customMappers;
    }

    /**
     * Sets the list with custom converters required to convert complex types, which are not supported by Orika OOTB
     * @param customConverters list of custom converters
     */
    public void setCustomConverters(final List<CustomConverter> customConverters)
    {
        this.customConverters = customConverters;
    }

    /**
     * Sets the map with name converters required to convert complex types, which are not supported by Orika OOTB
     * @param fieldConverters map of custom converters
     */
    public void setFieldConverters(final Map<String, CustomConverter> fieldConverters)
    {
        this.fieldConverters = fieldConverters;
    }
}
