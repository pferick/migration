package com.cybersource.payment.report.service;

import de.hybris.platform.servicelayer.time.TimeService;
import org.joda.time.DateTime;
import org.joda.time.Interval;

import javax.annotation.Resource;
import java.util.Date;

import static org.apache.commons.lang.math.NumberUtils.INTEGER_ONE;
import static org.apache.commons.lang3.time.DateUtils.isSameDay;

/**
 * Calculates time interval for daily conversion report
 */
public class DailyConversionReportTimeService implements ReportTimeService
{
    @Resource
    private TimeService timeService;

    @Override
    public Interval getReportInterval(final Date lastRunDate)
    {
        final Date now = timeService.getCurrentTime();
        final Date yesterday = new DateTime(now).minusDays(INTEGER_ONE).toDate();

        final Date start = shouldFallbackToYesterday(lastRunDate) ? yesterday : oneDayLater(lastRunDate);
        final Date end = new DateTime(start).plusDays(INTEGER_ONE).toDate();

        return new Interval(start.getTime(), end.getTime());
    }

    private boolean shouldFallbackToYesterday(final Date lastRunDate)
    {
        final Date now = timeService.getCurrentTime();
        return lastRunDate == null || isSameDay(now, oneDayLater(lastRunDate));
    }

    private Date oneDayLater(final Date date)
    {
        return new DateTime(date).plusDays(INTEGER_ONE).toDate();
    }
}
