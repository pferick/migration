package com.cybersource.payment.data;

/**
 * Defines converter interface
 * @param <S> source type
 * @param <D> destination type
 */
public interface Converter<S, D>
{
    /**
     * Convert from source object to destination object
     *
     * @param source object which should be converted
     * @return an instance of destination object
     */
    D convert(S source);
}
