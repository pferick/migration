package com.cybersource.payment.exception;

/**
 * Defines application exception used by Cybersource payment service extension
 */
public class CybersourcePaymentException extends RuntimeException

{
    public CybersourcePaymentException(final String message)
    {
        super(message);
    }

    public CybersourcePaymentException(final String message, final Throwable cause)
    {
        super(message, cause);
    }
}
