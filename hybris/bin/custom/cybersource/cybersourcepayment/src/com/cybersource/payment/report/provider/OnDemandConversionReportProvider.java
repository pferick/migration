package com.cybersource.payment.report.provider;

import com.cybersource.payment.report.request.OnDemandConversionReportRequest;
import com.cybersource.reports.conversion.Report;
import org.apache.http.auth.Credentials;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.beans.factory.annotation.Required;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import static com.cybersource.payment.utils.TimeUtils.toConversionReportDate;
import static com.cybersource.payment.utils.TimeUtils.toConversionReportTime;

/**
 * This class is responsible for obtaining on demand conversion report
 */
public class OnDemandConversionReportProvider extends AbstractHttpReportProvider<OnDemandConversionReportRequest, Report>
{
    private String reportUrl;

    private Credentials credentials;

    @Override
    protected HttpUriRequest buildRequest(final OnDemandConversionReportRequest request)
    {
        final HttpPost httpPost = new HttpPost(reportUrl);
        httpPost.setEntity(new UrlEncodedFormEntity(httpRequestParams(request)));
        return httpPost;
    }

    private Collection<BasicNameValuePair> httpRequestParams(final OnDemandConversionReportRequest request)
    {
        final Collection<BasicNameValuePair> params = new ArrayList<>();

        params.add(new BasicNameValuePair("merchantID", request.getMerchantId()));
        params.add(new BasicNameValuePair("username", credentials.getUserPrincipal().getName()));
        params.add(new BasicNameValuePair("password", credentials.getPassword()));

        final Date startDate = request.getStartDate();

        params.add(new BasicNameValuePair("startDate", toConversionReportDate(startDate)));
        params.add(new BasicNameValuePair("startTime", toConversionReportTime(startDate)));

        final Date endDate = request.getEndDate();

        params.add(new BasicNameValuePair("endDate", toConversionReportDate(endDate)));
        params.add(new BasicNameValuePair("endTime", toConversionReportTime(endDate)));

        return params;
    }

    @Required
    public void setReportUrl(final String reportUrl)
    {
        this.reportUrl = reportUrl;
    }

    @Required
    public void setCredentials(final Credentials credentials)
    {
        this.credentials = credentials;
    }
}
