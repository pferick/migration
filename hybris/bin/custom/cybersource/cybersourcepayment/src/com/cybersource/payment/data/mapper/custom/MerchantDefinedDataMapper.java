package com.cybersource.payment.data.mapper.custom;

import java.math.BigInteger;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;

import com.cybersource.payment.service.request.CybersourceRequest;
import com.cybersource.schemas.transaction_data_1.MDDField;
import com.cybersource.schemas.transaction_data_1.MerchantDefinedData;
import com.cybersource.schemas.transaction_data_1.RequestMessage;

import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MappingContext;

/**
 * Custom Data Mapper to convert cybersource request custom fields into request message merchant defined data.
 */
public class MerchantDefinedDataMapper extends CustomMapper<CybersourceRequest, RequestMessage>
{
    @Override
    public void mapAtoB(final CybersourceRequest request, final RequestMessage requestMessage, final MappingContext context)
    {
        final List<MDDField> mddFields = request.getCustomFields().entrySet().stream()
                .map(customField -> {
                    final MDDField mddField = new MDDField();
                    mddField.setId(new BigInteger(customField.getKey().getName()));
                    mddField.setValue("" + customField.getValue());

                    return mddField;
                })
                .collect(Collectors.toList());

        if (CollectionUtils.isNotEmpty(mddFields))
        {
            final MerchantDefinedData mdd = new MerchantDefinedData();
            mdd.getMddField().addAll(mddFields);

            requestMessage.setMerchantDefinedData(mdd);
        }
    }
}
