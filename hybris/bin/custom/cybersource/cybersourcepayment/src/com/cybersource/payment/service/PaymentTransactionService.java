package com.cybersource.payment.service;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;

import java.util.Optional;

import com.cybersource.payment.enums.CybsPaymentType;

/**
 * Service to provide methods to work with payment transaction and transaction entries.
 */
public interface PaymentTransactionService
{
    /**
     * Gets order payment transaction that matches given payment type.
     *
     * @param paymentType the payment type to match against payment transaction
     * @param order the order that contains payment transactions
     * @return payment transaction that matches payment type, if exists
     */
    Optional<PaymentTransactionModel> getTransaction(final CybsPaymentType paymentType, final AbstractOrderModel order);

    /**
     * Gets payment transaction entry that matches given type and statuses.
     *
     * @param paymentTransaction the payment transaction that contains transaction entries
     * @param type the payment transaction type (e.g. AUTHORIZATION, CAPTURE, etc.)
     * @param statuses the payment transaction statuses (e.g. ACCEPT, REVIEW, etc.)
     * @return payment transaction entry that matches given criteria, if exists
     */
    Optional<PaymentTransactionEntryModel> getLatestTransactionEntry(final PaymentTransactionModel paymentTransaction,
                                                                     final PaymentTransactionType type,
                                                                     final String... statuses);

    /**
     * Gets payment transaction entry that matches given type and in ACCEPTED status.
     *
     * @param paymentTransaction the payment transaction that contains transaction entries
     * @param type the payment transaction type (e.g. AUTHORIZATION, CAPTURE, etc.)
     * @return payment transaction entry that matches given criteria, if exists
     */
    Optional<PaymentTransactionEntryModel> getLatestAcceptedTransactionEntry(final PaymentTransactionModel paymentTransaction,
                                                                             final PaymentTransactionType type);
}
