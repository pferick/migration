package com.cybersource.payment.setup;

import com.cybersource.payment.constants.CybersourcePaymentConstants;
import de.hybris.platform.commerceservices.setup.SetupImpexService;
import de.hybris.platform.core.initialization.SystemSetup;

import javax.annotation.Resource;

/**
 * This class is responsible for executing system maintaining tasks during initialization or update system,
 * like importing project and essential data
 */
@SystemSetup(extension = CybersourcePaymentConstants.EXTENSIONNAME)
public class CybersourcePaymentSystemSetup
{
    private static final String DATA_IMPORT_FOLDER = "/import/";

    @Resource
    private SetupImpexService setupImpexService;


    @SystemSetup(process = SystemSetup.Process.INIT, type = SystemSetup.Type.ESSENTIAL)
	public void createEssentialData()
	{
        setupImpexService.importImpexFile(DATA_IMPORT_FOLDER + "cybersource_reporting_cronjobs.impex", true);
    }
}
