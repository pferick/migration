/**
 * Contains classes used to implement circuit breaker pattern based on hystrix interfaces
 */
package com.cybersource.payment.hystrix;