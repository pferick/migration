package com.cybersource.payment.service.executor;

import java.util.Set;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import com.cybersource.payment.service.executor.request.PaymentServiceRequest;
import com.cybersource.payment.service.provider.PaymentServiceProvider;

/**
 * Default implementation of payment service executor. Dispatches payment service request to an instance of
 * {@link PaymentServiceProvider} which supports requested payment operation.
 */
public class DefaultPaymentServiceExecutor implements PaymentServiceExecutor
{
    private Set<PaymentServiceProvider> serviceProviders;

    @Override
    public PaymentServiceResult execute(final PaymentServiceRequest request)
    {
        Assert.notNull(request, "payment service request cannot be null");

        final PaymentServiceProvider serviceProvider = serviceProviders.stream()
                .filter(ps -> ps.supports(request))
                .findFirst()
                .orElseThrow(
                        () -> new UnsupportedOperationException("no payment service found for request: " + request)
                );

        return serviceProvider.invoke(request);
    }

    @Required
    public void setServiceProviders(final Set<PaymentServiceProvider> serviceProviders)
    {
        this.serviceProviders = serviceProviders;
    }
}
