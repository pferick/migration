package com.cybersource.payment.report.provider;

/**
 * An interface responsible for retrieving a report object
 *
 * @param <P>
 * @param <R>
 */
public interface ReportProvider<P, R>
{
    /**
     * Retrieve a report based on input params
     *
     * @param param data that identifies a report
     * @return a report instance
     */
    R getReport(final P param);
}
