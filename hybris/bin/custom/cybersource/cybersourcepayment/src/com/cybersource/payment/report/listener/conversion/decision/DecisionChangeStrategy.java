package com.cybersource.payment.report.listener.conversion.decision;

import com.cybersource.reports.conversion.Notes;
import de.hybris.platform.core.model.order.OrderModel;

/**
 * Defines an interface responsible for updating order fraud status
 */
public interface DecisionChangeStrategy
{
    void updateOrderFraudStatus(final OrderModel order, final Notes notes);

    boolean supports(final String decision);
}
