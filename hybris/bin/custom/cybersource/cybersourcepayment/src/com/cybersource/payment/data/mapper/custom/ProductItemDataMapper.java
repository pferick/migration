package com.cybersource.payment.data.mapper.custom;

import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Required;

import com.cybersource.payment.service.request.CybersourceRequest;
import com.cybersource.schemas.transaction_data_1.Item;
import com.cybersource.schemas.transaction_data_1.RequestMessage;
import com.google.common.collect.Maps;

import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MappingContext;

/**
 * Custom Data Mapper to convert multivalue fields (e.g. 0:productName, 1:quantity) into request message items.
 */
public class ProductItemDataMapper extends CustomMapper<CybersourceRequest, RequestMessage>
{
    private static final String INDEXED_KEY_REGEX = "\\d+:.*";

    private MapperFacade fieldDataMapper;

    @Override
    public void mapAtoB(final CybersourceRequest request, final RequestMessage requestMessage, final MappingContext context)
    {
        final Map<String, Map<String, Object>> multivalueFields = request.getRequestFields().entrySet().stream()
                .filter(this::isMultivalue)
                .map(entry -> {
                    final String[] keyParts = entry.getKey().split(":", 2);
                    return Maps.immutableEntry(keyParts[0], Maps.immutableEntry(keyParts[1], entry.getValue()));
                })
                .collect(Collectors.groupingBy(
                        Map.Entry::getKey,
                        Collectors.mapping(Map.Entry::getValue, Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue))
                ));

        requestMessage.getItem().addAll(
                multivalueFields.entrySet().stream().map(field -> fieldDataMapper.map(field.getValue(), Item.class))
                        .collect(Collectors.toList())
        );
    }

    private boolean isMultivalue(final Map.Entry<String, Object> requestField)
    {
        return requestField.getKey() != null && requestField.getKey().matches(INDEXED_KEY_REGEX);
    }

    @Required
    public void setFieldDataMapper(final MapperFacade fieldDataMapper)
    {
        this.fieldDataMapper = fieldDataMapper;
    }
}
