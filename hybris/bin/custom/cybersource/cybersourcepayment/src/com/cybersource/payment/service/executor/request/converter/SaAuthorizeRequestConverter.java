package com.cybersource.payment.service.executor.request.converter;

import com.cybersource.payment.service.executor.request.PaymentServiceRequest;
import com.cybersource.payment.service.request.CybersourceRequest;
import de.hybris.platform.core.model.order.AbstractOrderModel;

import java.util.Map;

/*
* A component that encapsulates conversion logic from {@link PaymentServiceRequest} to
* {@link CybersourceRequest} for payment authorization request.
*/
public class SaAuthorizeRequestConverter extends AbstractRequestConverter
{
    @Override
    public CybersourceRequest convert(final PaymentServiceRequest source)
    {
        final AbstractOrderModel order = source.getRequiredParam("order");
        final Map<String, String> paymentResponse = source.getRequiredParam("paymentResponse");

        return cybersourceRequestFactory.saAuthorize()
                .order(order)
                .paymentResponse(paymentResponse)
                .request();
    }
}
