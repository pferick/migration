package com.cybersource.payment.executor

import com.cybersource.payment.hystrix.HystrixWrapperExecutor

import java.util.function.Supplier

class WrapperExecutorMock extends HystrixWrapperExecutor
{
    @Override
    Object execute(final String commandKey, final Supplier action)
    {
        return action.get()
    }
}
