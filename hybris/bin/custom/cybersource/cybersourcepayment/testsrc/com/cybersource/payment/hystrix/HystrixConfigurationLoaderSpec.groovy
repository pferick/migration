package com.cybersource.payment.hystrix

import com.google.common.collect.Maps
import com.netflix.config.ConcurrentCompositeConfiguration
import com.netflix.config.ConcurrentMapConfiguration
import de.hybris.bootstrap.annotations.UnitTest
import de.hybris.platform.servicelayer.config.ConfigurationService
import org.apache.commons.configuration.MapConfiguration
import org.junit.Ignore
import org.junit.Test
import spock.lang.Specification

@UnitTest
class HystrixConfigurationLoaderSpec extends Specification
{
    def configurationService = Mock([useObjenesis: false], ConfigurationService)

    def masterConfiguration = Mock([useObjenesis: false], ConcurrentCompositeConfiguration)

    def hybirisConfiguration = new MapConfiguration(Maps.newConcurrentMap())

    def loader = Spy(HystrixConfigurationLoader, constructorArgs: [configurationService])

    @Test
    @Ignore
    def 'loader should add hybris properties into global config'()
    {
        when:
        loader.loadHystrixConfiguration()

        then:
        1 * configurationService.getConfiguration() >> hybirisConfiguration
        1 * loader.getConfigurationInstance() >> masterConfiguration
        1 * masterConfiguration.configurationNames >> []
        1 * masterConfiguration.addConfiguration(_ as ConcurrentMapConfiguration, "hybrisConfig")
    }
}
