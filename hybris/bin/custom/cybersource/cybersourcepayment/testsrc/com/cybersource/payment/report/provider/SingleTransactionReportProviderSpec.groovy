package com.cybersource.payment.report.provider

import com.cybersource.payment.report.request.SingleTransactionReportRequest
import com.cybersource.reporting.request.value.TransactionQuerySubType
import com.cybersource.reporting.request.value.TransactionQueryType
import de.hybris.bootstrap.annotations.UnitTest
import org.apache.commons.io.IOUtils
import org.junit.Test
import spock.lang.Specification

@UnitTest
class SingleTransactionReportProviderSpec extends Specification {

    def provider = new SingleTransactionReportProvider(reportUrl: 'report.url', reportName: 'report.name')

    @Test
    def 'should build single transaction report http post request'() {
        given:
        def reportRequest = new SingleTransactionReportRequest(merchantID: 'merchantID', requestID: 'requestID',
                merchantReferenceNumber: 'merchantReferenceNumber', type: TransactionQueryType.TRANSACTION,
                subtype: TransactionQuerySubType.TRANSACTION_DETAIL, versionNumber: '1.10'
        )

        when:
        def httpRequest = provider.buildRequest(reportRequest)

        then:
        httpRequest.method == 'POST'
        httpRequest.URI.toString() == provider.reportUrl
        IOUtils.toString(httpRequest.entity.content) == 'merchantID=merchantID&' +
                'requestID=requestID&merchantReferenceNumber=merchantReferenceNumber&targetDate=&' +
                'type=transaction&subtype=transactionDetail&versionNumber=1.10'
    }
}
