package com.cybersource.payment.data.mapper.converter

import de.hybris.bootstrap.annotations.UnitTest
import org.junit.Test
import spock.lang.Specification

@UnitTest
class ObjectToJsonConverterSpec extends Specification
{
    def converter = new ObjectToJsonConverter()

    @Test
    def 'should convert source to destination as json string'()
    {
        when:
        def result = converter.convert([firstName: 'John', lastName: 'Smith'], null)

        then:
        result == '{"firstName":"John","lastName":"Smith"}'

    }
}
