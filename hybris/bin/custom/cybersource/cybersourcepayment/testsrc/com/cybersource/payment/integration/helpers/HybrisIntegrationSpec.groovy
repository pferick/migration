package com.cybersource.payment.integration.helpers

import de.hybris.platform.core.Registry
import de.hybris.platform.servicelayer.ServicelayerBaseTest
import org.apache.log4j.Logger
import org.junit.Before
import org.springframework.beans.BeansException
import org.springframework.beans.factory.config.AutowireCapableBeanFactory
import org.springframework.context.ApplicationContext
import org.springframework.util.ReflectionUtils
import spock.lang.Specification

import javax.annotation.Resource
import java.lang.reflect.Field

abstract class HybrisIntegrationSpec extends Specification
{



    private static final Logger LOG = Logger.getLogger(ServicelayerBaseTest.class)

    @Before
    void prepareApplicationContextAndSession() throws Exception
    {
        ApplicationContext applicationContext = Registry.getApplicationContext()
        this.autowireProperties(applicationContext)
    }

    protected void autowireProperties(ApplicationContext applicationContext)
    {
        final AutowireCapableBeanFactory beanFactory = applicationContext.getAutowireCapableBeanFactory()
        final LinkedHashSet missing = new LinkedHashSet()
        ReflectionUtils.doWithFields(this.getClass(), new ReflectionUtils.FieldCallback() {
            void doWith(Field field) throws IllegalArgumentException, IllegalAccessException
            {
                Resource resource = (Resource) field.getAnnotation(Resource.class)
                if (resource != null)
                {
                    field.setAccessible(true)
                    Object bean = ReflectionUtils.getField(field, HybrisIntegrationSpec.this)
                    if (bean == null)
                    {
                        String beanName = getBeanName(resource, field)

                        try
                        {
                            bean = beanFactory.getBean(beanName, field.getType())
                            if (bean != null)
                            {
                                ReflectionUtils.setField(field, HybrisIntegrationSpec.this, bean)
                            }
                        } catch (BeansException var6)
                        {
                            LOG.error("error fetching bean " + beanName + " : " + var6.getMessage(), var6)
                        }

                        if (bean == null)
                        {
                            missing.add(field.getName())
                        }
                    }
                }

            }
        })
        if (!missing.isEmpty())
        {
            throw new IllegalStateException("test " + this.getClass().getSimpleName() + " is not properly initialized - missing bean references " + missing)
        }
    }

    protected static String getBeanName(Resource resource, Field field)
    {
        return resource.mappedName() != null && resource.mappedName().length() > 0 ? resource.mappedName() : (resource.name() != null && resource.name().length() > 0 ? resource.name() : field.getName())
    }
}
