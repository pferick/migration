package com.cybersource.payment.service.executor.request.converter

import com.cybersource.payment.service.executor.request.PaymentServiceRequest
import com.cybersource.payment.service.request.CybersourceRequestFactory
import com.cybersource.payment.service.request.value.CardType
import com.cybersource.payment.service.request.value.Month
import de.hybris.bootstrap.annotations.UnitTest
import de.hybris.platform.core.enums.CreditCardType
import de.hybris.platform.core.model.c2l.RegionModel
import de.hybris.platform.core.model.order.AbstractOrderModel
import de.hybris.platform.core.model.user.AddressModel
import de.hybris.platform.payment.dto.CardInfo
import org.junit.Test
import spock.lang.Specification

@UnitTest
class AuthorizeRequestConverterSpec extends Specification {
    def cybersourceRequestFactory = Mock([useObjenesis: false], CybersourceRequestFactory)

    def order = Mock([useObjenesis: false], AbstractOrderModel)

    def region = Mock([useObjenesis: false], RegionModel)

    def billingAddress = Mock([useObjenesis: false], AddressModel)

    def cardInfo = Mock([useObjenesis: false], CardInfo)

    def source = PaymentServiceRequest.create()

    def converter = new AuthorizeRequestConverter()

    def authorizationType = new CybersourceRequestFactory(validators: []).authorize()

    def setup() {
        order.guid >> '123'
        order.getPaymentAddress() >> billingAddress
        order.currency >> [isocode: 'USD']
        order.totalPrice >> 100D

        cardInfo.cardType >> CreditCardType.VISA
        cardInfo.cardNumber >> '4111111111111111'
        cardInfo.cv2Number >> '123'
        cardInfo.expirationMonth >> 10
        cardInfo.expirationYear >> 2017

        region.isocodeShort >> 'CA'

        billingAddress.firstname >> 'First'
        billingAddress.lastname >> 'Last'
        billingAddress.email >> 'sample@email.com'
        billingAddress.country >> [isocode: 'US']
        billingAddress.town >> 'San Francisco'
        billingAddress.postalcode >> '98111'
        billingAddress.region >> region
        billingAddress.streetnumber >> '5th'
        billingAddress.streetname >> 'Embarcadero'

        source.addParam('order', order)
        source.addParam('card', cardInfo)
        source.addParam('merchantId', 'tacit_hybris_2')

        converter.cybersourceRequestFactory = cybersourceRequestFactory

        cybersourceRequestFactory.authorize() >> authorizationType
    }

    @Test
    def 'creator should create and populate cybersource request object'() {
        when:
        def target = converter.convert(source)
        def requestFields = target.requestFields

        then:
        target == authorizationType.request

        requestFields['merchantId'] == 'tacit_hybris_2'
        requestFields['merchantReferenceCode'] == '123'
        requestFields['ccAuthServiceRun'] == true
        requestFields['purchaseTotalsCurrency'] == 'USD'
        requestFields['purchaseTotalsGrandTotalAmount'] == 100

        requestFields['cardType'] == CardType.VISA
        requestFields['cardAccountNumber'] == '4111111111111111'
        requestFields['cardCvNumber'] == '123'
        requestFields['cardExpirationMonth'] == Month.OCT
        requestFields['cardExpirationYear'] == '2017'
        requestFields['cardStartMonth'] == null
        requestFields['cardStartYear'] == null

        requestFields['billToFirstName'] == 'First'
        requestFields['billToLastName'] == 'Last'
        requestFields['billToEmail'] == 'sample@email.com'
        requestFields['billToCountry'] == 'US'
        requestFields['billToCity'] == 'San Francisco'
        requestFields['billToPostalCode'] == '98111'
        requestFields['billToState'] == 'CA'
        requestFields['billToStreet1'] == '5th'
        requestFields['billToStreet2'] == 'Embarcadero'
    }

    @Test
    def 'should set subscription id instead of card info if provided'() {
        when:
        source.addParam("subscriptionID", "1234567890")
        def target = converter.convert(source)
        def requestFields = target.requestFields

        then:
        requestFields["recurringSubscriptionInfoSubscriptionID"] == '1234567890'
        requestFields["cardType"] == null
        requestFields["cardAccountNumber"] == null
    }

    @Test
    def 'creator should populate authorization response if provided'() {
        when:
        source.addParam("paRes", "eNqdmElz6kg")
        def target = converter.convert(source)
        def requestFields = target.requestFields

        then:
        requestFields["payerAuthValidateServiceRun"] == true
        requestFields["payerAuthValidateServiceSignedPARes"] == 'eNqdmElz6kg'
    }
}
