package com.cybersource.payment.integration

import com.cybersource.payment.integration.helpers.CybsIntegrationSpec
import com.cybersource.payment.service.executor.request.builder.creditcard.PaymentTokenDeleteRequestBuilder
import de.hybris.bootstrap.annotations.ManualTest
import de.hybris.platform.payment.enums.PaymentTransactionType
import org.junit.Test

@ManualTest
class PaymentTokenDeleteIntegrationSpec extends CybsIntegrationSpec
{

    def builder = new PaymentTokenDeleteRequestBuilder()

    @Test
    "should get ACCEPT from cybs for payment token delete request"()
    {
        given:
        def card = testCard()
        def order = testOrderUk()
        transactionCreator.addAuthorization(order, card)
        transactionCreator.addPaymentToken(order)

        def operationStartTime = new Date()


        when:
        def request = builder
                .setMerchantId(testConfig.merchant)
                .setOrder(order)
                .setTransaction(order.paymentTransactions.first())
                .build()
        def result = paymentServiceExecutor.execute(request)

        then:
        with(result.getData().transactionEntry) {
            type == PaymentTransactionType.DELETE_SUBSCRIPTION
            transactionStatus == 'ACCEPT'
            transactionStatusDetails == '100'
            subscriptionID == null

            code.toString().contains(order.code)
            requestId != null
            currency == null
            requestToken != null


            time > operationStartTime
            time < new Date()
            with(properties) {
                merchantReferenceCode == order.guid
                reasonCode == '100'
                decision == 'ACCEPT'

                invalidField == '[]'
                missingField == '[]'

                requestID != null
                requestToken != null

                paySubscriptionDeleteReplyReasonCode != null
                paySubscriptionDeleteReplySubscriptionID != null
            }
        }
    }

    @Test
    "should get REJECT for wrong existing subscription"()
    {
        given:
        def paymentTransaction = testTransaction(PaymentTransactionType.CREATE_SUBSCRIPTION)
        def order = testOrderUk()
        order.paymentTransactions.add(paymentTransaction)

        def operationStartTime = new Date()

        when:
        def request = builder
                .setMerchantId(testConfig.merchant)
                .setOrder(order)
                .setTransaction(order.paymentTransactions.first())
                .build()
        def result = paymentServiceExecutor.execute(request)

        then:
        with(result.getData().transactionEntry) {
            type == PaymentTransactionType.DELETE_SUBSCRIPTION
            transactionStatus == 'REJECT'
            transactionStatusDetails == '102'
            subscriptionID == null

            code != null
            requestId != null
            currency == null
            requestToken != null

            time > operationStartTime
            time < new Date()
        }
    }

}
