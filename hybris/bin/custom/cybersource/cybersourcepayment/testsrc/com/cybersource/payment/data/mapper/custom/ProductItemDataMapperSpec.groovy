package com.cybersource.payment.data.mapper.custom

import com.cybersource.payment.service.request.CybersourceRequest
import com.cybersource.schemas.transaction_data_1.Item
import com.cybersource.schemas.transaction_data_1.RequestMessage
import de.hybris.bootstrap.annotations.UnitTest
import ma.glasnost.orika.MapperFacade
import ma.glasnost.orika.MappingContext
import org.junit.Test
import spock.lang.Specification

@UnitTest
class ProductItemDataMapperSpec extends Specification
{
    def request = Mock([useObjenesis: false], CybersourceRequest)

    def context = Mock([useObjenesis: false], MappingContext)

    def fieldDataMapper = Mock([useObjenesis:false], MapperFacade)

    def mapper = new ProductItemDataMapper()

    def setup()
    {
        mapper.fieldDataMapper = fieldDataMapper
    }

    @Test
    def 'should group and create request product items'()
    {
        given:
        request.getRequestFields() >> [requestId: 'R001', '0:productName':'USB2', '0:quantity':'3', '1:productName':'USB3']

        when:
        def requestMessage = new RequestMessage()
        mapper.mapAtoB(request, requestMessage, context)

        then:
        requestMessage.item.size() == 2
    }

    @Test
    def 'should not set request product items if no multivalue fields provided'()
    {
        given:
        request.getRequestFields() >> [requestId: 'R001']

        when:
        def requestMessage = new RequestMessage()
        mapper.mapAtoB(request, requestMessage, context)

        then:
        requestMessage.item.size() == 0
    }

    @Test
    def 'should convert product entries using configured item data mapper'()
    {
        given:
        request.getRequestFields() >> [requestId: 'R001', '0:productName':'USB2', '0:quantity':'3', '1:productName':'USB3']

        when:
        mapper.mapAtoB(request, new RequestMessage(), context)

        then:
        1 * fieldDataMapper.map([productName:'USB2', quantity:'3'], Item.class)
        1 * fieldDataMapper.map([productName:'USB3'], Item.class)
    }
}
