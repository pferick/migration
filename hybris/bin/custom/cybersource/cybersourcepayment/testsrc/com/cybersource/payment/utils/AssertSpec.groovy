package com.cybersource.payment.utils

import de.hybris.bootstrap.annotations.UnitTest
import org.junit.Test
import spock.lang.Specification

import java.util.function.Supplier

@UnitTest
class AssertSpec extends Specification
{
    @Test
    def 'assert should throw an exception for invalid expression'()
    {
        when:
        Assert.isTrue(false, new Supplier<RuntimeException>() {
            @Override
            RuntimeException get()
            {
                return new RuntimeException('')
            }
        })

        then:
        thrown RuntimeException
    }
}
