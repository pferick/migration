package com.cybersource.payment.service.executor.request.converter

import com.cybersource.payment.constants.CybersourcePaymentConstants
import com.cybersource.payment.enums.CybsPaymentType
import com.cybersource.payment.service.PaymentTransactionService
import com.cybersource.payment.service.executor.request.PaymentServiceRequest
import com.cybersource.payment.service.request.CaptureType
import com.cybersource.payment.service.request.CybersourceRequest
import com.cybersource.payment.service.request.CybersourceRequestFactory
import de.hybris.bootstrap.annotations.UnitTest
import de.hybris.platform.core.model.c2l.CurrencyModel
import de.hybris.platform.core.model.order.AbstractOrderModel
import de.hybris.platform.payment.enums.PaymentTransactionType
import de.hybris.platform.payment.model.PaymentTransactionEntryModel
import de.hybris.platform.payment.model.PaymentTransactionModel
import org.junit.Test
import spock.lang.Specification

@UnitTest
class CaptureRequestConverterSpec extends Specification {
    def source = PaymentServiceRequest.create()

    def currency = Mock([useObjenesis: false], CurrencyModel)

    def order = Mock([useObjenesis: false], AbstractOrderModel)

    def transaction = Mock([useObjenesis: false], PaymentTransactionModel)

    def transactionEntry = Mock([useObjenesis: false], PaymentTransactionEntryModel)

    def cybersourceRequestFactory = Mock([useObjenesis: false], CybersourceRequestFactory)

    def paymentTransactionService = Mock([useObjenesis: false], PaymentTransactionService)

    def target = Mock([useObjenesis: false], CybersourceRequest)

    def captureType = new CaptureType(target)

    def requestCreator = new CaptureRequestConverter()

    def setup() {
        requestCreator.cybersourceRequestFactory = cybersourceRequestFactory
        requestCreator.paymentTransactionService = paymentTransactionService

        source.paymentType = CybsPaymentType.CREDIT_CARD
        source.paymentTransactionType = PaymentTransactionType.AUTHORIZATION

        source.addParam('merchantId', 'tacit_hybris_2')
        source.addParam('order', order)
        source.addParam('transaction', transaction)

        currency.isocode >> 'EUR'

        order.guid >> '00000001'

        transactionEntry.requestId >> '48481888'
        transactionEntry.requestToken >> 'DpESDdw4ZNX'
        transactionEntry.amount >> BigDecimal.TEN
        transactionEntry.currency >> currency

        cybersourceRequestFactory.capture() >> captureType
    }

    @Test
    def 'should create and populate payment capture cybersource request'() {
        given:
        paymentTransactionService.getLatestTransactionEntry(transaction, PaymentTransactionType.AUTHORIZATION,
                CybersourcePaymentConstants.TransactionStatus.ACCEPT, CybersourcePaymentConstants.TransactionStatus.REVIEW) >> Optional.of(transactionEntry)

        when:
        def target = requestCreator.convert(source)

        then:
        target != null
        1 * target.addRequestField('purchaseTotalsCurrency', 'EUR', _ as String)
        1 * target.addRequestField('merchantReferenceCode', '00000001', _ as String)
        1 * target.addRequestField('ccCaptureServiceAuthRequestID', '48481888', _ as String)
        1 * target.addRequestField('merchantId', 'tacit_hybris_2', _ as String)
        1 * target.addRequestField('ccCaptureServiceAuthRequestToken', 'DpESDdw4ZNX', _ as String)
        1 * target.addRequestField('ccCaptureServiceRun', true, _ as String)
        1 * target.addRequestField('purchaseTotalsGrandTotalAmount', 10, _ as String)
    }

    @Test
    def 'should throw exception if order payment transaction entry not found'() {
        given:
        paymentTransactionService.getLatestTransactionEntry(_ as PaymentTransactionModel, _ as PaymentTransactionType, _) >>
                Optional.empty()

        when:
        requestCreator.convert(source)

        then:
        thrown IllegalStateException
    }
}
