package com.cybersource.payment.service.executor

import com.cybersource.payment.service.executor.request.PaymentServiceRequest
import com.cybersource.payment.service.provider.PaymentServiceProvider
import de.hybris.bootstrap.annotations.UnitTest
import org.junit.Test
import spock.lang.Specification

@UnitTest
class DefaultPaymentServiceExecutorSpec extends Specification
{
    def request = PaymentServiceRequest.create()

    def result = PaymentServiceResult.create()

    def authorizeServiceProvider = Mock([useObjenesis: false], PaymentServiceProvider)

    def captureServiceProvider = Mock([useObjenesis: false], PaymentServiceProvider)

    def serviceExecutor = new DefaultPaymentServiceExecutor()

    def setup()
    {
        serviceExecutor.serviceProviders = [authorizeServiceProvider, captureServiceProvider]
    }

    @Test
    def 'should invoke service provider if supports given request'()
    {
        given:
        request.addParam('orderCode', 'DEV001')
        captureServiceProvider.supports(request) >> true

        when:
        serviceExecutor.execute(request)

        then:
        1 * captureServiceProvider.invoke(request)
        0 * authorizeServiceProvider.invoke(_)
    }

    @Test
    def 'should return service provider invocation result'()
    {
        given:
        request.addParam('orderCode', 'DEV001')
        captureServiceProvider.supports(request) >> true
        captureServiceProvider.invoke(request) >> result

        when:
        def serviceResult = serviceExecutor.execute(request)

        then:
        serviceResult == result
    }

    @Test
    def 'should throw exception if none of service providers support given request'()
    {
        when:
        serviceExecutor.execute(request)

        then:
        thrown UnsupportedOperationException
    }

    @Test
    def 'should throw exception if given request is null'()
    {
        when:
        serviceExecutor.execute(null)

        then:
        thrown IllegalArgumentException
    }
}
