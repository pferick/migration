package com.cybersource.payment.service.executor.request.builder

import com.cybersource.payment.service.executor.request.builder.TaxRequestBuilder
import de.hybris.bootstrap.annotations.UnitTest
import de.hybris.platform.core.model.order.AbstractOrderModel
import org.junit.Test
import spock.lang.Specification

@UnitTest
class TaxRequestBuilderSpec extends Specification
{
    def order = Mock([useObjenesis: false], AbstractOrderModel)

    def builder = new TaxRequestBuilder()

    @Test
    def 'builder should create tax request'()
    {
        given:
        builder.order = order

        when:
        def request = builder.build()

        then:
        request.getParam('order') == order

    }
}
