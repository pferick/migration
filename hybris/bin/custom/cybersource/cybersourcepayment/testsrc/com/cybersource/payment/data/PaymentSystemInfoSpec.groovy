package com.cybersource.payment.data

import de.hybris.bootstrap.annotations.UnitTest
import org.junit.Test
import spock.lang.Specification

@UnitTest
class PaymentSystemInfoSpec extends Specification
{
    def DEFAULT_PARTNER_SOLUTION_ID = "QCWL23QN";

    @Test
    def "should provide correct partnerSolutionID"()
    {
        given:
        def paymentSystemInfo = new PaymentSystemInfo()

        expect:
        paymentSystemInfo.getPartnerSolutionID() == DEFAULT_PARTNER_SOLUTION_ID
    }

}
