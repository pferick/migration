package com.cybersource.payment.service.command

import com.cybersource.payment.executor.WrapperExecutorMock
import com.cybersource.payment.service.request.CybersourceRequest
import com.cybersource.payment.service.response.CybersourceResponse
import com.cybersource.schemas.transaction_data.transactionprocessor.ITransactionProcessor
import com.cybersource.schemas.transaction_data_1.ReplyMessage
import com.cybersource.schemas.transaction_data_1.RequestMessage
import de.hybris.bootstrap.annotations.UnitTest
import ma.glasnost.orika.MapperFacade
import org.junit.Test
import spock.lang.Specification

@UnitTest
class DefaultPaymentCommandSpec extends Specification
{
    def commandName = "commandName"

    def request = Mock([useObjenesis: false], CybersourceRequest)

    def requestMessage = Mock([useObjenesis: false], RequestMessage)

    def replyMessage = Mock([useObjenesis: false], ReplyMessage)

    def requestMapper = Mock([useObjenesis: false], MapperFacade)

    def responseMapper = Mock([useObjenesis: false], MapperFacade)

    def cybersourceWebService = Mock([useObjenesis: false], ITransactionProcessor)

    def paymentWrapperExecutor = new WrapperExecutorMock()

    def command = new DefaultPaymentCommand()

    def setup()
    {
        command.commandName = commandName
        command.requestMapper = requestMapper
        command.responseMapper = responseMapper
        command.paymentWrapperExecutor = paymentWrapperExecutor
        command.cybersourceWebService = cybersourceWebService
    }

    @Test
    def 'should convert cybersource request to request message'()
    {
        when:
        command.perform(request)

        then:
        1 * requestMapper.map(request, RequestMessage.class)
    }

    @Test
    def 'should delegate run transaction to cybersource web service'()
    {
        given:
        requestMapper.map(request, RequestMessage.class) >> requestMessage

        when:
        command.perform(request)

        then:
        1 * cybersourceWebService.runTransaction(requestMessage)
    }

    @Test
    def 'should convert reply message to cybersource response'()
    {
        given:
        requestMapper.map(request, RequestMessage.class) >> requestMessage
        cybersourceWebService.runTransaction(requestMessage) >> replyMessage

        when:
        command.perform(request)

        then:
        1 * responseMapper.map(replyMessage, _ as CybersourceResponse)
    }

    @Test
    def 'should return cybersource response'()
    {
        when:
        def response = command.perform(request)

        then:
        response != null
    }

    @Test
    def 'should throw exception if request is null'()
    {
        when:
        command.perform(null)

        then:
        thrown IllegalArgumentException
    }
}
