package com.cybersource.payment.service.executor.request.builder.creditcard

import com.cybersource.payment.enums.CybsPaymentType
import de.hybris.bootstrap.annotations.UnitTest
import de.hybris.platform.core.model.order.AbstractOrderModel
import de.hybris.platform.payment.enums.PaymentTransactionType
import org.junit.Test
import spock.lang.Specification

@UnitTest
class ValidateRequestBuilderSpec extends Specification
{
    def order = Mock([useObjenesis: false], AbstractOrderModel)

    def builder = new ValidateRequestBuilder()

    @Test
    def 'should set enrollment payment service parameters'()
    {
        when:
        def request = builder.setOrder(order).setPARes("ABCDEF").build();

        then:
        request.paymentType == CybsPaymentType.CREDIT_CARD
        request.paymentTransactionType == PaymentTransactionType.VALIDATE
        request.requestParams.order == order
        request.requestParams.paRes == 'ABCDEF'
    }
}
