package com.cybersource.payment.security.ws

import de.hybris.bootstrap.annotations.UnitTest
import org.junit.Test
import spock.lang.Specification

import javax.xml.ws.handler.Handler
import javax.xml.ws.handler.PortInfo

@UnitTest
class SingleHandlerResolverSpec extends Specification
{
    def portInfo = Mock([useObjenesis: false], PortInfo)

    def securityHandler = Mock([useObjenesis: false], Handler)

    def handlerResolver = new SingleHandlerResolver()

    def setup()
    {
        handlerResolver.securityHandler = securityHandler
    }

    @Test
    def 'should return a list that contains single handler'()
    {
        when:
        def handlerList = handlerResolver.getHandlerChain(portInfo)

        then:
        handlerList.size() == 1
        handlerList.get(0) == securityHandler
    }
}
