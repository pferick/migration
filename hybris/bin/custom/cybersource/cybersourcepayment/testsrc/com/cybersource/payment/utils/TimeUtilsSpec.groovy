package com.cybersource.payment.utils

import de.hybris.bootstrap.annotations.UnitTest
import org.joda.time.DateTime
import org.junit.Test
import spock.lang.Specification

import static com.cybersource.payment.utils.TimeUtils.*
import static org.joda.time.DateTimeZone.UTC

@UnitTest
class TimeUtilsSpec extends Specification 
{
    def date = new DateTime(2017, 1, 25, 11, 10, 50, 2).toDate()

    def utcDate = new DateTime(2017, 1, 25, 11, 10, 50, 2, UTC).toDate()

    @Test
    def 'should convert date to UTC format'()
    {
        when:
        def result = toUTCDateTime(utcDate)

        then:
        result == '2017-01-25T11:10:50Z'
    }

    @Test
    def 'should convert date to cybersrouce format'() {
        when:
        def result = toCybersourceReportDate(date)

        then:
        result == '2017/01/25'
    }

    @Test
    def 'shold format single transaction report date'() {
        when:
        def result = toCybersourceSingleTransactionDate(date)

        then:
        result == '20170125'
    }

    @Test
    def 'should provide conversion report date'() {
        when:
        def result = toConversionReportDate(date)

        then:
        result == '2017-01-25'
    }

    @Test
    def 'should provide conversion report time'() {
        when:
        def result = toConversionReportTime(utcDate)

        then:
        result == '11:10:50'
    }
}
