package com.cybersource.payment.report.listener.conversion

import com.cybersource.payment.dao.PaymentOrderDao
import com.cybersource.payment.model.CybsMerchantModel
import com.cybersource.payment.report.listener.conversion.decision.DecisionChangeStrategy
import com.cybersource.reports.conversion.Conversion
import com.cybersource.reports.conversion.Notes
import com.cybersource.reports.conversion.Report
import de.hybris.bootstrap.annotations.UnitTest
import de.hybris.platform.core.enums.OrderStatus
import de.hybris.platform.core.model.order.OrderModel
import de.hybris.platform.processengine.BusinessProcessService
import org.junit.Test
import spock.lang.Specification

@UnitTest
class ProcessConversionReportListenerSpec extends Specification
{
    def merchant = Mock([useObjenesis: false], CybsMerchantModel)

    def order = Mock([useObjenesis: false], OrderModel)

    def report = Mock([useObjenesis: false], Report)

    def conversion = Mock([useObjenesis: false], Conversion)

    def notes = Mock([useObjenesis: false], Notes)

    def orderDao = Mock([useObjenesis: false], PaymentOrderDao)

    def businessProcessService = Mock([useObjenesis: false], BusinessProcessService)

    def decisionStrategy = Mock([useObjenesis: false], DecisionChangeStrategy)

    def handler = new ProcessConversionReportListener()

    void setup()
    {
        handler.orderDao = orderDao
        handler.businessProcessService = businessProcessService
        handler.decisionStrategies = [decisionStrategy]

        report.conversion >> [conversion]

        conversion.merchantReferenceNumber >> '1234567890'
        conversion.originalDecision >> 'review'
        conversion.newDecision >> 'reject'
        conversion.notes >> notes

        decisionStrategy.supports("REVIEWREJECT") >> true

        orderDao.findOrderByGuid('1234567890') >> order

        order.guid >> '1234567890'
        order.code >> '0987654321'
    }

    @Test
    def 'handler should process conversion report'()
    {
        given:
        order.status >> OrderStatus.FRAUD_DECISION

        when:
        handler.handle(merchant, report)

        then:
        1 * decisionStrategy.updateOrderFraudStatus(order, notes)
        1 * businessProcessService.triggerEvent('0987654321_ReviewDecision')
    }

    @Test
    def 'handler should skip already processed report'()
    {
        given:
        order.status >> OrderStatus.COMPLETED

        when:
        handler.handle(merchant, report)

        then:
        0 * decisionStrategy.updateOrderFraudStatus(_, _)
        0 * businessProcessService.triggerEvent(_)
    }
}
