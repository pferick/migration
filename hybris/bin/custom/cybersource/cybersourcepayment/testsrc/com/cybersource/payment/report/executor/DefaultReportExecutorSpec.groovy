package com.cybersource.payment.report.executor

import com.cybersource.payment.report.provider.ReportProvider
import com.cybersource.payment.report.request.SingleTransactionReportRequest
import de.hybris.bootstrap.annotations.UnitTest
import org.junit.Test
import spock.lang.Specification

import static com.google.common.collect.ImmutableMap.builder

@UnitTest
class DefaultReportExecutorSpec extends Specification {
    def provider = Mock(ReportProvider)

    def executor = new DefaultReportExecutor(providers: builder().put(SingleTransactionReportRequest.class, provider).build())

    @Test
    def 'should invoke provider and return report'() {
        given:
        def report = new Object()
        def query = new SingleTransactionReportRequest()
        provider.getReport(query) >> report

        when:
        def actual = executor.getReport(query)

        then:
        actual == report
    }

    @Test
    def 'should throw exception when report provider not found'() {
        when:
        executor.getReport(new Object())

        then:
        thrown IllegalArgumentException
    }
}
