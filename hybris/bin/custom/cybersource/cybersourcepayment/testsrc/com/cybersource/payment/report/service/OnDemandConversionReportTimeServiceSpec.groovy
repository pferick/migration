package com.cybersource.payment.report.service

import de.hybris.bootstrap.annotations.UnitTest
import de.hybris.platform.servicelayer.time.TimeService
import org.junit.Test
import spock.lang.Specification

import static org.joda.time.DateTime.now

@UnitTest
class OnDemandConversionReportTimeServiceSpec extends Specification {

    def now = now()

    def correction = 5

    def timeService = Mock([useObjenesis: false], TimeService)

    def onDemandConversionReportTimeService = new OnDemandConversionReportTimeService(timeService: timeService, correctionMinutes: correction)

    def setup() {
        timeService.currentTime >> now.toDate()
    }

    @Test
    def 'should provide on-demand first conversion report interval'() {
        when:
        def firstInterval = onDemandConversionReportTimeService.getReportInterval(null)

        then:
        firstInterval.start == now.minusDays(1).plusMinutes(correction)
        firstInterval.end == now.minusMinutes(correction)
    }

    @Test
    def 'should provide on-demand next conversion report interval'() {
        given:
        def lastRunDate = now.minusHours(4)

        when:
        def nextInterval = onDemandConversionReportTimeService.getReportInterval(lastRunDate.toDate())

        then:
        nextInterval.start == lastRunDate.minusMinutes(correction)
        nextInterval.end == now.minusMinutes(correction)
    }

    def 'should fallback to first interval for on-demand report when last run date is more than one day ago'() {
        given:
        def lastRunDate = now.minusDays(2)

        when:
        def nextInterval = onDemandConversionReportTimeService.getReportInterval(lastRunDate.toDate())

        then:
        nextInterval.start == now.minusDays(1).plusMinutes(correction)
        nextInterval.end == now.minusMinutes(correction)
    }

}
