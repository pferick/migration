package com.cybersource.payment.integration

import com.cybersource.payment.integration.helpers.CybsIntegrationSpec
import com.cybersource.payment.service.executor.request.builder.creditcard.VoidRequestBuilder
import de.hybris.bootstrap.annotations.ManualTest
import de.hybris.platform.payment.enums.PaymentTransactionType
import org.junit.Test

@ManualTest
class VoidIntegrationSpec extends CybsIntegrationSpec
{

    def builder = new VoidRequestBuilder()

    @Test
    'should receive Accept from cybs for voiding Capture'()
    {
        given:
        def card = testCard()
        def order = testOrderUk()
        transactionCreator.addAuthorization(order, card)
        transactionCreator.addCapture(order)

        def captureTransactionEntry = order.paymentTransactions.first().entries
                .find { e -> e.type == PaymentTransactionType.CAPTURE }

        def operationStartTime = new Date()

        when:
        def request = builder
                .setMerchantId(testConfig.merchant)
                .setOrder(order)
                .setTransactionEntry(captureTransactionEntry)
                .build()
        def result = paymentServiceExecutor.execute(request)

        then:
        with(result.getData().transactionEntry) {
            type == PaymentTransactionType.VOID
            transactionStatus == 'ACCEPT'
            transactionStatusDetails == '100'

            amount == order.totalPrice
            requestId != null
            requestToken != null
            currency.isocode == order.currency.isocode

            code.toString().contains(order.code)

            time > operationStartTime
            time < new Date()

            with(properties) {
                merchantReferenceCode == order.guid
                reasonCode == '100'
                decision == 'ACCEPT'
                purchaseTotalsCurrency == order.currency.isocode
                requestID != null
                requestToken != null

                invalidField == '[]'
                missingField == '[]'

                voidReplyReasonCode == '100'
                voidReplyCurrency != null
                voidReplyRequestDateTime != null
            }
        }
    }

    @Test
    'should receive Accept from cybs for voiding Refund Standalone'()
    {
        given:
        def card = testCard()
        def order = testOrderUk()
        transactionCreator.addRefundStandalone(order, card)

        def captureTransactionEntry = order.paymentTransactions.first().entries
                .find { e -> e.type == PaymentTransactionType.REFUND_STANDALONE }

        def operationStartTime = new Date()

        when:
        def request = builder
                .setMerchantId(testConfig.merchant)
                .setOrder(order)
                .setTransactionEntry(captureTransactionEntry)
                .build()
        def result = paymentServiceExecutor.execute(request)

        then:
        with(result.getData().transactionEntry) {
            type == PaymentTransactionType.VOID
            transactionStatus == 'ACCEPT'
            transactionStatusDetails == '100'

            amount == order.totalPrice
            requestId != null
            requestToken != null
            currency.isocode == order.currency.isocode

            code.toString().contains(order.code)

            time > operationStartTime
            time < new Date()

            with(properties) {
                merchantReferenceCode == order.guid
                reasonCode == '100'
                decision == 'ACCEPT'
                purchaseTotalsCurrency == order.currency.isocode
                requestID != null
                requestToken != null

                invalidField == '[]'
                missingField == '[]'

                voidReplyReasonCode == '100'
                voidReplyCurrency != null
                voidReplyRequestDateTime != null
            }
        }
    }

    @Test
    'should receive Reject from cybs for not existing authorisation'()
    {
        given:

        def order = testOrderUk()
        def captureTransaction = testTransaction(PaymentTransactionType.CAPTURE)
        order.paymentTransactions << captureTransaction

        def operationStartTime = new Date()

        when:
        def request = builder
                .setMerchantId(testConfig.merchant)
                .setOrder(order)
                .setTransactionEntry(captureTransaction.entries.first())
                .build()
        def result = paymentServiceExecutor.execute(request)

        then:
        with(result.getData().transactionEntry) {
            type == PaymentTransactionType.VOID
            transactionStatus == 'REJECT'
            transactionStatusDetails == '102'
            requestId != null
            requestToken != null

            time > operationStartTime
            time < new Date()

            properties.invalidField == '[c:voidService/c:voidRequestID]'
        }
    }
}
