package com.cybersource.payment.service.response

import de.hybris.bootstrap.annotations.UnitTest
import org.junit.Test
import spock.lang.Specification

@UnitTest
class DefaultCybersourceResponseSpec extends Specification
{
    def response = new DefaultCybersourceResponse()

    @Test
    def 'should add response field to the map'()
    {
        when:
        response.addResponseField('requestId', 100)

        then:
        response.responseFields.requestId == 100
    }

    @Test
    def 'should return response field'()
    {
        given:
        response.addResponseField("field", "fieldValue")

        when:
        def result = response.getResponseField('field')

        then:
        result == "fieldValue"
    }
}
