package com.cybersource.payment.service.executor.request.converter

import com.cybersource.payment.enums.CybsPaymentType
import com.cybersource.payment.service.PaymentTransactionService
import com.cybersource.payment.service.executor.request.PaymentServiceRequest
import com.cybersource.payment.service.request.CybersourceRequest
import com.cybersource.payment.service.request.CybersourceRequestFactory
import com.cybersource.payment.service.request.PaymentTokenCreateType
import com.cybersource.payment.service.request.value.RecurringFrequency
import de.hybris.bootstrap.annotations.UnitTest
import de.hybris.platform.core.model.order.AbstractOrderModel
import de.hybris.platform.payment.enums.PaymentTransactionType
import de.hybris.platform.payment.model.PaymentTransactionEntryModel
import de.hybris.platform.payment.model.PaymentTransactionModel
import org.junit.Test
import spock.lang.Specification

@UnitTest
class PaymentTokenCreateRequestConverterSpec extends Specification
{
    def source = PaymentServiceRequest.create()

    def order = Mock([useObjenesis: false], AbstractOrderModel)

    def transaction = Mock([useObjenesis: false], PaymentTransactionModel)

    def transactionEntry = Mock([useObjenesis: false], PaymentTransactionEntryModel)

    def cybersourceRequestFactory = Mock([useObjenesis: false], CybersourceRequestFactory)

    def paymentTransactionService = Mock([useObjenesis: false], PaymentTransactionService)

    def target = Mock([useObjenesis: false], CybersourceRequest)

    def createTokenType = new PaymentTokenCreateType(target)

    def requestCreator = new PaymentTokenCreateRequestConverter()

    def setup()
    {
        requestCreator.cybersourceRequestFactory = cybersourceRequestFactory
        requestCreator.paymentTransactionService = paymentTransactionService

        source.paymentType = CybsPaymentType.CREDIT_CARD
        source.paymentTransactionType = PaymentTransactionType.AUTHORIZATION

        source.addParam('merchantId', 'tacit_hybris_2')
        source.addParam('order', order)
        source.addParam('transaction', transaction)

        order.guid >> '00000001'

        transactionEntry.requestId >> '48481888'
        transactionEntry.requestToken >> 'DpESDdw4ZNX'

        cybersourceRequestFactory.paymentTokenCreate() >> createTokenType
    }

    @Test
    def 'should create and populate payment CreateToken cybersource request'()
    {
        given:
        paymentTransactionService.getLatestAcceptedTransactionEntry(transaction, PaymentTransactionType.AUTHORIZATION) >>
                Optional.of(transactionEntry)

        when:
        def target = requestCreator.convert(source)

        then:
        target != null
        1 * target.addRequestField('merchantId', 'tacit_hybris_2', _ as String)
        1 * target.addRequestField('merchantReferenceCode', '00000001', _ as String)
        1 * target.addRequestField('recurringSubscriptionInfoFrequency',  RecurringFrequency.ON_DEMAND, _ as String)
        1 * target.addRequestField('paySubscriptionCreateServiceRun', true, _ as String)
        1 * target.addRequestField('paySubscriptionCreateServicePaymentRequestID', '48481888', _ as String)
    }

    @Test
    def 'should throw exception if order payment authorization transaction not found'()
    {
        given:
        paymentTransactionService.getLatestAcceptedTransactionEntry(_ as PaymentTransactionModel, _ as PaymentTransactionType) >>
                Optional.empty()

        when:
        requestCreator.convert(source)

        then:
        thrown IllegalStateException
    }
}
