package com.cybersource.payment.service.executor.request.converter

import com.cybersource.payment.model.CybsPaymentTransactionModel
import com.cybersource.payment.request.ReportAuthorizeType
import com.cybersource.payment.service.executor.request.PaymentServiceRequest
import com.cybersource.payment.service.request.CybersourceRequest
import com.cybersource.payment.service.request.CybersourceRequestFactory
import de.hybris.bootstrap.annotations.UnitTest
import de.hybris.platform.core.model.order.AbstractOrderModel
import org.junit.Test
import spock.lang.Specification

@UnitTest
class ReportingAuthorizeRequestConverterSpec extends Specification
{
    def order = Mock([useObjenesis: false], AbstractOrderModel)

    def cybersourceRequest = Mock([useObjenesis: false], CybersourceRequest)

    def cybersourceRequestFactory = Mock([useObjenesis: false], CybersourceRequestFactory)

    def transaction = Mock([useObjenesis: false], CybsPaymentTransactionModel)

    def converter = new ReportingAuthorizeRequestConverter()

    def setup()
    {
        converter.cybersourceRequestFactory = cybersourceRequestFactory
    }

    @Test
    def 'should create cybersource request and set required parameters'()
    {
        given:
        def source = PaymentServiceRequest.create()
                .addParam('order', order)
                .addParam('paymentTransaction', transaction)
        cybersourceRequestFactory.reportAuthorize() >> new ReportAuthorizeType(cybersourceRequest)

        when:
        def target = converter.convert(source)

        then:
        1 * cybersourceRequest.addRequestField('order', order, _ as String)
        1 * cybersourceRequest.addRequestField('paymentTransaction', transaction, _ as String)
        target == cybersourceRequest
    }


    @Test
    def 'fail when order not set'()
    {
        given:
        def source = PaymentServiceRequest.create()
                .addParam('paymentTransaction', transaction)
        cybersourceRequestFactory.reportAuthorize() >> new ReportAuthorizeType(cybersourceRequest)

        when:
        converter.convert(source)

        then:
        thrown IllegalArgumentException
    }

    @Test
    def 'fail when transaction not set'()
    {
        given:
        def source = PaymentServiceRequest.create()
                .addParam('order', order)
        cybersourceRequestFactory.reportAuthorize() >> new ReportAuthorizeType(cybersourceRequest)

        when:
        converter.convert(source)

        then:
        thrown IllegalArgumentException
    }
}
