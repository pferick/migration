package com.cybersource.payment.service.executor.request.builder.creditcard

import de.hybris.bootstrap.annotations.UnitTest
import de.hybris.platform.core.model.order.AbstractOrderModel
import de.hybris.platform.payment.model.PaymentTransactionEntryModel
import org.junit.Test
import spock.lang.Specification

@UnitTest
class VoidRequestBuilderSpec extends Specification
{
    def order = Mock([useObjenesis: false], AbstractOrderModel)

    def transactionEntry = Mock([useObjenesis: false], PaymentTransactionEntryModel)

    def builder = new VoidRequestBuilder()

    @Test
    def 'build should create void transaction type'()
    {
        given:
        builder.order = order
        builder.transactionEntry = transactionEntry

        when:
        def result = builder.build()

        then:
        result.getRequiredParam('order') == order
        result.getRequiredParam('transactionEntry') == transactionEntry
    }
}
