package com.cybersource.payment.integration

import com.cybersource.payment.integration.helpers.CybsIntegrationSpec
import com.cybersource.payment.report.executor.ReportExecutor
import com.cybersource.payment.report.request.SingleTransactionReportRequest
import com.cybersource.reporting.request.value.TransactionQuerySubType
import com.cybersource.reporting.request.value.TransactionQueryType
import de.hybris.bootstrap.annotations.ManualTest
import org.junit.Test

import javax.annotation.Resource

@ManualTest
class SingleTransactionReportIntegrationSpec extends CybsIntegrationSpec
{

    @Resource
    ReportExecutor reportExecutor

    @Test
    'should receive requested report for a transaction ID'()
    {
        given:
        def card = testCard()
        def order = testOrderUk()
        transactionCreator.addAuthorization(order, card)
        def transaction = order.paymentTransactions.first()

        def reportRequest = new SingleTransactionReportRequest(
                merchantID: testConfig.merchant,
                requestID: transaction.requestId,
                type: TransactionQueryType.TRANSACTION,
                subtype: TransactionQuerySubType.TRANSACTION_DETAIL,
                versionNumber: '1.8'
        )

        when:
        def result = reportExecutor.getReport(reportRequest)

        then:
        result != null
        with(result.requests.request.first()) {
            merchantReferenceNumber == order.guid
            requestID == transaction.requestId
            paymentMethod.card != null
            profileList.profile.first().profileDecision == "ACCEPT"
        }
    }

    @Test
    'should receive requested report for merchantReference and date'()
    {
        given:
        def today = new Date()
        def order = testOrderUk()
        def card = testCard()
        transactionCreator.addAuthorization(order, card)
        def transaction = order.paymentTransactions.first()


        def reportRequest = new SingleTransactionReportRequest(
                merchantID: testConfig.merchant,
                merchantReferenceNumber: order.guid,
                targetDate: today,
                type: TransactionQueryType.TRANSACTION,
                subtype: TransactionQuerySubType.TRANSACTION_DETAIL,
                versionNumber: '1.8'
        )

        when:
        def result = reportExecutor.getReport(reportRequest)

        then:
        result != null
        with(result.requests.request.first()) {
            merchantReferenceNumber == order.guid
            requestID == transaction.requestId
            paymentMethod.card != null
            profileList.profile.first().profileDecision == "ACCEPT"
        }
    }


    @Test
    'should throw exception for report with wrong date'()
    {
        given:
        def reportRequest = new SingleTransactionReportRequest(
                merchantID: testConfig.merchant,
                merchantReferenceNumber: '123',
                type: TransactionQueryType.TRANSACTION,
                subtype: TransactionQuerySubType.TRANSACTION_DETAIL
        )

        when:
        reportExecutor.getReport(reportRequest)

        then:
        thrown IllegalArgumentException

    }

}
