package com.cybersource.payment.service.provider

import com.cybersource.payment.enums.CybsPaymentSource
import com.cybersource.payment.enums.CybsPaymentType
import com.cybersource.payment.model.CybsPaymentTransactionEntryModel
import com.cybersource.payment.service.command.PaymentCommand
import com.cybersource.payment.service.executor.request.PaymentServiceRequest
import com.cybersource.payment.service.executor.request.converter.AbstractRequestConverter
import com.cybersource.payment.service.request.CybersourceRequest
import com.cybersource.payment.service.response.CybersourceResponse
import de.hybris.bootstrap.annotations.UnitTest
import de.hybris.platform.payment.enums.PaymentTransactionType
import org.junit.Test
import spock.lang.Specification

@UnitTest
class DefaultPaymentServiceProviderSpec extends Specification
{
    def paymentCommand = Mock([useObjenesis: false], PaymentCommand)

    def paymentTransactionCreator = Mock([useObjenesis: false], PaymentTransactionCreator)

    def cybersourceRequestCreator = Mock([useObjenesis: false], AbstractRequestConverter)

    def cybersourceRequest = Mock([useObjenesis: false], CybersourceRequest)

    def cybersourceResponse = Mock([useObjenesis: false], CybersourceResponse)

    def transactionEntry = Mock([useObjenesis: false], CybsPaymentTransactionEntryModel)

    def provider = new DefaultPaymentServiceProvider()

    def request = PaymentServiceRequest.create()

    def paymentSource = CybsPaymentSource.SECURE_ACCEPTANCE

    def paymentType = CybsPaymentType.CREDIT_CARD

    def paymentTransactionType = PaymentTransactionType.AUTHORIZATION

    def setup()
    {
        provider.paymentSource = paymentSource
        provider.paymentType = paymentType
        provider.paymentTransactionType = paymentTransactionType
        provider.paymentCommand = paymentCommand
        provider.cybersourceRequestConverter = cybersourceRequestCreator
        provider.paymentTransactionCreator = paymentTransactionCreator

        request.source(paymentSource)
        request.method(paymentType)
        request.service(paymentTransactionType)
    }

    @Test
    def 'support should return true for valid request'()
    {
        when:
        def result = provider.supports(request)

        then:
        result
    }

    @Test
    def 'support should return false for invalid request'()
    {
        given:
        request.service(PaymentTransactionType.CAPTURE)

        when:
        def result = provider.supports(request)

        then:
        !result
    }

    @Test
    def 'invoke should forward request to command and return transaction entry'()
    {
        when:
        def result = provider.invoke(request)

        then:
        1 * cybersourceRequestCreator.convert(request) >> cybersourceRequest
        1 * paymentCommand.perform(cybersourceRequest) >> cybersourceResponse
        1 * paymentTransactionCreator.createTransactionEntry(request, cybersourceResponse) >> transactionEntry
        result.getData().get("transactionEntry") == transactionEntry
    }
}
