package com.cybersource.payment.service.executor.request.builder

import com.cybersource.payment.service.executor.request.PaymentServiceRequest
import de.hybris.bootstrap.annotations.UnitTest
import org.junit.Test
import spock.lang.Specification

@UnitTest
class PaymentServiceRequestBuilderSpec extends Specification
{
    def builder = new PaymentServiceRequestBuilder()

    @Test
    def 'should build payment service request'()
    {
        when:
        def request = builder.build();

        then:
        request != null
        request.class == PaymentServiceRequest.class
    }
}
