package com.cybersource.payment.data.mapper.custom

import com.cybersource.payment.data.mapper.converter.ObjectToBigIntegerConverter
import de.hybris.bootstrap.annotations.UnitTest
import ma.glasnost.orika.metadata.TypeFactory
import org.junit.Test
import spock.lang.Specification

@UnitTest
class ObjectToBigIntegerConverterSpec extends Specification
{
    def value = "10"

    def type = TypeFactory.valueOf(BigInteger.class)

    def converter = new ObjectToBigIntegerConverter()

    @Test
    def 'should convert valid String to BigInteger'()
    {
        when:
        def result = converter.convert(value, type)

        then:
        result == BigInteger.valueOf(Long.valueOf(value))
    }

    @Test
    def 'should skip to convert null string'()
    {
        when:
        def result = converter.convert(null, type)

        then:
        result == null
    }

    @Test
    def 'should skip to convert empty string'()
    {
        when:
        converter.convert('', type)

        then:
        thrown NumberFormatException
    }
}
