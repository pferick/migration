package com.cybersource.payment.service.executor.request.builder.creditcard

import de.hybris.bootstrap.annotations.UnitTest
import de.hybris.platform.core.model.order.AbstractOrderModel
import de.hybris.platform.payment.model.PaymentTransactionModel
import spock.lang.Specification

import static com.cybersource.payment.enums.CybsPaymentType.CREDIT_CARD
import static de.hybris.platform.payment.enums.PaymentTransactionType.REFUND_FOLLOW_ON
import static java.math.BigDecimal.TEN

@UnitTest
class RefundFollowOnRequestBuilderSpec extends Specification
{
    def order = Mock([useObjenesis: false], AbstractOrderModel)

    def transaction = Mock([useObjenesis: false], PaymentTransactionModel)

    def 'should create refund follow-on request'()
    {
        when:
        def request = new RefundFollowOnRequestBuilder()
                .setOrder(order).setTransaction(transaction).setAmount(TEN).build()

        then:
        request.paymentType == CREDIT_CARD
        request.paymentTransactionType == REFUND_FOLLOW_ON
        request.requestParams.amount == TEN
        request.requestParams.order == order
        request.requestParams.transaction == transaction
    }

}
