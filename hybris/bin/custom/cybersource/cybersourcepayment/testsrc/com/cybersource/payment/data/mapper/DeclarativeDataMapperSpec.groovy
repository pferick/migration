package com.cybersource.payment.data.mapper

import com.cybersource.payment.data.mapper.converter.ObjectToJsonConverter
import com.cybersource.payment.service.response.CybersourceResponse
import com.cybersource.payment.service.response.DefaultCybersourceResponse
import com.cybersource.schemas.transaction_data_1.ReplyMessage
import com.google.gson.Gson
import de.hybris.bootstrap.annotations.UnitTest
import ma.glasnost.orika.CustomMapper
import ma.glasnost.orika.MappingContext
import ma.glasnost.orika.MappingException
import org.junit.Test
import spock.lang.Specification

@UnitTest
class DeclarativeDataMapperSpec extends Specification
{
    def customMapper = Mock([useObjenesis: false], CustomMapper)

    def dataMapper = new DeclarativeDataMapper()

    def setup()
    {
        dataMapper.setSource(ReplyMessage.class)
        dataMapper.setDestination(CybersourceResponse.class)
    }

    @Test
    def 'should set source to destination fields as per field mappings'()
    {
        given:
        def source = new ReplyMessage(merchantReferenceCode: 'MRC001', requestID: 'RID300')
        dataMapper.fieldMappings = [
                merchantReferenceCode: 'responseFields["merchantReferenceCode"]',
                requestID: 'responseFields["requestID"]'
        ]
        dataMapper.initialize()

        when:
        def destination = dataMapper.map(source, DefaultCybersourceResponse.class)

        then:
        destination.responseFields.merchantReferenceCode == source.merchantReferenceCode
        destination.responseFields.requestID == source.requestID
    }

    @Test
    def 'should set source to destination fields using field converter'()
    {
        given:
        def source = new ReplyMessage(requestID: '300')
        dataMapper.fieldMappings = ['requestID': 'responseFields["requestID"]|toJson']
        dataMapper.fieldConverters = ['toJson': new ObjectToJsonConverter()]
        dataMapper.initialize()

        when:
        def destination = dataMapper.map(source, DefaultCybersourceResponse.class)

        then:
        destination.responseFields.requestID == new Gson().toJson(source.requestID)
    }

    @Test
    def 'should not set source to destination fields by default if mappings are not declared'()
    {
        given:
        def source = new ReplyMessage(merchantReferenceCode: 'MRC001', requestID: 'RID300')
        dataMapper.fieldMappings = [:]
        dataMapper.initialize()

        when:
        def destination = dataMapper.map (source, DefaultCybersourceResponse.class)

        then:
        destination.responseFields == [:]
    }

    @Test
    def 'should throw exception on initialization if mappings contain unknown fields'()
    {
        given:
        dataMapper.setFieldMappings([unknown: 'id'])

        when:
        dataMapper.initialize()

        then:
        thrown MappingException
    }

    @Test
    def 'should use custom mapper if one provided'()
    {
        given:
        dataMapper.fieldMappings = [:]
        dataMapper.customMappers = [customMapper]
        dataMapper.initialize()

        when:
        dataMapper.map(new ReplyMessage(), new DefaultCybersourceResponse())

        then:
        1 * customMapper.mapAtoB(_, _, _ as MappingContext)
    }
}
