package com.cybersource.payment.report.provider.unmarshaller

import com.cybersource.reports.conversion.Report
import de.hybris.bootstrap.annotations.UnitTest
import org.junit.Test
import spock.lang.Specification

@UnitTest
class XmlReportUnmarshallerSpec extends Specification {
    def reportContent = '<?xml version="1.0" encoding="utf-8"?>\n' +
            '<!DOCTYPE Report SYSTEM "https://ebctest.cybersource.com/ebctest/reports/dtd/cdr.dtd">\n' +
            '<Report MerchantID="tacit_hybris_2" \n' +
            '        Name="Conversion Detail Report" \n' +
            '        ReportStartDate="2017-02-02T00:00:00+00:00" \n' +
            '        ReportEndDate="2017-02-03T00:00:00+00:00" \n' +
            '        Version="1.1" \n' +
            '        xmlns="https://ebctest.cybersource.com/ebctest/reports/dtd/cdr.dtd">\n' +
            '    <Conversion MerchantReferenceNumber="00058008" \n' +
            '\t         ConversionDate="2017-02-02T14:57:28+00:00" \n' +
            '\t         RequestID="4860473543426617204108">\n' +
            '        <OriginalDecision>REVIEW</OriginalDecision>\n' +
            '        <NewDecision>REJECT</NewDecision>\n' +
            '        <Reviewer>jsmith</Reviewer>\n' +
            '        <ReviewerComments>some comments</ReviewerComments>\n' +
            '        <Queue>queue</Queue>\n' +
            '        <Profile>profile</Profile>\n' +
            '    </Conversion>\n' +
            '</Report>'

    def unmarshaller = new XmlReportUnmarshaller()

    def setup() {
        unmarshaller.packageName = 'com.cybersource.reports.conversion'
        unmarshaller.reportType = Report
    }

    @Test
    def 'unmarshaller should return report object'() {
        when:
        def report = unmarshaller.apply(reportContent)

        then:
        report.merchantID == 'tacit_hybris_2'
        report.reportStartDate == '2017-02-02T00:00:00+00:00'
        report.reportEndDate == '2017-02-03T00:00:00+00:00'
        report.conversion[0].merchantReferenceNumber == '00058008'
        report.conversion[0].conversionDate == '2017-02-02T14:57:28+00:00'
        report.conversion[0].requestID == '4860473543426617204108'
        report.conversion[0].originalDecision == 'REVIEW'
        report.conversion[0].newDecision == 'REJECT'
        report.conversion[0].reviewer == 'jsmith'
        report.conversion[0].reviewerComments == 'some comments'
        report.conversion[0].queue == 'queue'
        report.conversion[0].profile == 'profile'
    }
}
