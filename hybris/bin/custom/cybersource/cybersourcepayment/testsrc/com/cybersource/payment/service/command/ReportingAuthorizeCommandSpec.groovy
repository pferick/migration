package com.cybersource.payment.service.command

import com.cybersource.payment.enums.CybsPaymentType
import com.cybersource.payment.model.CybsPaymentTransactionModel
import com.cybersource.payment.report.executor.ReportExecutor
import com.cybersource.payment.report.request.SingleTransactionReportRequest
import com.cybersource.payment.service.request.CybersourceRequest
import com.cybersource.payment.service.response.CybersourceResponse
import com.cybersource.reports.transaction.detail.*
import com.google.common.collect.Lists
import de.hybris.bootstrap.annotations.UnitTest
import de.hybris.platform.core.model.order.OrderModel
import ma.glasnost.orika.MapperFacade
import org.junit.Test
import spock.lang.Specification

@UnitTest
class ReportingAuthorizeCommandSpec extends Specification
{
    def reportExecutor = Mock([useObjenesis: false], ReportExecutor)
    def cybersourceRequest = Mock([useObjenesis: false], CybersourceRequest)
    def responseMapper = Mock([useObjenesis: false], MapperFacade)
    def order = Mock([useObjenesis: false], OrderModel)
    def transaction = Mock([useObjenesis: false], CybsPaymentTransactionModel)
    def transactionReport = new Report()
    def request = new Request()

    def command = new ReportingAuthorizeCommand()

    def setup()
    {
        command.responseMapper = responseMapper
        command.reportExecutor = reportExecutor

        transactionReport.requests = new Requests()
        transactionReport.requests.request = Lists.newArrayList(request)
        def replies = new ApplicationReplies()
        request.applicationReplies = replies
        def reply = new ApplicationReply()
        replies.applicationReply = Lists.newArrayList(reply)
        reply.name = "ics_auth"
        reply.RCode = "1"
        reply.RFlag = "SOK"
    }

    @Test
    def 'should get payment response and convert to cybersource response object'()
    {
        given:
        cybersourceRequest.requestFields >> [order: order, paymentTransaction: transaction, method: CybsPaymentType.CREDIT_CARD]


        when:
        def response = command.perform(cybersourceRequest)

        then:
        response != null

        1 * responseMapper.map(request, _ as CybersourceResponse)
        1 * reportExecutor.getReport(_ as SingleTransactionReportRequest) >> transactionReport
    }
}
