package com.cybersource.payment.service.executor.request.builder.creditcard

import com.cybersource.payment.enums.CybsPaymentType
import com.cybersource.payment.service.executor.request.PaymentServiceRequest
import de.hybris.bootstrap.annotations.UnitTest
import de.hybris.platform.core.model.order.AbstractOrderModel
import de.hybris.platform.payment.dto.CardInfo
import org.junit.Test
import spock.lang.Specification

@UnitTest
class RefundStandaloneRequestBuilderSpec extends Specification
{

    def order = Mock([useObjenesis: false], AbstractOrderModel)

    def card = Mock([useObjenesis: false], CardInfo)

    def subscriptionID = '000011'

    @Test
    def 'should build refund standalone service request based on subscription id'()
    {

        when:
        def request = new RefundStandaloneRequestBuilder().setOrder(order).setSubscriptionId(subscriptionID).build()

        then:
        request != null
        request.class == PaymentServiceRequest.class
        request.paymentType == CybsPaymentType.CREDIT_CARD
        request.requestParams.order == order
        request.requestParams.card == null
        request.requestParams.subscriptionID == '000011'
    }

    @Test
    def 'should build refund standalone service request based on card details'()
    {

        when:
        def request = new RefundStandaloneRequestBuilder().setOrder(order).setCard(card).build()

        then:
        request != null
        request.class == PaymentServiceRequest.class
        request.requestParams.order == order
        request.requestParams.card == card
        request.requestParams.subscriptionID == null

    }
}
