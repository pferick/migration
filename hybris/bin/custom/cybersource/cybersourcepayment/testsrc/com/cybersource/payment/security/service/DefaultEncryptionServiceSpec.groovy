package com.cybersource.payment.security.service

import com.cybersource.payment.security.service.DefaultEncryptionService
import de.hybris.bootstrap.annotations.UnitTest
import org.junit.Test
import spock.lang.Specification

@UnitTest
class DefaultEncryptionServiceSpec extends Specification
{
    def service = new DefaultEncryptionService()

    @Test
    def 'should sign data using given secret key'()
    {
        when:
        def signature = service.signForSecureAcceptance('data to sign', 'ABC123')

        then:
        signature == 'dYFZkeDREh3fTv4oiH14uFOTxIFRdQEyFFyJOkSb8g4='
    }

    @Test
    def 'should return empty signature if data cannot be signed'()
    {
        when:
        def signature = service.signForSecureAcceptance(null, 'ABC123')

        then:
        signature == ''
    }
}
