package com.cybersource.payment.dao

import de.hybris.bootstrap.annotations.UnitTest
import de.hybris.platform.core.model.order.OrderModel
import de.hybris.platform.servicelayer.search.SearchResult
import de.hybris.platform.servicelayer.search.impl.SearchResultImpl
import org.junit.Test
import org.spockframework.util.Assert
import spock.lang.Specification

@UnitTest
class DefaultPaymentOrderDaoSpec extends Specification
{
    def order = Mock([useObjenesis: false], OrderModel)

    def dao = new DefaultPaymentOrderDao() {
        @Override
        protected <T> SearchResult<T> search(final String query, final Map params)
        {
            Assert.that(query == 'SELECT {pk} FROM {Order} WHERE {guid} = ?guid')
            Assert.that(params.get('guid') == '1234567890')

            new SearchResultImpl(Collections.singletonList(order), 1, 1, 0)
        }
    }

    @Test
    def 'order dao should return order by guid'()
    {
        when:
        def result = dao.findOrderByGuid("1234567890")

        then:
        result == order
    }
}
