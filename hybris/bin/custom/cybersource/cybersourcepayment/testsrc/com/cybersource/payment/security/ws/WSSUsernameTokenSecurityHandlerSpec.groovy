package com.cybersource.payment.security.ws

import com.cybersource.payment.exception.CybersourcePaymentException
import com.cybersource.payment.model.CybsMerchantModel
import com.cybersource.payment.service.MerchantService
import de.hybris.bootstrap.annotations.UnitTest
import org.junit.Test
import spock.lang.Specification

import javax.xml.soap.SOAPElement
import javax.xml.soap.SOAPEnvelope
import javax.xml.soap.SOAPException
import javax.xml.soap.SOAPHeader
import javax.xml.soap.SOAPMessage
import javax.xml.soap.SOAPPart
import javax.xml.ws.handler.MessageContext
import javax.xml.ws.handler.soap.SOAPMessageContext

@UnitTest
class WSSUsernameTokenSecurityHandlerSpec extends Specification
{
    private static
    final String SECURITY_HEADER_XML = '<?xml version="1.0" encoding="UTF-8"?><wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">\n' +
            '<wsse:UsernameToken xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">\n' +
            '<wsse:Username xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">MID123</wsse:Username>\n' +
            '<wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">ABCDEF</wsse:Password>\n' +
            '</wsse:UsernameToken>\n' +
            '</wsse:Security>\n'

    def messageContext = Mock([useObjenesis: false], SOAPMessageContext)

    def soapMessage = Mock([useObjenesis: false], SOAPMessage)

    def soapPart = Mock([useObjenesis: false], SOAPPart)

    def soapEnvelope = Mock([useObjenesis: false], SOAPEnvelope)

    def soapHeader = Mock([useObjenesis: false], SOAPHeader)

    def merchant = Mock([useObjenesis: false], CybsMerchantModel)

    def merchantService = Mock([useObjenesis: false], MerchantService)

    def wssMerchantResolver = Mock([useObjenesis: false], WSSMerchantResolver)

    def securityHandler = new WSSUsernameTokenSecurityHandler()

    void setup()
    {
        securityHandler.merchantService = merchantService
        securityHandler.wssMerchantResolver = wssMerchantResolver

        messageContext.message >> soapMessage
        soapMessage.getSOAPPart() >> soapPart
        soapPart.envelope >> soapEnvelope
        soapEnvelope.header >> soapHeader

        merchantService.getMerchant('MID123') >> merchant
    }

    @Test
    def 'should add security header with merchant id and password token'()
    {
        given:
        messageContext.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY) >> true
        wssMerchantResolver.getMerchantId(soapEnvelope) >> 'MID123'
        merchant.passwordToken >> 'ABCDEF'

        when:
        securityHandler.handleMessage(messageContext)

        then:
        1 * soapHeader.addChildElement(_ as SOAPElement) >> { arguments ->
            SOAPElement securityElement = arguments[0] as SOAPElement

            assert securityElement as String == SECURITY_HEADER_XML
        }
    }

    @Test
    def 'should not add security header outbound property not set'()
    {
        given:
        messageContext.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY) >> false

        when:
        securityHandler.handleMessage(messageContext)

        then:
        0 * soapHeader.addChildElement(_ as SOAPElement)
    }

    @Test
    def 'should throw exception if merchant id not specified'()
    {
        given:
        messageContext.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY) >> true
        wssMerchantResolver.getMerchantId(soapEnvelope) >> null

        when:
        securityHandler.handleMessage(messageContext)

        then:
        thrown IllegalArgumentException
    }

    @Test
    def 'should throw exception if security header cannot be created'()
    {
        given:
        messageContext.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY) >> true

        when:
        securityHandler.handleMessage(messageContext)

        then:
        soapPart.getEnvelope() >> { throw new SOAPException('test exception') }
        thrown CybersourcePaymentException
    }
}
