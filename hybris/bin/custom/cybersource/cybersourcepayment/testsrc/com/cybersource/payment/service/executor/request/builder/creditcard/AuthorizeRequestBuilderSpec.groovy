package com.cybersource.payment.service.executor.request.builder.creditcard

import com.cybersource.payment.enums.CybsPaymentType
import de.hybris.bootstrap.annotations.UnitTest
import de.hybris.platform.core.model.order.AbstractOrderModel
import de.hybris.platform.payment.dto.CardInfo
import de.hybris.platform.payment.enums.PaymentTransactionType
import org.junit.Test
import spock.lang.Specification

@UnitTest
class AuthorizeRequestBuilderSpec extends Specification
{
    def order = Mock([useObjenesis: false], AbstractOrderModel)

    def cardInfo = Mock([useObjenesis: false], CardInfo)

    def builder = new AuthorizeRequestBuilder()

    @Test
    def 'should set authorize payment service parameters'()
    {
        when:
        def request = builder.setMerchantId("123").setOrder(order).setCardInfo(cardInfo).setPARes('PARes').build();

        then:
        request.paymentType == CybsPaymentType.CREDIT_CARD
        request.paymentTransactionType == PaymentTransactionType.AUTHORIZATION
        request.requestParams.order == order
        request.requestParams.card == cardInfo
        request.requestParams.paRes == 'PARes'
    }
}
