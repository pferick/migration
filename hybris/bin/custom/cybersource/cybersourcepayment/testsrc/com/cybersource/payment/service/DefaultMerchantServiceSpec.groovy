package com.cybersource.payment.service

import com.cybersource.payment.configuration.service.PaymentConfigurationService
import com.cybersource.payment.enums.CybsConfigurationType
import com.cybersource.payment.enums.CybsMerchantProfileType
import com.cybersource.payment.enums.CybsPaymentChannel
import com.cybersource.payment.enums.CybsPaymentType
import com.cybersource.payment.model.CybsMerchantModel
import com.cybersource.payment.model.CybsMerchantPaymentConfigurationModel
import com.cybersource.payment.model.CybsMerchantProfileModel
import com.google.common.collect.ImmutableMap
import de.hybris.bootstrap.annotations.UnitTest
import de.hybris.platform.basecommerce.model.site.BaseSiteModel
import de.hybris.platform.core.model.c2l.CurrencyModel
import de.hybris.platform.servicelayer.i18n.CommonI18NService
import de.hybris.platform.servicelayer.search.FlexibleSearchService
import de.hybris.platform.servicelayer.search.SearchResult
import de.hybris.platform.site.BaseSiteService
import org.junit.Test
import spock.lang.Specification

@UnitTest
class DefaultMerchantServiceSpec extends Specification
{
    def site = Mock([useObjenesis: false], BaseSiteModel)

    def currency = Mock([useObjenesis: false], CurrencyModel)

    def merchant = Mock([useObjenesis: false], CybsMerchantModel)

    def paymentConfiguration = Mock([useObjenesis: false], CybsMerchantPaymentConfigurationModel)

    def merchantProfileSOP = Mock([useObjenesis: false], CybsMerchantProfileModel)

    def merchantProfileHOP = Mock([useObjenesis: false], CybsMerchantProfileModel)

    def siteService = Mock([useObjenesis: false], BaseSiteService)

    def commonI18NService = Mock([useObjenesis: false], CommonI18NService)

    def paymentConfigurationService = Mock([useObjenesis: false], PaymentConfigurationService)

    def flexibleSearchService = Mock([useObjenesis: false], FlexibleSearchService)

    def service = new DefaultMerchantService()

    def setup()
    {
        service.baseSiteService = siteService
        service.commonI18NService = commonI18NService
        service.paymentConfigurationService = paymentConfigurationService
        service.flexibleSearchService = flexibleSearchService

        siteService.getCurrentBaseSite() >> site
        commonI18NService.getCurrentCurrency() >> currency

        paymentConfiguration.merchant >> merchant

        merchantProfileSOP.profileType >> CybsMerchantProfileType.SOP
        merchantProfileHOP.profileType >> CybsMerchantProfileType.HOP

        merchant.profiles >> [merchantProfileSOP, merchantProfileHOP]
    }

    @Test
    def 'should return merchant by given id'()
    {
        given:
        paymentConfigurationService.getConfiguration(CybsConfigurationType.MERCHANT, [id: "merchant_1"]) >> merchant

        when:
        def resultMerchant = service.getMerchant('merchant_1')

        then:
        resultMerchant == merchant
    }

    @Test
    def 'should return merchant profile by given string type'()
    {
        when:
        def profile = service.getMerchantProfile(merchant, 'HOP')

        then:
        profile == merchantProfileHOP
    }

    @Test
    def 'should return merchant profile by given type'()
    {
        when:
        def profile = service.getMerchantProfile(merchant, CybsMerchantProfileType.SOP)

        then:
        profile == merchantProfileSOP
    }

    @Test
    def 'should return current merchant by site, currency, channel, etc'()
    {
        given:
        paymentConfigurationService.getConfiguration(CybsConfigurationType.MERCHANT_CONFIG, ImmutableMap.of(
                CybsMerchantPaymentConfigurationModel.SITE, siteService.getCurrentBaseSite(),
                CybsMerchantPaymentConfigurationModel.PAYMENTTYPE, CybsPaymentType.CREDIT_CARD,
                CybsMerchantPaymentConfigurationModel.PAYMENTCHANNEL, CybsPaymentChannel.WEB,
                CybsMerchantPaymentConfigurationModel.CURRENCY, commonI18NService.getCurrentCurrency()
        )) >> paymentConfiguration

        when:
        def profile = service.getCurrentMerchant()

        then:
        profile == merchant
    }

    @Test
    def 'should return current merchant profile by given type'()
    {
        given:
        paymentConfigurationService.getConfiguration(CybsConfigurationType.MERCHANT_CONFIG, ImmutableMap.of(
                CybsMerchantPaymentConfigurationModel.SITE, siteService.getCurrentBaseSite(),
                CybsMerchantPaymentConfigurationModel.PAYMENTTYPE, CybsPaymentType.CREDIT_CARD,
                CybsMerchantPaymentConfigurationModel.PAYMENTCHANNEL, CybsPaymentChannel.WEB,
                CybsMerchantPaymentConfigurationModel.CURRENCY, commonI18NService.getCurrentCurrency()
        )) >> paymentConfiguration

        when:
        def profile = service.getCurrentMerchantProfile(CybsMerchantProfileType.SOP)

        then:
        profile == merchantProfileSOP
    }

    @Test
    def 'service should return all merchants'()
    {
        def searchResult = Mock([useObjenesis: false], SearchResult)
        searchResult.result >> []

        when:
        service.getAllMerchants()

        then:
        1 * flexibleSearchService.search('SELECT {PK} FROM {' + CybsMerchantModel._TYPECODE + '}') >> searchResult
    }
}
