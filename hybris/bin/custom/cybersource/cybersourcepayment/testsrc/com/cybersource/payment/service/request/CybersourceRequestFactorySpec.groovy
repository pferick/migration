package com.cybersource.payment.service.request

import com.cybersource.payment.service.request.validation.CybersourceFieldValidator
import de.hybris.bootstrap.annotations.UnitTest
import org.junit.Test
import spock.lang.Specification

@UnitTest
class CybersourceRequestFactorySpec extends Specification {
    def validator = Mock([useObjenesis: false], CybersourceFieldValidator)

    def factory = new CybersourceRequestFactory()

    def setup() {
        factory.setValidators(Collections.singletonList(validator))
    }

    @Test
    def 'should create authorization object and set request and validators'() {
        when:
        def authorization = factory.authorize()

        then:
        authorization.class == AuthorizationType
        (authorization.request as DefaultCybersourceRequest).validators.each { assert it == validator }
    }

    @Test
    def 'should create secure acceptance authorization object and set request and validators'() {
        when:
        def authorization = factory.saAuthorize()

        then:
        authorization.class == SaAuthorizationType
        (authorization.request as DefaultCybersourceRequest).validators.each { assert it == validator }
    }

    def 'should create capture object and set request and validators'() {
        when:
        def capture = factory.capture()

        then:
        capture.class == CaptureType
        (capture.request as DefaultCybersourceRequest).validators.each { assert it == validator }
    }

    @Test
    def 'should create delete payment token request'() {
        when:
        def paymentTokenDelete = factory.paymentTokenDelete()

        then:
        paymentTokenDelete.class == PaymentTokenDeleteType
        (paymentTokenDelete.request as DefaultCybersourceRequest).validators.each { assert it == validator }
    }

    @Test
    def 'should create authorization reversal object and set request and validators'() {
        when:
        def capture = factory.authorizeReversal()

        then:
        capture.class == AuthorizationReversalType
        (capture.request as DefaultCybersourceRequest).validators.each { assert it == validator }
    }

    @Test
    def 'should create void transaction object and set request and validators'() {
        when:
        def voidTransaction = factory.voidTransaction()

        then:
        voidTransaction.class == VoidType
        (voidTransaction.request as DefaultCybersourceRequest).validators.each { assert it == validator }
    }

    @Test
    def 'should create refund follow on object and set request and validators'() {
        when:
        def refund = factory.refundFollowOn()

        then:
        refund.class == RefundFollowOnType
        (refund.request as DefaultCybersourceRequest).validators.each { assert it == validator }
    }

    @Test
    def 'should create enrollment object and set request and validators'() {
        when:
        def enrollment = factory.enrollment()

        then:
        enrollment.class == EnrollmentType
        (enrollment.request as DefaultCybersourceRequest).validators.each { assert it == validator }
    }

    @Test
    def 'should create validate object and set request and validators'() {
        when:
        def validate = factory.validate()

        then:
        validate.class == ValidateType
        (validate.request as DefaultCybersourceRequest).validators.each { assert it == validator }
    }
}
