package com.cybersource.payment.service.executor.request.converter

import com.cybersource.payment.service.request.CybersourceRequest
import com.cybersource.payment.service.request.CybersourceRequestFactory
import com.cybersource.payment.service.request.TaxType
import com.cybersource.payment.service.executor.request.PaymentServiceRequest
import de.hybris.bootstrap.annotations.UnitTest
import de.hybris.platform.core.model.c2l.CountryModel
import de.hybris.platform.core.model.c2l.CurrencyModel
import de.hybris.platform.core.model.c2l.RegionModel
import de.hybris.platform.core.model.order.AbstractOrderEntryModel
import de.hybris.platform.core.model.order.AbstractOrderModel
import de.hybris.platform.core.model.product.ProductModel
import de.hybris.platform.core.model.user.AddressModel
import org.junit.Test
import spock.lang.Specification

@UnitTest
class TaxRequestConverterSpec extends Specification
{
    def source = PaymentServiceRequest.create()

    def cybersourceRequest = Mock([useObjenesis: false], CybersourceRequest)

    def order = Mock([useObjenesis: false], AbstractOrderModel)

    def orderEntry = Mock([useObjenesis: false], AbstractOrderEntryModel)

    def currency = Mock([useObjenesis: false], CurrencyModel)

    def country = Mock([useObjenesis: false], CountryModel)

    def address = Mock([useObjenesis: false], AddressModel)

    def region = Mock([useObjenesis: false], RegionModel)

    def product = Mock([useObjenesis: false], ProductModel)

    def tax = Mock([useObjenesis: false], TaxType)

    def optional = Mock([useObjenesis: false], TaxType.Optional)

    def merchantReferenceCode = Mock([useObjenesis: false], TaxType.MerchantReferenceCode)

    def purchaseTotalsCurrency = Mock([useObjenesis: false], TaxType.PurchaseTotalsCurrency)

    def taxServiceRun = Mock([useObjenesis: false], TaxType.TaxServiceRun)

    def billToCountry = Mock([useObjenesis: false], TaxType.BillToCountry)

    def cybersourceRequestFactory = Mock([useObjenesis: false], CybersourceRequestFactory)

    def converter = new TaxRequestConverter(cybersourceRequestFactory:  cybersourceRequestFactory)

    void setup()
    {
        cybersourceRequestFactory.tax() >> tax
        order.getDeliveryAddress() >> address
        order.getPaymentAddress() >> address

        order.entries >> [orderEntry]

        source.addParam('order', order)
        source.addParam('merchantId', 'merchant')

        order.guid >> '1234567890'
        order.currency >> currency

        orderEntry.product >> product
        orderEntry.quantity >> 2
        orderEntry.basePrice >> 5D

        product.code >> '232323'
        product.name >> 'product'

        currency.isocode >> 'GBP'
        country.isocode >> 'GB'

        address.country >> country
        address.company >> 'company'
        address.town >> 'city'
        address.district >> 'district'
        address.email >> 'jsmith@mail.com'
        address.firstname >> 'john'
        address.lastname >> 'smith'
        address.phone1 >> '0987654321'
        address.postalcode >> '54321'
        address.region >> region
        address.streetnumber >> 'nr.1'
        address.streetname >> 'str. name'
        address.building >> 'bl.2'

        region.isocodeShort >> 'CA'

        optional.request() >> cybersourceRequest
    }

    @Test
    def 'converter should create cybersource request'()
    {
        when:
        def target = converter.convert(source)

        then:
        1 * tax.merchantId('merchant') >> merchantReferenceCode
        1 * merchantReferenceCode.merchantReferenceCode('1234567890') >> purchaseTotalsCurrency
        1 * purchaseTotalsCurrency.purchaseTotalsCurrency('GBP') >> taxServiceRun
        1 * taxServiceRun.taxServiceRun(true) >> billToCountry

        1 * billToCountry.billToCountry('GB') >> optional
        1 * optional.billToCity('city') >> optional
        1 * optional.billToCompany('company') >> optional
        1 * optional.billToCounty('district') >> optional
        1 * optional.billToEmail('jsmith@mail.com') >> optional
        1 * optional.billToFirstName('john') >> optional
        1 * optional.billToLastName('smith') >> optional
        1 * optional.billToPhoneNumber('0987654321') >> optional
        1 * optional.billToPostalCode('54321') >> optional
        1 * optional.billToState('CA') >> optional
        1 * optional.billToStreet1('nr.1') >> optional
        1 * optional.billToStreet2('str. name') >> optional
        1 * optional.billToStreet3('bl.2') >> optional

        1 * optional.shipToBuildingNumber('bl.2') >> optional
        1 * optional.shipToCity('city') >> optional
        1 * optional.shipToCompany('company') >> optional
        1 * optional.shipToCountry('GB') >> optional
        1 * optional.shipToCounty('district') >> optional
        1 * optional.shipToDistrict('district') >> optional
        1 * optional.shipToFirstName('john') >> optional
        1 * optional.shipToLastName('smith') >> optional
        1 * optional.shipToPhoneNumber('0987654321') >> optional
        1 * optional.shipToState('CA') >> optional
        1 * optional.shipToStreet1('nr.1') >> optional
        1 * optional.shipToStreet2('str. name') >> optional
        1 * optional.shipToStreet3('bl.2') >> optional

        1 * optional.itemId(0, BigInteger.ZERO)
        1 * optional.itemProductCode(0, '232323')
        1 * optional.itemProductName(0, 'product')
        1 * optional.itemProductSKU(0, '232323')
        1 * optional.itemQuantity(0, 2)
        1 * optional.itemUnitPrice(0, 5D)

        target == cybersourceRequest
    }
}
