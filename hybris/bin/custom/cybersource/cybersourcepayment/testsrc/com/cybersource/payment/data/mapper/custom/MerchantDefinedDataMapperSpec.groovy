package com.cybersource.payment.data.mapper.custom

import com.cybersource.payment.service.request.CybersourceRequest
import com.cybersource.payment.service.request.value.MerchantDefinedField
import com.cybersource.schemas.transaction_data_1.RequestMessage
import de.hybris.bootstrap.annotations.UnitTest
import ma.glasnost.orika.MappingContext
import org.junit.Test
import spock.lang.Specification

@UnitTest
class MerchantDefinedDataMapperSpec extends Specification
{
    def request = Mock([useObjenesis: false], CybersourceRequest)

    def context = Mock([useObjenesis: false], MappingContext)

    def converter = new MerchantDefinedDataMapper()

    @Test
    def 'should convert merchant data to cybersource mdd fields'()
    {
        given:
        request.getCustomFields() >> Collections.singletonMap(MerchantDefinedField.CUSTOMER_DATA, 'CUST001')

        when:
        def requestMessage = new RequestMessage()
        converter.mapAtoB(request, requestMessage, context)

        then:
        requestMessage.merchantDefinedData.mddField.size() == 1
        requestMessage.merchantDefinedData.mddField[0].id == BigInteger.ONE
        requestMessage.merchantDefinedData.mddField[0].value == 'CUST001'
    }

    @Test
    def 'should not create merchant defined data if no custom fields provided'()
    {
        given:
        request.getCustomFields() >> [:]

        when:
        def requestMessage = new RequestMessage()
        converter.mapAtoB(request, requestMessage, context)

        then:
        requestMessage.merchantDefinedData == null
    }
}
