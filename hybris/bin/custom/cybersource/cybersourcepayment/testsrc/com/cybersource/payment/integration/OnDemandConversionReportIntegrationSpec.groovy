package com.cybersource.payment.integration

import com.cybersource.payment.integration.helpers.CybsIntegrationSpec
import com.cybersource.payment.report.executor.ReportExecutor
import com.cybersource.payment.report.request.OnDemandConversionReportRequest
import de.hybris.bootstrap.annotations.ManualTest
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import org.junit.Test

import javax.annotation.Resource

@ManualTest
class OnDemandConversionReportIntegrationSpec extends CybsIntegrationSpec
{

    @Resource
    ReportExecutor reportExecutor

    @Test
    'should receive requested report'()
    {
        given:
        def now = new DateTime()
        def halfDayAgo = new DateTime().minusHours(12)

        def reportRequest = new OnDemandConversionReportRequest(
                startDate: halfDayAgo.toDate(),
                endDate: now.toDate(),
                merchantId: testConfig.merchant
        )
        def fmt = DateTimeFormat.forPattern("yyyy-MM-dd").withZoneUTC()

        when:
        def result = reportExecutor.getReport(reportRequest)

        then:
        result != null
        with(result) {
            name == 'Conversion Detail Report'
            merchantID == testConfig.merchant
            reportStartDate.contains(fmt.print(halfDayAgo))
            reportEndDate.contains(fmt.print(now))
        }
    }

    @Test
    'should throw exception for report with wrong date'()
    {
        given:
        def yesterday = new Date() - 1
        def theOtherDay = yesterday - 1

        def reportRequest = new OnDemandConversionReportRequest(
                startDate: theOtherDay,
                endDate: yesterday,
                merchantId: testConfig.merchant
        )

        when:
        reportExecutor.getReport(reportRequest)

        then:
        thrown IllegalArgumentException
    }

}
