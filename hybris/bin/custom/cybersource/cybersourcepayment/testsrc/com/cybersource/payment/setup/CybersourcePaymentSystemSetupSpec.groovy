package com.cybersource.payment.setup

import de.hybris.bootstrap.annotations.UnitTest
import de.hybris.platform.commerceservices.setup.SetupImpexService
import org.junit.Test
import spock.lang.Specification

@UnitTest
class CybersourcePaymentSystemSetupSpec extends Specification
{
    def setupImpexService = Mock([useObjenesis: false], SetupImpexService)

    def setup = new CybersourcePaymentSystemSetup(setupImpexService: setupImpexService)

    @Test
    def 'setup should import essential data'()
    {
        when:
        setup.createEssentialData()

        then:
        1 * setupImpexService.importImpexFile("/import/cybersource_reporting_cronjobs.impex", true);
    }
}
