package com.cybersource.payment.service.provider

import com.cybersource.payment.enums.CybsPaymentType
import com.cybersource.payment.model.CybsPaymentTransactionEntryModel
import com.cybersource.payment.model.CybsPaymentTransactionModel
import com.cybersource.payment.service.response.DefaultCybersourceResponse
import com.cybersource.payment.service.executor.request.PaymentServiceRequest
import de.hybris.bootstrap.annotations.UnitTest
import de.hybris.platform.core.model.c2l.CurrencyModel
import de.hybris.platform.core.model.order.AbstractOrderModel
import de.hybris.platform.payment.strategy.TransactionCodeGenerator
import de.hybris.platform.servicelayer.i18n.CommonI18NService
import de.hybris.platform.servicelayer.model.ModelService
import de.hybris.platform.servicelayer.time.TimeService
import org.junit.Test
import spock.lang.Specification

@UnitTest
class DefaultPaymentTransactionCreatorSpec extends Specification
{
    def modelService = Mock([useObjenesis: false], ModelService)

    def transactionCodeGenerator = Mock([useObjenesis: false], TransactionCodeGenerator)

    def timeService = Mock([useObjenesis: false], TimeService)

    def currency = Mock([useObjenesis: false], CurrencyModel)

    def order = Mock([useObjenesis: false], AbstractOrderModel)

    def transaction = Mock([useObjenesis: false], CybsPaymentTransactionModel)

    def transactionEntry = Mock([useObjenesis: false], CybsPaymentTransactionEntryModel)

    def currentTime = Mock([useObjenesis: false], Date)

    def commonI18NService = Mock([useObjenesis: false], CommonI18NService)

    def request = PaymentServiceRequest.create()

    def cybersourceResponse = new DefaultCybersourceResponse()

    def creator = new DefaultPaymentTransactionCreator()

    def setup()
    {
        creator.modelService = modelService
        creator.transactionCodeGenerator = transactionCodeGenerator
        creator.timeService = timeService
        creator.commonI18NService = commonI18NService

        request.addParam('merchantId', 'merchant_1')
        request.addParam("order", order)
        request.paymentType = CybsPaymentType.CREDIT_CARD

        cybersourceResponse.addResponseField('requestID', '123')
        cybersourceResponse.addResponseField('requestToken', '321')
        cybersourceResponse.addResponseField('decision', 'ACCEPT')
        cybersourceResponse.addResponseField('reasonCode', new BigInteger('100'))
        cybersourceResponse.addResponseField('purchaseTotalsCurrency', 'EUR')
        cybersourceResponse.addResponseField('amount', '10.12')
        cybersourceResponse.addResponseField('paySubscriptionCreateReplySubscriptionID', '4857756716436115104009')

        currency.isocode >> 'EUR'

        order.code >> '890'

        transactionCodeGenerator.generateCode('890') >> '890-1'
        timeService.currentTime >> currentTime

        transaction.code >> '890-1'
        transaction.paymentProvider >> 'CREDIT_CARD'
        transaction.entries >> []

        commonI18NService.getCurrency(currency.isocode) >> currency
    }

    @Test
    def 'creator should create and save payment transaction and add new transaction entry'()
    {
        given:
        order.getPaymentTransactions() >> []

        when:
        def result = creator.createTransactionEntry(request, cybersourceResponse)

        then:
        1 * modelService.create(CybsPaymentTransactionModel) >> transaction
        1 * transaction.setMerchantId('merchant_1')
        1 * transaction.setCode('890-1')
        1 * transaction.setRequestId('123')
        1 * transaction.setRequestToken('321')
        1 * transaction.setPaymentProvider('CREDIT_CARD')
        1 * transaction.setOrder(order)
        1 * modelService.saveAll(transaction, order)

        1 * modelService.create(CybsPaymentTransactionEntryModel) >> transactionEntry
        1 * transactionEntry.setCode('890-1-1')
        1 * transactionEntry.setPaymentTransaction(transaction)
        1 * transactionEntry.setRequestId('123')
        1 * transactionEntry.setRequestToken('321')
        1 * transactionEntry.setTransactionStatus('ACCEPT')
        1 * transactionEntry.setTransactionStatusDetails('100')
        1 * transactionEntry.setTime(currentTime)
        1 * transactionEntry.setCurrency(currency)
        1 * transactionEntry.setAmount(new BigDecimal('10.12'))
        1 * transactionEntry.setProperties([
                decision: 'ACCEPT', requestID: '123', requestToken: '321', purchaseTotalsCurrency: 'EUR', amount: '10.12',
                reasonCode: '100', paySubscriptionCreateReplySubscriptionID: '4857756716436115104009'
        ])
        1 * transactionEntry.setSubscriptionID('4857756716436115104009')
        1 * modelService.saveAll(transactionEntry, transaction)

        result == transactionEntry
    }

    @Test
    def 'creator should find existing payment transaction and add new transaction entry'()
    {
        given:
        order.getPaymentTransactions() >> [transaction]

        when:
        def result = creator.createTransactionEntry(request, cybersourceResponse)

        then:
        0 * modelService.create(CybsPaymentTransactionModel) >> transaction
        0 * modelService.saveAll(transaction, order)

        1 * modelService.create(CybsPaymentTransactionEntryModel) >> transactionEntry
        1 * transactionEntry.setPaymentTransaction(transaction)
        1 * modelService.saveAll(transactionEntry, transaction)

        result == transactionEntry
    }
}
