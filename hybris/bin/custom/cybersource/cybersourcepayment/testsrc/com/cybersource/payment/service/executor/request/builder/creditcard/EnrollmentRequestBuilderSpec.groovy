package com.cybersource.payment.service.executor.request.builder.creditcard

import com.cybersource.payment.enums.CybsPaymentType
import de.hybris.bootstrap.annotations.UnitTest
import de.hybris.platform.core.model.order.AbstractOrderModel
import de.hybris.platform.payment.dto.CardInfo
import de.hybris.platform.payment.enums.PaymentTransactionType
import org.junit.Test
import spock.lang.Specification

@UnitTest
class EnrollmentRequestBuilderSpec extends Specification
{
    def order = Mock([useObjenesis: false], AbstractOrderModel)

    def cardInfo = Mock([useObjenesis: false], CardInfo)

    def builder = new EnrollmentRequestBuilder()

    @Test
    def 'should set enrollment payment service parameters'()
    {
        when:
        def request = builder.setOrder(order).setCardInfo(cardInfo).build();

        then:
        request.paymentType == CybsPaymentType.CREDIT_CARD
        request.paymentTransactionType == PaymentTransactionType.ENROLLMENT
        request.requestParams.order == order
        request.requestParams.card == cardInfo
    }
}
