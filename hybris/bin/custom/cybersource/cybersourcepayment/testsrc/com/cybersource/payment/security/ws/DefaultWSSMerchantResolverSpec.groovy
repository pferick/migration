package com.cybersource.payment.security.ws

import de.hybris.bootstrap.annotations.UnitTest
import org.junit.Test
import org.w3c.dom.Node
import org.w3c.dom.NodeList
import spock.lang.Specification

import javax.xml.soap.SOAPBody
import javax.xml.soap.SOAPElement
import javax.xml.soap.SOAPEnvelope

@UnitTest
class DefaultWSSMerchantResolverSpec extends Specification
{
    private static final String MERCHANT_ID = 'MID123'

    def soapEnvelope = Mock([useObjenesis: false], SOAPEnvelope)

    def soapBody = Mock([useObjenesis: false], SOAPBody)

    def nodeList = Mock([useObjenesis: false], NodeList)

    def soapElement = Mock([useObjenesis: false], SOAPElement)

    def node = Mock([useObjenesis: false], Node)

    def wssMerchantResolver = new DefaultWSSMerchantResolver()

    def setup()
    {
        soapEnvelope.body >> soapBody
        nodeList.length >> 1
        nodeList.item(0) >> soapElement
        soapElement.firstChild >> node
    }

    @Test
    def 'should get merchant id from soap message body'()
    {
        given:
        soapBody.getElementsByTagNameNS("*", "merchantID") >> nodeList
        node.nodeValue >> MERCHANT_ID

        when:
        def merchantId = wssMerchantResolver.getMerchantId(soapEnvelope)

        then:
        merchantId == MERCHANT_ID
    }

    @Test
    def 'should return null if merchant id cannot be found'()
    {
        given:
        soapBody.getElementsByTagNameNS("*", "merchantID") >> null

        when:
        def merchantId = wssMerchantResolver.getMerchantId(soapEnvelope)

        then:
        merchantId == null
    }
}
