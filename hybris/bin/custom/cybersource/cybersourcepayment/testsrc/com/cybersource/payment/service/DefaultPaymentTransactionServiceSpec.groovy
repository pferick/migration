package com.cybersource.payment.service

import com.cybersource.payment.constants.CybersourcePaymentConstants
import com.cybersource.payment.enums.CybsPaymentType
import de.hybris.bootstrap.annotations.UnitTest
import de.hybris.platform.core.model.order.AbstractOrderModel
import de.hybris.platform.payment.enums.PaymentTransactionType
import de.hybris.platform.payment.model.PaymentTransactionEntryModel
import de.hybris.platform.payment.model.PaymentTransactionModel
import org.junit.Test
import spock.lang.Specification

@UnitTest
class DefaultPaymentTransactionServiceSpec extends Specification
{
    def creditCardTransaction = Mock([useObjenesis: false], PaymentTransactionModel)

    def authorizationTransactionEntry1 = Mock([useObjenesis: false], PaymentTransactionEntryModel)

    def authorizationTransactionEntry2 = Mock([useObjenesis: false], PaymentTransactionEntryModel)

    def order = Mock([useObjenesis: false], AbstractOrderModel)

    def service = new DefaultPaymentTransactionService()

    def setup()
    {
        creditCardTransaction.paymentProvider >> 'CREDIT_CARD'

        authorizationTransactionEntry1.type >> PaymentTransactionType.AUTHORIZATION
        authorizationTransactionEntry1.transactionStatus >> CybersourcePaymentConstants.TransactionStatus.ACCEPT

        authorizationTransactionEntry2.type >> PaymentTransactionType.AUTHORIZATION
        authorizationTransactionEntry2.transactionStatus >> CybersourcePaymentConstants.TransactionStatus.ACCEPT
    }

    @Test
    def 'should get order payment transaction by payment type'()
    {
        given:
        order.getPaymentTransactions() >> [creditCardTransaction]

        when:
        def transaction = service.getTransaction(CybsPaymentType.CREDIT_CARD, order)

        then:
        transaction.isPresent()
        transaction.get() == creditCardTransaction
    }

    @Test
    def 'should return empty if payment transaction not found for specified payment type'()
    {
        given:
        order.getPaymentTransactions() >> [creditCardTransaction]

        when:
        def transaction = service.getTransaction(CybsPaymentType.PAY_PAL, order)

        then:
        !transaction.isPresent()
    }

    @Test
    def 'should get order latest payment transaction entry by payment type and status'()
    {
        given:
        order.paymentTransactions >> [creditCardTransaction]
        creditCardTransaction.entries >> [authorizationTransactionEntry1, authorizationTransactionEntry2]

        when:
        def entry = service.getLatestTransactionEntry(creditCardTransaction, PaymentTransactionType.AUTHORIZATION,
                CybersourcePaymentConstants.TransactionStatus.ACCEPT)

        then:
        entry.isPresent()
        entry.get() == authorizationTransactionEntry2
    }

    @Test
    def 'should return empty if payment transaction entry not found'()
    {
        given:
        order.paymentTransactions >> [creditCardTransaction]
        creditCardTransaction.entries >> [authorizationTransactionEntry1, authorizationTransactionEntry2]

        when:
        def entry = service.getLatestTransactionEntry(creditCardTransaction, PaymentTransactionType.CANCEL,
                CybersourcePaymentConstants.TransactionStatus.ACCEPT)

        then:
        !entry.isPresent()
    }


    @Test
    def 'should return last authorization payment transaction entry'()
    {
        given:
        order.paymentTransactions >> [creditCardTransaction]
        creditCardTransaction.entries >> [authorizationTransactionEntry1, authorizationTransactionEntry2]

        when:
        def entry = service.getLatestAcceptedTransactionEntry(creditCardTransaction, PaymentTransactionType.AUTHORIZATION)

        then:
        entry.isPresent()
        entry.get() == authorizationTransactionEntry2
    }

    @Test
    def 'should return empty if authorization transaction entry not found'()
    {
        given:
        order.paymentTransactions >> [creditCardTransaction]
        creditCardTransaction.entries >> []

        when:
        def entry = service.getLatestAcceptedTransactionEntry(creditCardTransaction, PaymentTransactionType.AUTHORIZATION)

        then:
        !entry.isPresent()
    }
}
