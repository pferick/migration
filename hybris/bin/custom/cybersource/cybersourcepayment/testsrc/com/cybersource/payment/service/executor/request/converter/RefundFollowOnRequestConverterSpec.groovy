package com.cybersource.payment.service.executor.request.converter

import com.cybersource.payment.service.PaymentTransactionService
import com.cybersource.payment.service.executor.request.PaymentServiceRequest
import com.cybersource.payment.service.request.CybersourceRequestFactory
import de.hybris.bootstrap.annotations.UnitTest
import de.hybris.platform.core.model.c2l.CurrencyModel
import de.hybris.platform.core.model.order.OrderModel
import de.hybris.platform.payment.model.PaymentTransactionEntryModel
import de.hybris.platform.payment.model.PaymentTransactionModel
import org.junit.Test
import spock.lang.Specification

import static com.cybersource.payment.enums.CybsPaymentType.CREDIT_CARD
import static de.hybris.platform.payment.enums.PaymentTransactionType.CAPTURE
import static java.math.BigDecimal.TEN
import static java.util.Optional.empty
import static java.util.Optional.of
import static org.apache.commons.collections.ListUtils.EMPTY_LIST

@UnitTest
class RefundFollowOnRequestConverterSpec extends Specification {
    def transaction = Mock([useObjenesis: false], PaymentTransactionModel)

    def transactionEntry = Mock([useObjenesis: false], PaymentTransactionEntryModel)

    def paymentTransactionService = Mock(PaymentTransactionService)

    def order = new OrderModel(currency: new CurrencyModel(isocode: 'GBP'), guid: 'order_code')

    def source = new PaymentServiceRequest()

    def creator = new RefundFollowOnRequestConverter()

    def setup() {
        source.addParam('order', order)
        source.addParam('amount', TEN)
        source.addParam('merchantId', 'tacit_hybris_2')
        source.addParam('transaction', transaction)
        source.paymentType = CREDIT_CARD

        creator.cybersourceRequestFactory = new CybersourceRequestFactory(validators: EMPTY_LIST)
        creator.paymentTransactionService = paymentTransactionService

        transaction.requestId >> 'requestId'
    }

    @Test
    def 'should create refund follow-on request'() {
        given:
        paymentTransactionService.getLatestAcceptedTransactionEntry(transaction, CAPTURE) >> of(transactionEntry)

        when:
        def target = creator.convert(source)

        then:
        target.requestFields.merchantId == 'tacit_hybris_2'
        target.requestFields.purchaseTotalsGrandTotalAmount == TEN
        target.requestFields.merchantReferenceCode == order.guid
        target.requestFields.purchaseTotalsCurrency == order.currency.isocode
        target.requestFields.ccCreditServiceCaptureRequestID == transactionEntry.requestId
        target.requestFields.ccCreditServiceRun == true
    }

    @Test
    def 'should throw exception if order payment transaction entry not found'() {
        given:
        paymentTransactionService.getLatestAcceptedTransactionEntry(transaction, CAPTURE) >> empty()

        when:
        creator.convert(source)

        then:
        thrown IllegalStateException
    }
}
