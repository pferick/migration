package com.cybersource.payment.data.mapper.custom

import com.cybersource.payment.data.PaymentSystemInfo
import com.cybersource.payment.service.request.CybersourceRequest
import com.cybersource.schemas.transaction_data_1.RequestMessage
import de.hybris.bootstrap.annotations.UnitTest
import ma.glasnost.orika.MappingContext
import org.junit.Test
import spock.lang.Specification

@UnitTest
class PaymentSystemInfoDataMapperSpec extends Specification
{
    def request = Mock([useObjenesis: false], CybersourceRequest)

    def requestMessage = Mock([useObjenesis: false], RequestMessage)

    def context = Mock([useObjenesis: false], MappingContext)

    def paymentSystemInfo = Mock([useObjenesis: false], PaymentSystemInfo)

    def mapper = new PaymentSystemInfoDataMapper()

    def setup()
    {
        mapper.paymentSystemInfo = paymentSystemInfo
    }

    @Test
    def 'should set payment system information on request message'()
    {
        given:
        paymentSystemInfo.partnerSolutionID >> 'QWE123'
        paymentSystemInfo.developerID >> 'TK'
        paymentSystemInfo.clientEnvironment >> 'Hybris'
        paymentSystemInfo.clientApplication >> 'SOAP Toolkit API'
        paymentSystemInfo.clientApplicationVersion >> '6.2.0.1'
        paymentSystemInfo.clientLibrary >> 'CYBSXT'
        paymentSystemInfo.clientLibraryVersion >> '1.0.0'

        when:
        mapper.mapAtoB(request, requestMessage, context)

        then:
        1 * requestMessage.setPartnerSolutionID(paymentSystemInfo.partnerSolutionID)
        1 * requestMessage.setDeveloperID(paymentSystemInfo.developerID)
        1 * requestMessage.setClientEnvironment(paymentSystemInfo.clientEnvironment)
        1 * requestMessage.setClientApplication(paymentSystemInfo.clientApplication)
        1 * requestMessage.setClientApplicationVersion(paymentSystemInfo.clientApplicationVersion)
        1 * requestMessage.setClientLibrary(paymentSystemInfo.clientLibrary)
        1 * requestMessage.setClientLibraryVersion(paymentSystemInfo.clientLibraryVersion)
    }
}
