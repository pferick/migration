package com.cybersource.payment.service.executor.request.builder.creditcard

import com.cybersource.payment.enums.CybsPaymentType
import de.hybris.bootstrap.annotations.UnitTest
import de.hybris.platform.core.model.order.AbstractOrderModel
import de.hybris.platform.payment.enums.PaymentTransactionType
import de.hybris.platform.payment.model.PaymentTransactionModel
import org.junit.Test
import spock.lang.Specification

@UnitTest
class AuthorizeReversalRequestBuilderSpec extends Specification
{
    def order = Mock([useObjenesis: false], AbstractOrderModel)

    def transaction = Mock([useObjenesis: false], PaymentTransactionModel)

    def builder = new AuthorizeReversalRequestBuilder()

    @Test
    def 'should set authorize reversal payment service parameters'()
    {
        when:
        def request = builder.setOrder(order).setTransaction(transaction).build()

        then:
        request.paymentType == CybsPaymentType.CREDIT_CARD
        request.paymentTransactionType == PaymentTransactionType.AUTHORIZATION_REVERSAL
        request.requestParams.order == order
        request.requestParams.transaction == transaction
    }
}
