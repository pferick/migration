package com.cybersource.payment.report.provider

import com.cybersource.payment.report.request.DailyConversionReportRequest
import de.hybris.bootstrap.annotations.UnitTest
import org.apache.http.client.methods.HttpGet
import org.joda.time.DateTime
import org.junit.Test
import spock.lang.Specification

@UnitTest
class DailyConversionReportProviderSpec extends Specification
{
    def provider = new DailyConversionReportProvider(requestUrlPattern: 'https://ebctest.cybersource.com/ebctest/DownloadReport/{0}/{1}/ConversionDetailReport.xml')

    @Test
    def 'provider should build valid report request'()
    {
        when:
        def param = new DailyConversionReportRequest(merchantId: 'merchant', date: new DateTime(2017, 01, 01, 0, 0).toDate())
        def request = provider.buildRequest(param)

        then:
        request instanceof HttpGet
        request.getMethod() == 'GET'
        request.getURI().toString() == 'https://ebctest.cybersource.com/ebctest/DownloadReport/2017/01/01/merchant/ConversionDetailReport.xml'
    }
}
