package com.cybersource.payment.integration

import com.cybersource.payment.integration.helpers.CybsIntegrationSpec
import com.cybersource.payment.service.executor.request.builder.creditcard.RefundFollowOnRequestBuilder
import de.hybris.bootstrap.annotations.ManualTest
import de.hybris.platform.payment.enums.PaymentTransactionType
import org.junit.Test

@ManualTest
class RefundFollowOnIntegrationSpec extends CybsIntegrationSpec
{
    def builder = new RefundFollowOnRequestBuilder()

    @Test
    'should receive Accept from cybs'()
    {
        given:
        def card = testCard()
        def order = testOrderUk()
        transactionCreator.addAuthorization(order, card)
        transactionCreator.addCapture(order)

        def refundAmount = order.totalPrice

        def operationStartTime = new Date()

        when:
        def request = builder
                .setMerchantId(testConfig.merchant)
                .setOrder(order)
                .setAmount(refundAmount)
                .setTransaction(order.paymentTransactions.first())
                .build()
        def result = paymentServiceExecutor.execute(request)

        then:
        with(result.getData().transactionEntry) {
            type == PaymentTransactionType.REFUND_FOLLOW_ON
            transactionStatus == 'ACCEPT'
            transactionStatusDetails == '100'

            amount == order.totalPrice
            requestId != null
            requestToken != null
            currency.isocode == order.currency.isocode

            code.toString().contains(order.code)

            time > operationStartTime
            time < new Date()

            with(properties) {
                merchantReferenceCode == order.guid
                reasonCode == '100'
                decision == 'ACCEPT'
                purchaseTotalsCurrency == order.currency.isocode

                invalidField == '[]'
                missingField == '[]'

                ccCreditReplyRequestDateTime != null
                ccCreditReplyReconciliationID != null
                requestID != null
                requestToken != null
            }
        }
    }

    @Test
    'should receive Reject from cybs for not existing authorisation'()
    {
        given: "An order authorization not existing in cybs"

        def order = testOrderUk()
        def captureTransaction = testTransaction(PaymentTransactionType.CAPTURE)
        order.paymentTransactions << captureTransaction

        def refundAmount = order.totalPrice

        def operationStartTime = new Date()

        when:
        def request = builder
                .setMerchantId(testConfig.merchant)
                .setOrder(order)
                .setAmount(refundAmount)
                .setTransaction(order.paymentTransactions.first())
                .build()
        def result = paymentServiceExecutor.execute(request)

        then:
        with(result.getData().transactionEntry) {
            type == PaymentTransactionType.REFUND_FOLLOW_ON
            transactionStatus == 'REJECT'
            transactionStatusDetails == '102'
            requestId != null
            requestToken != null

            time > operationStartTime
            time < new Date()

            properties.invalidField == '[*/c:captureRequestID]'
        }
    }


}
