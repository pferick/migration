package com.cybersource.payment.service.executor

import com.cybersource.payment.model.CybsPaymentTransactionEntryModel
import de.hybris.bootstrap.annotations.UnitTest
import org.junit.Test
import spock.lang.Specification

@UnitTest
class PaymentServiceResultSpec extends Specification
{
    def transactionEntry = Mock([useObjenesis: false], CybsPaymentTransactionEntryModel)

    @Test
    def 'should create payment service result instance'()
    {
        when:
        def result = PaymentServiceResult.create()

        then:
        result.class == PaymentServiceResult
    }

    @Test
    def 'should chain payment service result data'()
    {
        given:
        def serviceResult = PaymentServiceResult.create()

        when:
        serviceResult
                .addData('transaction', 'TR001')
                .addData('status', 'accepted')

        then:
        serviceResult.data == [transaction: 'TR001', status: 'accepted']
    }

    @Test
    def 'should add transaction as result data'()
    {
        given:
        def serviceResult = PaymentServiceResult.create()

        when:
        serviceResult.addTransaction(transactionEntry)

        then:
        serviceResult.getTransaction() == transactionEntry
    }
}
