package com.cybersource.payment.report.provider

import com.cybersource.payment.executor.WrapperExecutorMock
import de.hybris.bootstrap.annotations.UnitTest
import org.apache.http.HttpEntity
import org.apache.http.client.methods.CloseableHttpResponse
import org.apache.http.client.methods.HttpUriRequest
import org.apache.http.impl.client.CloseableHttpClient
import org.junit.Test
import spock.lang.Specification

import java.util.function.Function

@UnitTest
class AbstractHttpReportProviderSpec extends Specification
{
    def reportHttpClient = Mock([useObjenesis: false], CloseableHttpClient)

    def httpResponse = Mock([useObjenesis: false], CloseableHttpResponse)

    def httpEntity = Mock([useObjenesis: false], HttpEntity)

    def unmarshaller = Mock([useObjenesis: false], Function)

    def httpRequest = Mock([useObjenesis: false], HttpUriRequest)

    def params = new Object()

    def report = new Object()

    def reportWrapperExecutor = new WrapperExecutorMock()

    def provider = new AbstractHttpReportProvider() {

        @Override
        HttpUriRequest buildRequest(final Object param)
        {
            return httpRequest
        }
    }

    void setup()
    {
        provider.reportHttpClient = reportHttpClient
        provider.reportWrapperExecutor = reportWrapperExecutor
        provider.unmarshaller = unmarshaller
        provider.reportName = 'report'

        reportHttpClient.execute(httpRequest) >> httpResponse
        httpResponse.getEntity() >> httpEntity
        httpEntity.getContent() >> new ByteArrayInputStream('reportContent'.bytes)
    }

    @Test
    def 'provider should return report'()
    {
        when:
        def response = provider.getReport(params)

        then:
        1 * unmarshaller.apply('reportContent') >> report
        response == report
    }
}
