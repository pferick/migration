package com.cybersource.payment.integration

import com.cybersource.payment.integration.helpers.CybsIntegrationSpec
import com.cybersource.payment.report.executor.ReportExecutor
import com.cybersource.payment.report.request.DailyConversionReportRequest
import de.hybris.bootstrap.annotations.ManualTest
import org.junit.Test

import javax.annotation.Resource
import java.text.SimpleDateFormat

@ManualTest
class DailyConversionReportIntegrationSpec extends CybsIntegrationSpec
{

    @Resource
    ReportExecutor reportExecutor

    @Test
    'should receive requested report'()
    {
        given:
        def date = new Date() - 2

        def reportRequest = new DailyConversionReportRequest(
                date: date,
                merchantId: testConfig.merchant
        )

        when:
        def result = reportExecutor.getReport(reportRequest)

        then:
        result != null
        with(result) {
            merchantID == testConfig.merchant
            reportStartDate.contains(new SimpleDateFormat("yyyy-MM-dd").format(date))
        }
    }

    @Test
    'should throw exception for report with wrong date'()
    {
        given:
        def today = new Date()

        def reportRequest = new DailyConversionReportRequest(
                date: today,
                merchantId: testConfig.merchant
        )

        when:
        reportExecutor.getReport(reportRequest)

        then:
        thrown IllegalArgumentException
    }

}
