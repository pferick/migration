package com.cybersource.payment.httpclient

import de.hybris.bootstrap.annotations.UnitTest
import org.apache.http.auth.AuthScope
import org.apache.http.auth.Credentials
import org.junit.Test
import spock.lang.Specification

@UnitTest
class CredentialsProviderFactorySpec extends Specification
{
    def authScope = Mock(AuthScope)

    def credentials = Mock([useObjenesis: false], Credentials)

    def factory = new CredentialsProviderFactory(authScope: authScope, credentials: credentials)

    @Test
    def 'factory should return credential providers instance'()
    {
        when:
        def result = factory.create()

        then:
        result.getCredentials(authScope) == credentials
    }
}
