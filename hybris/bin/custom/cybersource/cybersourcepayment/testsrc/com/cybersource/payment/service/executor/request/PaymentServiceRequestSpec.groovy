package com.cybersource.payment.service.executor.request

import com.cybersource.payment.enums.CybsPaymentSource
import com.cybersource.payment.enums.CybsPaymentType
import com.cybersource.payment.service.executor.request.PaymentServiceRequest
import de.hybris.bootstrap.annotations.UnitTest
import de.hybris.platform.payment.enums.PaymentTransactionType
import org.junit.Test
import spock.lang.Specification

@UnitTest
class PaymentServiceRequestSpec extends Specification
{
    def 'should create payment service request instance'()
    {
        when:
        def request = PaymentServiceRequest.create()

        then:
        request.class == PaymentServiceRequest
    }

    @Test
    def 'should chain payment service request method, service and parameters'()
    {
        when:
        def request = PaymentServiceRequest.create()
        request.method(CybsPaymentType.ALTERNATIVE_PAYMENT)
                .source(CybsPaymentSource.SECURE_ACCEPTANCE)
                .service(PaymentTransactionType.ENROLLMENT)
                .addParam('orderCode', 'DEV001')
                .addParam('customer', 'C001')

        then:
        request.paymentSource == CybsPaymentSource.SECURE_ACCEPTANCE
        request.paymentType == CybsPaymentType.ALTERNATIVE_PAYMENT
        request.paymentTransactionType == PaymentTransactionType.ENROLLMENT
        request.requestParams == [orderCode:'DEV001', customer:'C001']
    }

    @Test
    def 'should set default payment source value for request'()
    {
        when:
        def request = PaymentServiceRequest.create()

        then:
        request.paymentSource == CybsPaymentSource.SERVICE_API
    }

    @Test
    def 'should return required param'()
    {
        when:
        def request = PaymentServiceRequest.create()
        request.addParam("key", "value")

        def result = request.getRequiredParam("key")

        then:
        result == "value"
    }

    @Test
    def 'should throw exception when required param is missing'()
    {
        when:
        def request = PaymentServiceRequest.create()

        request.getRequiredParam("key")

        then:
        thrown IllegalArgumentException
    }

    @Test
    def 'should return param by key and don not throw exception if it is missing'()
    {
        when:
        def request = PaymentServiceRequest.create()

        def result = request.getParam("key")

        then:
        result == null
    }
}
