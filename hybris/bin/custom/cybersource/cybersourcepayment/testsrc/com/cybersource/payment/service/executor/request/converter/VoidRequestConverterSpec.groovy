package com.cybersource.payment.service.executor.request.converter

import com.cybersource.payment.service.executor.request.PaymentServiceRequest
import com.cybersource.payment.service.request.CybersourceRequest
import com.cybersource.payment.service.request.CybersourceRequestFactory
import com.cybersource.payment.service.request.VoidType
import de.hybris.bootstrap.annotations.UnitTest
import de.hybris.platform.core.model.order.OrderModel
import de.hybris.platform.core.model.user.AddressModel
import de.hybris.platform.payment.model.PaymentTransactionEntryModel
import org.junit.Test
import spock.lang.Specification

@UnitTest
class VoidRequestConverterSpec extends Specification {
    def source = PaymentServiceRequest.create()

    def cybersourceRequest = Mock([useObjenesis: false], CybersourceRequest)

    def order = Mock([useObjenesis: false], OrderModel)

    def billingAddress = Mock([useObjenesis: false], AddressModel)

    def transactionEntry = Mock([useObjenesis: false], PaymentTransactionEntryModel)

    def voidTransaction = Mock([useObjenesis: false], VoidType)

    def cybersourceRequestFactory = Mock([useObjenesis: false], CybersourceRequestFactory)

    def merchantReferenceCode = Mock([useObjenesis: false], VoidType.MerchantReferenceCode)

    def voidServiceVoidRequestID = Mock([useObjenesis: false], VoidType.VoidServiceVoidRequestID)

    def voidServiceRun = Mock([useObjenesis: false], VoidType.VoidServiceRun)

    def optional = Mock([useObjenesis: false], VoidType.Optional)

    def converter = new VoidRequestConverter()

    void setup() {
        converter.cybersourceRequestFactory = cybersourceRequestFactory

        cybersourceRequestFactory.voidTransaction() >> voidTransaction

        source.addParam('merchantId', 'merchant')
        source.addParam('order', order)
        source.addParam('transactionEntry', transactionEntry)

        order.guid >> '1234567890'
        order.paymentAddress >> billingAddress

        billingAddress.email >> 'jsmith@mail.com'
        billingAddress.firstname >> 'john'
        billingAddress.lastname >> 'smith'

        transactionEntry.requestId >> '2345678901'
        transactionEntry.requestToken >> '3456789012'

        optional.request() >> cybersourceRequest
    }

    @Test
    def 'converter should create valid cybersource request for void transaction operation'() {
        when:
        def target = converter.convert(source)

        then:
        voidTransaction.merchantId('merchant') >> merchantReferenceCode
        merchantReferenceCode.merchantReferenceCode('1234567890') >> voidServiceVoidRequestID
        voidServiceVoidRequestID.voidServiceVoidRequestID('2345678901') >> voidServiceRun
        voidServiceRun.voidServiceRun(true) >> optional
        optional.voidServiceVoidRequestToken('3456789012') >> optional
        optional.billToEmail('jsmith@mail.com') >> optional
        optional.billToFirstName('john') >> optional
        optional.billToLastName('smith') >> optional

        target == cybersourceRequest
    }
}
