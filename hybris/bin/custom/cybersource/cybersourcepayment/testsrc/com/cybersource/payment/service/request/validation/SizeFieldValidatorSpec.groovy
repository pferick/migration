package com.cybersource.payment.service.request.validation

import de.hybris.bootstrap.annotations.UnitTest
import spock.lang.Specification

import static io.qala.datagen.RandomShortApi.unicode


@UnitTest
class SizeFieldValidatorSpec extends Specification
{
    def validator = new SizeFieldValidator()
    static maxSize = 255

    def "should return result as per field value validity"()
    {
        expect:
        validator.validate(value, rules) == valid

        where:
        value            | rules         | valid
        unicode(maxSize) | [:]           | true
        unicode(maxSize) | [size:'']     | true
        unicode(maxSize) | [size:'0']    | true
        unicode(3)       | [size:'3']    | true
        unicode(2)       | [size:'3']    | true
        unicode(3)       | [size:'2']    | false
        null             | [:]           | true
        null             | [size:'']     | true
        null             | [size:'0']    | true
        null             | [size:'3']    | true
        ''               | [:]           | true
        ''               | [size:'']     | true
        ''               | [size:'0']    | true
        ''               | [size:'3']    | true
    }
















}
