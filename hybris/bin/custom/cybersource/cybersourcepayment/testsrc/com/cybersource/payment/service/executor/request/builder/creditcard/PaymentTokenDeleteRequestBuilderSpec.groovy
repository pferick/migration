package com.cybersource.payment.service.executor.request.builder.creditcard

import de.hybris.bootstrap.annotations.UnitTest
import de.hybris.platform.core.model.order.AbstractOrderModel
import de.hybris.platform.payment.model.PaymentTransactionModel
import org.junit.Test
import spock.lang.Specification

import static com.cybersource.payment.enums.CybsPaymentType.CREDIT_CARD
import static de.hybris.platform.payment.enums.PaymentTransactionType.DELETE_SUBSCRIPTION

@UnitTest
class PaymentTokenDeleteRequestBuilderSpec extends Specification
{
    def order = Mock([useObjenesis: false], AbstractOrderModel)

    def transaction = Mock([useObjenesis: false], PaymentTransactionModel)

    @Test
    def 'should build payment service request'()
    {
        when:
        def request = new PaymentTokenDeleteRequestBuilder()
                .setOrder(order).setTransaction(transaction).build()

        then:
        request.paymentType == CREDIT_CARD
        request.paymentTransactionType == DELETE_SUBSCRIPTION
        request.requestParams.order == order
        request.requestParams.transaction == transaction
    }

}
