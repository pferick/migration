package com.cybersource.payment.report.provider

import com.cybersource.payment.report.request.OnDemandConversionReportRequest
import de.hybris.bootstrap.annotations.UnitTest
import org.apache.commons.io.IOUtils
import org.apache.http.auth.Credentials
import org.joda.time.DateTime
import org.junit.Test
import spock.lang.Specification

import java.security.Principal

import static org.joda.time.DateTimeZone.UTC


@UnitTest
class OnDemandConversionReportProviderSpec extends Specification {

    def provider = new OnDemandConversionReportProvider();

    def setup() {
        def credentials = Mock(Credentials)
        def principal = Mock(Principal)

        credentials.password >> 'password'
        principal.name >> 'name'
        credentials.userPrincipal >> principal

        provider.reportUrl = 'http://on.demand.conversion.report.url';
        provider.credentials = credentials
    }
    
    @Test
    def 'should provide on demand report post request with params'() {
        when:
        def date = new DateTime(2017, 10, 14, 12, 15, 50, 1, UTC).toDate()

        def request = new OnDemandConversionReportRequest(merchantId: 'merchantId', startDate: date, endDate: date)
        def httpRequest = provider.buildRequest(request)

        then:
        httpRequest.method == 'POST'
        IOUtils.toString(httpRequest.entity.content) == 'merchantID=merchantId&username=name&password=password&' +
                'startDate=2017-10-14&startTime=12%3A15%3A50&endDate=2017-10-14&endTime=12%3A15%3A50'
    }
}
