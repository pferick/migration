package com.cybersource.payment.cronjob

import com.cybersource.payment.model.CybsMerchantModel
import com.cybersource.payment.model.cron.CybsConversionReportPollingCronJobModel
import com.cybersource.payment.report.executor.ReportExecutor
import com.cybersource.payment.report.listener.ReportListener
import com.cybersource.payment.report.request.DailyConversionReportRequest
import com.cybersource.payment.service.MerchantService
import com.cybersource.payment.report.service.ReportTimeService
import com.cybersource.reports.conversion.Report
import de.hybris.bootstrap.annotations.UnitTest
import de.hybris.platform.cronjob.enums.CronJobResult
import de.hybris.platform.cronjob.enums.CronJobStatus
import de.hybris.platform.servicelayer.model.ModelService
import org.joda.time.DateTime
import org.joda.time.Interval
import org.junit.Test
import spock.lang.Specification

@UnitTest
class DailyConversionReportPollingJobSpec extends Specification {

    def reportTimeService = Mock([useObjenesis: false], ReportTimeService)

    def merchantService = Mock([useObjenesis: false], MerchantService)

    def reportExecutor = Mock([useObjenesis: false], ReportExecutor)

    def reportListener = Mock([useObjenesis: false], ReportListener)

    def modelService = Mock([useObjenesis: false], ModelService)

    def job = new DailyConversionReportPollingJob(reportTimeService: reportTimeService,
            reportExecutor: reportExecutor, merchantService: merchantService, modelService: modelService,
            listeners: [reportListener])

    def merchant = Mock([useObjenesis: false], CybsMerchantModel)

    def report = Mock([useObjenesis: false], Report)

    def cronJob = new CybsConversionReportPollingCronJobModel()

    def startDate = new Date();

    void setup() {
        merchantService.getAllMerchants() >> [merchant]
        merchant.id >> 'merchant'
        reportTimeService.getReportInterval(null) >> new Interval(startDate.time, DateTime.now().millis)
    }

    @Test
    def 'job should perform load daily report and call listeners'() {
        when:
        def result = job.perform(cronJob)

        then:
        1 * reportListener.handle(merchant, report)
        1 * reportExecutor.getReport(_) >> { args ->
            def requestParam = args[0] as DailyConversionReportRequest

            assert requestParam.merchantId == 'merchant'
            assert requestParam.date == startDate

            report
        }
        1 * modelService.save(_) >> { args ->
            def cronJobToSave = args[0] as CybsConversionReportPollingCronJobModel
            assert cronJobToSave.lastRunDate == startDate
        }

        result.status == CronJobStatus.FINISHED
        result.result == CronJobResult.SUCCESS
    }
}
