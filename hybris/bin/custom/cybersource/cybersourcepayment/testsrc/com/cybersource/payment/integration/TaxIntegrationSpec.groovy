package com.cybersource.payment.integration

import com.cybersource.payment.integration.helpers.CybsIntegrationSpec
import com.cybersource.payment.service.executor.request.builder.TaxRequestBuilder
import com.google.gson.Gson
import de.hybris.bootstrap.annotations.ManualTest
import de.hybris.platform.payment.enums.PaymentTransactionType
import org.junit.Test

@ManualTest
class TaxIntegrationSpec extends CybsIntegrationSpec
{
    def builder = new TaxRequestBuilder()

    @Test
    'should receive accept from cybs'()
    {
        given:
        def order = testOrderUk()
        def operationStartTime = new Date()

        when:
        def request = builder
                .setMerchantId(testConfig.merchant)
                .setOrder(order)
                .build()
        def result = paymentServiceExecutor.execute(request)

        then:
        with(result.getData().transactionEntry) {
            type == PaymentTransactionType.TAX
            transactionStatus == 'ACCEPT'
            transactionStatusDetails == '100'

            code.toString().contains(order.code)
            requestId != null
            currency.isocode == order.currency.isocode

            time > operationStartTime
            time < new Date()

            with(properties) {
                merchantReferenceCode == order.guid
                reasonCode == '100'
                decision == 'ACCEPT'

                invalidField == '[]'
                missingField == '[]'

                requestID != null
                requestToken != null

                purchaseTotalsCurrency == order.currency.isocode
                taxReplyGrandTotalAmount != null


            }
        }
        and:
        def gson = new Gson()
        String taxItemsJson = result.getData().transactionEntry.properties.taxReplyItems
        def taxItems = gson.fromJson(taxItemsJson, Collection.class)
        with(taxItems.first()) {
            id == 0
            totalTaxAmount != null
        }

    }

    @Test
    'should receive reject from cybs for missing data'()
    {
        given:
        def order = testOrderMissingFields()
        def operationStartTime = new Date()

        when:
        def request = builder
                .setMerchantId(testConfig.merchant)
                .setOrder(order)
                .build()
        def result = paymentServiceExecutor.execute(request)

        then:
        with(result.getData().transactionEntry) {
            type == PaymentTransactionType.TAX
            transactionStatus == 'REJECT'
            transactionStatusDetails == '101'

            code.toString().contains(order.code)
            requestId != null

            time > operationStartTime
            time < new Date()

            with(properties) {
                reasonCode == '101'
                decision == 'REJECT'

                invalidField == '[]'
                missingField == '[c:billTo/c:state]'
            }

        }
    }


}
