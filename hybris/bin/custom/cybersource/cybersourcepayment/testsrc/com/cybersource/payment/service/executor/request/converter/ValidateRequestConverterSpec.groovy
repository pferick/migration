package com.cybersource.payment.service.executor.request.converter

import com.cybersource.payment.service.request.CybersourceRequestFactory
import com.cybersource.payment.service.executor.request.PaymentServiceRequest
import de.hybris.bootstrap.annotations.UnitTest
import de.hybris.platform.core.enums.CreditCardType
import de.hybris.platform.core.model.order.AbstractOrderModel
import de.hybris.platform.payment.dto.CardInfo
import org.junit.Test
import spock.lang.Specification

@UnitTest
class ValidateRequestConverterSpec extends Specification
{
    def cybersourceRequestFactory = Mock([useObjenesis: false], CybersourceRequestFactory)

    def order = Mock([useObjenesis: false], AbstractOrderModel)

    def cardInfo = Mock([useObjenesis: false], CardInfo)

    def source = PaymentServiceRequest.create()

    def converter = new ValidateRequestConverter()

    def validateType = new CybersourceRequestFactory(validators: []).validate()

    def setup()
    {
        order.guid >> '123'
        order.currency >> [isocode: 'USD']

        cardInfo.cardType >> CreditCardType.VISA
        cardInfo.cardNumber >> '4111111111111111'
        cardInfo.expirationMonth >> 10
        cardInfo.expirationYear >> 2017

        source.addParam("merchantId", 'tacit_hybris_2')
        source.addParam("order", order)
        source.addParam("paRes", "ABCDEF")

        converter.cybersourceRequestFactory = cybersourceRequestFactory

        cybersourceRequestFactory.validate() >> validateType
    }

    @Test
    def 'creator should create and populate cybersource request object'()
    {
        when:
        def target = converter.convert(source)
        def requestFields = target.requestFields

        then:
        target == validateType.request

        requestFields["merchantId"] == 'tacit_hybris_2'
        requestFields["merchantReferenceCode"] == '123'
        requestFields["payerAuthValidateServiceRun"] == true
        requestFields["purchaseTotalsCurrency"] == 'USD'

        requestFields["payerAuthValidateServiceSignedPARes"] == 'ABCDEF'
    }
}
