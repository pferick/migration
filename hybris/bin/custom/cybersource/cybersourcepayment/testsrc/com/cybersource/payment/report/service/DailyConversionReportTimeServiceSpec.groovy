package com.cybersource.payment.report.service

import de.hybris.bootstrap.annotations.UnitTest
import de.hybris.platform.servicelayer.time.TimeService
import org.junit.Test
import spock.lang.Specification

import static org.apache.commons.lang.math.NumberUtils.INTEGER_ONE
import static org.joda.time.DateTime.now

@UnitTest
class DailyConversionReportTimeServiceSpec extends Specification {

    def now = now()

    def timeService = Mock([useObjenesis: false], TimeService)

    def dailyConversionReportTimeService = new DailyConversionReportTimeService(timeService: timeService)

    def setup() {
        timeService.currentTime >> now.toDate()
    }

    @Test
    def 'should provide daily first conversion report start date'() {
        given:
        def yesterday = now.minusDays(INTEGER_ONE)

        when:
        def firstStartDate = dailyConversionReportTimeService.getReportInterval(null).start.toDate()

        then:
        firstStartDate == yesterday.toDate()
    }

    @Test
    def 'should provide daily next conversion report start date'() {
        given:
        def sixDaysAgo = now.minusDays(6)
        def fiveDaysAgo = now.minusDays(5)

        when:
        def nextStartDate = dailyConversionReportTimeService.getReportInterval(sixDaysAgo.toDate()).start.toDate()

        then:
        nextStartDate == fiveDaysAgo.toDate()
    }

    @Test
    def 'should fallback to yesterday when daily conversion report is invoked more than once per day'() {
        given:
        def yesterday = now.minusDays(INTEGER_ONE).toDate()

        when:
        def nextStartDate = dailyConversionReportTimeService.getReportInterval(yesterday).start.toDate()

        then:
        nextStartDate == yesterday
    }
}
