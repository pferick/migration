package com.cybersource.payment.service.request

import com.cybersource.payment.service.request.validation.CybersourceFieldValidator
import com.cybersource.payment.service.request.value.MerchantDefinedField
import de.hybris.bootstrap.annotations.UnitTest
import org.junit.Test
import spock.lang.Specification

@UnitTest
class DefaultCybersourceRequestSpec extends Specification
{
    def validator = Mock([useObjenesis: false], CybersourceFieldValidator)

    def request = new DefaultCybersourceRequest(Collections.singletonList(validator))

    @Test
    def 'should add request field and validation rules'()
    {
        when:
        request.addRequestField('authType', 'autocapture', 'size:3')

        then:
        request.requestFields.authType == 'autocapture'
        request.requestFieldRules.authType == [size:'3']
    }

    @Test
    def 'should add custom (merchant defined data) field and validation rules'()
    {
        when:
        request.addCustomField(MerchantDefinedField.CUSTOMER_DATA, 'agent1', 'size:30')

        then:
        request.customFields[MerchantDefinedField.CUSTOMER_DATA] == 'agent1'
        request.customFieldRules[MerchantDefinedField.CUSTOMER_DATA] == [size:'30']
    }

    @Test
    def 'should throw exception if request fields are not valid'()
    {
        given:
        validator.validate(_ as String, _ as Map) >> false
        request.addRequestField('authType', 'autocapture', 'size:3')

        when:
        request.validate()

        then:
        thrown IllegalArgumentException
    }

    @Test
    def 'should not throw any exceptions if request fields are valid'()
    {
        given:
        validator.validate(_ as String, _ as Map) >> true
        request.addRequestField('authType', 'autocapture', 'size:11')

        when:
        request.validate()

        then:
        noExceptionThrown()
    }

    @Test
    def 'should throw exception if custom fields are not valid'()
    {
        given:
        validator.validate(_ as String, _ as Map) >> false
        request.addCustomField(MerchantDefinedField.CUSTOMER_DATA, 'agent1', 'size:30')

        when:
        request.validate()

        then:
        thrown IllegalArgumentException
    }

    @Test
    def 'should not throw any exceptions if custom fields are valid'()
    {
        given:
        validator.validate(_ as String, _ as Map) >> true
        request.addCustomField(MerchantDefinedField.CUSTOMER_DATA, 'agent1', 'size:30')

        when:
        request.validate()

        then:
        noExceptionThrown()
    }

    @Test
    def 'should not invoke field validation if no rules specified'()
    {
        given:
        validator.validate(_ as String, _ as Map) >> false
        request.addRequestField('authType', 'autocapture', '')

        when:
        request.validate()

        then:
        noExceptionThrown()
        0 * validator.validate(_ as String, _ as Map)
    }

    @Test
    def 'should trim validation rule if contains spaces'()
    {
        when:
        request.addRequestField('loginAttempts', '3', 'size:10,      pattern:[abc]')

        then:
        request.requestFieldRules.loginAttempts == [size:'10', pattern:'[abc]']
    }

    @Test
    def 'should not add validation rule if empty'()
    {
        when:
        request.addRequestField('loginAttempts', '3', '')

        then:
        request.requestFieldRules.loginAttempts == null
    }
}
