package com.cybersource.payment.data.mapper

import de.hybris.bootstrap.annotations.UnitTest
import org.junit.Test
import spock.lang.Specification

@UnitTest
class FieldInfoSpec extends Specification
{
    def FIELD_NAME = 'fieldName'

    def FIELD_CONVERTER = 'converter'

    @Test
    def "should create FieldInfo instance with name and converter"()
    {
        when:
        def fieldInfo = FieldInfo.parse(FIELD_NAME + '|' + FIELD_CONVERTER)

        then:
        fieldInfo.name == FIELD_NAME
        fieldInfo.hasConverterId()
        fieldInfo.converter == FIELD_CONVERTER
    }

    @Test
    def "should create FieldInfo instance with name only"()
    {
        when:
        def fieldInfo = FieldInfo.parse(FIELD_NAME)

        then:
        fieldInfo.name == FIELD_NAME
        !fieldInfo.hasConverterId()
        fieldInfo.converter == null
    }
}
