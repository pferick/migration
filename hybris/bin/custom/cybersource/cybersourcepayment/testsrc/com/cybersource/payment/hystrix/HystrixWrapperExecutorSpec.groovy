package com.cybersource.payment.hystrix

import de.hybris.bootstrap.annotations.UnitTest
import org.junit.Test
import spock.lang.Specification

import java.util.function.Supplier

@UnitTest
class HystrixWrapperExecutorSpec extends Specification
{
    def action = Mock([useObjenesis: false], Supplier)

    def executor = new HystrixWrapperExecutor()

    void setup()
    {
        executor.groupName = 'groupName'
        executor.threadPoolName = 'threadPoll'
    }

    @Test
    def 'executor should run provided action'()
    {
        when:
        executor.execute('command', action)

        then:
        1 * action.get()
    }
}
