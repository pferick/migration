package com.cybersource.payment.integration.helpers

import org.apache.commons.configuration.CompositeConfiguration
import org.apache.commons.configuration.ConfigurationException
import org.apache.commons.configuration.PropertiesConfiguration

class TestConfig
{
    String merchant
    String token
    Double processorErrorTrigger
    String nameReviewTrigger

    def getTestConfig(configDir)
    {

        def compositeConfiguration = new CompositeConfiguration()

        try
        {
            def localConfig = new PropertiesConfiguration("${configDir}/cybs.test.properties")
            compositeConfiguration.addConfiguration(localConfig)
        }
        catch (ConfigurationException ignored)
        {
        }
        finally
        {
            def config = new PropertiesConfiguration("test.properties")
            compositeConfiguration.addConfiguration(config)
        }

        setConstants(compositeConfiguration)
        return this
    }

    private setConstants(compositeConfiguration)
    {
        merchant = compositeConfiguration.getProperty("cybersource.test.merchant.id") as String
        token = compositeConfiguration.getProperty("cybersource.test.merchant.token") as String
        processorErrorTrigger = compositeConfiguration.getProperty("cybersource.test.processor.error.code") as Double
        nameReviewTrigger = compositeConfiguration.getProperty("cybersource.test.review.name") as String
    }
}


