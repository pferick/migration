package com.cybersource.payment.integration

import com.cybersource.payment.integration.helpers.CybsIntegrationSpec
import com.cybersource.payment.service.executor.request.builder.creditcard.PaymentTokenCreateRequestBuilder
import de.hybris.bootstrap.annotations.ManualTest
import de.hybris.platform.payment.enums.PaymentTransactionType
import org.junit.Test

@ManualTest
class PaymentTokenCreateIntegrationSpec extends CybsIntegrationSpec
{
    def builder = new PaymentTokenCreateRequestBuilder()

    @Test
    "should get ACCEPT from cybs for payment token creation request"()
    {
        given:
        def card = testCard()
        def order = testOrderUk()
        transactionCreator.addAuthorization(order, card)

        def operationStartTime = new Date()

        when:
        def request = builder
                .setMerchantId(testConfig.merchant)
                .setOrder(order)
                .setTransaction(order.paymentTransactions.first())
                .build()
        def result = paymentServiceExecutor.execute(request)

        then:
        with(result.getData().transactionEntry) {
            type == PaymentTransactionType.CREATE_SUBSCRIPTION
            transactionStatus == 'ACCEPT'
            transactionStatusDetails == '100'
            subscriptionID != null

            code.toString().contains(order.code)
            requestId != null
            currency == null
            requestToken != null


            time > operationStartTime
            time < new Date()
            with(properties) {
                merchantReferenceCode == order.guid
                reasonCode == '100'
                decision == 'ACCEPT'

                invalidField == '[]'
                missingField == '[]'

                requestID != null
                requestToken != null

                paySubscriptionCreateReplyReasonCode != null
                paySubscriptionCreateReplySubscriptionID != null
            }
        }
    }

    @Test
    "should get REJECT for wrong Auth Request Results"()
    {
        given:
        def paymentTransaction = testTransaction()
        def order = testOrderUk()
        order.paymentTransactions.add(paymentTransaction)

        def operationStartTime = new Date()

        when:
        def request = builder
                .setMerchantId(testConfig.merchant)
                .setOrder(order)
                .setTransaction(order.paymentTransactions.first())
                .build()
        def result = paymentServiceExecutor.execute(request)

        then:
        with(result.getData().transactionEntry) {
            type == PaymentTransactionType.CREATE_SUBSCRIPTION
            transactionStatus == 'REJECT'
            transactionStatusDetails == '102'
            subscriptionID == null

            code != null
            requestId != null
            currency == null
            requestToken != null

            time > operationStartTime
            time < new Date()

            properties.invalidField == '[c:paySubscriptionCreateService/c:paymentRequestID]'
        }
    }

}
