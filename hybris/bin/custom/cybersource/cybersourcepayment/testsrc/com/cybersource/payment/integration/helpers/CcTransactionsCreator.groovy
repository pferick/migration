package com.cybersource.payment.integration.helpers

import com.cybersource.payment.service.executor.request.builder.creditcard.AuthorizeRequestBuilder
import com.cybersource.payment.service.executor.request.builder.creditcard.CaptureRequestBuilder
import com.cybersource.payment.service.executor.request.builder.creditcard.PaymentTokenCreateRequestBuilder
import com.cybersource.payment.service.executor.request.builder.creditcard.RefundStandaloneRequestBuilder

class CcTransactionsCreator
{
    def paymentServiceExecutor
    def merchantId

    def addAuthorization(order, card)
    {
        def request = new AuthorizeRequestBuilder()
                .setMerchantId(merchantId)
                .setOrder(order)
                .setCardInfo(card)
                .build()

        def result = paymentServiceExecutor.execute(request)

        return orderWithTransaction(order, result)
    }

    def addPaymentToken(order)
    {
        def request = new PaymentTokenCreateRequestBuilder()
                .setMerchantId(merchantId)
                .setOrder(order)
                .setTransaction(order.paymentTransactions.first())
                .build()

        def result = paymentServiceExecutor.execute(request)

        return orderWithTransaction(order, result)
    }

    def addCapture(order)
    {
        def request = new CaptureRequestBuilder()
                .setMerchantId(merchantId)
                .setOrder(order)
                .setTransaction(order.paymentTransactions.first())
                .build()

        def result = paymentServiceExecutor.execute(request)

        return orderWithTransaction(order, result)
    }

    def addRefundStandalone(order, card)
    {
        def request = new RefundStandaloneRequestBuilder()
                .setMerchantId(merchantId)
                .setOrder(order)
                .setCard(card)
                .build()

        def result = paymentServiceExecutor.execute(request)

        return orderWithTransaction(order, result)
    }

    def getSubscription(order)
    {
        def request = new PaymentTokenCreateRequestBuilder()
                .setMerchantId(merchantId)
                .setOrder(order)
                .setTransaction(order.paymentTransactions.first())
                .build()

        def result = paymentServiceExecutor.execute(request)
        return result.getData().transactionEntry.subscriptionID
    }

    private static orderWithTransaction(order, result)
    {

        def paymentTransactionEntry = result.getData().transactionEntry

        if (order.paymentTransactions.isEmpty())
        {
            def paymentTransaction = result.getData().transactionEntry.paymentTransaction
            paymentTransaction.entries = [paymentTransactionEntry]
            order.paymentTransactions << paymentTransaction
        } else
        {
            order.paymentTransactions.first().entries << paymentTransactionEntry
        }

        return order
    }

}
