package com.cybersource.payment.integration

import com.cybersource.payment.integration.helpers.CybsIntegrationSpec
import com.cybersource.payment.service.executor.request.builder.creditcard.CaptureRequestBuilder
import de.hybris.bootstrap.annotations.ManualTest
import de.hybris.platform.payment.enums.PaymentTransactionType
import org.junit.Test

@ManualTest
class CaptureIntegrationSpec extends CybsIntegrationSpec
{
    def builder = new CaptureRequestBuilder()

    @Test
    'should receive Accept from cybs'()
    {
        given:
        def card = testCard()
        def order = testOrderUk()
        transactionCreator.addAuthorization(order, card)

        def operationStartTime = new Date()

        when:
        def request = builder
                .setMerchantId(testConfig.merchant)
                .setOrder(order)
                .setTransaction(order.paymentTransactions.first())
                .build()
        def result = paymentServiceExecutor.execute(request)

        then:
        with(result.getData().transactionEntry) {
            type == PaymentTransactionType.CAPTURE
            transactionStatus == 'ACCEPT'
            transactionStatusDetails == '100'

            requestId != null
            requestToken != null
            currency.isocode == order.currency.isocode

            amount == order.totalPrice

            code.toString().contains(order.code)

            time > operationStartTime
            time < new Date()
            with(properties) {
                merchantReferenceCode == order.guid
                reasonCode == '100'
                decision == 'ACCEPT'
                purchaseTotalsCurrency == order.currency.isocode

                invalidField == '[]'
                missingField == '[]'

                ccCaptureReplyReconciliationID != null
                requestID != null
                requestToken != null
            }

        }
    }


    @Test
    'should receive Reject from cybs for not existing authorisation'()
    {
        given:
        def paymentTransaction = testTransaction()

        def order = testOrderUk()
        order.paymentTransactions.add(paymentTransaction)

        def operationStartTime = new Date()

        when:
        def request = builder
                .setMerchantId(testConfig.merchant)
                .setOrder(order)
                .setTransaction(order.paymentTransactions.first())
                .build()
        def result = paymentServiceExecutor.execute(request)

        then:
        with(result.getData().transactionEntry) {
            type == PaymentTransactionType.CAPTURE
            transactionStatus == 'REJECT'
            transactionStatusDetails == '102'
            requestId != null
            requestToken != null

            time > operationStartTime
            time < new Date()

            properties.invalidField == '[c:authRequestID]'
        }
    }
}
