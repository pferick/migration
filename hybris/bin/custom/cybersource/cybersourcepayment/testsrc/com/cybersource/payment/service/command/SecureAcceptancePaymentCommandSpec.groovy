package com.cybersource.payment.service.command

import com.cybersource.payment.service.request.CybersourceRequest
import com.cybersource.payment.service.response.CybersourceResponse
import de.hybris.bootstrap.annotations.UnitTest
import ma.glasnost.orika.MapperFacade
import org.junit.Test
import spock.lang.Specification

@UnitTest
class SecureAcceptancePaymentCommandSpec extends Specification
{
    def request = Mock([useObjenesis: false], CybersourceRequest)

    def responseMapper = Mock([useObjenesis: false], MapperFacade)

    def command = new SecureAcceptancePaymentCommand()

    def setup()
    {
        command.responseMapper = responseMapper
    }

    @Test
    def 'should get payment response and convert to cybersource response object'()
    {
        given:
        request.requestFields >> [paymentResponse: [decision: 'ACCEPT']]

        when:
        def response = command.perform(request)

        then:
        1 * responseMapper.map([decision: 'ACCEPT'], _ as CybersourceResponse)
        response != null
    }

    @Test
    def 'should throw exception if payment response parameter not provided'()
    {
        given:
        request.requestFields >> [:]

        when:
        command.perform(request)

        then:
        thrown IllegalArgumentException
    }

    @Test
    def 'should throw exception if request object is null'()
    {
        when:
        command.perform(null)

        then:
        thrown IllegalArgumentException
    }
}
