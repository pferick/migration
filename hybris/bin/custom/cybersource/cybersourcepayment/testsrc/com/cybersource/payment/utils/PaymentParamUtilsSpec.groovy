package com.cybersource.payment.utils

import com.cybersource.payment.service.request.value.Month
import com.google.common.collect.ImmutableMap
import com.google.common.collect.Maps
import de.hybris.bootstrap.annotations.UnitTest
import org.junit.Test
import spock.lang.Specification

import java.util.function.Function

@UnitTest
class PaymentParamUtilsSpec extends Specification
{
    @Test
    def 'getParam should return value for existing key'()
    {
        given:
        def map = ImmutableMap.of('key', 'value')

        when:
        def result = PaymentParamUtils.getParam('key', map)

        then:
        result == "value"
    }

    @Test
    def 'getParam should throw exception for required key'()
    {
        when:
        PaymentParamUtils.getParam('key', Maps.newHashMap())

        then:
        thrown IllegalArgumentException
    }

    @Test
    def 'toString should return param toString if param is not null'()
    {
        when:
        def result = PaymentParamUtils.toString('sample')

        then:
        result == 'sample'
    }

    def 'toString should return null for null param'()
    {
        when:
        def result = PaymentParamUtils.toString(null)

        then:
        result == null
    }

    def 'should return month enumeration by given integer value'()
    {
        when:
        def result = PaymentParamUtils.getMonth(1)

        then:
        result == Month.JAN
    }

    def 'should return null when month enumeration cannot be found'()
    {
        when:
        PaymentParamUtils.getMonth(-1)

        then:
        thrown IllegalArgumentException
    }

    @Test
    def 'should return value for object'()
    {
        given:
        def date = new Date()

        when:
        def result = PaymentParamUtils.<Date, Long>getValue(date, new Function<Date, Long>() {
            @Override
            Long apply(final Date d) {
                return d.getTime()
            }
        })

        then:
        result == date.getTime()
    }
}
