package com.cybersource.payment.configuration.service

import com.cybersource.payment.configuration.resolver.PaymentConfigurationResolver
import com.cybersource.payment.configuration.service.DefaultPaymentConfigurationService
import com.cybersource.payment.model.CybsMerchantPaymentConfigurationModel
import de.hybris.bootstrap.annotations.UnitTest
import org.junit.Test
import spock.lang.Specification

import static com.cybersource.payment.enums.CybsConfigurationType.MERCHANT_CONFIG

@UnitTest
class DefaultPaymentConfigurationServiceSpec extends Specification {

    def service = new DefaultPaymentConfigurationService()

    def type = MERCHANT_CONFIG
    def params = [:]

    @Test
    def 'should provide configuration'() {
        given:
        def resolver = Mock(PaymentConfigurationResolver)
        def expected = new CybsMerchantPaymentConfigurationModel()

        and:
        resolver.resolve(params) >> expected
        service.resolverMap = [(type): resolver]

        when:
        def actual = service.getConfiguration(type, params)

        then:
        actual == expected
    }

    @Test
    def 'should throw exception when no resolver found'() {
        when:
        service.getConfiguration(type, params)

        then:
        thrown(IllegalArgumentException)
    }
}
