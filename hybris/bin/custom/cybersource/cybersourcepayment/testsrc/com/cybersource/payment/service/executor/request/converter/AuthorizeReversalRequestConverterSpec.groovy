package com.cybersource.payment.service.executor.request.converter

import com.cybersource.payment.constants.CybersourcePaymentConstants
import com.cybersource.payment.enums.CybsPaymentType
import com.cybersource.payment.model.CybsPaymentTransactionEntryModel
import com.cybersource.payment.model.CybsPaymentTransactionModel
import com.cybersource.payment.service.PaymentTransactionService
import com.cybersource.payment.service.executor.request.PaymentServiceRequest
import com.cybersource.payment.service.request.CybersourceRequestFactory
import de.hybris.bootstrap.annotations.UnitTest
import de.hybris.platform.core.model.order.AbstractOrderModel
import de.hybris.platform.payment.enums.PaymentTransactionType
import de.hybris.platform.payment.model.PaymentTransactionModel
import org.junit.Test
import spock.lang.Specification

@UnitTest
class AuthorizeReversalRequestConverterSpec extends Specification
{
    def cybersourceRequestFactory = Mock([useObjenesis: false], CybersourceRequestFactory)

    def paymentTransactionService = Mock([useObjenesis: false], PaymentTransactionService)

    def creditCardTransaction = Mock([useObjenesis: false], CybsPaymentTransactionModel)

    def authorizationTransactionEntry1 = Mock([useObjenesis: false], CybsPaymentTransactionEntryModel)

    def order = Mock([useObjenesis: false], AbstractOrderModel)

    def source = PaymentServiceRequest.create()

    def requestCreator = new AuthorizeReversalRequestConverter()

    def authorizationReversalType = new CybersourceRequestFactory(validators: []).authorizeReversal()

    def setup()
    {
        authorizationTransactionEntry1.type >> PaymentTransactionType.AUTHORIZATION
        authorizationTransactionEntry1.transactionStatus >> CybersourcePaymentConstants.TransactionStatus.REVIEW
        authorizationTransactionEntry1.requestId >> '00001'

        order.guid >> '123'
        order.currency >> [isocode: 'USD']
        order.totalPrice >> 100D

        source.addParam('order', order)
        source.addParam('merchantId', 'tacit_hybris_2')
        source.addParam('transaction', creditCardTransaction)
        source.method(CybsPaymentType.CREDIT_CARD)

        requestCreator.cybersourceRequestFactory = cybersourceRequestFactory
        requestCreator.paymentTransactionService = paymentTransactionService

        cybersourceRequestFactory.authorizeReversal() >> authorizationReversalType
    }

    @Test
    def 'creator should create and populate cybersource request object'()
    {
        given:
        paymentTransactionService.getLatestAcceptedTransactionEntry(creditCardTransaction, PaymentTransactionType.AUTHORIZATION) >>
                Optional.of(authorizationTransactionEntry1)

        when:
        def target = requestCreator.convert(source)
        def requestFields = target.requestFields

        then:
        target == authorizationReversalType.request

        requestFields['merchantId'] == 'tacit_hybris_2'
        requestFields['merchantReferenceCode'] == '123'
        requestFields['ccAuthReversalServiceRun'] == true
        requestFields['ccAuthReversalServiceAuthRequestID'] == '00001'
        requestFields['purchaseTotalsCurrency'] == 'USD'
        requestFields['purchaseTotalsGrandTotalAmount'] == 100
    }

    @Test
    def 'should throw exception if order payment authorization transaction entry not found'()
    {
        given:
        paymentTransactionService.getLatestAcceptedTransactionEntry(_ as PaymentTransactionModel, _ as PaymentTransactionType) >>
                Optional.empty()

        when:
        requestCreator.convert(source)

        then:
        thrown IllegalStateException
    }
}
