package com.cybersource.payment.integration

import com.cybersource.payment.integration.helpers.CybsIntegrationSpec
import com.cybersource.payment.service.executor.request.builder.creditcard.AuthorizeRequestBuilder
import de.hybris.bootstrap.annotations.ManualTest
import de.hybris.platform.payment.enums.PaymentTransactionType
import org.junit.Test

@ManualTest
class AuthorizationIntegrationSpec extends CybsIntegrationSpec
{

    def builder = new AuthorizeRequestBuilder()

    @Test
    'should receive accept from cybs'()
    {
        given:
        def order = testOrderUk()
        def cardInfo = testCard()
        def operationStartTime = new Date()

        when:
        def request = builder
                .setMerchantId(testConfig.merchant)
                .setOrder(order)
                .setCardInfo(cardInfo)
                .build()
        def result = paymentServiceExecutor.execute(request)

        then:
        with(result.getData().transactionEntry) {
            type == PaymentTransactionType.AUTHORIZATION
            transactionStatus == 'ACCEPT'
            transactionStatusDetails == '100'

            amount == order.totalPrice
            code.toString().contains(order.code)
            requestId != null
            currency.isocode == order.currency.isocode

            time > operationStartTime
            time < new Date()

            with(properties) {
                merchantReferenceCode == order.guid
                reasonCode == '100'
                decision == 'ACCEPT'

                invalidField == '[]'
                missingField == '[]'

                requestID != null
                requestToken != null

                purchaseTotalsCurrency == order.currency.isocode
                decisionReplyCasePriority != null

                ccAuthReplyReasonCode == '100'
                ccAuthReplyCvCode == '1'
                ccAuthReplyProcessorResponse != null
                ccAuthReplyAvsCode != null
                ccAuthReplyAuthorizedDateTime != null
                ccAuthReplyAuthorizationCode != null

                afsReplyCardIssuer == 'JPMORGAN CHASE BANK, N.A.'
                afsReplyBinCountry == 'US'
                afsReplyReasonCode == '100'
                afsReplyScoreModelUsed == 'default_eu'
                afsReplyCardSchemer == 'VISA CREDIT'
                afsReplyAfsResult != null
                afsReplyHostSeverity != null
                afsReplyIdentityInfoCode != null
                afsReplyVelocityInfoCode != null
                afsReplyAfsFactorCode != null
                afsReplyAddressInfoCode != null
                afsReplyConsumerLocalTime != null
            }
        }
    }

    @Test
    'should receive accept from cybs for US order'()
    {
        given:
        def order = testOrderUs()
        def cardInfo = testCard()
        def operationStartTime = new Date()

        when:
        def request = builder
                .setMerchantId(testConfig.merchant)
                .setOrder(order)
                .setCardInfo(cardInfo)
                .build()
        def result = paymentServiceExecutor.execute(request)

        then:
        with(result.getData().transactionEntry) {
            type == PaymentTransactionType.AUTHORIZATION
            transactionStatus == 'ACCEPT'
            transactionStatusDetails == '100'

            amount == order.totalPrice
            code.toString().contains(order.code)
            requestId != null
            currency.isocode == order.currency.isocode

            time > operationStartTime
            time < new Date()

            with(properties) {
                merchantReferenceCode == order.guid
                reasonCode == '100'
                decision == 'ACCEPT'

                invalidField == '[]'
                missingField == '[]'

                requestID != null
                requestToken != null

                purchaseTotalsCurrency == order.currency.isocode
                decisionReplyCasePriority != null
            }
        }
    }

    @Test
    'should receive review from cybs'()
    {
        given:
        def order = testOrderForReview()
        def cardInfo = testCard()
        def operationStartTime = new Date()

        when:
        def request = builder
                .setMerchantId(testConfig.merchant)
                .setOrder(order)
                .setCardInfo(cardInfo)
                .build()
        def result = paymentServiceExecutor.execute(request)

        then:
        with(result.getData().transactionEntry) {
            type == PaymentTransactionType.AUTHORIZATION
            transactionStatus == 'REVIEW'
            transactionStatusDetails == '480'

            amount == order.totalPrice
            code.toString().contains(order.code)
            requestId != null
            currency.isocode == order.currency.isocode

            time > operationStartTime
            time < new Date()

            with(properties) {
                merchantReferenceCode == order.guid
                reasonCode == '480'
                decision == 'REVIEW'

                invalidField == '[]'
                missingField == '[]'

                requestID != null
                requestToken != null

                purchaseTotalsCurrency == order.currency.isocode
                decisionReplyCasePriority != null
            }
        }
    }

    @Test
    'should receive reject from cybs when invalid field sent (wrong country code)'()
    {
        given:
        def order = testOrderInvalidFields()
        def cardInfo = testCard()
        def operationStartTime = new Date()

        when: "Request is sent"
        def request = builder
                .setMerchantId(testConfig.merchant)
                .setOrder(order)
                .setCardInfo(cardInfo)
                .build()
        def result = paymentServiceExecutor.execute(request)

        then:
        with(result.getData().transactionEntry) {
            type == PaymentTransactionType.AUTHORIZATION
            transactionStatus == 'REJECT'
            transactionStatusDetails == '102'

            code != null
            requestId != null
            currency == null

            time > operationStartTime
            time < new Date()

            properties.invalidField == '[c:billTo/c:country]'
        }
    }

    @Test
    'should receive reject from cybs when invalid card'()
    {
        given:
        def order = testOrderUk()
        def cardInfo = wrongTestCard()
        def operationStartTime = new Date()

        when:
        def request = builder
                .setMerchantId(testConfig.merchant)
                .setOrder(order)
                .setCardInfo(cardInfo)
                .build()
        def result = paymentServiceExecutor.execute(request)

        then:
        with(result.getData().transactionEntry) {
            type == PaymentTransactionType.AUTHORIZATION
            transactionStatus == 'REJECT'
            transactionStatusDetails == '231'

            code != null
            requestId != null
            currency == null

            time > operationStartTime
            time < new Date()
            properties.merchantReferenceCode == order.guid
        }
    }

    @Test
    'should receive reject from cybs when required field is missing (state)'()
    {
        given:
        def order = testOrderMissingFields()
        def cardInfo = testCard()
        def operationStartTime = new Date()

        when:
        def request = builder
                .setMerchantId(testConfig.merchant)
                .setOrder(order)
                .setCardInfo(cardInfo)
                .build()
        def result = paymentServiceExecutor.execute(request)

        then:
        with(result.getData().transactionEntry) {
            type == PaymentTransactionType.AUTHORIZATION
            transactionStatus == 'REJECT'
            transactionStatusDetails == '101'

            code != null
            requestId != null
            currency == null

            time > operationStartTime
            time < new Date()

            properties.missingField == '[c:billTo/c:state]'
        }
    }

    @Test
    'should receive error from cybs'()
    {
        given:
        def order = testOrderWithErrorCode()
        def cardInfo = testCard()
        def operationStartTime = new Date()

        when:
        def request = builder
                .setMerchantId(testConfig.merchant)
                .setOrder(order)
                .setCardInfo(cardInfo)
                .build()
        def result = paymentServiceExecutor.execute(request)

        then:
        with(result.getData().transactionEntry) {
            type == PaymentTransactionType.AUTHORIZATION
            transactionStatus == 'ERROR'
            transactionStatusDetails != null

            code != null
            requestId != null
            currency == null

            time > operationStartTime
            time < new Date()

            properties.ccAuthReplyProcessorResponse != null
        }
    }

    @Test
    'should receive accept for auth request with correct subscription'()
    {
        given:
        def card = testCard()
        def oldOrder = testOrderUk()
        transactionCreator.addAuthorization(oldOrder, card)

        String subscriptionId = transactionCreator.getSubscription(oldOrder)
        def newOrder = testOrderUk()

        def operationStartTime = new Date()

        when:
        def request = builder
                .setMerchantId(testConfig.merchant)
                .setOrder(newOrder)
                .setSubscriptionID(subscriptionId)
                .build()
        def result = paymentServiceExecutor.execute(request)

        then:
        with(result.getData().transactionEntry) {
            type == PaymentTransactionType.AUTHORIZATION
            transactionStatus == 'ACCEPT'
            transactionStatusDetails == '100'

            amount == newOrder.totalPrice
            code.toString().contains(newOrder.code)
            requestId != null
            currency.isocode == newOrder.currency.isocode
            subscriptionID == null

            time > operationStartTime
            time < new Date()
            properties.merchantReferenceCode == newOrder.guid
        }
    }

    @Test
    'should receive accept for auth request with correct subscription and card'()
    {
        given:
        def card = testCard()
        def oldOrder = testOrderUk()
        transactionCreator.addAuthorization(oldOrder, card)

        String subscriptionId = transactionCreator.getSubscription(oldOrder)
        def newOrder = testOrderUk()

        def operationStartTime = new Date()

        when:
        def request = builder
                .setMerchantId(testConfig.merchant)
                .setOrder(newOrder)
                .setSubscriptionID(subscriptionId)
                .setCardInfo(card)
                .build()
        def result = paymentServiceExecutor.execute(request)

        then:
        with(result.getData().transactionEntry) {
            type == PaymentTransactionType.AUTHORIZATION
            transactionStatus == 'ACCEPT'
            transactionStatusDetails == '100'

            amount == newOrder.totalPrice
            code.toString().contains(newOrder.code)
            requestId != null
            currency.isocode == newOrder.currency.isocode
            subscriptionID == null

            time > operationStartTime
            time < new Date()
            properties.merchantReferenceCode == newOrder.guid
        }
    }

    @Test
    'should receive accept for auth request with correct subscription and wrong card'()
    {
        given:
        def card = testCard()
        def oldOrder = testOrderUk()
        transactionCreator.addAuthorization(oldOrder, card)

        String subscriptionId = transactionCreator.getSubscription(oldOrder)
        def newOrder = testOrderUk()
        def wrongCard = wrongTestCard()

        def operationStartTime = new Date()

        when:
        def request = builder
                .setMerchantId(testConfig.merchant)
                .setOrder(newOrder)
                .setSubscriptionID(subscriptionId)
                .setCardInfo(wrongCard)
                .build()
        def result = paymentServiceExecutor.execute(request)

        then:
        with(result.getData().transactionEntry) {
            type == PaymentTransactionType.AUTHORIZATION
            transactionStatus == 'ACCEPT'
            transactionStatusDetails == '100'

            amount == newOrder.totalPrice
            code.toString().contains(newOrder.code)
            requestId != null
            currency.isocode == newOrder.currency.isocode
            subscriptionID == null

            time > operationStartTime
            time < new Date()
            properties.merchantReferenceCode == newOrder.guid
        }
    }

    @Test
    'should receive reject for auth request with wrong subscription'()
    {
        given:
        String subscriptionId = randomSubscriptionId()
        def newOrder = testOrderUk()

        def operationStartTime = new Date()

        when:
        def request = builder
                .setMerchantId(testConfig.merchant)
                .setOrder(newOrder)
                .setSubscriptionID(subscriptionId)
                .build()
        def result = paymentServiceExecutor.execute(request)

        then:
        with(result.getData().transactionEntry) {
            type == PaymentTransactionType.AUTHORIZATION
            transactionStatus == 'REJECT'
            transactionStatusDetails == '102'

            amount == null
            code.toString().contains(newOrder.code)
            requestId != null
            currency == null
            requestToken != null
            subscriptionID == null

            time > operationStartTime
            time < new Date()
        }
    }

    @Test
    'should receive reject for auth request with wrong subscription and correct card'()
    {
        given:
        String subscriptionId = randomSubscriptionId()
        def newOrder = testOrderUk()
        def card = testCard()

        def operationStartTime = new Date()

        when:
        def request = builder
                .setMerchantId(testConfig.merchant)
                .setOrder(newOrder)
                .setSubscriptionID(subscriptionId)
                .setCardInfo(card)
                .build()
        def result = paymentServiceExecutor.execute(request)

        then:
        with(result.getData().transactionEntry) {
            type == PaymentTransactionType.AUTHORIZATION
            transactionStatus == 'REJECT'
            transactionStatusDetails == '102'

            amount == null
            code.toString().contains(newOrder.code)
            requestId != null
            currency == null
            requestToken != null
            subscriptionID == null

            time > operationStartTime
            time < new Date()
        }
    }
}
