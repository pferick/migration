package com.cybersource.payment.integration.helpers

import com.cybersource.payment.enums.CybsPaymentType
import com.cybersource.payment.model.CybsMerchantModel
import com.cybersource.payment.model.CybsPaymentTransactionEntryModel
import com.cybersource.payment.model.CybsPaymentTransactionModel
import com.cybersource.payment.security.ws.WSSUsernameTokenSecurityHandler
import com.cybersource.payment.service.MerchantService
import com.cybersource.payment.service.executor.PaymentServiceExecutor
import com.cybersource.payment.service.provider.PaymentTransactionCreator
import de.hybris.bootstrap.config.ConfigUtil
import de.hybris.platform.core.enums.CreditCardType
import de.hybris.platform.core.model.c2l.CountryModel
import de.hybris.platform.core.model.c2l.CurrencyModel
import de.hybris.platform.core.model.c2l.RegionModel
import de.hybris.platform.core.model.order.AbstractOrderEntryModel
import de.hybris.platform.core.model.order.AbstractOrderModel
import de.hybris.platform.core.model.product.ProductModel
import de.hybris.platform.core.model.user.AddressModel
import de.hybris.platform.payment.dto.CardInfo
import de.hybris.platform.payment.enums.PaymentTransactionType
import de.hybris.platform.payment.model.PaymentTransactionEntryModel
import de.hybris.platform.payment.model.PaymentTransactionModel
import de.hybris.platform.servicelayer.i18n.CommonI18NService
import de.hybris.platform.servicelayer.model.ModelService

import javax.annotation.Resource

import static io.qala.datagen.RandomShortApi.*
import static io.qala.datagen.RandomValue.*
import static io.qala.datagen.StringModifier.Impls.*

abstract class CybsIntegrationSpec extends HybrisIntegrationSpec
{
    private modelServiceBkp
    private commonI18NServiceBkp
    private merchantServiceBkp

    private configDir = ConfigUtil.getPlatformConfig(this.getClass()).getSystemConfig().getDir('HYBRIS_CONFIG_DIR')

    def testConfig = new TestConfig().getTestConfig(configDir)
    def transactionCreator

    @Resource
    PaymentServiceExecutor paymentServiceExecutor

    @Resource
    PaymentTransactionCreator paymentTransactionCreator

    @Resource
    WSSUsernameTokenSecurityHandler wssUsernameTokenSecurityHandler




    def setup()
    {

        stubModelService(paymentTransactionCreator)
        stubCommonI18NService(paymentTransactionCreator)
        stubMerchantService(wssUsernameTokenSecurityHandler)

        transactionCreator = new CcTransactionsCreator(
                paymentServiceExecutor: paymentServiceExecutor,
                merchantId: testConfig.merchant)
    }

    def cleanup()
    {
        unstubModelService(paymentTransactionCreator)
        unstubCommonI18NService(paymentTransactionCreator)
        unstubMerchantService(wssUsernameTokenSecurityHandler)
    }

    //region Stubs
    def stubModelService(paymentTransactionCreator)
    {
        def modelService = Mock(ModelService)
        modelServiceBkp = paymentTransactionCreator.modelService
        paymentTransactionCreator.modelService = modelService

        _ * modelService.create(CybsPaymentTransactionModel.class) >> new CybsPaymentTransactionModel(entries: Mock(List))
        _ * modelService.saveAll(_ as CybsPaymentTransactionModel, _ as AbstractOrderModel)
        _ * modelService.create(CybsPaymentTransactionEntryModel.class) >> new CybsPaymentTransactionEntryModel()
        _ * modelService.saveAll(_ as CybsPaymentTransactionEntryModel, _ as CybsPaymentTransactionModel)
    }

    def unstubModelService(paymentTransactionModel)
    {
        paymentTransactionModel.modelService = modelServiceBkp ?: paymentTransactionModel.modelService
    }

    def stubCommonI18NService(paymentTransactionCreator)
    {
        def commonI18NService = Mock(CommonI18NService)
        commonI18NServiceBkp = paymentTransactionCreator.commonI18NService
        paymentTransactionCreator.commonI18NService = commonI18NService

        _ * commonI18NService.getCurrency(_ as String) >> { String arg -> new CurrencyModel(isocode: arg) }
    }

    def unstubCommonI18NService(paymentTransactionModel)
    {
        paymentTransactionModel.commonI18NService = commonI18NServiceBkp ?: paymentTransactionModel.commonI18NService
    }

    def stubMerchantService(wssUsernameTokenSecurityHandler)
    {
        def merchantService = Mock(MerchantService)
        merchantServiceBkp = wssUsernameTokenSecurityHandler.merchantService
        wssUsernameTokenSecurityHandler.merchantService = merchantService

        _ * merchantService.getMerchant(_ as String) >> new CybsMerchantModel(
                id: testConfig.merchant,
                passwordToken: testConfig.token
        )
    }

    def unstubMerchantService(wssUsernameTokenSecurityHandler)
    {
        wssUsernameTokenSecurityHandler.merchantService = merchantServiceBkp ?:
                wssUsernameTokenSecurityHandler.merchantService
    }
    //endregion

    //region DataGen

    def testOrderUk()
    {
        def order = testOrder()
        order.paymentAddress >> addressUk()
        order.deliveryAddress >> addressUk()

        return order

    }

    def testOrderUs()
    {
        def order = testOrder()

        order.paymentAddress >> addressUs()

        return order

    }

    def testOrderInvalidFields()
    {
        def order = testOrder()

        order.paymentAddress >> invalidAddress()

        return order
    }

    def testOrderWithErrorCode()
    {
        def order = testOrder(testConfig.processorErrorTrigger)

        order.paymentAddress >> addressUk()

        return order
    }

    def testOrderMissingFields()
    {
        def order = testOrder()

        order.paymentAddress >> addressWithEmptyRegion()
        order.deliveryAddress >> addressWithEmptyRegion()

        return order
    }

    def testOrderForReview()
    {
        def order = testOrder()

        order.paymentAddress >> addressForReview()

        return order
    }

    private testOrder(totalPrice = null)
    {
        totalPrice = totalPrice ?: integer(1000)
        def order = Mock([useObjenesis: false], AbstractOrderModel)

        order.paymentTransactions >> []
        order.currency >> testCurrency()
        order.totalPrice >> totalPrice
        order.code >> alphanumeric(10)
        order.guid >> alphanumeric(10)
        order.entries >> [testOrderItem()]

        return order
    }


    private addressUk()
    {
        def address = testAddress()

        address.country >> testCountry('UK')

        return address
    }

    private addressForReview()
    {
        def address = testAddress(true)

        address.country >> testCountry('UK')

        return address
    }

    private addressUs()
    {
        def address = testAddress()

        address.country >> testCountry('US')
        address.region >> testRegion()

        return address
    }

    private addressWithEmptyRegion()
    {
        def address = testAddress()

        address.country >> testCountry('US')

        return address
    }

    private invalidAddress()
    {
        def address = testAddress()

        address.country >> testCountry('ZZ')

        return address
    }

    private testRegion()
    {
        def region = Mock([useObjenesis: false], RegionModel)

        region.isocodeShort >> 'CA'

        return region
    }

    private testAddress(forReview = false)
    {
        def firstName = forReview ? testConfig.nameReviewTrigger : 'FN' + english(10)
        def address = Mock([useObjenesis: false], AddressModel)

        address.town >> 'town' + english(10)
        address.streetnumber >> 'Address' + length(5).with(spaces()).alphanumeric()
        address.firstname >> firstName
        address.lastname >> 'LN' + english(10)
        address.email >> 'test@test.com'
        address.postalcode >> '94111'

        return address
    }


    def testCard()
    {
        def card = Mock([useObjenesis: false], CardInfo)

        card.cardHolderFullName >> "Name" + alphanumeric(10)
        card.cardType >> CreditCardType.VISA
        card.cardNumber >> '4111111111111111'
        card.cv2Number >> '111'
        card.expirationMonth >> 12
        card.expirationYear >> 2025

        return card
    }

    def wrongTestCard()
    {
        def card = Mock([useObjenesis: false], CardInfo)

        card.cardHolderFullName >> "Name" + alphanumeric(10)
        card.cardType >> CreditCardType.VISA
        card.cardNumber >> '4111111111111110'
        card.cv2Number >> '111'
        card.expirationMonth >> 12
        card.expirationYear >> 2025

        return card
    }

    def testTransaction(type = null)
    {
        def transaction = Mock([useObjenesis: false], PaymentTransactionModel)

        transaction.paymentProvider >> CybsPaymentType.CREDIT_CARD.name()
        transaction.code >> alphanumeric(10)
        transaction.entries >> [testTransactionEntry(type)]

        return transaction
    }

    def randomSubscriptionId()
    {
        return alphanumeric(10)
    }


    private testTransactionEntry(type)
    {
        def typeName = (type ?: PaymentTransactionType.AUTHORIZATION).name()
        def transactionEntry = Mock([useObjenesis: false], PaymentTransactionEntryModel)

        transactionEntry.requestId >> alphanumeric(10)
        transactionEntry.currency >> testCurrency()
        transactionEntry.requestToken >> alphanumeric(10)
        transactionEntry.amount >> integer(1000)
        transactionEntry.transactionStatus >> 'ACCEPT'
        transactionEntry.type >> typeName
        transactionEntry.code >> alphanumeric(10)

        return transactionEntry
    }

    private testCurrency()
    {
        def currency = Mock([useObjenesis: false], CurrencyModel)

        currency.isocode >> "USD"

        return currency
    }

    private testCountry(country)
    {
        def testCountry = Mock([useObjenesis: false], CountryModel)

        testCountry.isocode >> country

        return testCountry
    }

    private testOrderItem()
    {
        def orderItem = Mock([useObjenesis: false], AbstractOrderEntryModel)

        orderItem.product >> testProduct()
        orderItem.quantity >> integer(10)
        orderItem.basePrice >> integer(10, 99)

        return orderItem
    }

    private testProduct()
    {
        def product = Mock([useObjenesis: false], ProductModel)

        product.code >> alphanumeric(10)
        product.name >> alphanumeric(10)

        return product

    }

    //endregion

}
