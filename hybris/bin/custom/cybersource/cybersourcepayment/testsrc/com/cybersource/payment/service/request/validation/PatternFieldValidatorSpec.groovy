package com.cybersource.payment.service.request.validation

import de.hybris.bootstrap.annotations.UnitTest
import org.junit.Test
import spock.lang.Specification
import static io.qala.datagen.RandomShortApi.*;

@UnitTest
class PatternFieldValidatorSpec extends Specification
{
    def validator = new PatternFieldValidator()
    static maxSize = 255

    @Test
    def "should return result as per field value validity"()
    {
        expect:
        validator.validate(value, rules) == valid

        where:
        value                 | rules            | valid
        unicode(maxSize)      | [:]              | true
        unicode(maxSize)      | [pattern:'']     | true
        alphanumeric(maxSize) | [pattern:'\\w*'] | true
        english(maxSize)      | [pattern:'\\d*'] | false
        ''                    | [:]              | true
        ''                    | [pattern:'']     | true
        ''                    | [pattern:'\\w*'] | true
        ''                    | [pattern: '\\w'] | false
        null                  | [:]              | true
        null                  | [pattern:'']     | true
        null                  | [pattern:'\\w*'] | false
    }



}
