package com.cybersource.payment.service.request

import com.cybersource.payment.service.request.value.AuthType
import com.cybersource.payment.service.request.value.MerchantDefinedField
import de.hybris.bootstrap.annotations.UnitTest
import org.junit.Test
import spock.lang.Specification

@UnitTest
class AuthorizationTypeSpec extends Specification
{
    def request = Mock([useObjenesis: false], CybersourceRequest)

    def authorizationType = new AuthorizationType(request);

    @Test
    def 'should return request object passed on instantiation'()
    {
        when:
        def requestObject = authorizationType
                .merchantId("MID001")
                .merchantReferenceCode("10")
                .ccAuthServiceRun(true)
                .purchaseTotalsCurrency("GBP")
                .request();

        then:
        requestObject == request
    }

    @Test
    def 'should trigger validation on get request method'()
    {
        when:
        authorizationType
                .merchantId("MID001")
                .merchantReferenceCode("10")
                .ccAuthServiceRun(true)
                .purchaseTotalsCurrency("GBP")
                .request();

        then:
        1 * request.validate()
    }

    @Test
    def 'should set request fields as per methods invoked'()
    {
        when:
        authorizationType
                .merchantId("MID001")
                .merchantReferenceCode("10")
                .ccAuthServiceRun(true)
                .purchaseTotalsCurrency("GBP")
                .ccAuthServiceAuthType(AuthType.AUTOCAPTURE)
                .payerAuthEnrollServiceRun(true)
                .request();

        then:
        1 * request.addRequestField('merchantId', 'MID001', _ as String)
        1 * request.addRequestField('merchantReferenceCode', "10", _ as String)
        1 * request.addRequestField('ccAuthServiceRun', true, _ as String)
        1 * request.addRequestField('purchaseTotalsCurrency', 'GBP', _ as String)
        1 * request.addRequestField('payerAuthEnrollServiceRun', true, _ as String)
        1 * request.addRequestField('ccAuthServiceAuthType', AuthType.AUTOCAPTURE, _ as String)
    }

    @Test
    def 'should add multivalue request fields'()
    {
        when:
        authorizationType
                .merchantId("MID001")
                .merchantReferenceCode("10")
                .ccAuthServiceRun(true)
                .purchaseTotalsCurrency("GBP")
                .itemProductName(0, "USB2")
                .itemQuantity(0, 3)
                .itemProductName(1, "USB3")
                .itemQuantity(1, 2)
                .request();

        then:
        request.addRequestField('0:itemProductName', 'USB2', _ as String)
        request.addRequestField('0:itemQuantity', 3, _ as String)
        request.addRequestField('1:itemProductName', 'USB3', _ as String)
        request.addRequestField('1:itemQuantity', 2, _ as String)
    }

    @Test
    def 'should add custom (merchant defined data) request fields'()
    {
        when:
        authorizationType
                .merchantId("MID001")
                .merchantReferenceCode("10")
                .ccAuthServiceRun(true)
                .purchaseTotalsCurrency("GBP")
                .custom(MerchantDefinedField.CUSTOMER_DATA, "true")
                .request();

        then:
        1 * request.addCustomField(MerchantDefinedField.CUSTOMER_DATA, 'true', _ as String)
    }
}
