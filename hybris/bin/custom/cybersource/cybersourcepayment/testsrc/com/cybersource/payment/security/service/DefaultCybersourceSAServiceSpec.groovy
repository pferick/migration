package com.cybersource.payment.security.service

import com.cybersource.payment.security.service.DefaultCybersourceSAService
import com.cybersource.payment.security.service.EncryptionService
import de.hybris.bootstrap.annotations.UnitTest
import org.junit.Test
import spock.lang.Specification

@UnitTest
class DefaultCybersourceSAServiceSpec extends Specification
{
    def encryptionService = Mock([useObjenesis: false], EncryptionService);

    def service = new DefaultCybersourceSAService()

    def setup()
    {
        service.encryptionService = encryptionService
    }

    @Test
    def 'should create signature for given signed payment fields'()
    {
        given:
        encryptionService.signForSecureAcceptance('field1=value1,field2=value2', '1234567890') >> 'ABCDEF123'

        when:
        def signature = service.getDigest(
                [field1: 'value1', field2: 'value2', field3: 'value3', signed_field_names: 'field1,field2'],
                '1234567890'
        )

        then:
        signature == 'ABCDEF123'
    }

    @Test
    def 'should return empty signature if signature cannot be created'()
    {
        given:
        encryptionService.signForSecureAcceptance(_ as String, _ as String) >> {
            throw new RuntimeException("test exception")
        }

        when:
        def signature = service.getDigest([field1: 'value1', signed_field_names: 'field1'], '1234567890')

        then:
        signature == ''
    }

    @Test
    def 'should throw exception if payment field map to be signed not provided'()
    {
        when:
        service.getDigest(null, '1234567890')

        then:
        thrown IllegalArgumentException
    }

    @Test
    def 'should validate transaction signature'()
    {
        given:
        encryptionService.signForSecureAcceptance('field1=value1,field2=value2', '1234567890') >> signature

        when:
        def validSignature = service.validateTransactionSignature(
                [field1: 'value1', field2: 'value2', field3: 'value3', signed_field_names: 'field1,field2', signature: cybsSignature],
                '1234567890'
        )

        then:
        validSignature == valid

        where:
        signature   | cybsSignature | valid
        'ABCDEF123' | 'ABCDEF123'   | true
        'ABCDEF123' | 'ABCDEF124'   | false
        'ABCDEF124' | null          | false
    }

    @Test
    def 'should throw exception if payment field map not provided'()
    {
        when:
        service.validateTransactionSignature(null, '1234567890')

        then:
        thrown IllegalArgumentException
    }
}
