package com.cybersource.payment.service.executor.request.converter

import com.cybersource.payment.service.request.CybersourceRequest
import com.cybersource.payment.service.request.CybersourceRequestFactory
import com.cybersource.payment.service.request.SaAuthorizationType
import com.cybersource.payment.service.executor.request.PaymentServiceRequest
import de.hybris.bootstrap.annotations.UnitTest
import de.hybris.platform.core.model.order.AbstractOrderModel
import org.junit.Test
import spock.lang.Specification

@UnitTest
class SaAuthorizeRequestConverterSpec extends Specification
{
    def order = Mock([useObjenesis: false], AbstractOrderModel)

    def cybersourceRequest = Mock([useObjenesis: false], CybersourceRequest)

    def cybersourceRequestFactory = Mock([useObjenesis: false], CybersourceRequestFactory)

    def creator = new SaAuthorizeRequestConverter()

    def setup()
    {
        creator.cybersourceRequestFactory = cybersourceRequestFactory
    }

    @Test
    def 'should create cybersource request and set required parameters'()
    {
        given:
        def paymentResponse = [decision: 'ACCEPT'] as Map
        def source = PaymentServiceRequest.create()
                .addParam('order', order)
                .addParam('paymentResponse', paymentResponse)
        cybersourceRequestFactory.saAuthorize() >> new SaAuthorizationType(cybersourceRequest)

        when:
        def target = creator.convert(source)

        then:
        1 * cybersourceRequest.addRequestField('order', order, _ as String)
        1 * cybersourceRequest.addRequestField('paymentResponse', paymentResponse, _ as String)
        target == cybersourceRequest
    }
}
