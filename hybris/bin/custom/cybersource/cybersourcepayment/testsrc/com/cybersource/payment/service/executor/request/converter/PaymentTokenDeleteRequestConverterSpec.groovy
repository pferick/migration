package com.cybersource.payment.service.executor.request.converter

import com.cybersource.payment.service.PaymentTransactionService
import com.cybersource.payment.service.executor.request.PaymentServiceRequest
import com.cybersource.payment.service.request.CybersourceRequestFactory
import de.hybris.bootstrap.annotations.UnitTest
import de.hybris.platform.core.model.c2l.CurrencyModel
import de.hybris.platform.core.model.order.OrderModel
import de.hybris.platform.payment.model.PaymentTransactionEntryModel
import de.hybris.platform.payment.model.PaymentTransactionModel
import org.junit.Test
import spock.lang.Specification

import static com.cybersource.payment.enums.CybsPaymentType.CREDIT_CARD
import static de.hybris.platform.payment.enums.PaymentTransactionType.CREATE_SUBSCRIPTION
import static java.util.Optional.empty
import static java.util.Optional.of
import static org.apache.commons.collections.ListUtils.EMPTY_LIST

@UnitTest
class PaymentTokenDeleteRequestConverterSpec extends Specification {

    def transaction = Mock([useObjenesis: false], PaymentTransactionModel)

    def transactionEntry = Mock([useObjenesis: false], PaymentTransactionEntryModel)

    def paymentTransactionService = Mock([useObjenesis: false], PaymentTransactionService)

    def order = new OrderModel(currency: new CurrencyModel(isocode: 'GBP'), guid: 'order_code')

    def source = new PaymentServiceRequest(paymentType: CREDIT_CARD)

    def creator = new PaymentTokenDeleteRequestConverter()

    def setup() {
        source.addParam('order', order)
        source.addParam('merchantId', 'tacit_hybris_2')
        source.addParam('transaction', transaction)

        creator.cybersourceRequestFactory = new CybersourceRequestFactory(validators: EMPTY_LIST)
        creator.paymentTransactionService = paymentTransactionService

        transactionEntry.subscriptionID >> 'subscriptionId'
    }

    @Test
    def 'should create delete token request'() {
        given:
        paymentTransactionService.getLatestAcceptedTransactionEntry(transaction, CREATE_SUBSCRIPTION) >> of(transactionEntry)

        when:
        def target = creator.convert(source)

        then:
        target.requestFields.merchantId == 'tacit_hybris_2'
        target.requestFields.merchantReferenceCode == order.guid
        target.requestFields.recurringSubscriptionInfoSubscriptionID == transactionEntry.subscriptionID
        target.requestFields.paySubscriptionDeleteServiceRun == true
    }

    @Test
    def 'should throw exception when no transaction entry found for given status'() {
        given:
        paymentTransactionService.getLatestAcceptedTransactionEntry(transaction, CREATE_SUBSCRIPTION) >> empty()

        when:
        creator.convert(source)

        then:
        thrown IllegalStateException
    }
}
