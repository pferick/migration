package com.cybersource.payment.cronjob

import com.cybersource.payment.model.CybsMerchantModel
import com.cybersource.payment.model.cron.CybsConversionReportPollingCronJobModel
import com.cybersource.payment.report.executor.ReportExecutor
import com.cybersource.payment.report.listener.ReportListener
import com.cybersource.payment.report.request.OnDemandConversionReportRequest
import com.cybersource.payment.service.MerchantService
import com.cybersource.payment.report.service.ReportTimeService
import com.cybersource.reports.conversion.Report
import de.hybris.bootstrap.annotations.UnitTest
import de.hybris.platform.cronjob.enums.CronJobResult
import de.hybris.platform.cronjob.enums.CronJobStatus
import de.hybris.platform.servicelayer.model.ModelService
import de.hybris.platform.servicelayer.time.TimeService
import org.joda.time.DateTime
import org.joda.time.Interval
import org.junit.Test
import spock.lang.Specification

@UnitTest
class OnDemandConversionReportPollingJobSpec extends Specification {

    def merchantService = Mock([useObjenesis: false], MerchantService)

    def reportExecutor = Mock([useObjenesis: false], ReportExecutor)

    def modelService = Mock([useObjenesis: false], ModelService)

    def timeService = Mock([useObjenesis: false], TimeService)

    def reportTimeService = Mock([useObjenesis: false], ReportTimeService)

    def job = new OnDemandConversionReportPollingJob(merchantService: merchantService, reportExecutor: reportExecutor,
            modelService: modelService, timeService: timeService, reportTimeService: reportTimeService)

    def merchant = Mock([useObjenesis: false], CybsMerchantModel)

    def listener = Mock([useObjenesis: false], ReportListener)

    def report = Mock([useObjenesis: false], Report)

    def currentDate = new Date()

    def interval = new Interval(new DateTime().minusDays(1), DateTime.now())

    def setup() {
        job.listeners = [listener]
        merchant.id = "merchant"

        merchantService.getAllMerchants() >> [merchant]
        timeService.currentTime >> currentDate

        reportTimeService.getReportInterval(null) >> interval
    }

    @Test
    def 'should execute on demand report polling'() {
        when:
        def cronJob = new CybsConversionReportPollingCronJobModel()

        def result = job.perform(cronJob)

        then:
        1 * listener.handle(merchant, report)
        1 * reportExecutor.getReport(_) >> { args ->

            def request = args[0] as OnDemandConversionReportRequest

            assert request.merchantId == merchant.id
            assert request.startDate == interval.start.toDate()
            assert request.endDate == interval.end.toDate()

            report
        }
        1 * modelService.save(_) >> { args ->
            def cronJobToSave = args[0] as CybsConversionReportPollingCronJobModel
            assert cronJobToSave.lastRunDate == currentDate
        }

        result.status == CronJobStatus.FINISHED
        result.result == CronJobResult.SUCCESS
    }
}
