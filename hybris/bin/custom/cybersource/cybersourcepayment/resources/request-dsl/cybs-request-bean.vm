/*
* ----------------------------------------------------------------
* --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
* --- Generated at $currentDateTime
* ----------------------------------------------------------------
*/
package com.cybersource.payment.service.request;

import java.util.Map;

import com.cybersource.payment.service.request.value.MerchantDefinedField;

/**
* Defines a Cybersource request object.
*/
public interface $shortClassName
{
	#set ( $D = '#' )
	/**
	* Returns request fields added by {@link CybersourceRequest${D}addRequestField}(String, Object, String)} method.
	*
	* @return request fields map
	*/
	Map<String, Object> getRequestFields();

	/**
	* Returns custom (merchant defined data) fields added by
	* {@link CybersourceRequest${D}addCustomField(MerchantDefinedField, Object, String)} (String, Object, String)} method.
	*
	* @return merchant defined data fields
	*/
	Map<MerchantDefinedField, Object> getCustomFields();

	/**
	* Adds a new request field for given name.
	*
	* @param name the name of the request field
	* @param value the value of the request field
	* @param rulesStr a string representation of field constraints (e.g. size:3, pattern:[abc]*)
	*/
	void addRequestField(final String name, final Object value, final String rulesStr);

	/**
	* Adds a new custom (merchant defined data) field for given name.
	*
	* @param field custom field (merchant defined field)
	* @param value the value of the custom field
	* @param rulesStr a string representation of field constraints (e.g. size:3, pattern:[abc]*)
	*/
	void addCustomField(final MerchantDefinedField field, final Object value, final String rulesStr);

	/**
	* Triggers validation for all request and custom fields.
	*
	* @throws IllegalArgumentException if fields validation fails
	*/
	void validate();
}

