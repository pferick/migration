import org.openqa.selenium.Dimension
import org.openqa.selenium.firefox.FirefoxDriver

//driver = "firefox"

driver = {
    def driverInstance = new FirefoxDriver()
    driverInstance.manage().window().setSize(new Dimension(1280, 1024))
    driverInstance
}

waiting {
    timeout = 10
    retryInterval = 0.5
}

reportsDir = "hybris/bin/cybsxt/cybersourcepaymentautomation/build/reports/geb/"
resources = "hybris/bin/cybsxt/cybersourcepaymentautomation/build/resources/test"
server = "https://apparel-uk.local:9002"
baseUrl = "${server}/yb2cacceleratorstorefront/"

environments {
    dev {
        baseUrl = "https://apparel-uk.local:9002/yb2cacceleratorstorefront/"
        reportsDir = "build/reports/devTest/geb"
        resources = "build/resources/test"
        baseUrl = "${server}/yb2cacceleratorstorefront/"
    }
    smoke {

    }
    test {

    }
}

