#!/usr/bin/env bash


##
# Configurations.
##

# Base URL of your web site.
site_url=$1

# Key to be updated and the value
key=$2
value=$3

# Endpoint URL for login action.
login_url="$site_url/admin/login.jsp"

# authentication credentials.
password="nimda"
username="admin"

# Path to temporary file which will store your cookie data.
cookie_path=/tmp/flexcookie.txt

# URL of your custom action.
# action_url="$site_url/admin/console/flexsearch/execute"
action_url="$site_url/admin/platform/configstore"

# This is data that you want to send to your custom endpoint.
#data="user=admin&maxCount=200&flexibleSearchQuery="
data="user=admin&maxCount=200&sqlQuery="

##
# Logic.
##

regex="<meta name=\"_csrf\" content=\"([^\"]+)"

# Get token and construct the cookie, save the returned token.
body=$(curl -b $cookie_path -c $cookie_path --request GET "$login_url" -s -k)
[[ $body =~ $regex ]]

token=${BASH_REMATCH[1]}



# Authentication. POST to $login_url with the token in header "X-CSRF-Token: $token".
result=$(curl -H "X-CSRF-Token: $token" -b $cookie_path -c $cookie_path -d "j_username=$username&j_password=$password&_csrf=$token&submit=login" "$site_url/admin/j_spring_security_check" -s -k)



# Get new token after authentication.
body=$(curl -b $cookie_path -c $cookie_path --request GET "$site_url/admin/platform/config" -s -k)

[[ $body =~ $regex ]]

token=${BASH_REMATCH[1]}

# Send POST to you custom action URL. With the token in header "X-CSRF-Token: $token"
curl -H "X-CSRF-Token: $token" -b $cookie_path -c $cookie_path -d "key=$key&val=$value" "$action_url" -s -k
# curl -H "X-CSRF-Token: $token" -b $cookie_path -c $cookie_path -d "$data" --data-urlencode "flexibleSearchQuery=$query" "$action_url" -s -k

