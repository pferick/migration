/*
 * [y] hybris Platform
 * 
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information of SAP 
 * Hybris ("Confidential Information"). You shall not disclose such 
 * Confidential Information and shall use it only in accordance with the 
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.cybersource.payment.automation.tests.setup;

import static com.cybersource.payment.automation.tests.constants.CybersourcepaymentautomationConstants.PLATFORM_LOGO_CODE;

import de.hybris.platform.core.initialization.SystemSetup;

import java.io.InputStream;

import com.cybersource.payment.automation.tests.constants.CybersourcepaymentautomationConstants;
import com.cybersource.payment.automation.tests.service.CybersourcepaymentautomationService;


@SystemSetup(extension = CybersourcepaymentautomationConstants.EXTENSIONNAME)
public class CybersourcepaymentautomationSystemSetup
{
	private final CybersourcepaymentautomationService cybersourcepaymentautomationService;

	public CybersourcepaymentautomationSystemSetup(final CybersourcepaymentautomationService cybersourcepaymentautomationService)
	{
		this.cybersourcepaymentautomationService = cybersourcepaymentautomationService;
	}

	@SystemSetup(process = SystemSetup.Process.INIT, type = SystemSetup.Type.ESSENTIAL)
	public void createEssentialData()
	{
		cybersourcepaymentautomationService.createLogo(PLATFORM_LOGO_CODE);
	}

	private InputStream getImageStream()
	{
		return CybersourcepaymentautomationSystemSetup.class.getResourceAsStream("/cybersourcepaymentautomation/sap-hybris-platform.png");
	}
}
