package com.cybersource.payment.automation.tests.pageobjects

import com.cybersource.payment.automation.tests.pageobjects.modules.AddToCartDialog
import geb.Page

class ProductDescriptionPage extends Page
{
    static at = { $("body.page-productDetails") }
    static content = {
        qtyInput { $("input.js-qty-selector-input") }
        addToCartCta(wait: true) { $("#addToCartButton") }
        addToCartDialog { module AddToCartDialog }
    }
}
