package com.cybersource.payment.automation.tests.pageobjects.hop

import com.cybersource.payment.automation.tests.pageobjects.secure3d.AuthorizationPage
import geb.Page

/**
 * Created by dsurchis on 14/12/16.
 */
class SaHopPaymentPage extends Page
{
//    static at = { $('iframe')}
    static content = {
        paymentFrame(page: PaymentFramePage) { $('iframe') }
        reviewFrame(page: ReviewFramePage) { $('iframe') }
//        authFrame(page: AuthorizationPage) { $('iframe') }
        authFrame(page: AuthorizationPage) { $('iframe#authWindow') }
    }
}
