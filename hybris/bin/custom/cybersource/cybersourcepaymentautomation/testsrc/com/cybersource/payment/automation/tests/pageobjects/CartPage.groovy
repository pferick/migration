package com.cybersource.payment.automation.tests.pageobjects

import geb.Page

class CartPage extends Page
{
    static url = "cart"
    static at = { title.startsWith("Your Shopping Bag") }
    static content = {
        checkoutCta(to: CheckoutPage, wait: true) { $("button.btn--continue-checkout", 0) }
        guestCheckoutCta(to: CheckoutLoginPage, wait: true) { $("button.btn--continue-checkout", 0) }
    }
}
