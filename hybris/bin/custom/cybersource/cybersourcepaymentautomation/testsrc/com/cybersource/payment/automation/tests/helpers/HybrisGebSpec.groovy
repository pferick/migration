package com.cybersource.payment.automation.tests.helpers

import com.anotherchrisberry.spock.extensions.retry.RetryOnFailure
import com.cybersource.payment.automation.tests.pageobjects.*
import com.google.gson.Gson
import geb.spock.GebReportingSpec

import static io.qala.datagen.RandomShortApi.*

@RetryOnFailure
class HybrisGebSpec extends GebReportingSpec
{
    static PRODUCT_ID = "300613859"
    static EMAIL
    static COUNTRY
    static TITLE
    static FIRST_NAME
    static LAST_NAME
    static ADDRESS
    static CITY
    static POST_CODE
    static PASSWORD

    def setup ()
    {
        EMAIL = "${english(10)}@testemail.xyz".toLowerCase()
        COUNTRY = 'GB'
        TITLE = 'mr'
        FIRST_NAME = english(10)
        LAST_NAME = english(10)
        ADDRESS = english(10)
        CITY = english(10)
        POST_CODE = "A9A 9AA"
        PASSWORD = english(10)
    }

    //region Set Strategy
    def setPciStrategyHop()
    {
        setHybrisProperty('site.pci.strategy', 'CYBS_HOP')
    }

    def setPciStrategySop()
    {
        setHybrisProperty('site.pci.strategy', 'CYBS_SOP')
    }

    private setHybrisProperty(key, value)
    {
        def proc = [
                "./${browser.config.rawConfig.resources}/set_property.sh",
                browser.config.rawConfig.server,
                key,
                value
        ].execute()
        def b = new StringBuffer()
        proc.consumeProcessErrorStream(b)

        assert proc.text == '{"action":"update","hasEdited":false}'
    }
    //endregion

    //region RunFlexibleSearch
    def getOrderStatus(orderCode)
    {
        def query = "SELECT {orderstatus.code} \n" +
                "from {\n" +
                "orderstatus join order\n" +
                "on {order:status} = {orderstatus:pk}\n" +
                "}\n" +
                "where {order.code} ='${orderCode}'"
        return runSearch(query).first().first()
    }

    def getTransactionPaymentProvider(orderCode)
    {
        def query = "SELECT {PaymentProvider}" +
                "FROM {\n" +
                "CybsPaymentTransaction JOIN order \n" +
                "ON {CybsPaymentTransaction:order} = {order:pk}\n" +
                "}\n" +
                "WHERE {order.code} = '${orderCode}' "
        return runSearch(query).first().first()
    }
    def getTransactionStatus(orderCode, entryType)
    {
        def query = "SELECT {pte.TransactionStatus}\n" +
                "FROM {CybsPaymentTransaction AS pt\n" +
                "  JOIN order\n" +
                "      ON {pt:order} = {order:pk}\n" +
                "  JOIN CybsPaymentTransactionEntry AS pte \n" +
                "      ON {pte:PaymentTransaction} = {pt:pk}\n" +
                "  JOIN PaymentTransactionType as ptt\n" +
                "  \t  ON {pte:type} = {ptt:pk}\n" +
                "  }\n" +
                "WHERE {order.code} = '${orderCode}' AND {ptt.code} = '${entryType}'"
        return runSearch(query).first().first()
    }

    def getCartTransactionStatus(userEmail)
    {
        def query = "SELECT {pte.TransactionStatus}\n" +
                "FROM {CybsPaymentTransaction as pt\n" +
                "\tJOIN cart ON {pt:order} = {cart:pk}\n" +
                "\tJOIN customer ON {cart:user} = {customer:PK}\n" +
                "    JOIN CybsPaymentTransactionEntry AS pte \n" +
                "\tON {pte:PaymentTransaction} = {pt:pk}\n" +
                "    }\n" +
                "WHERE {customer.uid} = '${userEmail}'"
        return runSearch(query).first().first()
    }

    private runSearch(query)
    {
        def proc = [
                "./${browser.config.rawConfig.resources}/run_flexible_query.sh",
                browser.config.rawConfig.server,
                query
        ].execute()
        def b = new StringBuffer()
        proc.consumeProcessErrorStream(b)

        def gson = new Gson()
        String taxItemsJson = proc.text
        def result = gson.fromJson(taxItemsJson, HashMap.class)

        return result.resultList
    }
    //endregion

    def checkoutWithShippingInfo()
    {
        at HomePage

        startChecoutAsRegistered()
        shippingForm.country = COUNTRY
        shippingForm.title = TITLE
        shippingForm.firstName = FIRST_NAME
        shippingForm.lastName = LAST_NAME
        shippingForm.addressLine1 = ADDRESS
        shippingForm.city = CITY
        shippingForm.postCode = POST_CODE

        shippingCta.click()

        deliveryMethodCta.click()

        at CheckoutPage
    }

    def populateShippingInfo()
    {
        at CheckoutPage

        shippingForm.country = COUNTRY
        shippingForm.title = TITLE
        shippingForm.firstName = FIRST_NAME
        shippingForm.lastName = LAST_NAME
        shippingForm.addressLine1 = ADDRESS
        shippingForm.city = CITY
        shippingForm.postCode = POST_CODE

        shippingCta.click()

        deliveryMethodCta.click()

        at CheckoutPage
    }

    def guestCheckout

    def startChecoutFofGuest()
    {
        at HomePage

        openProductPage()
        addProductToCart()
        guestCheckoutCta.click()

        proceedAsGuest()

        at CheckoutPage
    }

    def proceedAsGuest()
    {
        at CheckoutLoginPage

        def email = EMAIL
        guestLogin.email << email
        guestLogin.confirmEmail << email
        guestLogin.checkoutAsGuest.click()



        at CheckoutPage
    }

    def startChecoutAsRegistered()
    {
        at HomePage

        registerAUser()
        openProductPage()
        addProductToCart()
        checkoutCta.click()


        at CheckoutPage
    }

    def registerAUser()
    {
        at HomePage

        header.signInRegister.click()
        titleCode = TITLE
        firstName = FIRST_NAME
        lastName = LAST_NAME
        email = EMAIL
        pwd = PASSWORD
        checkPwd = PASSWORD
        registrationForm.register.click()

        at HomePage
    }

    def addProductToCart()
    {
        at ProductDescriptionPage

        addToCartCta.click()
        addToCartDialog.checoutCta.click()

        at CartPage
    }

    def openProductPage()
    {
        at HomePage

        header.search.input = PRODUCT_ID
        header.search.submit.click()
        productList.products.first().picture.click()

        at ProductDescriptionPage
    }

}
