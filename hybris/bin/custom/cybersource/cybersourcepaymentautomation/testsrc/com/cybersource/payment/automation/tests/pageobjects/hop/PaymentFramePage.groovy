package com.cybersource.payment.automation.tests.pageobjects.hop

import geb.Page

/**
 * Created by dsurchis on 14/12/16.
 */
class PaymentFramePage extends Page
{
    static content = {
        visa { $('input#card_type_001') } //TODO: Create universal car picker
        cardType { type -> $(id: type) }
        cardNumber { $('input#card_number') }
        cardCvn(wait: true) { $('input#card_cvn') }
        expirationMonth { $('select#card_expiry_month') }
        expirationYear { $('select#card_expiry_year') }
        nextBtn(to: SaHopPaymentPage) { $('input.right') }
        completeBtn(wait: true) { $('input.complete') }
    }
}
