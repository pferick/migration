package com.cybersource.payment.automation.tests.pageobjects.secure3d

import geb.Page

class SopRequestPage extends Page
{
//    static at = { title == 'Authentication' }
    static content = {
        authFrame(page: AuthorizationPage, wait: true) { $('iframe#authWindow') }
    }
}
