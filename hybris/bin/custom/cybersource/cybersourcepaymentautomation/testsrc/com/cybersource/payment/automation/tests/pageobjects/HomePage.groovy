package com.cybersource.payment.automation.tests.pageobjects

import com.cybersource.payment.automation.tests.pageobjects.modules.HeaderModule
import geb.Page

class HomePage extends Page
{
    static url = ""
    static at = { title.endsWith("Homepage") }
    static content = {
        header { module HeaderModule }
    }
}
