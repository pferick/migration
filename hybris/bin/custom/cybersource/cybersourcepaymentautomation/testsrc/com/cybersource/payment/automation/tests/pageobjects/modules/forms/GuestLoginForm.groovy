package com.cybersource.payment.automation.tests.pageobjects.modules.forms

import com.cybersource.payment.automation.tests.pageobjects.CheckoutPage
import geb.Module

class GuestLoginForm extends Module
{
    static base = { $("form#guestForm") }
    static content = {
        email { $("input", id: "guest.email") }
        confirmEmail { $("input", id: "guest.confirm.email") }
        checkoutAsGuest(to: CheckoutPage) { $("button.guestCheckoutBtn") }
    }
}
