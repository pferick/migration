package com.cybersource.payment.automation.tests.pageobjects.modules.listitems

import com.cybersource.payment.automation.tests.pageobjects.ProductDescriptionPage
import geb.Module

class PlpCell extends Module
{
//    static base = { $("div.product-item") }
    static content = {
        picture(to: ProductDescriptionPage) { $("a.thumb") }
        addToCartCta { $("button", type: "submit") }
    }
}
