package com.cybersource.payment.automation.tests.pageobjects

import com.cybersource.payment.automation.tests.pageobjects.modules.HeaderModule
import com.cybersource.payment.automation.tests.pageobjects.modules.ProductListModule
import geb.Page

/**
 * Created by dsurchis on 14/12/16.
 */
class CategoryPage extends Page
{
    static at = { $("body.pageType-CategoryPage") }
    static content = {
        header { module HeaderModule }
        productList { module ProductListModule }
    }
}
