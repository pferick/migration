package com.cybersource.payment.automation.tests.pageobjects

import com.cybersource.payment.automation.tests.pageobjects.modules.forms.CardForm
import com.cybersource.payment.automation.tests.pageobjects.modules.forms.ShippingAddressForm
import com.cybersource.payment.automation.tests.pageobjects.hop.SaHopPaymentPage
import com.cybersource.payment.automation.tests.pageobjects.secure3d.AuthorizationPage
import com.cybersource.payment.automation.tests.pageobjects.secure3d.SopRequestPage
import geb.Page

class CheckoutPage extends Page
{
    static atCheckWaiting = true
//    static url = "checkout"
    static at = { title.trim().startsWith("Checkout") }
    static content = {
        shippingForm { module ShippingAddressForm }
        shippingCta { $("#addressSubmit") }
        deliveryMethodCta { $("#deliveryMethodSubmit") }
        paymentCta(wait: true) { $("button.submit_silentOrderPostForm") }
        card(wait: true) { module CardForm }
        billingCta { $("button.submit_silentOrderPostForm") }

        placeOrderCtaSop(to: OrderConfirmationPage, toWait: true) { $("#placeOrder", 0) }
        placeOrderCtaHop(to: SaHopPaymentPage, toWait: true) { $("#placeOrder", 0) }
        placeOrderCta { $("#placeOrder", 0) }

        acceptCheckBox { $("#Terms1", 0) }

        authFrame(page: AuthorizationPage, wait: true) { $('iframe#authWindow') }
        sopRequestFrame(page: SopRequestPage, wait: true) { $('iframe#sopRequestIframe') }

        globalError(wait: true) { $('div.global-alerts') }
    }
}
