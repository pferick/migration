package com.cybersource.payment.automation.tests.pageobjects

import com.cybersource.payment.automation.tests.pageobjects.modules.forms.RegisterForm
import geb.Page

class LoginPage extends Page
{
    static url = "login"
    static at = { title.contains("Login") }
    static content = {
        registrationForm(wait: true) { module RegisterForm }
    }
}
