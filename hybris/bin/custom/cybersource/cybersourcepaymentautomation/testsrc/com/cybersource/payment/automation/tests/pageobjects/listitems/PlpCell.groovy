package com.cybersource.payment.automation.tests.pageobjects.listitems

import geb.Module

class PlpCell extends Module
{
//    static base = { $("div.product-item") }
    static content = {
        picture { $("a.thumb") }
        addToCartCta { $("button", type: "submit") }
    }
}
