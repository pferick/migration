package com.cybersource.payment.automation.tests.pageobjects.modules

import com.cybersource.payment.automation.tests.pageobjects.CartPage
import geb.Module

class AddToCartDialog extends Module
{
    static base = { $("#colorbox") }
    static content = {
        checoutCta(to: CartPage, wait: true) { $("a.add-to-cart-button") }
    }
}
