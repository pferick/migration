package com.cybersource.payment.automation.tests.helpers


class Constants
{
    static VISA = '4111111111111111'
    static VISA_3D_VALID = '4000000000000002'
    static VISA_3D_INVALID = '4000000000000010'
    static MASTERCARD = '5555555555554444'
    static AMEX = '378282246310005'
    static AMEX_ALT = '371449111020228'
    static DINERS = '38000000000006'
    static MAESTRO = '50339619890917'
    static DISCOVER = '6011111111111117'
    static JCB = '3566111111111113'
}
