package com.cybersource.payment.automation.tests.gebtests

import com.cybersource.payment.automation.tests.helpers.HybrisGebSpec
import com.cybersource.payment.automation.tests.pageobjects.CheckoutPage
import com.cybersource.payment.automation.tests.pageobjects.HomePage
import com.cybersource.payment.automation.tests.pageobjects.OrderConfirmationPage

import static com.cybersource.payment.automation.tests.helpers.Constants.*

class CreditCardHopSpec extends HybrisGebSpec
{
    def setupSpec()
    {
        setPciStrategyHop()
    }

    def "should create order for HOP (registered user)"()
    {
        given: "Checkout is started and shipping address and method is selected"
        to HomePage
        startChecoutAsRegistered()
        populateShippingInfo()

        when: "I submit the Order"
        paymentCta.click()
        acceptCheckBox.click()
        placeOrderCtaHop.click()

        and: "Populate card data in iframe"
        withFrame(paymentFrame) {
            cardType(type).click()
            cardNumber = number
            (cvv != null) ? cardCvn = cvv : null //JCB does not have CVV field dipslayed
            expirationMonth = '01'
            expirationYear = '2020'
            nextBtn.click()
        }
        withFrame(reviewFrame) {
            completeBtn.click()
        }

        then: "Order is created"
        at OrderConfirmationPage
        def order = orderNumber

        and: "Transactions are created"
        getTransactionPaymentProvider(order) == 'CREDIT_CARD'
        getTransactionStatus(order, 'AUTHORIZATION') == 'ACCEPT'
        getTransactionStatus(order, 'CAPTURE') == 'ACCEPT'

        and: "Order is completed"
        waitFor { getOrderStatus(order) == 'COMPLETED' }

        where: "Following cards are used"
        type            | number     | cvv
        'card_type_001' | VISA       | '123'
        'card_type_002' | MASTERCARD | '123'
        'card_type_003' | AMEX       | '1234'
        'card_type_005' | DINERS     | '123'
        'card_type_024' | MAESTRO    | '123'


    }

    def "should create order for HOP with 3D secure"()
    {
        given: "Checkout is started and shipping address and method is selected"
        to HomePage
        startChecoutAsRegistered()
        populateShippingInfo()

        when: "I submit the Order"
        paymentCta.click()
        acceptCheckBox.click()
        placeOrderCtaHop.click()

        and: "Populate card data in iframe"
        withFrame(paymentFrame) {
            cardType('card_type_001').click()
            cardNumber = VISA_3D_VALID
            cardCvn = '123'
            expirationMonth = '01'
            expirationYear = '2020'
            nextBtn.click()
        }
        withFrame(reviewFrame) {
            completeBtn.click()
        }
        and: "Populate 3D secure Password"
        withFrame(reviewFrame) {
            withFrame(authFrame) {
                password << '123'
                submitBtn.click()
            }
        }

        then: "Order is created"
        at OrderConfirmationPage
        def order = orderNumber

        and: "Transactions are created"
        getTransactionPaymentProvider(order) == 'CREDIT_CARD'
        getTransactionStatus(order, 'AUTHORIZATION') == 'ACCEPT'
        getTransactionStatus(order, 'CAPTURE') == 'ACCEPT'

        and: "Order is completed"
        waitFor { getOrderStatus(order) == 'COMPLETED' }
    }

    def "should not create order for HOP with wrong 3D secure"()
    {
        given: "Checkout is started and shipping address and method is selected"
        to HomePage
        startChecoutAsRegistered()
        populateShippingInfo()

        when: "I submit the Order"
        paymentCta.click()
        acceptCheckBox.click()
        placeOrderCtaHop.click()

        and: "Populate card data in iframe (the casrd set up to fail 3d secure)"
        withFrame(paymentFrame) {
            cardType('card_type_001').click()
            cardNumber = VISA_3D_INVALID
            cardCvn = '123'
            expirationMonth = '01'
            expirationYear = '2020'
            nextBtn.click()
        }
        withFrame(reviewFrame) {
            completeBtn.click()
        }
        and: "Populate 3d Secure"
        withFrame(reviewFrame) {
            withFrame(authFrame) {
                password << '123'
                submitBtn.click()
            }
        }

        then: "Order is Not created"
        at CheckoutPage
        globalError.displayed

        and: "Transaction is Declined"
        getCartTransactionStatus(EMAIL) == "DECLINE"
    }

    def "should create order for HOP (guest)"()
    {
        given: "Checkout for guest user is started"
        to HomePage
        startChecoutFofGuest()
        populateShippingInfo()

        when: "I place the order"
        paymentCta.click()
        acceptCheckBox.click()
        placeOrderCtaHop.click()

        and: "Populate Card Data"
        withFrame(paymentFrame) {
            cardType(type).click()
            cardNumber = number
            (cvv != null) ? cardCvn = cvv : null //JCB does not have CVV field dipslayed
            expirationMonth = '01'
            expirationYear = '2020'
            nextBtn.click()
        }
        withFrame(reviewFrame) {
            completeBtn.click()
        }

        then: "Order is created"
        at OrderConfirmationPage
        def order = orderNumber

        and: "Transactions are created"
        getTransactionPaymentProvider(order) == 'CREDIT_CARD'
        getTransactionStatus(order, 'AUTHORIZATION') == 'ACCEPT'
        getTransactionStatus(order, 'CAPTURE') == 'ACCEPT'

        and: "Order is completed"
        waitFor { getOrderStatus(order) == 'COMPLETED' }

        where:
        type            | number     | cvv
        'card_type_001' | VISA       | '123'
        'card_type_002' | MASTERCARD | '123'
        'card_type_003' | AMEX       | '1234'
        'card_type_005' | DINERS     | '123'
        'card_type_024' | MAESTRO    | '123'
    }


}
