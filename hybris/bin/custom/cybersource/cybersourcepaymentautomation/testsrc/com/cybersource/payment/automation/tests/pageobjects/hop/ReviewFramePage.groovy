package com.cybersource.payment.automation.tests.pageobjects.hop

import com.cybersource.payment.automation.tests.pageobjects.secure3d.AuthorizationPage
import geb.Page

class ReviewFramePage extends Page
{
    static content = {
        completeBtn(wait: true) { $('input.complete') }
//        complete3dBtn(wait: true, to: Secure3dPage, toWait: true) { $('input.complete')}
        authFrame(page: AuthorizationPage) { $('iframe#authWindow') }
    }
}
