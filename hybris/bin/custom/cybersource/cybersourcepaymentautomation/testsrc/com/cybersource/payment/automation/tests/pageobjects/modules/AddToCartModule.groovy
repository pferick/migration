package com.cybersource.payment.automation.tests.pageobjects.modules

import geb.Module

class AddToCartModule extends Module
{
    static base = { $("div.addtocart-component") }
    static content = {
        qtyInput { $("input.js-qty-selector-input") }
        cta { $("#addToCartButton") }
    }
}
