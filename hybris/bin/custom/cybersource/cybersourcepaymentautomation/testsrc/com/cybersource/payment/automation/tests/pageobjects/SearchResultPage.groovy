package com.cybersource.payment.automation.tests.pageobjects

import com.cybersource.payment.automation.tests.pageobjects.modules.HeaderModule
import com.cybersource.payment.automation.tests.pageobjects.modules.ProductListModule
import geb.Page

/**
 * Created by dsurchis on 14/12/16.
 */
class SearchResultPage extends Page
{
    static at = { title.startsWith("Search") }
    static content = {
        header { module HeaderModule }
        productList { module ProductListModule }
    }
}
