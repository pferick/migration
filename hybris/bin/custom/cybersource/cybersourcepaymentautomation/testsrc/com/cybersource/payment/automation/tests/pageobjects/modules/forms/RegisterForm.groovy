package com.cybersource.payment.automation.tests.pageobjects.modules.forms

import com.cybersource.payment.automation.tests.pageobjects.HomePage
import geb.Module

/**
 * Created by dsurchis on 14/12/16.
 */
class RegisterForm extends Module
{
    static base = { $("form#registerForm") }
    static content = {
        register(to: HomePage) { $("button") }
    }
}
