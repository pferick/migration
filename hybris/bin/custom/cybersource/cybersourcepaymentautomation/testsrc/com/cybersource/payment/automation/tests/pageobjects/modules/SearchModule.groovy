package com.cybersource.payment.automation.tests.pageobjects.modules

import com.cybersource.payment.automation.tests.pageobjects.SearchResultPage
import geb.Module

/**
 * Created by dsurchis on 14/12/16.
 */
class SearchModule extends Module
{
    static base = { $("div.input-group") }

    static content = {
        input { $("input") }
        submit(to: SearchResultPage) { $("button") }
    }
}
