package com.cybersource.payment.automation.tests.pageobjects.modules

import com.cybersource.payment.automation.tests.pageobjects.modules.listitems.PlpCell
import geb.Module

/**
 * Created by dsurchis on 14/12/16.
 */
class ProductListModule extends Module
{
    static base = { $("*.product__listing") }
    static content = {
        products { $("div.product-item").moduleList(PlpCell) }
    }
}
