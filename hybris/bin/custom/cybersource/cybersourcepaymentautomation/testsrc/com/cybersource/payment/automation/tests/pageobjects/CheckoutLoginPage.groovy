package com.cybersource.payment.automation.tests.pageobjects

import com.cybersource.payment.automation.tests.pageobjects.modules.forms.GuestLoginForm
import geb.Page

class CheckoutLoginPage extends Page
{
    static at = { $("body.page-checkout-login") }
    static content = {
        guestLogin { module GuestLoginForm }
    }
}
