package com.cybersource.payment.automation.tests.gebtests

import com.cybersource.payment.automation.tests.helpers.HybrisGebSpec
import com.cybersource.payment.automation.tests.pageobjects.CheckoutPage
import com.cybersource.payment.automation.tests.pageobjects.HomePage
import com.cybersource.payment.automation.tests.pageobjects.OrderConfirmationPage

import static com.cybersource.payment.automation.tests.helpers.Constants.*

class CreditCardSopSpec extends HybrisGebSpec
{
    def setupSpec()
    {
        setPciStrategySop()
    }

    def "should create order for SOP (registered user)"()
    {
        given: "Checkout is started and shipping address and method is selected"
        to HomePage
        startChecoutAsRegistered()
        populateShippingInfo()

        when: "I start Payment"
        paymentCta.click()

        and: "Populate the card data"
        card.type = type
        card.number = number
        card.expMonth = '01'
        card.expYear = '2018'
        card.cvv = cvv

        and: "Place the order"
        acceptCheckBox.click()
        placeOrderCtaSop.click()

        then: "Order is created"
        at OrderConfirmationPage
        def order = orderNumber

        and: "Transactions are created"
        getTransactionPaymentProvider(order) == 'CREDIT_CARD'
        getTransactionStatus(order, 'AUTHORIZATION') == 'ACCEPT'
        getTransactionStatus(order, 'CAPTURE') == 'ACCEPT'

        and: "Order is completed"
        waitFor {getOrderStatus(order) == 'COMPLETED'}

        where: "Following cards are used"
        type  | number     | cvv
        '001' | VISA       | '123'
        '002' | MASTERCARD | '123'
        '003' | AMEX       | '1234'
        '005' | DINERS     | '123'
        '024' | MAESTRO    | '123'
    }

    def "should create order for SOP with 3d secure"()
    {
        given: "Checkout is started and shipping address and method is selected"
        to HomePage
        startChecoutAsRegistered()
        populateShippingInfo()

        when: "I start Payment"
        paymentCta.click()

        and: "Populate the card data"
        card.type = '001'
        card.number = VISA_3D_VALID
        card.expMonth = '1'
        card.expYear = '2020'
        card.cvv = '123'

        and: "Place the order"
        acceptCheckBox.click()
        placeOrderCta.click()
        and:
        withFrame(sopRequestFrame) {
            withFrame(authFrame) {

                waitFor { password << '1234' }
                submitBtn.click()
            }
        }

        then: "Order is created"
        at OrderConfirmationPage
        def order = orderNumber


        and: "Transactions are created"
        getTransactionPaymentProvider(order) == 'CREDIT_CARD'
        getTransactionStatus(order, 'AUTHORIZATION') == 'ACCEPT'
        getTransactionStatus(order, 'CAPTURE') == 'ACCEPT'

        and: "Order is completed"
        waitFor {getOrderStatus(order) == 'COMPLETED'}
    }

    def "should not create order for SOP with wrong 3d secure"()
    {
        given: "Checkout is started and shipping address and method is selected"
        to HomePage
        startChecoutAsRegistered()
        populateShippingInfo()

        when: "I start Payment"
        paymentCta.click()

        and: "Populate the card data"
        card.type = '001'
        card.number = VISA_3D_INVALID
        card.expMonth = '1'
        card.expYear = '2020'
        card.cvv = '123'

        and: "Place the order"
        acceptCheckBox.click()
        placeOrderCta.click()
        and:
        withFrame(sopRequestFrame) {
            withFrame(authFrame) {

                waitFor { password << '1234' }
                submitBtn.click()
            }
        }

        then: "Order is not created"
        at CheckoutPage
        globalError.displayed

        and: "Transaction is Declined"
        getCartTransactionStatus(EMAIL) == "DECLINE"
    }

    def "should create order for SOP (guest user)"()
    {
        given: "Checkout is started and shipping address and method is selected"
        to HomePage
        startChecoutFofGuest()
        populateShippingInfo()

        when: "I start Payment"
        paymentCta.click()

        and: "Populate the card data"
        card.type = type
        card.number = number
        card.expMonth = '1'
        card.expYear = '2018'
        card.cvv = '123'

        and: "Place the order"
        acceptCheckBox.click()
        placeOrderCtaSop.click()

        then: "Order is created"
        at OrderConfirmationPage
        def order = orderNumber


        and: "Transactions are created"
        getTransactionPaymentProvider(order) == 'CREDIT_CARD'
        getTransactionStatus(order, 'AUTHORIZATION') == 'ACCEPT'
        getTransactionStatus(order, 'CAPTURE') == 'ACCEPT'

        and: "Order is completed"
        waitFor {getOrderStatus(order) == 'COMPLETED'}

        where: "Following cards are used"
        type  | number
        '001' | VISA
        '002' | MASTERCARD
        '003' | AMEX
        '005' | DINERS
        '024' | MAESTRO
    }

    def "should display error when card fields not populated"()
    {
        given: "Checkout is started and shipping address and method is selected"
        to HomePage
        checkoutWithShippingInfo()

        when: "I start Payment"
        paymentCta.click()

        and: "Populate the card data with missing fields"
        card.type = type
        card.number = number
        card.expMonth = month
        card.expYear = year
        card.cvv = '123'

        and: "Try to place the order"
        acceptCheckBox.click()
        placeOrderCta.click()

        then: "Relevant Error message is displayed"
        card.errorMessage(error).displayed

        where: "Following data is populated"
        type | number | month | year | error
        ''   | VISA   | '1'   | 2020 | 'card_cardType_errors'
    }
}
