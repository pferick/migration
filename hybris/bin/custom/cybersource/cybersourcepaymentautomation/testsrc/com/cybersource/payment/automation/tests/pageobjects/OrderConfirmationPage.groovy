package com.cybersource.payment.automation.tests.pageobjects

import geb.Page

/**
 * Created by dsurchis on 14/12/16.
 */
class OrderConfirmationPage extends Page
{
    static atCheckWaiting = true
    static at = { title.trim().startsWith("Order Confirmation") }
    static content = {
        orderNumber(wait:true) { $("div.checkout-success").find("b").first().text() }
    }
}
