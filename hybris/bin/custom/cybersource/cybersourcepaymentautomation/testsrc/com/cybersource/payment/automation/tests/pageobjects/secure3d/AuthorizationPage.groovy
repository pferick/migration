package com.cybersource.payment.automation.tests.pageobjects.secure3d

import geb.Page

class AuthorizationPage extends Page
{
//    static at = { title == 'Authentication' }
    static at = { $('img', src: 'screens/images/logo_bank.gif') }
    static content = {
        password(wait: true) { $('input', type: "password") }
        submitBtn(wait: true) { $('input', type: 'submit') }
    }
}
