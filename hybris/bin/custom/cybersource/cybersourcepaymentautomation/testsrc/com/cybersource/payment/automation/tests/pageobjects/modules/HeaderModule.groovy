package com.cybersource.payment.automation.tests.pageobjects.modules

import com.cybersource.payment.automation.tests.pageobjects.LoginPage
import geb.Module

/**
 * Created by dsurchis on 14/12/16.
 */
class HeaderModule extends Module
{

    static content = {
        signInRegister(to: LoginPage) { $("ul.nav__links--account") }
        search { module SearchModule }
    }

}
