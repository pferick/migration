/**
 *
 */
package com.soriana.core.dynamicAttribute.handler;

import de.hybris.platform.acceleratorservices.urlresolver.SiteBaseUrlResolutionService;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.model.attribute.DynamicAttributeHandler;

import org.springframework.beans.factory.annotation.Required;



/**
 * @author akshay.chopra
 *
 */
public class OrderDetailsUrl implements DynamicAttributeHandler<String, OrderModel>
{
	private SiteBaseUrlResolutionService siteBaseUrlResolutionService;

	@Override
	public String get(final OrderModel order)
	{
		final StringBuffer orderDetailsUrl = new StringBuffer();
		final String siteUrl = getSiteBaseUrlResolutionService().getWebsiteUrlForSite(order.getSite(), false, "");
		return orderDetailsUrl.append(siteUrl).append("/guest/order/").append(order.getGuid()).toString();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.hybris.platform.servicelayer.model.attribute.DynamicAttributeHandler#set(de.hybris.platform.servicelayer.model
	 * .AbstractItemModel, java.lang.Object)
	 */
	@Override
	public void set(final OrderModel order, final String value)
	{
		//order.setOrderDetailsUrl(value);

	}


	protected SiteBaseUrlResolutionService getSiteBaseUrlResolutionService()
	{
		return siteBaseUrlResolutionService;
	}

	@Required
	public void setSiteBaseUrlResolutionService(final SiteBaseUrlResolutionService siteBaseUrlResolutionService)
	{
		this.siteBaseUrlResolutionService = siteBaseUrlResolutionService;
	}



}
