/**
 *
 */
package com.soriana.core.dataimport.batch.task;

import de.hybris.platform.acceleratorservices.dataimport.batch.BatchHeader;
import de.hybris.platform.acceleratorservices.dataimport.batch.converter.ImpexConverter;
import de.hybris.platform.acceleratorservices.dataimport.batch.task.ImpexTransformerTask;
import de.hybris.platform.acceleratorservices.email.EmailService;
import de.hybris.platform.acceleratorservices.model.email.EmailAddressModel;
import de.hybris.platform.acceleratorservices.model.email.EmailMessageModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.util.CSVConstants;
import de.hybris.platform.util.CSVReader;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;


/**
 * @author jair.viezca.esparza
 *
 */
public class SorianaImpexTransformerTask extends ImpexTransformerTask
{



	private static final Logger LOG = Logger.getLogger("SorianaImpexTransformerTask.class");

	@Resource
	private EmailService emailService;

	@Resource(name = "configurationService")
	private ConfigurationService configurationService;

	// Email Variables

	private static final String PARAM_SUBJECT = "hotfolder.email.subject";

	private static final String PARAM_USER = "hotfolder.email.sender.email";
	private static final String PARAM_SENDER_NAME = "hotfolder.email.sender.name";

	private static final String PARAM_RECEIVER_EMAIL = "hotfolder.email.to.email";
	private static final String PARAM_RECEIVER_NAME = "hotfolder.email.to.name";


	public SorianaImpexTransformerTask()
	{

	}


	/**
	 * Converts the CSV file to an impex file using the given converter
	 *
	 * @param header
	 * @param file
	 * @param impexFile
	 * @param converter
	 * @return true, if the file contains at least one converted row
	 * @throws UnsupportedEncodingException
	 * @throws FileNotFoundException
	 */
	@Override
	protected boolean convertFile(final BatchHeader header, final File file, final File impexFile, final ImpexConverter converter)
			throws UnsupportedEncodingException, FileNotFoundException
	{
		boolean result = false;

		// In case of errors
		boolean hasErrors = false;
		final StringBuilder errors = new StringBuilder();
		errors.append("Errores presentes en el proceso de carga de archivo en Hot Folder (Salto de linea) ");
		errors.append("Ruta del archivo -> ");
		errors.append(file.getPath());
		errors.append("(Salto de linea)(2) ");

		CSVReader csvReader = null;
		PrintWriter writer = null;
		PrintWriter errorWriter = null;

		try
		{
			csvReader = createCsvReader(file);
			writer = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(impexFile),
					CSVConstants.HYBRIS_ENCODING)));
			writer.println(getReplacedHeader(header, converter));
			while (csvReader.readNextLine())
			{
				final Map<Integer, String> row = csvReader.getLine();
				if (converter.filter(row))
				{
					try
					{
						writer.println(converter.convert(row, header.getSequenceId()));
						result = true;
					}
					catch (final IllegalArgumentException exc)
					{
						hasErrors = true;

						errorWriter = writeErrorLine(file, csvReader, errorWriter, exc);

						buildErrorMessageEmailBody(errors, csvReader, exc);


					}
				}
			}
		}
		finally
		{
			if (hasErrors == true)
			{
				sendNotificationEmail(file.getName(), errors.toString());
			}
			IOUtils.closeQuietly(writer);
			IOUtils.closeQuietly(errorWriter);
			closeQuietly(csvReader);
		}
		return result;
	}




	/**
	 * Builds error message for email body when something goes wrong during the conversion file during hot folder process
	 *
	 * @param errors
	 * @param csvReader
	 * @param exc
	 */
	private void buildErrorMessageEmailBody(final StringBuilder errors, final CSVReader csvReader,
			final IllegalArgumentException exc)
	{

		errors.append("Error en linea ");
		errors.append(csvReader.getCurrentLineNumber());
		errors.append(" ->  ");
		errors.append(csvReader.getSourceLine());
		errors.append("(Salto de linea) ");
		errors.append("Motivo -> ");
		errors.append(exc.getMessage());
		errors.append("(Salto de linea)(2) ");
	}


	/**
	 * Sends an email to notificate when there's an error during the conversion file in hot folder process
	 *
	 * @param fileName
	 * @param errors
	 */

	private void sendNotificationEmail(final String fileName, final String errors)
	{

		final String senderEmail = configurationService.getConfiguration().getString(PARAM_USER);
		final String senderName = configurationService.getConfiguration().getString(PARAM_SENDER_NAME);

		final String receiverEmail = configurationService.getConfiguration().getString(PARAM_RECEIVER_EMAIL);
		final String receiverName = configurationService.getConfiguration().getString(PARAM_RECEIVER_NAME);

		final String subject = configurationService.getConfiguration().getString(PARAM_SUBJECT) + "-----" + fileName;

		final List<EmailAddressModel> toAddresses = new ArrayList<EmailAddressModel>();
		final EmailAddressModel toAddress = emailService.getOrCreateEmailAddressForEmail(receiverEmail, receiverName);
		toAddresses.add(toAddress);

		final EmailAddressModel fromAddress = emailService.getOrCreateEmailAddressForEmail(senderEmail, senderName);

		try
		{
			final EmailMessageModel emailMessage = emailService.createEmailMessage(toAddresses, null, null, fromAddress,
					receiverEmail, subject, errors, null);
			emailService.send(emailMessage);
		}
		catch (final Exception e)
		{

			LOG.error("Email ERROR", e);
		}
	}


	/**
	 * @return the emailService
	 */
	public EmailService getEmailService()
	{
		return emailService;
	}


	/**
	 * @param emailService
	 *           the emailService to set
	 */
	public void setEmailService(final EmailService emailService)
	{
		this.emailService = emailService;
	}

	/**
	 * @return the configurationService
	 */
	public ConfigurationService getConfigurationService()
	{
		return configurationService;
	}


	/**
	 * @param configurationService
	 *           the configurationService to set
	 */
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}

}
