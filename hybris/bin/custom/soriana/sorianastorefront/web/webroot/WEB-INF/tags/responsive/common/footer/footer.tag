<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>


<footer>
	<cms:pageSlot position="Footer" var="feature">
        <cms:component component="${feature}"/>
    </cms:pageSlot>
	
    <cms:pageSlot position="FooterSection1" var="feature1">
        <cms:component component="${feature1}"/>
    </cms:pageSlot>
    
        <cms:pageSlot position="FooterSection2" var="feature2">
        <cms:component component="${feature2}"/>
    </cms:pageSlot>
    
        <cms:pageSlot position="FooterSection3" var="feature3">
        <cms:component component="${feature3}"/>
    </cms:pageSlot>
    
        <cms:pageSlot position="FooterSection4" var="feature4">
        <cms:component component="${feature4}"/>
    </cms:pageSlot>
    
        <cms:pageSlot position="FooterSection5" var="feature5">
        <cms:component component="${feature5}"/>
    </cms:pageSlot>
</footer>



