/**
 *
 */
package com.soriana.paypal.controller;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


/**
 * @author gino.o.nunez.picos
 *
 *         Controller to handle payment request and response
 */
@Controller
@RequestMapping(value = "checkout/multi/PaypalInitReqController")
public class PaypalInitReqController
{
	private static final Logger LOGGER = Logger.getLogger(PaypalInitReqController.class);

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.soriana.storefront.controllers.pages.checkout.steps.CheckoutStepController#enterStep(org.springframework.ui.
	 * Model, org.springframework.web.servlet.mvc.support.RedirectAttributes)
	 *
	 * Request Mapping to accept request after click of checkout with paypal button.
	 */
	@RequestMapping(method = RequestMethod.GET)

	public String enterStep()
	{

		final String token = "";

		LOGGER.info("Paypal controller");

		return token;
	}

}
