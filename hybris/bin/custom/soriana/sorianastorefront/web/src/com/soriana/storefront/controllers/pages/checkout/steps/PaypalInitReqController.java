/**
 *
 */
package com.soriana.storefront.controllers.pages.checkout.steps;

import de.hybris.platform.acceleratorfacades.order.AcceleratorCheckoutFacade;
import de.hybris.platform.commercefacades.order.data.CartData;

import java.util.Locale;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.paypal.hybris.data.SetExpressCheckoutRequestData;
import com.paypal.hybris.data.SetExpressCheckoutResultData;
import com.paypal.hybris.payment.service.impl.PayPalPaymentServiceImpl;


/**
 * @author gino.o.nunez.picos
 *
 *         Controller to handle payment request and response
 */
@Controller
@RequestMapping(value = "/checkout/multi/PaypalInitReqController")
public class PaypalInitReqController
{
	private static final Logger LOGGER = Logger.getLogger(PaypalInitReqController.class);
	@Resource(name = "payPalPaymentService")
	private PayPalPaymentServiceImpl paypalPaymentService;

	@Resource(name = "acceleratorCheckoutFacade")
	private AcceleratorCheckoutFacade checkoutFacade;

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.soriana.storefront.controllers.pages.checkout.steps.CheckoutStepController#enterStep(org.springframework.ui.
	 * Model, org.springframework.web.servlet.mvc.support.RedirectAttributes)
	 *
	 * Request Mapping to accept request after click of checkout with paypal button.
	 */
	@RequestMapping(value = "/token")
	public SetExpressCheckoutResultData enterStep()
	{
		LOGGER.debug("Paypal controller");
		final CartData cartData = getCheckoutFacade().getCheckoutCart();
		final SetExpressCheckoutRequestData paypalData = new SetExpressCheckoutRequestData();
		paypalData.setAddressOverride(true);
		paypalData.setCancelUrl("https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token=");
		paypalData.setPaymentAction("Authorization");
		paypalData.setReturnUrl("https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token=");
		paypalData.setSessionCart(cartData);
		paypalData.setUid("91fa3b102b");
		paypalData.setVersion("204.0");
		final Locale locale = new Locale("es", "MX");
		paypalData.setLocale(locale);


		final SetExpressCheckoutResultData resultData = getPaypalPaymentService().setExpressCheckout(paypalData);
		//final String token = "";

		LOGGER.debug("Paypal controller");
		System.out.print("Paypal controller token:" + resultData.getToken());
		return resultData;
	}

	/**
	 * @return the paypalPaymentService
	 */
	public PayPalPaymentServiceImpl getPaypalPaymentService()
	{
		return paypalPaymentService;
	}

	/**
	 * @param paypalPaymentService
	 *           the paypalPaymentService to set
	 */
	public void setPaypalPaymentService(final PayPalPaymentServiceImpl paypalPaymentService)
	{
		this.paypalPaymentService = paypalPaymentService;
	}

	/**
	 * @return the checkoutFacade
	 */
	public AcceleratorCheckoutFacade getCheckoutFacade()
	{
		return checkoutFacade;
	}

	/**
	 * @param checkoutFacade
	 *           the checkoutFacade to set
	 */
	public void setCheckoutFacade(final AcceleratorCheckoutFacade checkoutFacade)
	{
		this.checkoutFacade = checkoutFacade;
	}

}
