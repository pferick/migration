# -----------------------------------------------------------------------
# [y] hybris Platform
#
# Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
#
# This software is the confidential and proprietary information of SAP
# ("Confidential Information"). You shall not disclose such Confidential
# Information and shall use it only in accordance with the terms of the
# license agreement you entered into with SAP.
# -----------------------------------------------------------------------
#
# Import the Solr configuration for the Soriana store
#
$productCatalog=sorianaProductCatalog
$catalogVersions=catalogVersions(catalog(id),version);
$serverConfigName=sorianaSolrServerConfig
$indexConfigName=SolrIndexConfig
$searchConfigName=sorianaPageSize
$facetSearchConfigName=sorianaIndex
$facetSearchConfigDescription=soriana Solr Index
$searchIndexNamePrefix=soriana
$solrIndexedType=sorianaProductType
$indexBaseSite=soriana
$indexLanguages=en,es
$indexCurrencies=MXN


#
# Setup the Solr server, indexer, and search configs
#

# Import config properties into impex macros
UPDATE GenericItem[processor=de.hybris.platform.commerceservices.impex.impl.ConfigPropertyImportProcessor];pk[unique=true]

# Create the solr server configuration
INSERT_UPDATE SolrServerConfig;name[unique=true];mode(code)
;$serverConfigName;$config-solr.server.mode

INSERT_UPDATE SolrEndpointUrl;solrServerConfig(name)[unique=true];url[unique=true];master[unique=true,default=false]
;$serverConfigName;http://localhost:8983/solr;true

# Create the solr indexer configuration
INSERT_UPDATE SolrIndexConfig;name[unique=true];batchSize;numberOfThreads;indexMode(code);
;$indexConfigName;100;1;TWO_PHASE;

# Create the faceted search configuration
INSERT_UPDATE SolrSearchConfig;description[unique=true];pageSize;legacyMode
;$searchConfigName;20;true

#
# Setup the indexed types, their properties, and the update queries
#

# Declare the indexed type Product
INSERT_UPDATE SolrIndexedType;identifier[unique=true];type(code);variant;sorts(&sortRefID)
;$solrIndexedType;SorianaProduct;false;sortRef1,sortRef2,sortRef3,sortRef4,sortRef5,sortRef6

INSERT_UPDATE SolrFacetSearchConfig;name[unique=true];description;indexNamePrefix;languages(isocode);currencies(isocode);solrServerConfig(name);solrSearchConfig(description);solrIndexConfig(name);solrIndexedTypes(identifier);enabledLanguageFallbackMechanism;$catalogVersions
;$facetSearchConfigName;$facetSearchConfigDescription;$searchIndexNamePrefix;$indexLanguages;$indexCurrencies;$serverConfigName;$searchConfigName;$indexConfigName;$solrIndexedType;true;$productCatalog:Online,$productCatalog:Staged

UPDATE BaseSite;uid[unique=true];solrFacetSearchConfiguration(name)
;$indexBaseSite;$facetSearchConfigName

# Define price range set
INSERT_UPDATE SolrValueRangeSet;name[unique=true];qualifier;type;solrValueRanges(&rangeValueRefID)
;sorianaPriceRangeMXN;MXN;double;rangeRefMXN1,rangeRefMXN2,rangeRefMXN3,rangeRefMXN4,rangeRefMXN5,rangeRefMXN6,rangeRefMXN7,rangeRefMXN8,rangeRefMXN9,rangeRefMXN10

# Define price ranges
INSERT_UPDATE SolrValueRange;&rangeValueRefID;solrValueRangeSet(name)[unique=true];name[unique=true];from;to
;rangeRefMXN1;sorianaPriceRangeMXN;      $0-$500;   0;  499.99
;rangeRefMXN2;sorianaPriceRangeMXN;    $500-$1,000;  500; 999.99
;rangeRefMXN3;sorianaPriceRangeMXN;   $1,000-$2,500; 1000; 2499.99
;rangeRefMXN4;sorianaPriceRangeMXN;   $2,500-$5,000; 2500; 4999.99
;rangeRefMXN5;sorianaPriceRangeMXN;	  $5,000-$7,500;5000; 7499.99
;rangeRefMXN6;sorianaPriceRangeMXN;	  $7,500-$15,000;7500; 14999.99
;rangeRefMXN7;sorianaPriceRangeMXN;	  $15,000-$30,000;15000; 29999.99
;rangeRefMXN8;sorianaPriceRangeMXN;	  $30,000-$50,000;30000; 49999.99
;rangeRefMXN9;sorianaPriceRangeMXN;	  $50,000-$100,000;50000; 99999.99
;rangeRefMXN10;sorianaPriceRangeMXN;  Más de $100,000;100000; 999999.99


# Non-facet properties
INSERT_UPDATE SolrIndexedProperty;solrIndexedType(identifier)[unique=true];name[unique=true];type(code);sortableType(code);currency[default=false];localized[default=false];multiValue[default=false];useForSpellchecking[default=false];useForAutocomplete[default=false];fieldValueProvider;valueProviderParameter;ftsPhraseQuery[default=false];ftsPhraseQueryBoost;ftsQuery[default=false];ftsQueryBoost;ftsFuzzyQuery[default=false];ftsFuzzyQueryBoost;ftsWildcardQuery[default=false];ftsWildcardQueryType(code)[default=POSTFIX];ftsWildcardQueryBoost;ftsWildcardQueryMinTermLength
;$solrIndexedType; itemtype               ;string ;            ;    ;    ;    ;    ;    ;                                       ;              ;    ;   ;    ;   ;    ;  ;    ;   ;
;$solrIndexedType; code                   ;string ;            ;    ;    ;    ;true;true;springELValueProvider                  ;code          ;    ;   ;true;90 ;    ;  ;true;POSTFIX;45;3
;$solrIndexedType; name                   ;text   ;sortabletext;    ;true;    ;true;true;springELValueProvider                  ;getName(#lang);true;100;true;50 ;true;25;    ;   ;  ;
;$solrIndexedType; description            ;text   ;            ;    ;true;    ;    ;    ;                                       ;              ;    ;   ;    ;   ;    ;  ;    ;   ;  ;
;$solrIndexedType; summary                ;text   ;            ;    ;true;    ;    ;    ;                                       ;              ;    ;   ;    ;   ;    ;  ;    ;   ;  ;
;$solrIndexedType; manufacturerName       ;text   ;            ;    ;    ;    ;true;true;                                       ;              ;true;80 ;true;40 ;true;20;    ;   ;  ;
;$solrIndexedType; manufacturerAID        ;string ;            ;    ;    ;    ;    ;    ;                                       ;              ;    ;   ;    ;   ;    ;  ;    ;   ;  ;
;$solrIndexedType; ean                    ;string ;            ;    ;    ;    ;true;true;                                       ;              ;    ;   ;true;100;    ;  ;true;POSTFIX;50;3
;$solrIndexedType; priceValue             ;double ;            ;true;    ;    ;    ;    ;productPriceValueProvider              ;              ;    ;   ;    ;   ;    ;  ;    ;   ;  ;
;$solrIndexedType; keywords               ;text   ;            ;    ;true;    ;true;true;productKeywordsValueProvider           ;              ;true;40 ;true;20 ;true;10;    ;   ;  ;
;$solrIndexedType; reviewAvgRating        ;double ;            ;    ;true;    ;    ;    ;productReviewAverageRatingValueProvider;              ;    ;   ;    ;   ;    ;  ;    ;   ;  ;
;$solrIndexedType; img-515Wx515H          ;string ;            ;    ;    ;    ;    ;    ;image515ValueProvider                  ;              ;    ;   ;    ;   ;    ;  ;    ;   ;  ;
;$solrIndexedType; img-300Wx300H          ;string ;            ;    ;    ;    ;    ;    ;image300ValueProvider                  ;              ;    ;   ;    ;   ;    ;  ;    ;   ;  ;
;$solrIndexedType; img-96Wx96H            ;string ;            ;    ;    ;    ;    ;    ;image96ValueProvider                   ;              ;    ;   ;    ;   ;    ;  ;    ;   ;  ;
;$solrIndexedType; img-65Wx65H            ;string ;            ;    ;    ;    ;    ;    ;image65ValueProvider                   ;              ;    ;   ;    ;   ;    ;  ;    ;   ;  ;
;$solrIndexedType; img-30Wx30H            ;string ;            ;    ;    ;    ;    ;    ;image30ValueProvider                   ;              ;    ;   ;    ;   ;    ;  ;    ;   ;  ;
;$solrIndexedType; url                    ;string ;            ;    ;true;    ;    ;    ;productUrlValueProvider                ;              ;    ;   ;    ;   ;    ;  ;    ;   ;  ;
;$solrIndexedType; primaryPromotionCode   ;string ;            ;    ;    ;    ;    ;    ;promotionCodeValueProvider             ;              ;    ;   ;    ;   ;    ;  ;    ;   ;  ;
;$solrIndexedType; primaryPromotionBanner ;string ;            ;    ;    ;    ;    ;    ;promotionImageValueProvider            ;              ;    ;   ;    ;   ;    ;  ;    ;   ;  ;
;$solrIndexedType; stockLevelStatus       ;string ;            ;    ;    ;    ;    ;    ;productStockLevelStatusValueProvider   ;              ;    ;   ;    ;   ;    ;  ;    ;   ;  ;
;$solrIndexedType; inStockFlag            ;boolean;            ;    ;    ;    ;    ;    ;productInStockFlagValueProvider        ;              ;    ;   ;    ;   ;    ;  ;    ;   ;  ;
;$solrIndexedType; pickupAvailableFlag    ;boolean;            ;    ;    ;    ;    ;    ;productPickupAvailabilityValueProvider ;              ;    ;   ;    ;   ;    ;  ;    ;   ;  ;
;$solrIndexedType; configurable           ;boolean;            ;    ;    ;    ;    ;    ;productConfigurableProvider            ;              ;    ;   ;    ;   ;    ;  ;    ;   ;  ;
;$solrIndexedType; multidimensional       ;boolean;            ;    ;    ;    ;    ;    ;multidimentionalProductFlagValueProvider  ;              ;    ;   ;    ;   ;    ;  ;    ;   ;  ;


# Category fields
INSERT_UPDATE SolrIndexedProperty;solrIndexedType(identifier)[unique=true];name[unique=true];type(code);localized[default=false];multiValue[default=true];categoryField[default=true];useForSpellchecking[default=false];useForAutocomplete[default=false];fieldValueProvider;ftsPhraseQuery[default=false];ftsPhraseQueryBoost;ftsQuery[default=false];ftsQueryBoost;ftsFuzzyQuery[default=false];ftsFuzzyQueryBoost


# Category facets
INSERT_UPDATE SolrIndexedProperty;solrIndexedType(identifier)[unique=true];name[unique=true];type(code);multiValue[default=true];facet[default=true];facetType(code);facetSort(code);priority;visible;categoryField[default=true];fieldValueProvider;facetDisplayNameProvider;topValuesProvider
;$solrIndexedType; allCategories 	;string;;;Refine		;Alpha;-9999;false;;categoryCodeValueProvider;
;$solrIndexedType; categoryPath  	;string;;;Refine		;Alpha;-9999;false;false;categoryPathValueProvider;

# Other facet properties
INSERT_UPDATE SolrIndexedProperty;solrIndexedType(identifier)[unique=true];name[unique=true];type(code);sortableType(code);currency[default=false];localized[default=false];multiValue[default=false];facet[default=true];facetType(code);facetSort(code);priority;visible;useForSpellchecking[default=false];useForAutocomplete[default=false];fieldValueProvider;facetDisplayNameProvider;customFacetSortProvider;topValuesProvider;rangeSets(name)
;$solrIndexedType; price        	;double ;	 ;true ;     ;     ;     ;MultiSelectOr ;Alpha ; 4000;true;     ;     ;productPriceValueProvider 	;;;defaultTopValuesProvider;sorianaPriceRangeMXN
;$solrIndexedType; allPromotions	;string ;	 ;     ;     ;true ;     ;MultiSelectOr ;Alpha ; 0   ;false;     ;     ;promotionCodeValueProvider	;promotionFacetDisplayNameProvider;                         ;
;$solrIndexedType; availableInStores;string	;	 ;     ;     ;true ;     ;MultiSelectOr ;Custom;10000;true;     ;     ;productStoreStockValueProvider;;distanceAttributeSortProvider



# Create the queries that will be used to extract data for Solr
INSERT_UPDATE SolrIndexerQuery;solrIndexedType(identifier)[unique=true];identifier[unique=true];type(code);injectCurrentDate[default=true];injectCurrentTime[default=true];injectLastIndexTime[default=true];query;user(uid)
;$solrIndexedType;$searchIndexNamePrefix-fullQuery;full;;;false;"SELECT {PK} FROM {SorianaProduct}";anonymous


# Define the available sorts
INSERT_UPDATE SolrSort;&sortRefID;indexedType(identifier)[unique=true];code[unique=true];useBoost
;sortRef1;$solrIndexedType;relevance;true
;sortRef2;$solrIndexedType;topRated;false
;sortRef3;$solrIndexedType;name-asc;false
;sortRef4;$solrIndexedType;name-desc;false
;sortRef5;$solrIndexedType;price-asc;false
;sortRef6;$solrIndexedType;price-desc;false

# Define the sort fields
INSERT_UPDATE SolrSortField;sort(indexedType(identifier),code)[unique=true];fieldName[unique=true];ascending[unique=true]
;$solrIndexedType:relevance;inStockFlag;false
;$solrIndexedType:relevance;score;false
;$solrIndexedType:topRated;inStockFlag;false
;$solrIndexedType:topRated;reviewAvgRating;false
;$solrIndexedType:name-asc;name;true
;$solrIndexedType:name-desc;name;false
;$solrIndexedType:price-asc;priceValue;true
;$solrIndexedType:price-desc;priceValue;false



