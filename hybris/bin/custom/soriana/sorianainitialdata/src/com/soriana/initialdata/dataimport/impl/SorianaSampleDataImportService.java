/**
 *
 */
package com.soriana.initialdata.dataimport.impl;

import de.hybris.platform.catalog.jalo.CatalogManager;
import de.hybris.platform.catalog.jalo.CatalogVersion;
import de.hybris.platform.catalog.jalo.SyncItemJob;
import de.hybris.platform.commerceservices.dataimport.impl.SampleDataImportService;
import de.hybris.platform.commerceservices.setup.AbstractSystemSetup;
import de.hybris.platform.commerceservices.setup.data.ImportData;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.initialization.SystemSetupContext;
import de.hybris.platform.jalo.type.AttributeDescriptor;
import de.hybris.platform.servicelayer.config.ConfigurationService;

import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * @author mark.d.schlosser
 *
 */
public class SorianaSampleDataImportService extends SampleDataImportService
{
	private static final Logger LOG = LoggerFactory.getLogger(SorianaSampleDataImportService.class);
	private static final String IMPORT_SAMPLE_DATA_FLAG = "sorianainitialdata.shouldImportSampleData";
	private static final String FINANCING_OFFERS = "financingOffers";
	private static final String COMMA = ",";
	private static final String AVOID_PROPERTIES_TO_SYNC = "sorianainitialdata.avoid.sync.properties";
	private static final String ONLINE = "Online";
	private static final String STAGED = "Staged";
	private static final String SORIANA_PRODUCT_CATALOG = "sorianaProductCatalog";

	@Resource(name = "configurationService")
	private ConfigurationService configurationService;

	@Override
	protected void importProductCatalog(final String extensionName, final String productCatalogName)
	{
		super.importProductCatalog(extensionName, productCatalogName);

		getSetupImpexService().importImpexFile(
				String.format("/%s/import/sampledata/productCatalogs/%sProductCatalog/variant-products.impex", new Object[]
				{ extensionName, productCatalogName }), false);

		//Import Commission impex soriana\sorianainitialdata\resources\sorianainitialdata\import\sampledata\cockpits\reportcockpit
		getSetupImpexService().importImpexFile(
				String.format("/%s/import/sampledata/cockpits/reportcockpit/commissions.impex", new Object[]
				{ extensionName, productCatalogName }), false);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.hybris.platform.commerceservices.dataimport.impl.SampleDataImportService#execute(de.hybris.platform.
	 * commerceservices.setup.AbstractSystemSetup, de.hybris.platform.core.initialization.SystemSetupContext,
	 * java.util.List)
	 */
	@Override
	public void execute(final AbstractSystemSetup systemSetup, final SystemSetupContext context, final List<ImportData> importData)
	{
		final boolean shouldImportSampleData = getConfigurationService().getConfiguration().getBoolean(IMPORT_SAMPLE_DATA_FLAG,
				true);
		if (shouldImportSampleData)
		{
			super.execute(systemSetup, context, importData);
		}
		else
		{
			//If we don't import sample data the product/content catalog syncs will not run so we have to run them manually here
			syncProductAndContentCatalogs(systemSetup, context, importData);
			LOG.warn("WARNING: Sample data not imported because " + IMPORT_SAMPLE_DATA_FLAG + "=false.");
		}
		removePropertiesFromSynchronization();
	}

	/**
	 * @param systemSetup
	 * @param context
	 * @param importDataList
	 */
	private void syncProductAndContentCatalogs(final AbstractSystemSetup systemSetup, final SystemSetupContext context,
			final List<ImportData> importDataList)
	{
		for (final ImportData importData : importDataList)
		{
			synchronizeProductCatalog(systemSetup, context, importData.getProductCatalogName(), true);
			for (final String contentCatalogName : importData.getContentCatalogNames())
			{
				synchronizeContentCatalog(systemSetup, context, contentCatalogName, true);
			}
		}
	}

	/**
 *
 */
	private void removePropertiesFromSynchronization()
	{
		final CatalogManager cm = CatalogManager.getInstance();
		// find source and target version
		if (null != cm.getCatalog(SORIANA_PRODUCT_CATALOG))
		{
			final CatalogVersion src = cm.getCatalog(SORIANA_PRODUCT_CATALOG).getCatalogVersion(STAGED);
			final CatalogVersion tgt = cm.getCatalog(SORIANA_PRODUCT_CATALOG).getCatalogVersion(ONLINE);
			final ConfigurationService configurationService = (ConfigurationService) Registry.getApplicationContext().getBean(
					"configurationService");
			final String syncProperties = configurationService.getConfiguration().getString(AVOID_PROPERTIES_TO_SYNC);
			final String[] syncPropertiesList = syncProperties.split(COMMA);
			AttributeDescriptor attrDesc;
			for (final String property : syncPropertiesList)
			{
				if (property.equalsIgnoreCase(FINANCING_OFFERS))
				{
					attrDesc = de.hybris.platform.jalo.type.TypeManager.getInstance()
							.getComposedType(com.soriana.core.jalo.SorianaProduct.class).getDeclaredAttributeDescriptor(property);
				}
				else
				{
					attrDesc = de.hybris.platform.jalo.type.TypeManager.getInstance()
							.getComposedType(de.hybris.platform.jalo.product.Product.class).getDeclaredAttributeDescriptor(property);
				}
				// find synchronization by source and target version
				final SyncItemJob syncJob = cm.getSyncJob(src, tgt);
				if (null != syncJob)
				{
					final de.hybris.platform.catalog.jalo.SyncAttributeDescriptorConfig cfg = syncJob.getConfigFor(attrDesc, true);
					cfg.setIncludedInSync(false);
					cfg.setCopyByValue(false);
				}
				else
				{
					LOG.info(String.format("No Product Catalog Synchronization Job available ::: %s", syncJob));
				}
			}
		}
		else
		{
			LOG.info(String.format("Product Catalog not exist ::: %s ", cm.getCatalog(SORIANA_PRODUCT_CATALOG)));
		}
	}

	/**
	 * @return the configurationService
	 */
	public ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	/**
	 * @param configurationService
	 *           the configurationService to set
	 */
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}
}
