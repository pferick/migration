/**
 *
 */
package com.soriana.initialdata.dataimport.impl;

import de.hybris.platform.commerceservices.dataimport.impl.CoreDataImportService;


/**
 * @author mark.d.schlosser
 *
 */
public class SorianaCoreDataImportService extends CoreDataImportService
{
	@Override
	protected void importProductCatalog(final String extensionName, final String productCatalogName)
	{
		super.importProductCatalog(extensionName, productCatalogName);

		getSetupImpexService().importImpexFile(
				String.format("/%s/import/coredata/productCatalogs/%sProductCatalog/categories.impex", new Object[]
				{ extensionName, productCatalogName }), false);

		getSetupImpexService().importImpexFile(
				String.format("/%s/import/coredata/productCatalogs/%sProductCatalog/classifications-units.impex", new Object[]
				{ extensionName, productCatalogName }), false);

		getSetupImpexService().importImpexFile(
				String.format("/%s/import/coredata/productCatalogs/%sProductCatalog/categories-classifications.impex", new Object[]
				{ extensionName, productCatalogName }), false);

		getSetupImpexService().importImpexFile(String.format("/%s/import/coredata/cronJobs/jobs.impex", new Object[]
		{ extensionName, productCatalogName }), false);

		getSetupImpexService().importImpexFile(
				String.format("/%s/import/coredata/productCatalogs/%sProductCatalog/products.impex", new Object[]
				{ extensionName, productCatalogName }), false);

		getSetupImpexService().importImpexFile(String.format("/%s/import/coredata/stores/soriana/store.impex", new Object[]
		{ extensionName, productCatalogName }), false);

		getSetupImpexService().importImpexFile(
				String.format("/%s/import/coredata/stores/soriana/points-of-service-media.impex", new Object[]
				{ extensionName, productCatalogName }), false);

		/*
		 * getSetupImpexService().importImpexFile(
		 * String.format("/%s/import/coredata/stores/soriana/points-of-service.impex", new Object[] { extensionName,
		 * productCatalogName }), false);
		 */
	}
}
