/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.soriana.webservices.jalo;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import org.apache.log4j.Logger;
import com.soriana.webservices.constants.SorianawebservicesConstants;

@SuppressWarnings("PMD")
public class SorianawebservicesManager extends GeneratedSorianawebservicesManager
{
	@SuppressWarnings("unused")
	private static final Logger LOG = Logger.getLogger( SorianawebservicesManager.class.getName() );
	
	public static final SorianawebservicesManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (SorianawebservicesManager) em.getExtension(SorianawebservicesConstants.EXTENSIONNAME);
	}
	
}
