/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package sorianaservices.setup;

import static sorianaservices.constants.SorianaservicesConstants.PLATFORM_LOGO_CODE;

import de.hybris.platform.core.initialization.SystemSetup;

import java.io.InputStream;

import sorianaservices.constants.SorianaservicesConstants;
import sorianaservices.service.SorianaservicesService;


@SystemSetup(extension = SorianaservicesConstants.EXTENSIONNAME)
public class SorianaservicesSystemSetup
{
	private final SorianaservicesService sorianaservicesService;

	public SorianaservicesSystemSetup(final SorianaservicesService sorianaservicesService)
	{
		this.sorianaservicesService = sorianaservicesService;
	}

	@SystemSetup(process = SystemSetup.Process.INIT, type = SystemSetup.Type.ESSENTIAL)
	public void createEssentialData()
	{
		sorianaservicesService.createLogo(PLATFORM_LOGO_CODE);
	}

	private InputStream getImageStream()
	{
		return SorianaservicesSystemSetup.class.getResourceAsStream("/sorianaservices/sap-hybris-platform.png");
	}
}
